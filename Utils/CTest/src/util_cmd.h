#ifndef __UTIL_CMD_H
#define __UTIL_CMD_H

#include "stdint.h"

/* If dynamic memory allocation is used, there is no limit to the number of commands and parameters.
 * But you need to provide functions for memory allocation and relclamation. */
#define CMD_CONFIG_DYNAMIC_MEMORY_EN    0
#define CMD_CONFIG_MANAGER_METHOD       1

/* If dynamic memory allocation is used, you need to probide memory alloca and free functions.
 * For example:
 *      In windows/linux:
 *      '''
 *      #include "stdlib.h"
 *      #define CmdMalloc   malloc
 *      #define CmdFree     free
 *      '''
 * 
 *      In FreeRTOS:
 *      '''
 *      #include "FreeRTOS.h"
 *      #include "task.h"
 *      #define CmdMalloc   pvPortMalloc
 *      #define CmdFree     vPortFree
 * 
 *      In RTThread:
 *      '''
 *      #include "rtthread.h"
 *      #define CmdMalloc   rt_malloc
 *      #define CmdMalloc   rt_free
 *      '''
 */
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    #include "stdlib.h"
    #define CmdMalloc   malloc
    #define CmdFree     free
#else
    #define CMD_ARRAY_SIZE_MAX  16
    #define CMD_PARAM_MAX       16
#endif

#define CMD_PARAM_STR_LENGTH    8

#if (CMD_CONFIG_MANAGER_METHOD == 0)
    typedef struct __CmdSubCmd {
        char *name;
        uint16_t index;
    #if CMD_CONFIG_DYNAMIC_MEMORY_EN
        struct __CmdSubCmd *next;
    #endif
    } CmdParamTypeDef;
#endif

#if (CMD_CONFIG_MANAGER_METHOD == 0)
    typedef void (* CmdCallback)(CmdParamTypeDef *param);
#else
    typedef void (* CmdCallback)(int argc, char *argv[], int index);
#endif

#define CMD_STAT_OK         0
#define CMD_STAT_FULL       1
#define CMD_STAT_EMPTY      2
#define CMD_STAT_REPEAT     3
#define CMD_ERR_MEMORY      4
#define CMD_ERR_NO_INIT     5
#define CMD_ERR_NO_MATCH    6
#define CMD_ERR_NO_CB       7
#define CMD_ERR_CHAR_FAIL   8

uint8_t CmdInit(void);
uint8_t CmdAppend(const char *name, CmdCallback cb);
uint8_t CmdDelete(const char *name);
uint8_t CmdCheck(char *str);

#if (CMD_CONFIG_MANAGER_METHOD == 0)
    int8_t CmdAppendParam(char *cmdName, char *param);
    int8_t CmdDeleteParam(char *cmdName, char *parName);
#endif

#endif
