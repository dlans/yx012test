#include "util_cmd.h"
#include "string.h"
#include "stdio.h"

#if (CMD_CONFIG_MANAGER_METHOD == 0)

typedef struct __CmdStruct {
	char *name;
    CmdCallback cmdCallback;
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    struct __CmdStruct *next;
	CmdParamTypeDef *paramList;
#endif
} CmdTypeDef;

#if !CMD_CONFIG_DYNAMIC_MEMORY_EN
typedef struct {
    CmdParamTypeDef (*paramArray)[CMD_PARAM_MAX];
    uint8_t *paramLenArray;
} ParamManager;
#endif

typedef struct {
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdTypeDef *cmdList;
#else
    CmdTypeDef *cmdArray;
#endif
    uint16_t number;
} CmdManager;

static CmdManager *cmdManager = NULL;
#if !CMD_CONFIG_DYNAMIC_MEMORY_EN
static ParamManager *parManager = NULL;
#endif

static int16_t findCh(char *str, char ch)
{
    int16_t index;
    char *pc = str;

    for (index = 0; (pc != NULL) && (*pc != '\0'); pc++, index++) {
        if (*pc == ch) {
            return index;
        }
    }

    return -1;
}

static void showCmd(void)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
#else
    uint16_t i, j;

    for (i = 0; i < cmdManager->number; i++) {
        printf("cmdManager->cmdArray[%d].name = %s\n", i, cmdManager->cmdArray[i].name);
        for (j = 0; j < parManager->paramLenArray[i]; j++) {
            printf("\tparManager->paramArray[%d][%d].name = %s\n", i, j, parManager->paramArray[i][j].name);
        }
    }
    printf("parManager->paramLenArray:\n");
    for (i = 0; i < CMD_ARRAY_SIZE_MAX; i++) {
        printf("%d ", parManager->paramLenArray[i]);
    }
    printf("\n");

#endif
}

/**
 * @brief	指令管理器初始化
 */
void CmdInit(void)
{
	static CmdManager manager;

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    cmdManager = &manager;
    cmdManager->cmdList = NULL;

#else
    static ParamManager pManager;
    static CmdTypeDef cmdArray[CMD_ARRAY_SIZE_MAX];
    static CmdParamTypeDef pa[CMD_ARRAY_SIZE_MAX][CMD_PARAM_MAX];
    static uint8_t parLen[CMD_ARRAY_SIZE_MAX];

    uint8_t i, j;

    cmdManager = &manager;
    cmdManager->cmdArray = cmdArray;

    parManager = &pManager;
    parManager->paramArray = pa;
    parManager->paramLenArray = parLen;

    /* Init command array */
    for (i = 0; i < CMD_ARRAY_SIZE_MAX; i++) {
        cmdManager->cmdArray[i].name = NULL;
        cmdManager->cmdArray[i].cmdCallback = NULL;
    }

    /* Init parameter array */
    for (i = 0; i < CMD_ARRAY_SIZE_MAX; i++) {
        for (j = 0; j < CMD_PARAM_MAX; j++) {
            parManager->paramArray[i][j].name = NULL;
        }
        parManager->paramLenArray[i] = 0;
    }
#endif
    cmdManager->number = 0;
}

/**
 * @brief	追加指令
 * @param[in]	指令名
 * @param[in]   对应的回调函数
 */
int8_t CmdAppend(char *name, CmdCallback callBack)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdTypeDef *cmd = NULL;
    CmdTypeDef *cmd1 = NULL;

	cmd = (CmdTypeDef *)CmdMalloc(sizeof(CmdTypeDef));
    if (cmd == NULL) {
        return -1;
    }
    cmd->name = name;
    cmd->cmdCallback = callBack;
    cmd->paramList = NULL;
    cmd->next = NULL;
    if (cmdManager->cmdList == NULL) {
        cmdManager->cmdList = cmd;
    }
    else {
        cmd1 = cmdManager->cmdList;
        while (cmd1->next != NULL) {
            cmd1 = cmd1->next;
        }
        cmd1->next = cmd;
    }

#else
    if (cmdManager->number == CMD_ARRAY_SIZE_MAX) {
        return -2;
    }
    cmdManager->cmdArray[cmdManager->number].name = name;
    cmdManager->cmdArray[cmdManager->number].cmdCallback = callBack;
    
#endif
    cmdManager->number++;

    return 0;
}

int8_t CmdDelete(char *name)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdTypeDef *cmd1 = NULL;
    CmdTypeDef *cmd2 = NULL;
    CmdParamTypeDef *par1 = NULL;
    CmdParamTypeDef *par2 = NULL;

    if (cmdManager->cmdList == NULL) {
        return -1;
    }
    if (strcmp(cmdManager->cmdList->name, name) == 0) {
        cmd2 = cmdManager->cmdList; /* cmd2 is the command to be deleted */
        cmd1 = cmd2->next;
        /* Delete its param list */
        par1 = cmd2->paramList;
        while (par1 != NULL) {
            par2 = par1->next;
            CmdFree(par1);
            par1 = par2;
        }
        CmdFree(cmd2);
        cmdManager->cmdList = cmd1;
        cmdManager->number--;
        return 0;
    }
    /* Not the first */
    else {
        cmd1 = cmdManager->cmdList;
        while (cmd1->next != NULL) {
            if (strcmp(cmd1->next->name, name) == 0) {
                cmd2 = cmd1->next;
                break;
            }
            cmd1 = cmd1->next;
        }
    }
    par1 = cmd2->paramList;
    while (par1 != NULL) {
        par2 = par1->next;
        CmdFree(par1);
        par1 = par2;
    }
    /* Delete itself */
    cmd1->next = cmd2->next;
    CmdFree(cmd2);
    cmdManager->number--;

#else
    uint16_t index;
    uint16_t i, j;

    for (index = 0; index < cmdManager->number; index++) {
        if (strcmp(cmdManager->cmdArray[index].name, name) == 0) {
            break;
        }
    }
    if (index == cmdManager->number) {
        return -1;
    }


    if (index == (cmdManager->number-1)) {
        cmdManager->cmdArray[index].name = NULL;
        cmdManager->cmdArray[index].cmdCallback = NULL;
    }
    else {
        for (i = index; i < (cmdManager->number-1); i++) {
            cmdManager->cmdArray[i].name = cmdManager->cmdArray[i+1].name;
            cmdManager->cmdArray[i].cmdCallback = cmdManager->cmdArray[i+1].cmdCallback;
            parManager->paramLenArray[i] = parManager->paramLenArray[i+1];
            for (j = 0; j < parManager->paramLenArray[i]; j++) {
                parManager->paramArray[i][j].name = parManager->paramArray[i+1][j].name;
                parManager->paramArray[i][j].index = parManager->paramArray[i+1][j].index;
            }
        }
        cmdManager->cmdArray[i].name = NULL;
        cmdManager->cmdArray[i].cmdCallback = NULL;
        for (j = 0; j < parManager->paramLenArray[i]; j++) {
            parManager->paramArray[i][j].name = NULL;
            parManager->paramArray[i][j].index = 0;
        }
        parManager->paramLenArray[i] = 0;
    }
    cmdManager->number--;

    for (i = 0; i < parManager->paramLenArray[index]; i++) {
        parManager->paramArray[index][i].name = NULL;
        parManager->paramArray[index][i].index = 0;
    }
    parManager->paramLenArray[index] = 0;

#endif
    return 0;
}

/**
 * @brief   Add a parameter to the command
 * @param[in]   command name
 * @param[in]   A CmdParamTypeDef point
 * @note    This function can only add paremeters to command added by the CmdAppend()
 */
int8_t CmdAppendParam(char *cmdName, char *param)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdParamTypeDef *par = NULL;
    CmdParamTypeDef *par1 = NULL;
    CmdTypeDef *cmd = NULL;

    cmd = cmdManager->cmdList;
    while (cmd != NULL) {
        if (strcmp(cmd->name, cmdName) == 0) {
            break;
        }
        cmd = cmd->next;
    }
    if (cmd == NULL) {
        return -1;
    }
    par = (CmdParamTypeDef *)CmdMalloc(sizeof(CmdParamTypeDef));
    if (par == NULL) {
        return -2;
    }
    par->name = param;
    par->index = 0;
    par->next = NULL;
    par1 = cmd->paramList;
    if (par1 == NULL) {
        cmd->paramList = par;
    }
    else {
        while (par1->next != NULL) {
            par1 = par1->next;
        }
        par1->next = par;
    }

#else
    uint16_t i;

    for (i = 0; i < cmdManager->number; i++) {
        if (strcmp(cmdManager->cmdArray[i].name, cmdName) == 0) {
            break;
        }
    }
    if (i == cmdManager->number) {
        return -1;
    }
    if (parManager->paramLenArray[i] == CMD_PARAM_MAX) {
        return -2;
    }
    parManager->paramArray[i][parManager->paramLenArray[i]].name = param;
    parManager->paramLenArray[i]++;

#endif
    return 0;
}

int8_t CmdDeleteParam(char *cmdName, char *parName)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdTypeDef *cmd = NULL;
    CmdParamTypeDef *par1 = NULL;
    CmdParamTypeDef *par2 = NULL;

    cmd = cmdManager->cmdList;
    while (cmd != NULL) {
        if (strcmp(cmd->name, cmdName) == 0) {
            break;
        }
        cmd = cmd->next;
    }
    if (cmd == NULL) {
        return -1;
    }
    par1 = cmd->paramList;
    if (par1 == NULL) {
        return -2;
    }
    if (strcmp(par1->name, parName) == 0) {
        cmd->paramList = par1->next;
        CmdFree(par1);
    }
    else {
        while (par1->next != NULL) {
            if (strcmp(par1->next->name, parName) == 0) {
                break;
            }
            par1 = par1->next;
        }
        /* This is the last */
        if (par1->next == NULL) {
            par2 = par1->next;
            if (strcmp(par2->name, parName) == 0) {
                par1->next = NULL;
            }
            else {
                return -2;
            }
        }
        else {
            par2 = par1->next;
            par1->next = par2->next;
            CmdFree(par2);
        }
    }

#else
    uint16_t index;
    uint16_t i, j;

    for (index = 0; index < cmdManager->number; index++) {
        if (strcmp(cmdManager->cmdArray[index].name, cmdName) == 0) {
            break;
        }
    }
    if (index == cmdManager->number) {
        return -1;
    }

    for (i = 0; i < parManager->paramLenArray[index]; i++) {
        if (strcmp(parManager->paramArray[index][i].name, parName) == 0) {
            break;
        }
    }
    if (i == parManager->paramLenArray[index]) {
        return -2;
    }
    for (j = i; j < (parManager->paramLenArray[index] - 1); j++) {
        parManager->paramArray[index][j].name = parManager->paramArray[index][j+1].name;
        parManager->paramArray[index][j].index = parManager->paramArray[index][j+1].index;
    }
    parManager->paramArray[index][j].name = NULL;
    parManager->paramArray[index][j].index = 0;
    parManager->paramLenArray[index]--;

#endif
    return 0;
}

/**
 * @brief   Check command and run
 * @param[in]   Received str
 */
void CmdCheck(char *str)
{
    uint8_t strLen;
    int16_t index;
    char *pc = NULL;

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdTypeDef *cmd = cmdManager->cmdList;
    CmdParamTypeDef *par = NULL;
    if (cmd == NULL) {
        return;
    }
    while (cmd != NULL) {
        strLen = strlen(cmd->name);
        if (strncmp(cmd->name, str, strLen) == 0) {
            par = cmd->paramList;
            index = findCh(str, '-');
            /* This command has no parameters or the input did not pass in the correct parameters */
            if ((par == NULL) || (index == -1)) {
                cmd->cmdCallback(NULL);
                break;
            }
            pc = &str[index+1];
            while (par != NULL) {
                strLen = strlen(par->name);
                if (strncmp(par->name, pc, strLen) == 0) {
                    par->index = index + 1 + strLen;
                    cmd->cmdCallback(par);
                    return;
                }
                par = par->next;
            }
            /* No parameter was matched */
            if (par == NULL) {
                cmd->cmdCallback(NULL);
            }
        }
        cmd = cmd->next;
    }

#else
    uint8_t i, j;

    for (i = 0; i < cmdManager->number; i++) {
        strLen = strlen(cmdManager->cmdArray[i].name);
        if (strncmp(cmdManager->cmdArray[i].name, str, strLen) == 0) {
            /* This command doesn't have parameters */
            if (parManager->paramLenArray[i] == 0) {
                cmdManager->cmdArray[i].cmdCallback(NULL);
                break;
            }
            /* Match patameter */
            index = findCh(str, '-');
            if (index == -1) {
                cmdManager->cmdArray[i].cmdCallback(NULL);
                break;
            }
            pc = &str[index+1];
            for (j = 0; j < parManager->paramLenArray[i]; j++) {
                strLen = strlen(parManager->paramArray[i][j].name);
                if (strncmp(parManager->paramArray[i][j].name, pc, strLen) == 0) {
                    parManager->paramArray[i][j].index = index + 1 + strLen;
                    cmdManager->cmdArray[i].cmdCallback(&(parManager->paramArray[i][j]));
                    return;
                }
            }
            if (j == parManager->paramLenArray[i]) {
                cmdManager->cmdArray[i].cmdCallback(NULL);
            }
        }
    }

#endif
}

void CmdShow(void)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    CmdTypeDef *cmd = NULL;
    CmdParamTypeDef *par = NULL;

    printf("==> CMD_CONFIG_DYNAMIC_MEMORY_EN =1\n");
    printf("Current has %d command:\n", cmdManager->number);
    cmd = cmdManager->cmdList;
    while (cmd != NULL) {
        printf("\t%s\n", cmd->name);
        if (cmd->paramList != NULL) {
            printf("\t%s has follow parameter:\n", cmd->name);
            par = cmd->paramList;
            while (par != NULL) {
                printf("\t\t-%s\n", par->name);
                par = par->next;
            }
        }
        cmd = cmd->next;
    }

#else
    uint16_t i, j;

    printf("==> CMD_CONFIG_DYNAMIC_MEMORY_EN =0\n");
    printf("Current has %d command:\n", cmdManager->number);
    for (i = 0; i < cmdManager->number; i++) {
        printf("\t%s\n", cmdManager->cmdArray[i].name);
        if (parManager->paramLenArray[i] != 0) {
            printf("\t%s has %d parameter:\n", cmdManager->cmdArray[i].name, parManager->paramLenArray[i]);
            for (j = 0; j < parManager->paramLenArray[i]; j++) {
                printf("\t\t-%s\n", parManager->paramArray[i][j].name);
            }
        }
    }

#endif
}

#endif
