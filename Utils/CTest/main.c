#include "util_cmd.h"
#include "stdio.h"
#include "time.h"
#include "stdlib.h"

#if defined(__WIN32__)
    #include "synchapi.h"
#else
    #include "unistd.h"
#endif

#if (CMD_CONFIG_MANAGER_METHOD == 0)

int main(int argc, char *argv[])
{
    return 0;
}

#else
void cmd1Callback(int argc, char *argv[], int index);
void cmd2Callback(int argc, char *argv[], int index);
void cmd3Callback(int argc, char *argv[], int index);
void cmd4Callback(int argc, char *argv[], int index);
void cmd5Callback(int argc, char *argv[], int index);

int main(int argc, char *argv[])
{
    uint8_t result;

    result = CmdAppend("cmd1", cmd1Callback);
    printf("result = %d\n", result);
    result = CmdAppend("cmd2", cmd2Callback);
    printf("result = %d\n", result);
    result = CmdAppend("cmd2", cmd2Callback);
    printf("result = %d\n", result);
    result = CmdAppend("cmd3", cmd3Callback);
    printf("result = %d\n", result);

#if defined(__WIN32__)
    Sleep(1000);
#else
    sleep(1);
#endif
    result = CmdCheck("cmd1 -param1 -param2");
    printf("result = %d\n", result);
    result = CmdCheck("cmd2 -abcd -uyhu");
    printf("result = %d\n", result);
    result = CmdCheck("cmd3 -jiu90 -jafie 3435");
    printf("result = %d\n", result);

    printf("\n====== append and check done, then test the delete ======\n");
#if defined(__WIN32__)
    Sleep(1000);
#else
    sleep(1);
#endif

    printf("try to delete the first command:\n");
    result = CmdDelete("cmd1");
    printf("delete done, result = %d\n", result);
    result = CmdCheck("cmd1 -param01 -param02");
    printf("result = %d\n", result);
    result = CmdCheck("cmd2 -dddd -cccc\n");
    printf("result = %d\n", result);
    result = CmdCheck("cmd3 -aaaa -bbbb\n");
    printf("result = %d\n", result);

    return 0;
}

void cmd1Callback(int argc, char *argv[], int index)
{
    int i, j;

    printf("====== cmd1Callback start ======\n");
    printf("argc = %d\n", argc);
    printf("index = %d\n", index);
    for (i = 0; i < argc; i++) {
        printf("param %d = %s\n", i, argv[i]);
    }
    printf("====== cmd1Callback end ========\n");
}

void cmd2Callback(int argc, char *argv[], int index)
{
    int i, j;

    printf("====== cmd2Callback start ======\n");
    printf("argc = %d\n", argc);
    printf("index = %d\n", index);
    for (i = 0; i < argc; i++) {
        printf("param %d = %s\n", i, argv[i]);
    }
    printf("====== cmd2Callback end ========\n");
}

void cmd3Callback(int argc, char *argv[], int index)
{
    int i, j;

    printf("====== cmd3Callback start ======\n");
    printf("argc = %d\n", argc);
    printf("index = %d\n", index);
    for (i = 0; i < argc; i++) {
        printf("param %d = %s\n", i, argv[i]);
    }
    printf("after index:\n");
    printf("====== cmd3Callback end ========\n");
}

void cmd4Callback(int argc, char *argv[], int index)
{}

void cmd5Callback(int argc, char *argv[], int index)
{}

#endif
