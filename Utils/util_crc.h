#ifndef __UTIL_CRC_H
#define __UTIL_CRC_H

#include "stdint.h"

uint8_t CRC4itu(uint8_t *data, uint16_t len);
uint8_t CRC5epc(uint8_t *data, uint16_t len);
uint8_t CRC5itu(uint8_t *data, uint16_t len);
uint8_t CRC5usb(uint8_t *data, uint16_t len);
uint8_t CRC6itu(uint8_t *data, uint16_t len);
uint8_t CRC7mmc(uint8_t *data, uint16_t len);
uint8_t CRC8(uint8_t *data, uint16_t len);
uint8_t CRC8itu(uint8_t *data, uint16_t len);
uint8_t CRC8rohc(uint8_t *data, uint16_t len);
uint8_t CRC8maxim(uint8_t *data, uint16_t len);
uint16_t CRC12cdma2000(uint8_t *data, uint16_t len);
uint16_t CRC16ibm(uint8_t *data, uint16_t len);
uint16_t CRC16maxim(uint8_t *data, uint16_t len);
uint16_t CRC16usb(uint8_t *data, uint16_t len);
uint16_t CRC16modbus(uint8_t *data, uint16_t len);
uint16_t CRC16ccitt(uint8_t *data, uint16_t len);
uint16_t CRC16ccittFalse(uint8_t *data, uint16_t len);
uint16_t CRC16x25(uint8_t *data, uint16_t len);
uint16_t CRC16ymodem(uint8_t *data, uint16_t len);
uint16_t CRC16dnp(uint8_t *data, uint16_t len);
uint32_t CRC32(uint8_t *data, uint16_t len);
uint32_t CRC32mpeg2(uint8_t *data, uint16_t len);

/*********************************** Old API ************************************/
uint8_t CRC8MaxIM(uint8_t *ptr, uint16_t len, uint8_t refOut);
uint16_t CRC16Modbus(uint8_t *ptr, uint16_t len);
uint16_t CRC16XModem(uint16_t *ptr, uint16_t len);

#endif
