#include "util_crc.h"

typedef struct {
	uint8_t ref_in : 1;
	uint8_t ref_out : 1;
	uint8_t reserved : 6;
	
	uint8_t shift;
	uint8_t poly;
	uint8_t init;
	uint8_t xor_out;
} CRC8Param;

typedef struct {
	uint8_t ref_in : 1;
	uint8_t ref_out : 1;
	uint8_t reserved : 6;
	
	uint16_t shift;
	uint16_t poly;
	uint16_t init;
	uint16_t xor_out;
} CRC16Param;

typedef struct {
	uint8_t ref_in : 1;
	uint8_t ref_out : 1;
	uint8_t reserved : 6;
	
	uint32_t shift;
	uint32_t poly;
	uint32_t init;
	uint32_t xor_out;
} CRC32Param;

static uint8_t reverse8(uint8_t b)
{
	uint8_t i;
	
	uint8_t temp = 0;
	
	for (i = 0; i < 8; i++) {
		temp |= ((b >> i) & 0x01) << (7 - i);
	}
	
	return temp;
}

static uint16_t reverse16(uint16_t b)
{
	uint8_t i;
	
	uint16_t temp = 0;
	
	for (i = 0; i < 16; i++) {
		temp |= ((b >> i) & 0x01) << (15 - i);
	}
	
	return temp;
}

static uint32_t reverse32(uint32_t b)
{
	uint8_t i;
	
	uint32_t temp = 0;
	
	for (i = 0; i < 32; i++) {
		temp |= ((b >> i) & 0x01) << (31 - i);
	}
	
	return temp;
}

static uint8_t crc8Common(uint8_t *array, uint16_t len, CRC8Param *param)
{
	uint8_t crc;
	uint8_t data_byte;
	uint16_t i, j;
	
	crc = param->init << param->shift;
	param->poly = param->poly << param->shift;
	for (i = 0; i < len; i++) {
		if (param->ref_in) {
			data_byte = reverse8(array[i]);
		}
		else {
			data_byte = array[i];
		}
		crc = crc ^ data_byte;
		for (j = 0; j < 8; j++) {
			if ((crc & 0x80) != 0) {
				crc = (crc << 1) ^ param->poly;
			}
			else {
				crc = crc << 1;
			}
		}
	}
	if (param->ref_out) {
		crc = reverse8(crc);
	}
	else {
		crc >>= param->shift;
	}
	
	return (crc ^ param->xor_out);
}

static uint16_t crc16Common(uint8_t *array, uint16_t len, CRC16Param *param)
{
	uint16_t crc;
	uint8_t data_byte;
	uint16_t i, j;
	
	crc = param->init << param->shift;
	param->poly = param->poly << param->shift;
	for (i = 0; i < len; i++) {
		if (param->ref_in) {
			data_byte = reverse8(array[i]);
		}
		else {
			data_byte = array[i];
		}
		crc = crc ^ (data_byte << 8);
		for (j = 0; j < 8; j++) {
			if ((crc & 0x8000) != 0) {
				crc = (crc << 1) ^ param->poly;
			}
			else {
				crc = crc << 1;
			}
		}
	}
	if (param->ref_out) {
		crc = reverse16(crc);
	}
	else {
		crc >>= param->shift;
	}
	
	return (crc ^ param->xor_out);
}

static uint32_t crc32Common(uint8_t *array, uint16_t len, CRC32Param *param)
{
	uint8_t data_byte;
	uint16_t i, j;
	
	uint32_t crc = param->init;
	for (i = 0; i < len; i++) {
		if (param->ref_in) {
			data_byte = reverse8(array[i]);
		}
		else {
			data_byte = array[i];
		}
		crc = crc ^ (data_byte << 24);
		for (j = 0; j < 8; j++) {
			if ((crc & 0x80000000) != 0) {
				crc = (crc << 1) ^ param->poly;
			}
			else {
				crc = crc << 1;
			}
		}
	}
	if (param->ref_out) {
		crc = reverse32(crc);
	}
	
	return (crc ^ param->xor_out);
}

uint8_t CRC4itu(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 4;
	param.poly = 0x03;
	param.init = 0x00;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC5epc(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 3;
	param.poly = 0x09;
	param.init = 0x09;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC5itu(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 3;
	param.poly = 0x15;
	param.init = 0x00;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC5usb(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 3;
	param.poly = 0x05;
	param.init = 0x1F;
	param.xor_out = 0x1F;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC6itu(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 2;
	param.poly = 0x03;
	param.init = 0x00;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC7mmc(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 1;
	param.poly = 0x09;
	param.init = 0x00;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC8(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 0;
	param.poly = 0x07;
	param.init = 0x00;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC8itu(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 0;
	param.poly = 0x07;
	param.init = 0x00;
	param.xor_out = 0x55;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC8rohc(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x07;
	param.init = 0xFF;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint8_t CRC8maxim(uint8_t *data, uint16_t len)
{
	CRC8Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x31;
	param.init = 0x00;
	param.xor_out = 0x00;
	
	return crc8Common(data, len, &param);
}

uint16_t CRC12cdma2000(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 4;
	param.poly = 0x0E13;
	param.init = 0x0FFF;
	param.xor_out = 0x0000;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16ibm(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x8005;
	param.init = 0x0000;
	param.xor_out = 0x0000;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16maxim(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x8005;
	param.init = 0x0000;
	param.xor_out = 0xFFFF;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16usb(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x8005;
	param.init = 0xFFFF;
	param.xor_out = 0xFFFF;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16modbus(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x8005;
	param.init = 0xFFFF;
	param.xor_out = 0x0000;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16ccitt(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x1021;
	param.init = 0x0000;
	param.xor_out = 0x0000;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16ccittFalse(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 0;
	param.poly = 0x1021;
	param.init = 0xFFFF;
	param.xor_out = 0x0000;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16x25(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x1021;
	param.init = 0xFFFF;
	param.xor_out = 0xFFFF;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16ymodem(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 0;
	param.poly = 0x1021;
	param.init = 0x0000;
	param.xor_out = 0x0000;
	
	return crc16Common(data, len, &param);
}

uint16_t CRC16dnp(uint8_t *data, uint16_t len)
{
	CRC16Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x3D65;
	param.init = 0x0000;
	param.xor_out = 0xFFFF;
	
	return crc16Common(data, len, &param);
}

uint32_t CRC32(uint8_t *data, uint16_t len)
{
	CRC32Param param;
	
	param.ref_in = 1;
	param.ref_out = 1;
	param.shift = 0;
	param.poly = 0x04C11DB7;
	param.init = 0xFFFFFFFF;
	param.xor_out = 0xFFFFFFFF;
	
	return crc32Common(data, len, &param);
}

uint32_t CRC32mpeg2(uint8_t *data, uint16_t len)
{
	CRC32Param param;
	
	param.ref_in = 0;
	param.ref_out = 0;
	param.shift = 0;
	param.poly = 0x04C11DB7;
	param.init = 0xFFFFFFFF;
	param.xor_out = 0x00000000;
	
	return crc32Common(data, len, &param);
}

uint8_t CRC8MaxIM(uint8_t *ptr, uint16_t len, uint8_t refOut)
{
	uint8_t b;
	uint16_t i, j;
	uint8_t crc = 0;
	
	for (i = 0; i < len; i++) {
		b = reverse8(ptr[i]);
		crc = crc ^ b;
		for (j = 0; j < 8; j++) {
			if ((crc & 0x80) != 0) {
				crc = ((crc << 1) ^ 0x31);
			}
			else {
				crc = crc << 1;
			}
		}
	}
	
	crc = refOut ? reverse8(crc) : crc;
	
	return (crc ^ 0x00);
}

/*********************************** Old API ************************************/
/**
 * @brief	CRC modbus 模型计算
 * @param[in]	要校验的数据的地址
 * @param[in]	要校验的数据的长度
 */
uint16_t CRC16Modbus(uint8_t *ptr, uint16_t len)
{
    uint16_t i, crc=0x0000;

    while(len--) {
        crc ^= (*ptr++) << 8;
        for (i = 0; i < 8; i++) {
            if(crc & 0x8000) {
                crc <<= 1;
                crc ^= 0x1021;
            } else {
                crc <<= 1;
            }
        }
    }

    return (crc);
}

/**
 * @brief	CRC xmodem 模型计算
 * @param[in]	要校验的数据的地址
 * @param[in]	要校验的数据的长度
 */
uint16_t CRC16XModem(uint16_t *ptr, uint16_t len)
{
    uint16_t i;
    uint16_t crc = 0x0000;
    uint8_t temp[2] = {0};

    while (len--) {
        temp[0] = (*ptr) >> 8;
        temp[1] = (*ptr);
        ptr++;

        crc ^= temp[0] << 8;
        for (i = 0; i < 8; i++) {
            if (crc & 0x8000) {
                crc <<= 1;
                crc ^= 0x1021;
            }
            else {
                crc <<= 1;
            }
        }

        crc ^= temp[1] << 8;
        for (i = 0; i < 8; i++) {
            if (crc & 0x8000) {
                crc <<= 1;
                crc ^= 0x1021;
            }
            else {
                crc <<= 1;
            }
        }
    }

    return crc;
}
