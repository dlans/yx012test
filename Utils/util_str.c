#include "util_str.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"

/**
 * @brief	从字符串中提取十六进制数
 * @param[in]	str       输入字符串
 * @param[in]	start     字符串开始解析的位置
 * @param[in]	end       字符串结束解析的位置
 * @param[out]	hex_array 返回解析结果
 * @param[in]	len       数组长度
 * @retval		解析的字符数。表示提取到了多少个十六进制数而非处理了多少个字符
 * @note		- 对输出数组 hex_array 不检查处理时是否会越界。
 * 				- 在遇到结束字符时会停止处理，即使没有处理到 end 处。
 *				- 提取到的十六进制数将以四个为一组放在 hex_array 中。例如 "1234_ABCD"，则 hex_array
 *				  内容为 hex_array[0] = 1234, hex_array[1] = ABCD。
 */
uint16_t StrChar2Hex(const char *str, const uint8_t start, const uint16_t end, uint16_t *hex_array, uint16_t len)
{
    uint16_t i, j;
    uint16_t t;
	uint16_t code = 0;
	uint16_t cnt = 0;
    uint16_t *p = hex_array;

    i = start;
    j = 0;
    t = str[i];
    while ((t != '\0') && (i < end + 1)) {
        if ((t > 47) && (t < 58)) {
            t -= '0';
        }
        else if ((t > 64) && (t < 71)) {
            t -= 55;
        }
        else if ((t > 96) && (t < 103)) {
            t -= 87;
        }
        else {
            i++;
            t = str[i];
            continue;
        }
        code |= t;
        *p = code;
        i++;
        t = str[i];
        if ((j % 4) != 3) {
            code <<= 4;
        }
        else {
            p++;
			cnt++;
			if (cnt > len) {
				return j;
			}
            code = 0;
        }
        j++;
    }

    return j;
}

uint16_t StrChar2HexByByte(const char *str, const uint8_t start, const uint16_t end, uint8_t *byteArray, uint16_t len)
{
	uint16_t i, j;
	uint8_t c;
	uint8_t code = 0;
	uint16_t cnt = 0;
	uint8_t *p = byteArray;
	
	i = start;
	j = 0;
	c = str[i];
	while ((c != '\0') && (i < end + 1)) {
		if ((c > 47) && (c < 58)) {
			c -= '0';
		}
		else if ((c > 64) && (c < 71)) {
			c -= 55;
		}
		else if ((c > 96) && (c < 103)) {
			c -= 87;
		}
		else {
			i++;
			c = str[i];
			continue;
		}
		code |= c;
		
		*p = code;
		i++;
		c = str[i];
		if ((j % 2) != 1) {
			code <<= 4;
		}
		else {
			p++;
			cnt++;
			if (cnt > len) {
				return j;
			}
			code = 0;
		}
		j++;
	}
	
	return j;
}

uint16_t StrChar2Hex5ch(const char *str, const uint8_t start, const uint16_t end, uint32_t *hex_array, uint16_t len)
{
    uint16_t i, j;
    uint16_t t;
	uint32_t code = 0;
	uint16_t cnt = 0;
    uint32_t *p = hex_array;

    i = start;
    j = 0;
    t = str[i];
    while ((t != '\0') && (i < end + 1)) {
        if ((t > 47) && (t < 58)) {
            t -= '0';
        }
        else if ((t > 64) && (t < 71)) {
            t -= 55;
        }
        else if ((t > 96) && (t < 103)) {
            t -= 87;
        }
        else {
            i++;
            t = str[i];
            continue;
        }
        code |= t;
        *p = code;
        i++;
        t = str[i];
        if ((j % 5) != 4) {
            code <<= 4;
        }
        else {
            p++;
			cnt++;
			if (cnt > len) {
				return j;
			}
            code = 0;
        }
        j++;
    }

    return j;
}

/**
 * @brief   Hexadecimal string to array
 * @param   str         The hexadecimal string, must end of '\0'
 * @param   end         The end position of converting, it's ususlly the 'str' length.
 *                      If you don't want to convert the whole string, it can be less then 'str' length.
 *                      If you don't want to calculate the length of 'str', you can fill in a very large value,
 *                      the conversion process ends when it encounters '\0'.
 * @param   hex_array   The array that receive the converted data. It donesn't have to be uint8_t*.
 *                      The original type should depend on the 'width', for example:
 *                      - width is 4, this means you need a uint16_t* array to receive it,
 *                        so the original type is uint16_t*, and then to suppress the warning,
 *                        a cast is required when call
 * @param   length      The length of 'hex_array'
 * @param   width       How many characters as a group
 * @retval  How much data was actually converted, val = retval / (width + 1)
 */
uint16_t StrString2Hex(const char *str, const uint16_t end, uint8_t *hex_array, uint16_t length, uint8_t width)
{
    uint8_t c;
    uint16_t i, j, cnt;
    uint64_t code, code1;

    uint8_t *hex_arr8 = NULL;
    uint16_t *hex_arr16 = NULL;
    uint32_t *hex_arr32 = NULL;
    uint64_t *hex_arr64 = NULL;

    if (width < 3) {
        hex_arr8 = hex_array;
    }
    else if ((width > 2) && (width < 5)) {
        hex_arr16 = (uint16_t *)hex_array;
    }
    else if ((width > 4) && (width < 9)) {
        hex_arr32 = (uint32_t *)hex_array;
    }
    else if ((width > 8) && (width < 17)) {
        hex_arr64 = (uint64_t *)hex_array;
    }
    else {
        return 0;
    }

    i = 0;
    j = 0;
    cnt = 0;
    code = 0;
    c = str[i];
    while ((c != '\0') && (i < end + 1)) {
        if ((c > 47) && (c < 58)) {
            c -= '0';
        }
        else if ((c > 64) && (c < 71)) {
            c -= 55;
        }
        else if ((c > 96) && (c < 103)) {
            c -= 87;
        }
        else {
            /* The current character isn't a valid hex character */
            i++;
            c = str[i];
            continue;
        }
        code |= c;
        code1 = code;
        i++;
        c = str[i];
        if ((j % width) != (width - 1)) {
            code <<= 4;
        }
        else {
            if (hex_arr8 != NULL) {
                *hex_arr8 = code1;
                hex_arr8++;
            }
            else if (hex_arr16 != NULL) {
                *hex_arr16 = code1;
                hex_arr16++;
            }
            else if (hex_arr32 != NULL) {
                *hex_arr32 = code1;
                hex_arr32++;
            }
            else {
                *hex_arr64 = code1;
                hex_arr64++;
            }
            cnt++;
            if (cnt >= length) {
                return j;
            }
            code = 0;
        }
        j++;
    }

    return j;
}

uint32_t StrGetDec(const char *str, uint16_t start, uint16_t end)
{
	int16_t i;
	uint16_t actullyStart, actullyEnd;
	uint32_t value = 0;
	uint32_t result = 0;
	
	for (i = start; i < end; i++) {
		if ((str[i] > 47) && (str[i] < 58)) {
			break;
		}
	}
	actullyStart = i;
	
	while (i < end) {
		if ((str[i] < 48) || (str[i] > 57)) {
			break;
		}
		i++;
	}
	i--;
	actullyEnd = i;
	
	if (actullyEnd == 0) {
		return str[i] - 48;
	}
	
	while (i >= actullyStart) {
		value = str[i] - 48;
		result = result + value * pow(10, actullyEnd-i);
		i--;
	}
	
	return result;
}

uint32_t StrGetHex(const char *str, uint16_t start, uint16_t end)
{
	int16_t i;
    uint16_t actullyStart, actullyEnd;
    uint32_t value = 0;
	uint32_t result = 0;

    for (i = start; i < end; i++) {
        if ((str[i] > 47) && (str[i] < 58)) {
            break;
        }
        else if ((str[i] > 64) && (str[i] < 71)) {
            break;
        }
        else if ((str[i] > 96) && str[i] < 103) {
            break;
        }
    }
    actullyStart = i;
    
    while (i < end) {
        if (str[i] < 48) {
			break;
		}
		else if ((str[i] > 57) && (str[i] < 65)) {
			break;
		}
		else if ((str[i] > 70) && (str[i] < 97)) {
			break;
		}
		else if (str[i] > 102) {
			break;
		}
        i++;
    }
    i--;
    actullyEnd = i;

    while (i >= actullyStart) {
        if ((str[i] > 47) && (str[i] < 58)) {
            value = str[i] - 48;
        }
        else if ((str[i] > 64) && (str[i] < 71)) {
            value = str[i] - 55;
        }
        else {
            value = str[i] - 87;
        }
        result = result + value * pow(16, actullyEnd-i);
        i--;
    }

    return result;
}

/**
 * @brief   获得某个字符在字符串中第一次出现的位置
 * @param[in]   字符串
 * @param[in]   要寻找的字符
 * @retval  位置，-1 表示没有找到
*/
int16_t StrGetCHIndex(char *str, char ch)
{
    uint16_t index;
    char *pc = str;

    for (index = 0; (pc != NULL) && (*pc != '\0'); pc++, index++) {
        if (*pc == ch) {
            return index;
        }
    }

    return -1;
}

/**
 * @brief   字符串截取
 * @param[in]   要切片的字符串
 * @param[out]  指向 char 的指针，存储切片后的结果
 * @param[in]   截取开始位置
 * @param[in]   截取结束位置
 * @note    没有判断是否越界
*/
void StrCutOut(char *str, char *result, uint16_t start, uint16_t end)
{
    uint16_t i;
    char *ps = str;
    char *pd = result;

    for (i = start; i < end; i++) {
        *pd = ps[i];
        pd++;
    }
}

/**
 * @brief   寻找字符串中下一个非空白字符的位置
 * @param[in]   要查找的字符串
 * @retval  位置，-1 表示没有找到
 */
int16_t StrGetNextNotSpace(char *str)
{
    uint16_t index;
    char *pPrevious = str;
    char *pCurent = str;

    pCurent++;
    for (index = 1; (pCurent != NULL) && (*pCurent != '\0'); index++) {
        if ((*pCurent != ' ') && (*pCurent != '\t')) {
            if ((*pPrevious == ' ') || (*pPrevious == '\t')) {
                return index;
            }
        }
        pCurent++;
        pPrevious++;
    }

    return -1;
}

int16_t StrGetNextSpace(char *str)
{
	return -1;
}
