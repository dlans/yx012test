#ifndef __UTIL_STR_H
#define __UTIL_STR_H

#include "stdint.h"

uint16_t StrChar2Hex(const char *str, const uint8_t start, const uint16_t end, uint16_t *hex_array, uint16_t len);
uint16_t StrChar2HexByByte(const char *str, const uint8_t start, const uint16_t end, uint8_t *byteArray, uint16_t len);
uint16_t StrChar2Hex5ch(const char *str, const uint8_t start, const uint16_t end, uint32_t *hex_array, uint16_t len);
uint16_t StrString2Hex(const char *str, const uint16_t end, uint8_t *hex_array, uint16_t length, uint8_t width);
uint32_t StrGetDec(const char *str, uint16_t start, uint16_t end);
uint32_t StrGetHex(const char *str, uint16_t start, uint16_t end);
int16_t StrGetCHIndex(char *str, char ch);
void StrCutOut(char *str, char *result, uint16_t start, uint16_t end);
int16_t StrGetNextNotSpace(char *str);

#endif
