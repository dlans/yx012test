#include "util_cmd.h"
#include "stdio.h"
#include "string.h"

#if (CMD_CONFIG_MANAGER_METHOD == 2)

static int16_t findCh(char *str, char ch)
{
    int16_t index;
    char *pc = str;

    for (index = 0; (pc != NULL) && (*pc != '\0'); pc++, index++) {
        if (*pc == ch) {
            return index;
        }
    }

    return -1;
}

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
static uint8_t isWord(char ch)
{
    if ((ch > 47) && (ch < 58)) {
        return 1;
    }
    else if ((ch > 64) && (ch < 91)) {
        return 2;
    }
    else if ((ch > 96) && (ch < 123)) {
        return 3;
    }
    else {
        return 0;
    }
}

static int16_t getWordEnd(char *strIn, char *strOut, uint16_t outlen)
{
    uint16_t l;
    int16_t index = 0;
    char *ch1 = strIn;
    char *ch2 = NULL;

    if (strIn == NULL) {
        return -1;
    }

    if (strOut == NULL) {
        while (isWord(*ch1) && (ch1 != NULL)) {
            index++;
            ch1++;
        }
    }
    else {
        for (l = 0, ch2 = strOut; l < outlen; l++) {
            if (isWord(*ch1) && (ch1 != NULL)) {
                *ch2 = *ch1;
                ch1++;
                ch2++;
                index++;
            }
            else {
                break;
            }
        }
    }

    return index;
}
#endif

/**
 * @brief   Command manager init
 * @retval  CMD_STAT_OK or CMD_ERR_MEMORY when cmd_manager is NULL
 */
uint8_t CmdInit(CmdManagerType *cmd_manager)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
#else
    static struct CmdStruct cmdlist[CMD_ARRAY_SIZE_MAX];
#endif

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    if (cmd_manager == NULL) {
        return CMD_ERR_MEMORY;
    }
	cmd_manager->length = 0;
	cmd_manager->cmdlist = NULL;
	cmd_manager->argv = NULL;
#else
    uint8_t i, j;

    if (cmd_manager == NULL) {
		return CMD_ERR_MEMORY;
	}
	else {
        cmd_manager->length = 0;
        cmd_manager->cmdlist = cmdlist;

        for (i = 0; i < CMD_ARRAY_SIZE_MAX; i++) {
            cmd_manager->cmdlist[i].name = NULL;
            cmd_manager->cmdlist[i].cb = NULL;
            cmd_manager->argv[i] = NULL;
        }
        for (i = 0; i < CMD_PARAM_MAX; i++) {
            for (j = 0; j < CMD_PARAM_STR_LENGTH; j++) {
                cmd_manager->argArray[i][j] = 0;
            }
        }
    }
#endif

    return CMD_STAT_OK;
}

/**
 * @brief   Appen a command to command manager
 * @param   name command string
 * @param   cb   command's callback function
 * @return  Execution result
 *      @retval CMD_STAT_OK     Sucessful
 *      @retval CMD_STAT_FULL   The number of command managed by the command manager reached the upper limit
 *      @retval CMD_STAT_REPEAT The command to be append duplicates an existing command
 *      @retval CMD_ERR_MEMORY  Can't get memory from heap
 */
uint8_t CmdAppend(CmdManagerType *cmd_manager, const char *name, CmdCallback cb)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    struct CmdStruct *cmd1 = NULL;
    struct CmdStruct *cmd2 = NULL;
#else
    uint8_t i;
#endif

#if !CMD_CONFIG_DYNAMIC_MEMORY_EN
    if (cmd_manager->length == CMD_ARRAY_SIZE_MAX) {
        return CMD_STAT_FULL;
    }

    for (i = 0; i < cmd_manager->length; i++) {
        if (strcmp(cmd_manager->cmdlist[i].name, name) == 0) {
            return CMD_STAT_REPEAT;
        }
    }
#endif

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    cmd1 = cmd_manager->cmdlist;
    while (cmd1 != NULL) {
        if (strcmp(name, cmd1->name) == 0) {
            return CMD_STAT_REPEAT;
        }
        cmd2 = cmd1;
        cmd1 = cmd1->next;
    }

    cmd1 = (struct CmdStruct *)CmdMalloc(sizeof(struct CmdStruct));
    if (cmd1 == NULL) {
        return CMD_ERR_MEMORY;
    }
    cmd1->name = name;
    cmd1->cb = cb;
    cmd1->next = NULL;
    if (cmd_manager->cmdlist == NULL) {
        cmd_manager->cmdlist = cmd1;
    }
    else {
        cmd2->next = cmd1;
    }
    cmd_manager->length++;
#else
    cmd_manager->cmdlist[cmd_manager->length].name = name;
    cmd_manager->cmdlist[cmd_manager->length].cb = cb;
    cmd_manager->length++;
#endif

    return CMD_STAT_OK;
}

/**
 * @brief   Delete a command from command manager
 * @param   name The name of command to be deleted
 * @return  Execution result, if command manager is not initialized, the manager is initialized first and
 *          then CMD_ERR_NO_INIT is returned.
 *      @retval CMD_STAT_OK      Sucessful
 *      @retval CMD_ERR_NO_INIT  Command manager didn't init
 *      @retval CMD_STAT_EMPTY   Command manager has no command
 *      @retval CMD_ERR_NO_MATCH No matching command is found
 */
uint8_t CmdDelete(CmdManagerType *cmd_manager, const char *name)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    struct CmdStruct *cmd1 = NULL;
    struct CmdStruct *cmd2 = NULL;

    if (cmd_manager->cmdlist == NULL) {
        return CMD_STAT_EMPTY;
    }

    cmd1 = cmd_manager->cmdlist;
    if (strcmp(cmd1->name, name) == 0) {
        cmd_manager->cmdlist = cmd1->next;
        CmdFree(cmd1);
        return CMD_STAT_OK;
    }
    else {
        cmd2 = cmd1;
        cmd1 = cmd1->next;
        while (cmd1 != NULL) {
            if (strcmp(cmd1->name, name) == 0) {
                cmd2->next = cmd1->next;
                CmdFree(cmd1);
                return CMD_STAT_OK;
            }
            else {
                cmd2 = cmd1;
                cmd1 = cmd1->next;
            }
        }
    }
#else
    uint8_t i, j;

    if (cmd_manager == NULL) {
        return CMD_ERR_NO_INIT;
    }

    if (cmd_manager->length == 0) {
        return CMD_STAT_EMPTY;
    }

    for (i = 0; i < cmd_manager->length; i++) {
        if (strcmp(cmd_manager->cmdlist[i].name, name) == 0) {
            if (i == (cmd_manager->length - 1)) {
                cmd_manager->cmdlist[i].name = NULL;
                cmd_manager->cmdlist[i].cb = NULL;
            }
            else {
                for (j = i; j < (cmd_manager->length - 1); j++) {
                    cmd_manager->cmdlist[j].name = cmd_manager->cmdlist[j+1].name;
                    cmd_manager->cmdlist[j].cb = cmd_manager->cmdlist[j+1].cb;
                }
                cmd_manager->cmdlist[j].name = NULL;
                cmd_manager->cmdlist[j].cb = NULL;
            }
            cmd_manager->length--;

            return CMD_STAT_OK;
        }
    }
#endif

    return CMD_ERR_NO_MATCH;
}

/**
 * @brief   Retrieves the command form the string and executes the corresponding callback function
 * @param   str The string to be retrieved
 * @note    The command must be at the beginning of the string
 * @return  Execution result
 *      @retval CMD_STAT_OK       Sucessful
 *      @retval CMD_STAT_EMPTY    Command manager has no command
 *      @retval CMD_ERR_MEMORY    Can't get memory from heap
 *      @retval CMD_ERR_CHAR_FAIL An error occurred while parsing the string, it usually occured by end with '-'
 *      @retval CMD_ERR_NO_CB     The command was matched, but it has no callback function
 *      @retval CMD_ERR_NO_MATCH  No matching command is found
 */
uint8_t CmdCheck(CmdManagerType *cmd_manager, char *str)
{
    uint8_t strLen;
    int16_t index, offset;
	
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    uint16_t i;
	int16_t result;
	
	uint16_t i1 = 0;
    char *argStr = NULL;
    struct CmdStruct *cmd1 = NULL;
#else
    uint16_t i, i1, i2;
    uint16_t j;
#endif
	
	uint8_t error = 0;

    if (cmd_manager->length == 0) {
		error = CMD_STAT_EMPTY;
		
	#if CMD_CONFIG_DYNAMIC_MEMORY_EN
		goto CHECK_ERROR_HANDLE;
	#else
		return error;
	#endif
    }

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    cmd1 = cmd_manager->cmdlist;
    while (cmd1 != NULL) {
        strLen = strlen(cmd1->name);
        if (strncmp(cmd1->name, str, strLen) == 0) {
            i = 0;
            offset = strLen;
            index = findCh(&str[offset], '-');
            while (index != -1) {
                offset = offset + index + 1;
                i++;
                index = getWordEnd(&str[offset], NULL, 0);
                offset += index;
                index = findCh(&str[offset], '-');
            }
            cmd_manager->argv = (char **)CmdMalloc(sizeof(char **) * i);
            if (cmd_manager->argv == NULL) {
				error = CMD_ERR_MEMORY;
				
                goto CHECK_ERROR_HANDLE;
            }

            offset = strLen;
            index = findCh(&str[offset], '-');
            while (index != -1) {
                offset = offset + index + 1;
                result = getWordEnd(&str[offset], NULL, 0);
                if (result < 1) {
					error = CMD_ERR_CHAR_FAIL;
					
                    goto CHECK_ERROR_HANDLE;
                }

                argStr = (char *)CmdMalloc(sizeof(char) * (result + 1));
                if (argStr == NULL) {
					error = CMD_ERR_MEMORY;
					
                    goto CHECK_ERROR_HANDLE;
                }

                for (i = 0; i < result; i++) {
                    argStr[i] = str[offset+i];
                }
                argStr[i] = '\0';
                cmd_manager->argv[i1] = argStr;
                i1++;
                offset += result;
                index = findCh(&str[offset], '-');
            }

            if (cmd1->cb == NULL) {
				error = CMD_ERR_NO_CB;
				
                goto CHECK_ERROR_HANDLE;
            }
            cmd1->cb(i1, cmd_manager->argv, offset);

            for (i = 0; i < i1; i++) {
                CmdFree(cmd_manager->argv[i]);
            }
            CmdFree(cmd_manager->argv);
            cmd_manager->argv = NULL;

            return CMD_STAT_OK;
        }
        cmd1 = cmd1->next;
    }
#else
    for (i = 0; i < cmd_manager->length; i++) {
        strLen = strlen(cmd_manager->cmdlist[i].name);
        if (strncmp(cmd_manager->cmdlist[i].name, str, strLen) == 0) {
            offset = strLen;
            for (j = 0; j < CMD_PARAM_MAX; j++) {
                index = findCh(&str[offset], '-');
                if (index == -1) {
                    break;
                }
                i1 = offset + index + 1;
                i2 = 0;
                while (i2 < (CMD_PARAM_STR_LENGTH - 1)) {
                    if ((str[i1] > 47) && str[i1] < 58) {
                        cmd_manager->argArray[j][i2] = str[i1];
                    }
                    else if ((str[i1] > 64) && str[i1] < 91) {
                        cmd_manager->argArray[j][i2] = str[i1];
                    }
                    else if ((str[i1] > 96) && str[i1] < 123) {
                        cmd_manager->argArray[j][i2] = str[i1];
                    }
                    else {
                        break;
                    }
                    i2++;
                    i1++;
                }
                cmd_manager->argv[j] = cmd_manager->argArray[j];
                offset = i1 + 1;
            }
            if (cmd_manager->cmdlist[i].cb == NULL) {
                return CMD_ERR_NO_CB;
            }

            cmd_manager->cmdlist[i].cb(j, cmd_manager->argv, i1);
            for (i = 0; i < j; i++) {
                for (i1 = 0; i1 < CMD_PARAM_STR_LENGTH; i1++) {
                    cmd_manager->argArray[i][i1] = 0;
                }
                cmd_manager->argv[i] = NULL;
            }

            return CMD_STAT_OK;
        }
    }
#endif

    return CMD_ERR_NO_MATCH;

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
CHECK_ERROR_HANDLE:
	if (cmd_manager->argv != NULL) {
		for (i = 0; i < i1; i++) {
			CmdFree(cmd_manager->argv[i]);
		}
		CmdFree(cmd_manager->argv);
		cmd_manager->argv = NULL;
	}
	
	return error;
#endif

}

#endif
