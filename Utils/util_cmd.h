#ifndef __UTIL_CMD_H
#define __UTIL_CMD_H

#include "stdint.h"

/* If dynamic memory allocation is used, there is no limit to the number of commands and parameters.
 * But you need to provide functions for memory allocation and relclamation. */
#define CMD_CONFIG_DYNAMIC_MEMORY_EN    0
#define CMD_CONFIG_MANAGER_METHOD       2

/* If dynamic memory allocation is used, you need to probide memory alloca and free functions.
 * For example:
 *      In windows/linux:
 *      '''
 *      #include "stdlib.h"
 *      #define CmdMalloc   malloc
 *      #define CmdFree     free
 *      '''
 * 
 *      In FreeRTOS:
 *      '''
 *      #include "FreeRTOS.h"
 *      #include "task.h"
 *      #define CmdMalloc   pvPortMalloc
 *      #define CmdFree     vPortFree
 * 
 *      In RTThread:
 *      '''
 *      #include "rtthread.h"
 *      #define CmdMalloc   rt_malloc
 *      #define CmdMalloc   rt_free
 *      '''
 */
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    #include "stdlib.h"
    #define CmdMalloc   malloc
    #define CmdFree     free
#else
    #define CMD_ARRAY_SIZE_MAX  64
    #define CMD_PARAM_MAX       64
#endif

#define CMD_PARAM_STR_LENGTH    16

#if (CMD_CONFIG_MANAGER_METHOD == 0)
    typedef struct __CmdSubCmd {
        char *name;
        uint16_t index;
    #if CMD_CONFIG_DYNAMIC_MEMORY_EN
        struct __CmdSubCmd *next;
    #endif
    } CmdParamTypeDef;
#endif

#if (CMD_CONFIG_MANAGER_METHOD == 0)
    typedef void (* CmdCallback)(CmdParamTypeDef *param);
#else
    typedef void (* CmdCallback)(int argc, char *argv[], int index);
#endif

#if (CMD_CONFIG_MANAGER_METHOD == 2)

	struct CmdStruct {
		const char *name;
		CmdCallback cb;
	#if CMD_CONFIG_DYNAMIC_MEMORY_EN
		struct CmdStruct *next;
	#endif
	};

	typedef struct CmdManager {
		uint16_t length;
	#if CMD_CONFIG_DYNAMIC_MEMORY_EN
		char **argv;
	#else
		char argArray[CMD_PARAM_MAX][CMD_PARAM_STR_LENGTH];
		char *argv[CMD_PARAM_MAX];
	#endif
		struct CmdStruct *cmdlist;
	} CmdManagerType;

#endif

#define CMD_STAT_OK         0
#define CMD_STAT_FULL       1
#define CMD_STAT_EMPTY      2
#define CMD_STAT_REPEAT     3
#define CMD_ERR_MEMORY      4
#define CMD_ERR_NO_INIT     5
#define CMD_ERR_NO_MATCH    6
#define CMD_ERR_NO_CB       7
#define CMD_ERR_CHAR_FAIL   8

#if (CMD_CONFIG_MANAGER_METHOD == 2)
	uint8_t CmdInit(CmdManagerType *cmd_manager);
	uint8_t CmdAppend(CmdManagerType *cmd_manager, const char *name, CmdCallback cb);
	uint8_t CmdDelete(CmdManagerType *cmd_manager, const char *name);
	uint8_t CmdCheck(CmdManagerType *cmd_manager, char *str);
#else
	uint8_t CmdInit(void);
	uint8_t CmdAppend(const char *name, CmdCallback cb);
	uint8_t CmdDelete(const char *name);
	uint8_t CmdCheck(char *str);
#endif

#if (CMD_CONFIG_MANAGER_METHOD == 0)
    int8_t CmdAppendParam(char *cmdName, char *param);
    int8_t CmdDeleteParam(char *cmdName, char *parName);
#endif

#endif
