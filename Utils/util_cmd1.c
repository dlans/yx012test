#include "util_cmd.h"
#include "stdio.h"
#include "string.h"

#if (CMD_CONFIG_MANAGER_METHOD == 1)

struct CmdStruct {
    const char *name;
    CmdCallback cb;
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    struct CmdStruct *next;
#endif
};

struct CmdManager {
    uint16_t length;
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    char **argv;
#else
    char argArray[CMD_PARAM_MAX][CMD_PARAM_STR_LENGTH];
    char *argv[CMD_PARAM_MAX];
#endif
    struct CmdStruct *cmdlist;
};

static struct CmdManager *sManager = NULL;

/*
static uint8_t isWord(char ch)
{
    if ((ch > 47) && (ch < 58)) {
        return 1;
    }
    else if ((ch > 64) && (ch < 91)) {
        return 2;
    }
    else if ((ch > 96) && (ch < 123)) {
        return 3;
    }
    else {
        return 0;
    }
}
*/

static int16_t findCh(char *str, char ch)
{
    int16_t index;
    char *pc = str;

    for (index = 0; (pc != NULL) && (*pc != '\0'); pc++, index++) {
        if (*pc == ch) {
            return index;
        }
    }

    return -1;
}

/*
static int16_t getNextNotSpace(char *str)
{
    uint16_t index;
    char *pPrevious = str;
    char *pCurent = str;

    pCurent++;
    for (index = 1; (pCurent != NULL) && (*pCurent != '\0'); index++) {
        if ((*pCurent != ' ') && (*pCurent != '\t')) {
            if ((*pPrevious == ' ') || (*pPrevious == '\t')) {
                return index;
            }
        }
        pCurent++;
        pPrevious++;
    }

    return -1;
}
*/

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
static int16_t getWordEnd(char *strIn, char *strOut, uint16_t outlen)
{
    uint16_t l;
    int16_t index = 0;
    char *ch1 = strIn;
    char *ch2 = NULL;

    if (strIn == NULL) {
        return -1;
    }

    if (strOut == NULL) {
        while (isWord(*ch1) && (ch1 != NULL)) {
            index++;
            ch1++;
        }
    }
    else {
        for (l = 0, ch2 = strOut; l < outlen; l++) {
            if (isWord(*ch1) && (ch1 != NULL)) {
                *ch2 = *ch1;
                ch1++;
                ch2++;
                index++;
            }
            else {
                break;
            }
        }
    }

    return index;
}
#endif

/**
 * @brief   Command manager init
 * @retval  Only CMD_STAT_OK
 */
uint8_t CmdInit(void)
{
    static struct CmdManager manager;
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
#else
    static struct CmdStruct cmdlist[CMD_ARRAY_SIZE_MAX];
#endif

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    if (sManager == NULL) {
        manager.length = 0;
        manager.cmdlist = NULL;
        manager.argv = NULL;

        sManager = &manager;
    }
#else
    uint8_t i, j;

    if (sManager == NULL) {
        manager.length = 0;
        manager.cmdlist = cmdlist;

        for (i = 0; i < CMD_ARRAY_SIZE_MAX; i++) {
            manager.cmdlist[i].name = NULL;
            manager.cmdlist[i].cb = NULL;
            manager.argv[i] = NULL;
        }
        for (i = 0; i < CMD_PARAM_MAX; i++) {
            for (j = 0; j < CMD_PARAM_STR_LENGTH; j++) {
                manager.argArray[i][j] = 0;
            }
        }

        sManager = &manager;
    }
#endif

    return CMD_STAT_OK;
}

/**
 * @brief   Appen a command to command manager
 * @param   name command string
 * @param   cb   command's callback function
 * @return  Execution result
 *      @retval CMD_STAT_OK     Sucessful
 *      @retval CMD_STAT_FULL   The number of command managed by the command manager reached the upper limit
 *      @retval CMD_STAT_REPEAT The command to be append duplicates an existing command
 *      @retval CMD_ERR_MEMORY  Can't get memory from heap
 */
uint8_t CmdAppend(const char *name, CmdCallback cb)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    struct CmdStruct *cmd1 = NULL;
    struct CmdStruct *cmd2 = NULL;
#else
    uint8_t i;
#endif

    if (sManager == NULL) {
        CmdInit();
    }

#if !CMD_CONFIG_DYNAMIC_MEMORY_EN
    if (sManager->length == CMD_ARRAY_SIZE_MAX) {
        return CMD_STAT_FULL;
    }

    for (i = 0; i < sManager->length; i++) {
        if (strcmp(sManager->cmdlist[i].name, name) == 0) {
            return CMD_STAT_REPEAT;
        }
    }
#endif

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    cmd1 = sManager->cmdlist;
    while (cmd1 != NULL) {
        if (strcmp(name, cmd1->name) == 0) {
            return CMD_STAT_REPEAT;
        }
        cmd2 = cmd1;
        cmd1 = cmd1->next;
    }

    cmd1 = (struct CmdStruct *)CmdMalloc(sizeof(struct CmdStruct));
    if (cmd1 == NULL) {
        return CMD_ERR_MEMORY;
    }
    cmd1->name = name;
    cmd1->cb = cb;
    cmd1->next = NULL;
    if (sManager->cmdlist == NULL) {
        sManager->cmdlist = cmd1;
    }
    else {
        cmd2->next = cmd1;
    }
    sManager->length++;
#else
    sManager->cmdlist[sManager->length].name = name;
    sManager->cmdlist[sManager->length].cb = cb;
    sManager->length++;
#endif

    return CMD_STAT_OK;
}

/**
 * @brief   Delete a command from command manager
 * @param   name The name of command to be deleted
 * @return  Execution result, if command manager is not initialized, the manager is initialized first and
 *          then CMD_ERR_NO_INIT is returned.
 *      @retval CMD_STAT_OK      Sucessful
 *      @retval CMD_ERR_NO_INIT  Command manager didn't init
 *      @retval CMD_STAT_EMPTY   Command manager has no command
 *      @retval CMD_ERR_NO_MATCH No matching command is found
 */
uint8_t CmdDelete(const char *name)
{
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    struct CmdStruct *cmd1 = NULL;
    struct CmdStruct *cmd2 = NULL;

    if (sManager == NULL) {
        CmdInit();
        return CMD_ERR_NO_INIT;
    }

    if (sManager->cmdlist == NULL) {
        return CMD_STAT_EMPTY;
    }

    cmd1 = sManager->cmdlist;
    if (strcmp(cmd1->name, name) == 0) {
        sManager->cmdlist = cmd1->next;
        CmdFree(cmd1);
        return CMD_STAT_OK;
    }
    else {
        cmd2 = cmd1;
        cmd1 = cmd1->next;
        while (cmd1 != NULL) {
            if (strcmp(cmd1->name, name) == 0) {
                cmd2->next = cmd1->next;
                CmdFree(cmd1);
                return CMD_STAT_OK;
            }
            else {
                cmd2 = cmd1;
                cmd1 = cmd1->next;
            }
        }
    }
#else
    uint8_t i, j;

    if (sManager == NULL) {
        CmdInit();
        return CMD_ERR_NO_INIT;
    }

    if (sManager->length == 0) {
        return CMD_STAT_EMPTY;
    }

    for (i = 0; i < sManager->length; i++) {
        if (strcmp(sManager->cmdlist[i].name, name) == 0) {
            if (i == (sManager->length - 1)) {
                sManager->cmdlist[i].name = NULL;
                sManager->cmdlist[i].cb = NULL;
            }
            else {
                for (j = i; j < (sManager->length - 1); j++) {
                    sManager->cmdlist[j].name = sManager->cmdlist[j+1].name;
                    sManager->cmdlist[j].cb = sManager->cmdlist[j+1].cb;
                }
                sManager->cmdlist[j].name = NULL;
                sManager->cmdlist[j].cb = NULL;
            }
            sManager->length--;

            return CMD_STAT_OK;
        }
    }
#endif

    return CMD_ERR_NO_MATCH;
}

/**
 * @brief   Retrieves the command form the string and executes the corresponding callback function
 * @param   str The string to be retrieved
 * @note    The command must be at the beginning of the string
 * @return  Execution result
 *      @retval CMD_STAT_OK       Sucessful
 *      @retval CMD_STAT_EMPTY    Command manager has no command
 *      @retval CMD_ERR_MEMORY    Can't get memory from heap
 *      @retval CMD_ERR_CHAR_FAIL An error occurred while parsing the string
 *      @retval CMD_ERR_NO_CB     The command was matched, but it has no callback function
 *      @retval CMD_ERR_NO_MATCH  No matching command is found
 */
uint8_t CmdCheck(char *str)
{
    uint8_t strLen;
    int16_t index, offset;
#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    uint8_t i, i1;
    char *argStr = NULL;
    struct CmdStruct *cmd1 = NULL;
#else
    uint8_t i, i1, i2;
    uint8_t j;
#endif

    if (sManager == NULL) {
        CmdInit();
    }

    if (sManager->length == 0) {
        return CMD_STAT_EMPTY;
    }

#if CMD_CONFIG_DYNAMIC_MEMORY_EN
    cmd1 = sManager->cmdlist;
    while (cmd1 != NULL) {
        strLen = strlen(cmd1->name);
        if (strncmp(cmd1->name, str, strLen) == 0) {
            i = 0;
            offset = strLen;
            index = findCh(&str[offset], '-');
            while (index != -1) {
                offset = offset + index + 1;
                i++;
                index = getWordEnd(&str[offset], NULL, 0);
                offset += index;
                index = findCh(&str[offset], '-');
            }
            sManager->argv = (char **)CmdMalloc(sizeof(char **) * i);
            if (sManager->argv == NULL) {
                return CMD_ERR_MEMORY;
            }

            offset = strLen;
            index = findCh(&str[offset], '-');
            i1 = 0;
            while (index != -1) {
                offset = offset + index + 1;
                result = getWordEnd(&str[offset], NULL, 0);
                if (result < 1) {
                    return CMD_ERR_CHAR_FAIL;
                }

                argStr = (char *)CmdMalloc(sizeof(char) * (result + 1));
                if (argStr == NULL) {
                    return CMD_ERR_MEMORY;
                }

                for (i = 0; i < result; i++) {
                    argStr[i] = str[offset+i];
                }
                argStr[i] = '\0';
                sManager->argv[i1] = argStr;
                i1++;
                offset += result;
                index = findCh(&str[offset], '-');
            }

            if (cmd1->cb == NULL) {
                return CMD_ERR_NO_CB;
            }
            cmd1->cb(i1, sManager->argv, offset);

            for (i = 0; i < i1; i++) {
                CmdFree(sManager->argv[i]);
            }
            CmdFree(sManager->argv);
            sManager->argv = NULL;

            return CMD_STAT_OK;
        }
        cmd1 = cmd1->next;
    }
#else
    for (i = 0; i < sManager->length; i++) {
        strLen = strlen(sManager->cmdlist[i].name);
        if (strncmp(sManager->cmdlist[i].name, str, strLen) == 0) {
            offset = strLen;
            for (j = 0; j < CMD_PARAM_MAX; j++) {
                index = findCh(&str[offset], '-');
                if (index == -1) {
                    break;
                }
                i1 = offset + index + 1;
                i2 = 0;
                while (i2 < (CMD_PARAM_STR_LENGTH - 1)) {
                    if ((str[i1] > 47) && str[i1] < 58) {
                        sManager->argArray[j][i2] = str[i1];
                    }
                    else if ((str[i1] > 64) && str[i1] < 91) {
                        sManager->argArray[j][i2] = str[i1];
                    }
                    else if ((str[i1] > 96) && str[i1] < 123) {
                        sManager->argArray[j][i2] = str[i1];
                    }
                    else {
                        break;
                    }
                    i2++;
                    i1++;
                }
                sManager->argv[j] = sManager->argArray[j];
                offset = i1 + 1;
            }
            if (sManager->cmdlist[i].cb == NULL) {
                return CMD_ERR_NO_CB;
            }

            sManager->cmdlist[i].cb(j, sManager->argv, i1);
            for (i = 0; i < j; i++) {
                for (i1 = 0; i1 < CMD_PARAM_STR_LENGTH; i1++) {
                    sManager->argArray[i][i1] = 0;
                }
                sManager->argv[i] = NULL;
            }

            return CMD_STAT_OK;
        }
    }
#endif

    return CMD_ERR_NO_MATCH;
}

#endif
