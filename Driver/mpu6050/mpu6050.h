#ifndef __MPU6050_H
#define __MPU6050_H

#include "stdint.h"

#define MPU6050_REG_SMPRT_DIV       0x19 /* 陀螺仪采样频率分频 */
#define MPU6060_REG_CONFIG          0x1A /* FSYNC 采样控制，陀螺仪和加速度计低通滤波配置 */
#define MPU6050_REG_GYRO_CONFIG     0x1B /* 陀螺仪配置 */
#define MPU6050_REG_ACCEL_CONFIG    0x1C /* 加速度计自检和量程控制，高通滤波器 */

#define MPU6050_REG_ACCEL_XOUT_H	0x3B /* 加速度计测量值 */
#define MPU6050_REG_ACCEL_XOUT_L	0x3C
#define MPU6050_REG_ACCEL_YOUT_H	0x3D
#define MPU6050_REG_ACCEL_YOUT_L	0x3E
#define MPU6050_REG_ACCEL_ZOUT_H	0x3F
#define MPU6050_REG_ACCEL_ZOUT_L	0x40

#define MPU6050_REG_GYRO_XOUT_H		0x43 /* 陀螺仪测量值 */
#define MPU6050_REG_GYRO_XOUT_L		0x44
#define MPU6050_REG_GYRO_YOUT_H		0x45
#define MPU6050_REG_GYRO_YOUT_L		0x46
#define MPU6050_REG_GYRO_ZOUT_H		0x47
#define MPU6050_REG_GYRO_ZOUT_L		0x48

#define MPU6050_REG_PWR_MGMT_1      0x6B /* 电源管理 1 */
#define MPU6050_REG_WHO_AM_I		0x75

void MPU6050Init(void);
uint8_t MPU6050ReadID(void);
void MPU6050ReadAcceleration(uint16_t *acc);

#endif
