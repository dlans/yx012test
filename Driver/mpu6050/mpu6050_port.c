#include "mpu6050_port.h"
#include "hard_iic.h"

void MPU6050PortWriteReg(uint8_t reg, uint8_t regVal)
{
	HardIICWriteOneReg(HARD_IIC_CH0, MPU6050_ADDRESS, reg, regVal);
}

void MPU6050ReadOneReg(uint8_t reg, uint8_t *regVal)
{
	*regVal = HardIICReadOneReg(HARD_IIC_CH0, MPU6050_ADDRESS, reg);
}

void MPU6050PortReadArray(uint8_t reg, uint8_t *array, uint16_t len)
{
	HardIICReadByteArrayOneReg(HARD_IIC_CH0, MPU6050_ADDRESS, reg, array, len);
}
