#ifndef __MPU6050_PORT_H
#define __MPU6050_PORT_H

#include "stdint.h"

#define MPU6050_ADDRESS 0x68

void MPU6050PortWriteReg(uint8_t reg, uint8_t regVal);
void MPU6050ReadOneReg(uint8_t reg, uint8_t *regVal);
void MPU6050PortReadArray(uint8_t reg, uint8_t *array, uint16_t len);

#endif
