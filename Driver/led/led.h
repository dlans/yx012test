#ifndef __LED_H
#define __LED_H

typedef enum {
	LED_00,
	LED_01,
	LED_02,
	LED_ALL
} LedEnum;

void LedInit(void);
void LedOn(LedEnum led);
void LedOff(LedEnum led);
void LedToggle(LedEnum led);

#endif
