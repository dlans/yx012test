#include "led.h"
#include "M480.h"

#define LED_R_PORT	PH
#define LED_R_PIN	BIT4
#define LED_R		PH4

#define LED_Y_PORT	PH
#define LED_Y_PIN	BIT5
#define LED_Y		PH5

#define LED_G_PORT	PH
#define LED_G_PIN	BIT6
#define LED_G		PH6

void LedInit(void)
{
	GPIO_SetMode(PH, BIT4, GPIO_MODE_OUTPUT);
	GPIO_SetMode(PH, BIT5, GPIO_MODE_OUTPUT);
	GPIO_SetMode(PH, BIT6, GPIO_MODE_OUTPUT);
	GPIO_SetSlewCtl(PH, BIT4, GPIO_SLEWCTL_NORMAL);
	GPIO_SetSlewCtl(PH, BIT5, GPIO_SLEWCTL_NORMAL);
	GPIO_SetSlewCtl(PH, BIT6, GPIO_SLEWCTL_NORMAL);
	PH4 = 1;
	PH5 = 1;
	PH6 = 1;
}

void LedOn(LedEnum led)
{
	if (led == LED_00) {
		PH4 = 1;
	}
	else if (led == LED_01) {
		PH5 = 1;
	}
	else if (led == LED_02) {
		PH6 = 1;
	}
	else {
		PH4 = 1;
		PH5 = 1;
		PH6 = 1;
	}
}

void LedOff(LedEnum led)
{
	if (led == LED_00) {
		PH4 = 0;
	}
	else if (led == LED_01) {
		PH5 = 0;
	}
	else if (led == LED_02) {
		PH6 = 0;
	}
	else {
		PH4 = 0;
		PH5 = 0;
		PH6 = 0;
	}
}

void LedToggle(LedEnum led)
{
	if (led == LED_00) {
		if (PH4 == 0) {
			PH4 = 1;
		}
		else {
			PH4 = 0;
		}
	}
	else if (led == LED_01) {
		if (PH5 == 0) {
			PH5 = 1;
		}
		else {
			PH5 = 0;
		}
	}
	else if (led == LED_02) {
		if (PH6 == 0) {
			PH6 = 1;
		}
		else {
			PH6 = 0;
		}
	}
	else {
		if (PH4 == 0) {
			PH4 = 1;
		}
		else {
			PH4 = 0;
		}
		if (PH5 == 0) {
			PH5 = 1;
		}
		else {
			PH5 = 0;
		}
		if (PH6 == 0) {
			PH6 = 1;
		}
		else {
			PH6 = 0;
		}
	}
}
