#ifndef __YX027_CMD_H
#define __YX027_CMD_H

#include "stdint.h"

#define YX027_EVENT_FLASH	0x01
#define YX027_LOG_CACHE_SIZE   1024
#define YX027_FRAME_CACHE_SIZE 4096

void YX027AppInit(void);
void YX027CreateTask(uint8_t priorityFrame);

#endif
