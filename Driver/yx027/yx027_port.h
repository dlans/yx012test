#ifndef __YX027_PORT_H
#define __YX027_PORT_H

#include "stdint.h"

#define YX027PORT_SEND_CACHE_SIZE 64
#define YX027PORT_PWMDATA_MAXSIZE 10

#define CH0TX PA14
#define CH0RX PA15
#define CH1TX PG11
#define CH1RX PG12

#define TrimIO PE15

typedef struct YX027PortReg {
    uint8_t chipId;
    uint8_t regAdrr;
    uint8_t regValue;
} YX027PortReg;

typedef struct YX027PortDelaytime {
    uint16_t cmd2s;
    uint16_t byte;
	uint16_t bid1;	/* Bidirectional seq: 0xF5 -> 0x55 */
	uint16_t bid2;	/* Bidirectional seq: the delay of each sequence */
} YX027PortDelaytime;

typedef enum {
    YX027_MODEL_UART,
    YX027_MODEL_SPI
} YX027CmdModelEnum;

void showYX027PortManagerState(void);
void showYX027PortResState(void);

void YX027PortRes(uint8_t res, uint8_t RStatue);

void YX027PortTrimSqe1(uint8_t testmodel);
void YX027PortTrimSqe2(void);
void YX027PortTrimSqe3(uint8_t *data, uint8_t start, uint8_t end);
void YX027PortTrimRead(void);

void YX020PortInit(void);
void YX027PortSetSetIdSeq(uint16_t chipID);
void YX027PortSetCmd(YX027CmdModelEnum cmd);
void YX027PortSetWithvsync(uint8_t newstat);
uint8_t YX027PortGetWithvsync(void);
void YX027PortBroadcastReg(uint8_t numb, uint8_t regAddr, uint8_t *regData);
void YX027PortSingleReg(uint8_t numb, uint16_t chipID, uint8_t regAddr, uint8_t *regData);
void YX027PortReadReg(uint8_t numb, uint16_t chipID, uint8_t regAddr);

void YX027PortSendCMD2S(void);
void YX027PortSendCMD2D(void);
void YX027PortSendVsync(void);
void YX027PortSendSetID(uint16_t chipID, uint16_t num);
void YX027PortSendBidirection(uint16_t num);

void YX027PortMultiChipPWM(uint16_t chipID, uint8_t chipNum, uint8_t *pwmData);
void YX027PortGroupChipPWM(uint8_t grpId, uint8_t *pwmData);

void YX027PortSetFrame(uint8_t sysReg, uint8_t grpReg, YX027PortReg *regList, uint8_t *pwmData, uint8_t *pwmData2, uint8_t chipNum);
void YX027PortFrame(uint8_t sendSysReg);
#endif
