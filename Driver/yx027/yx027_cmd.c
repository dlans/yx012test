#include "yx027_cmd.h"
#include "yx027_port.h"
#include "main.h"
#include "util_crc.h"
#include "util_cmd.h"
#include "util_str.h"
#include "delay.h"
#include "stdio.h"
#include "string.h"

static uint8_t s_echo = 1;
static uint16_t s_buff_time = 100 ;

static char *s_cmd_yx027 = "yx027";

static char *s_par_model = "model";//uart��SPI������л�

static char *s_par_res = "res";//�����л�
static char *s_par_all = "ALL";
static char *s_par_rx0 = "RX0";
static char *s_par_rx1 = "RX1";
static char *s_par_tx0 = "TX0";
static char *s_par_tx1 = "TX1";

static char *s_par_trim = "trim";
static char *s_par_sq1 = "sq1";
static char *s_par_sq2 = "sq2";
static char *s_par_sq3 = "sq3";

static char *s_par_cmd = "cmd";//���������ͷ
static char *s_par_2s = "2s";
static char *s_par_2d = "2d";
static char *s_par_vsync = "vsync";
static char *s_par_setid = "setid";
static char *s_par_breg = "breg";
static char *s_par_sreg = "sreg";
static char *s_par_rreg = "rreg";
static char *s_par_bid = "bid";

static char *s_par_frame = "frame";//֡�����ͷ
static char *s_par_pwm = "pwm";
static char *s_par_gpwm = "gpwm";
static char *s_par_withv = "withv";

static char *s_par_en = "en";
static char *s_par_uart = "uart";
static char *s_par_spi = "spi";

static char *s_warn2 = "no match param";
static char *s_err_memory = "Can't get memory";

static uint8_t timerF0 = 1;

static EventGroupHandle_t eventGroup = NULL;

static TaskHandle_t frameTaskHandle = NULL;
static TimerHandle_t timerHandle = NULL;

static void timerCallback(TimerHandle_t xTimer)
{
    xEventGroupSetBits(eventGroup, YX027_EVENT_FLASH);
    if (s_buff_time) {
        xTimerChangePeriod(timerHandle, s_buff_time, 0);
    }
    else {
        xTimerStop(timerHandle, 0);
        vTaskSuspend(frameTaskHandle);
    }
}

static void frameTask(void *parameter)
{
    while (1) {
        xEventGroupWaitBits(eventGroup, YX027_EVENT_FLASH, pdTRUE, pdTRUE, portMAX_DELAY);
        if(timerF0) {
            YX027PortFrame(1);
            timerF0 = 0;
        }
        else {
            YX027PortFrame(0);
        }
    }
}

static void showState(void)
{
    showYX027PortManagerState();
}

static void parCmdPrecess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint8_t i;
	
	uint16_t id, num, reg_addr;
	int16_t index, offset;
	uint8_t reg_val[8];
	
    if (strcmp(param, s_par_2s) == 0) {
		YX027PortSendCMD2S();
	}
	else if (strcmp(param, s_par_2d) == 0) {
		YX027PortSendCMD2D();
	}
	else if (strcmp(param, s_par_vsync) == 0) {
		YX027PortSendVsync();
	}
	else if (strcmp(param, s_par_setid) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
		num = StrGetDec((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		id = StrGetHex((char *)&receive[offset], 0, 5);	
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "num = %d, id = %x", num, id);
		}
		YX027PortSendSetID(id, num);
	}
	else if (strcmp(param, s_par_breg) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
		num = StrGetDec((char *)&receive[offset], 0, 5);
		if (num > 7) {
			num = 7;
		}
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		reg_addr = StrGetHex((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		for (i = 0; i < num + 1; i++) {
			reg_val[i] = StrGetHex((char *)&receive[offset], 0, 5);
			index = StrGetCHIndex((char *)&receive[offset], ' ');
			offset += index;
			index = StrGetNextNotSpace((char *)&receive[offset]);
			offset += index;
		}
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "num = %d", num);
			UartPrintfEndl(&g_uart1, "reg addr = 0x%02X", reg_addr);
			UartPuts(&g_uart1, "reg val:");
			for (i = 0; i < (num+1); i++) {
				UartPrintf(&g_uart1, "%02X ", reg_val[i]);
			}
		}
		
		YX027PortBroadcastReg(num, reg_addr, reg_val);
	}
	else if (strcmp(param, s_par_sreg) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
		num = StrGetDec((char *)&receive[offset], 0, 5);
		if (num > 7) {
			num = 7;
		}
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		reg_addr = StrGetHex((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		id = StrGetHex((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		for (i = 0; i < num + 1; i++) {
			reg_val[i] = StrGetHex((char *)&receive[offset], 0, 5);
			index = StrGetCHIndex((char *)&receive[offset], ' ');
			offset += index;
			index = StrGetNextNotSpace((char *)&receive[offset]);
			offset += index;
		}
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "num = %d", num);
			UartPrintfEndl(&g_uart1, "reg addr = 0x%02X", reg_addr);
			UartPrintfEndl(&g_uart1, "id = %X", id);
			UartPuts(&g_uart1, "reg val:");
			for (i = 0; i < num; i++) {
				UartPrintf(&g_uart1, "%02X ", reg_val[i]);
			}
		}
		
		YX027PortSingleReg(num, id, reg_addr, reg_val);
	}
	else if (strcmp(param, s_par_rreg) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
		num = StrGetDec((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		id = StrGetHex((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		reg_addr = StrGetHex((char *)&receive[offset], 0, 5);
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "num = %d", num);
			UartPrintfEndl(&g_uart1, "id = %X", id);
			UartPrintfEndl(&g_uart1, "reg addr = 0x%02X", reg_addr);
		}
		
		YX027PortReadReg(num, id, reg_addr);
	}
	else if (strcmp(param, s_par_bid) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
		num = StrGetDec((char *)&receive[offset], 0, 5);
		if (num == 0) {
			num++;
		}
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "num = %d", num);
		}
		
		YX027PortSendBidirection(num);
	}
	else {
		UartPutsEndl(&g_uart1, s_warn2);
	}
}

static void parModelProcess(uint8_t *receive, char *param, uint16_t recelen)
{
    if (strcmp(param, s_par_spi) == 0) {
		if (s_echo) {
			UartPutsEndl(&g_uart1, "switch to SPI");
		}
		
		YX027PortSetCmd(YX027_MODEL_SPI);
	}
	else if (strcmp(param, s_par_uart) == 0) {
		if (s_echo) {
			UartPutsEndl(&g_uart1, "switch to UART");
		}
		
		YX027PortSetCmd(YX027_MODEL_UART);
	}
	else {
		UartPutsEndl(&g_uart1, s_warn2);
	}
}

static void parFrameProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint16_t num, id, i;
	int16_t index, offset;

    uint8_t *pwm_data = NULL;
    uint16_t *arr16 = NULL;
	
	if (strcmp(param, s_par_pwm) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
        id = StrGetHex((char *)&receive[offset], 0, 5);

        index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
        num = StrGetDec((char *)&receive[offset], 0, 5);
		num++;

		pwm_data = (uint8_t *)pvPortMalloc(num * 6);
        if (pwm_data == NULL) {
            UartPutsEndl(&g_uart1, s_err_memory);
        }
        else {
            index = StrGetCHIndex((char *)&receive[offset], ' ');
            offset += index;
            index = StrGetNextNotSpace((char *)&receive[offset]);
            offset += index;
            StrChar2Hex((char *)&receive[offset], 0, recelen, (uint16_t *)pwm_data, num * 3);

            if (s_echo) {
                UartPrintfEndl(&g_uart1, "id = %X", id);
                UartPrintfEndl(&g_uart1, "num = %d", num);
                arr16 = (uint16_t *)pwm_data;
                UartPuts(&g_uart1, "pwm data");
                for (i = 0; i < (num * 3); i++) {
                    UartPrintf(&g_uart1, "%04X ", arr16[i]);
                }
            }

			num--;
            YX027PortMultiChipPWM(id, num, pwm_data);
            vPortFree(pwm_data);
            pwm_data = NULL;
        }
	}
    else if (strcmp(param, s_par_gpwm) == 0) {
        index = StrGetNextNotSpace((char *)receive);
		offset = index ;
        id = StrGetHex((char *)&receive[offset], 0, 5);

        pwm_data = (uint8_t *)pvPortMalloc(6);
        index = StrGetCHIndex((char *)&receive[offset], ' ');
        offset += index;
        index = StrGetNextNotSpace((char *)&receive[offset]);
        offset += index;
        StrChar2Hex((char *)&receive[offset], 0, recelen, (uint16_t *)pwm_data, 3);

        if (s_echo) {
            UartPrintfEndl(&g_uart1, "id = %X", id);
            arr16 = (uint16_t *)pwm_data;
            UartPutsEndl(&g_uart1, "gpwm data:");
            for (i = 0; i < 3; i++) {
                UartPrintf(&g_uart1, "%04X ", arr16[i]);
            }
        }

        YX027PortGroupChipPWM(id, pwm_data);
		vPortFree(pwm_data);
        pwm_data = NULL;
    }
    else if (strcmp(param, s_par_vsync) == 0) {
        YX027PortSendVsync();
    }
    else if (strcmp(param, s_par_withv) == 0) {
        index = StrGetNextNotSpace((char *)receive);
        offset = index ;
        if (index != -1) {
            if (strncmp((char *)&receive[offset], s_par_en, 2) == 0) {
                YX027PortSetWithvsync(1);
                UartPutsEndl(&g_uart1, "done, new status is enable");
            }
            else {
                YX027PortSetWithvsync(0);
                UartPutsEndl(&g_uart1, "done, new status is disable");
            }
        }
    }
    else {
        UartPutsEndl(&g_uart1, s_warn2);
    }
}

static void parResProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint8_t state;
	int16_t index;
	
	index = StrGetNextNotSpace((char *)receive);
	state = StrGetDec((char *)&receive[index], 0, 4);
	
	if (strcmp(param, s_par_all) == 0) {
		YX027PortRes(0, state);
		YX027PortRes(1, state);
		YX027PortRes(2, state);
		YX027PortRes(3, state);
	}
	else if (strcmp(param, s_par_rx0) == 0) {
		YX027PortRes(0, state);
	}
	else if (strcmp(param, s_par_tx0) == 0) {
		YX027PortRes(1, state);
	}
	else if (strcmp(param, s_par_rx1) == 0) {
		YX027PortRes(2, state);
	}
	else if (strcmp(param, s_par_tx1) == 0) {
		YX027PortRes(3, state);
	}
	else {
		UartPutsEndl(&g_uart1, s_warn2);
	}
}

static void parTrimProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint8_t tm;
	int16_t index, offset;
	uint16_t length;
	
	uint8_t *trim_data = NULL;
	
	if (strcmp(param, s_par_sq1) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		tm = StrGetHex((char *)&receive[index], 0, 5);
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "tm = %X", tm);
		}
		
		YX027PortTrimSqe1(tm);
	}
	else if (strcmp(param, s_par_sq2) == 0) {
		YX027PortTrimSqe2();
	}
	else if (strcmp(param, s_par_sq3) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		offset = index ;
		length = StrGetDec((char *)&receive[offset], 0, 5);
		
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		offset += index;
		index = StrGetNextNotSpace((char *)&receive[offset]);
		offset += index;
		trim_data = (uint8_t *)pvPortMalloc(length);
		StrChar2HexByByte((char *)&receive[offset], 0, recelen, trim_data, length);
		
		if (s_echo) {
			UartPrintfEndl(&g_uart1, "length = %d", length);
			UartPuts(&g_uart1, "trim data: ");
			for (tm = 0; tm < length; tm++) {
				UartPrintf(&g_uart1, "%02X ", trim_data[tm]);
			}
		}
		
		YX027PortTrimSqe3(trim_data, 0, length);
		vPortFree(trim_data);
		trim_data = NULL;
	}
}

static void cmdYX027Callback(int argc, char *argv[], int index)
{
	uint16_t recelen = UartGetReceiveLen(&g_uart1);
    uint8_t *receive = UartGetRxBuffer(&g_uart1);

    if (argc == 0) {
		showState();
	}
	
	if (argc == 1) {
	}
	else if (argc == 2) {
		if (strcmp(argv[0], s_par_cmd) == 0) {
			parCmdPrecess(&receive[index], argv[1], recelen - index);
		}
		else if (strcmp(argv[0], s_par_model) == 0) {
			parModelProcess(&receive[index], argv[1], recelen - index);
		}
		else if (strcmp(argv[0], s_par_frame) == 0) {
			parFrameProcess(&receive[index], argv[1], recelen - index);
		}
		else if (strcmp(argv[0], s_par_res) == 0) {
			parResProcess(&receive[index], argv[1], recelen - index);
		}
		else if (strcmp(argv[0], s_par_trim) == 0) {
			parTrimProcess(&receive[index], argv[1], recelen - index);
		}
	}
}

void YX027AppInit(void)
{
    YX020PortInit();
	
	CmdAppend(&g_cmd_uart_manager, s_cmd_yx027, cmdYX027Callback);

    if (eventGroup == NULL) {
        eventGroup = xEventGroupCreate();
    }
}

void YX027CreateTask(uint8_t priorityFrame)
{
    BaseType_t result = pdPASS;

    result = xTaskCreate(frameTask, "frameTask", 256, NULL, priorityFrame, &frameTaskHandle);
    if (result != pdPASS) {
        UartPutsDMA(&g_uart1, "frameTask create fail\r\n");
    }
    else {
        vTaskSuspend(frameTaskHandle);
    }

    timerHandle = xTimerCreate("027TimerTask", s_buff_time, pdTRUE, (void *)2, timerCallback);
    if(timerHandle == NULL) {
        UartPutsDMA(&g_uart1, " create fail\r\n");
    }
    xTimerStop(timerHandle, 0);
}
