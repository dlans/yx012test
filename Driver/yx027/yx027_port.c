#include "yx027_port.h"
#include "util_crc.h"
#include "util_cmd.h"
#include "util_str.h"
#include "main.h"
#include "delay.h"
#include "stdio.h"
#include "string.h"

struct YX027PortRes {
    uint8_t RX0;
    uint8_t RX1;
    uint8_t TX0;
    uint8_t TX1;
};

struct YX027PortFrame {
    uint8_t sysReg;
    uint8_t gropReg;

    YX027PortReg *regList;
    uint8_t *pwmData;
    uint8_t *pwmData2;
    uint8_t chipNum;
};

struct YX027PortManager {
    uint8_t with_vsync : 1;
    uint8_t reversed : 7;

    YX027CmdModelEnum cmd;

    uint8_t cmd2sLen;
    uint8_t cmd2dLen;
    uint8_t setIdLen;
    uint8_t vsyncLen;
	uint8_t bidirectionalLen;
    uint8_t *sendCache1;

    uint8_t *setIdSeq;

    uint8_t const *cmd2sSeq;
    uint8_t const *cmd2dSeq;
    uint8_t const *vsyncSeq;
	uint8_t const *bidirectionalSeq;

    YX027PortDelaytime delayTime;

};

static struct YX027PortManager *s_manager = NULL;
static struct YX027PortFrame *fManger = NULL;
static struct YX027PortRes *rManger = NULL;

static uint8_t FrameFlag = 0;

void showYX027PortManagerState(void)
{
    UartPuts(&g_uart1, "YX027 state as follow:\r\n");
    UartPuts(&g_uart1, "Protocol: ");
    if (s_manager->cmd == YX027_MODEL_SPI) {
        UartPuts(&g_uart1, "YX027_MODEL_SPI");
    }
    else {
        UartPuts(&g_uart1, "YX027_MODEL_UART");
    }
    UartPuts(&g_uart1, "\r\n");
    UartPrintf(&g_uart1, "YX027_MODEL_SPI Speed: %d\r\n", SpiGetSpeed(&g_spi0));
    UartPrintf(&g_uart1, "YX027_MODEL_UART Baud: %d\r\n", UartGetBaud(&g_uart2));
	UartPuts(&g_uart1, "With VSYNC: ");
	if (YX027PortGetWithvsync() == 1) {
		UartPuts(&g_uart1, "enable");
	}
	else {
		UartPuts(&g_uart1, "disable");
	}
	UartPuts(&g_uart1, "\r\n");
}

static uint8_t reverseByte(uint8_t b)
{
    uint8_t i;
    uint8_t temp = 0;

    for (i = 0; i < 8; i++) {
        temp |= ((b >> i) & 0x01) << (7 - i);
    }

    return temp;
}

static uint8_t spi_CRC8MaxIM(uint8_t *ptr, uint16_t len, uint8_t refOut)
{
    uint8_t b;
    uint16_t i, j;
    uint8_t crc = 0;

    for (i = 0; i < len; i++) {
        b = ptr[i];
        crc = crc ^ b;
        for (j = 0; j < 8; j++) {
            if ((crc & 0x80) != 0) {
                crc = ((crc << 1) ^ 0x31);
            }
            else {
                crc = crc << 1;
            }
        }
    }

    crc = refOut ? reverseByte(crc) : crc;

    return (crc ^ 0x00);
}

void YX020PortInit(void)
{
    static struct YX027PortManager manager;
    static struct YX027PortFrame fmanager;
    static struct YX027PortRes rmanger;

    static uint8_t sendCache1[YX027PORT_SEND_CACHE_SIZE];

    static uint8_t const cmd2sSeq[] = {0x55, 0x0C, 0xAA, 0xD2};
    static uint8_t const cmd2dSeq[] = {0x55, 0x0C, 0x55, 0x1E};
    static uint8_t const vsyncSeq[] = {0x55, 0x08, 0x00, 0x38};
	static uint8_t const bidirectionalSeq[] = {0xF5, 0x55, 0x18, 0x79, 0xB0};
    static uint8_t setIdSeq[4];
    for (uint8_t i = 0; i < YX027PORT_SEND_CACHE_SIZE; i++) {
        sendCache1[i] = 0;
    }
    if (s_manager == NULL) {
        manager.with_vsync = 0;
        
        manager.cmd = YX027_MODEL_UART;

        manager.cmd2sLen = sizeof(cmd2sSeq) / sizeof(cmd2sSeq[0]);
        manager.cmd2dLen = sizeof(cmd2dSeq) / sizeof(cmd2dSeq[0]);
        manager.setIdLen = sizeof(setIdSeq) / sizeof(setIdSeq[0]);
        manager.vsyncLen = sizeof(vsyncSeq) / sizeof(vsyncSeq[0]);
		manager.bidirectionalLen = sizeof(bidirectionalSeq) / sizeof(bidirectionalSeq[0]);

        manager.sendCache1 = sendCache1;

        manager.cmd2sSeq = cmd2sSeq;
        manager.cmd2dSeq = cmd2dSeq;
        manager.vsyncSeq = vsyncSeq;
        manager.setIdSeq = setIdSeq;
		manager.bidirectionalSeq = bidirectionalSeq;

        manager.delayTime.cmd2s = 30;
        manager.delayTime.byte  = 50;
		manager.delayTime.bid1 = 50;
		manager.delayTime.bid2 = 50;

        s_manager = &manager;
    }

    if(fManger == NULL) {
        static YX027PortReg reglist[100] = {0} ;
        static uint8_t pwmData[512] = {0};
        static uint8_t pwmData2[512] = {0};

        fmanager.sysReg = 0x00;
        fmanager.gropReg = 0x00;
        fmanager.regList = reglist;
        fmanager.pwmData = pwmData;
        fmanager.pwmData2 = pwmData2;
        fmanager.chipNum = 1;
        fManger = &fmanager;
    }

    if(rManger == NULL) {
        GPIO_SetMode(PA, BIT14, GPIO_MODE_OUTPUT);
        GPIO_SetMode(PA, BIT15, GPIO_MODE_OUTPUT);
        GPIO_SetMode(PG, BIT11, GPIO_MODE_OUTPUT);
        GPIO_SetMode(PG, BIT12, GPIO_MODE_OUTPUT);

        GPIO_SetPullCtl(PA, BIT14, GPIO_PUSEL_DISABLE);
        GPIO_SetPullCtl(PA, BIT15, GPIO_PUSEL_DISABLE);
        GPIO_SetPullCtl(PG, BIT11, GPIO_PUSEL_DISABLE);
        GPIO_SetPullCtl(PG, BIT12, GPIO_PUSEL_DISABLE);
        CH0TX = 0;
        CH1TX = 0;
        CH0RX = 0;
        CH1RX = 0;
        rmanger.RX0 = 0;
        rmanger.RX1 = 0;
        rmanger.TX0 = 0;
        rmanger.TX1 = 0;

        rManger = &rmanger;
    }
    GPIO_SetMode(PE, BIT15, GPIO_MODE_OUTPUT);
    GPIO_SetPullCtl(PE, BIT15, GPIO_PUSEL_DISABLE);
    TrimIO = 0;
	
	SpiSetWidth(&g_spi0, 8);
}

void YX027PortSetCmd(YX027CmdModelEnum cmd)
{
    s_manager->cmd = cmd;
    return;
}

void YX027PortSetWithvsync(uint8_t newstat)
{
    s_manager->with_vsync = newstat;
}

uint8_t YX027PortGetWithvsync(void)
{
    return s_manager->with_vsync;
}

void YX027PortSetSetIdSeq(uint16_t chipID)
{
    uint8_t cnt = 0;
    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->setIdSeq[0] = 0x55;
        s_manager->setIdSeq[1] = ((uint8_t)(chipID >> 8 | 0x04));
        s_manager->setIdSeq[2] = ((uint8_t)(chipID & 0x00FF));
        s_manager->setIdSeq[3] = CRC8MaxIM(s_manager->setIdSeq, 3, 1);
        cnt = 4;
    }
    else if (s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->setIdSeq[0] = 0xFF;
        s_manager->setIdSeq[1] = 0xFF;
        s_manager->setIdSeq[2] = ((uint8_t)(chipID >> 8 | 0x04));
        s_manager->setIdSeq[3] = ((uint8_t)(chipID & 0x00FF));
        s_manager->setIdSeq[4] = spi_CRC8MaxIM(&(s_manager->setIdSeq[2]), 2, 0);
        s_manager->setIdSeq[5] = 0x00;
        s_manager->setIdSeq[6] = 0x00;
        cnt = 7;
    }
    else {
        UartPutsDMA(&g_uart1, "communication protocol is incorrect\r\n");
    }
    s_manager->setIdLen = cnt;
    return;
}

void YX027PortClearCache1(void)
{
    uint16_t i;
    for(i = 0; i < YX027PORT_SEND_CACHE_SIZE; i++) {
        s_manager->sendCache1[i] = 0x00;
    }
}

void YX027PortBroadcastReg(uint8_t numb, uint8_t regAddr, uint8_t *regData)
{
    uint8_t i;
    uint16_t cnt;

    if (s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[0] = 0x55;
        s_manager->sendCache1[1] = ((numb & 0x07) << 2) | 0x23;
        s_manager->sendCache1[2] = 0xFF;
        s_manager->sendCache1[3] = regAddr & 0x7F;
        cnt = 4;
    }
    else if (s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = ((numb & 0x07) << 2) | 0x23;
        s_manager->sendCache1[3] = 0xFF;
        s_manager->sendCache1[4] = regAddr & 0x7F;
        cnt = 5;
    }

    for(i = 0; i < numb + 1; i++) {
        s_manager->sendCache1[cnt] = regData[i];
        cnt = cnt + 1;
    }

    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[cnt] = CRC8MaxIM(s_manager->sendCache1, cnt, 1);
        UartSend(&g_uart2, s_manager->sendCache1, cnt + 1);
		UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[cnt] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), cnt - 2, 0);
        s_manager->sendCache1[cnt + 1] = 0x00;
        s_manager->sendCache1[cnt + 2] = 0x00;
        cnt = cnt + 3;
        SpiSend(&g_spi0, s_manager->sendCache1, cnt);
    }
    YX027PortClearCache1();
}

void YX027PortSingleReg(uint8_t numb, uint16_t chipID, uint8_t regAddr, uint8_t *regData)
{
    uint8_t i;
    uint16_t cnt;
    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[0] = 0x55;
        s_manager->sendCache1[1] = (((numb & 0x07) << 2) | 0x20) | ((uint8_t)(chipID >> 8) & 0x03);
        s_manager->sendCache1[2] = (uint8_t)(chipID & 0x00FF);
        s_manager->sendCache1[3] = regAddr & 0x7F;
        cnt = 4;
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = (((numb & 0x07) << 2) | 0x20) | ((uint8_t)(chipID >> 8) & 0x03);
        s_manager->sendCache1[3] = (uint8_t)(chipID & 0x00FF);
        s_manager->sendCache1[4] = regAddr & 0x7F;
        cnt = 5;
    }
    for(i = 0; i < numb + 1; i++) {
        s_manager->sendCache1[cnt] = regData[i];
        cnt = cnt + 1;
    }
    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[cnt] = CRC8MaxIM(s_manager->sendCache1, cnt, 1);
        UartSend(&g_uart2, s_manager->sendCache1, cnt + 1);
        UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[cnt] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), cnt - 2, 0);
        s_manager->sendCache1[cnt + 1] = 0x00;
        s_manager->sendCache1[cnt + 2] = 0x00;
        cnt = cnt + 3;
        SpiSend(&g_spi0, s_manager->sendCache1, cnt);
    }
    YX027PortClearCache1();
}

void YX027PortReadReg(uint8_t numb, uint16_t chipID, uint8_t regAddr)
{
    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[0] = 0x55;
        s_manager->sendCache1[1] = (((numb & 0x07) << 2) | 0x40) | ((uint8_t)(chipID >> 8) & 0x03);
        s_manager->sendCache1[2] = (uint8_t)(chipID & 0x00FF);
        s_manager->sendCache1[3] = (0x00 | (regAddr & 0x7F));
        s_manager->sendCache1[4] = CRC8MaxIM(s_manager->sendCache1, 4, 1);

        UartSend(&g_uart2, s_manager->sendCache1, 5);
        UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = (((numb & 0x07) << 2) | 0x40) | ((uint8_t)(chipID >> 8) & 0x03);
        s_manager->sendCache1[3] = (uint8_t)(chipID & 0x00FF);
        s_manager->sendCache1[4] = (0x00 | (regAddr & 0x7F));
        s_manager->sendCache1[5] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), 3, 0);
        s_manager->sendCache1[6] = 0x00;
        s_manager->sendCache1[7] = 0x00;
        SpiSend(&g_spi0, s_manager->sendCache1, 8);
    }
}

void YX027PortSendCMD2S(void)
{
    if(s_manager->cmd == YX027_MODEL_UART) {
        UartSend(&g_uart2, s_manager->cmd2sSeq, s_manager->cmd2sLen);
        UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = 0x0C;
        s_manager->sendCache1[3] = 0xAA;
        s_manager->sendCache1[4] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), 2, 0);
        s_manager->sendCache1[5] = 0x00;
        s_manager->sendCache1[6] = 0x00;
        SpiSend(&g_spi0, s_manager->sendCache1, 7);
    }

}

void YX027PortSendCMD2D(void)
{
    UartSend(&g_uart2, s_manager->cmd2dSeq, s_manager->cmd2dLen);
    UartWaitSendDone(&g_uart2);
}

void YX027PortSendVsync(void)
{
    if(s_manager->cmd == YX027_MODEL_UART) {
        UartSend(&g_uart2, s_manager->vsyncSeq, s_manager->vsyncLen);
        UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = 0x08;
        s_manager->sendCache1[3] = 0x00;
        s_manager->sendCache1[4] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), 2, 0);
        s_manager->sendCache1[5] = 0x00;
        s_manager->sendCache1[6] = 0x00;
        SpiSend(&g_spi0, s_manager->sendCache1, 7);
    }

}

void YX027PortSendSetID(uint16_t chipID, uint16_t num)
{
    uint16_t i;
	
    YX027PortSetSetIdSeq(chipID);
	/*
    YX027PortSendCMD2S();
    if(s_manager->cmd == YX027_MODEL_UART) {
        for (i = 0; i < num; i++) {
            UartSend(&g_uart2, s_manager->setIdSeq, s_manager->setIdLen);
            UartWaitSendDone(&g_uart2);
            delayUs(s_manager->delayTime.byte);
        }
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        SpiSend(&g_spi0, s_manager->setIdSeq, s_manager->setIdLen);
    }
	*/
	
	if (s_manager->cmd == YX027_MODEL_UART) {
		UartSend(&g_uart2, s_manager->cmd2sSeq, s_manager->cmd2sLen);
		UartWaitSendDone(&g_uart2);
		for (i = 0; i < num; i++) {
			UartSend(&g_uart2, s_manager->setIdSeq, s_manager->setIdLen);
			UartWaitSendDone(&g_uart2);
			delayUs(s_manager->delayTime.byte);
		}
	}
	else {
		/* CMD2S */
		s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = 0x0C;
        s_manager->sendCache1[3] = 0xAA;
        s_manager->sendCache1[4] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), 2, 0);
        s_manager->sendCache1[5] = 0x00;
        s_manager->sendCache1[6] = 0x00;
		for (i = 0; i < s_manager->setIdLen; i++) {
			s_manager->sendCache1[i+7] = s_manager->setIdSeq[i];
		}
		SpiSend(&g_spi0, s_manager->sendCache1, s_manager->setIdLen + 7);
	}
}

void YX027PortSendBidirection(uint16_t num)
{
	uint16_t i;
	
	if (s_manager->cmd == YX027_MODEL_UART) {
		for (i = 0; i < num; i++) {
			UartSendByte(&g_uart2, s_manager->bidirectionalSeq[0]);
			UartWaitSendDone(&g_uart2);
			delayUs(s_manager->delayTime.bid1);
			
			UartSend(&g_uart2, &s_manager->bidirectionalSeq[1], s_manager->bidirectionalLen - 1);
			UartWaitSendDone(&g_uart2);
			delayUs(s_manager->delayTime.bid2);
		}
	}
}

void YX027PortMultiChipPWM(uint16_t chipID, uint8_t chipNum, uint8_t *pwmData)
{
    uint16_t i;
    uint16_t cnt;
    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[0] = 0x55;
        s_manager->sendCache1[1] = ((uint8_t)(chipID >> 8 | 0x10));
        s_manager->sendCache1[2] = ((uint8_t)(chipID & 0x00FF));
        s_manager->sendCache1[3] = chipNum;
        cnt = 4;
        for(i = 0; i < (chipNum + 1) * 6; i++) {
            s_manager->sendCache1[cnt] = pwmData[i];
            cnt = cnt + 1;
        }
        s_manager->sendCache1[cnt] = CRC8MaxIM(s_manager->sendCache1, cnt, 1);
        UartSend(&g_uart2, s_manager->sendCache1, cnt + 1);
        UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = ((uint8_t)(chipID >> 8 | 0x10));
        s_manager->sendCache1[3] = ((uint8_t)(chipID & 0x00FF));
        s_manager->sendCache1[4] = chipNum;
        cnt = 5;
        for(i = 0; i < (chipNum + 1) * 6; i++) {
            s_manager->sendCache1[cnt] = pwmData[i];
            cnt = cnt + 1;
        }
        s_manager->sendCache1[cnt] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), cnt - 2, 0);
        s_manager->sendCache1[cnt + 1] = 0x00;
        s_manager->sendCache1[cnt + 2] = 0x00;
        cnt = cnt + 3;
        SpiSend(&g_spi0, s_manager->sendCache1, cnt);
    }
	
	if (s_manager->with_vsync) {
		YX027PortSendVsync();
	}

    YX027PortClearCache1();
}

void YX027PortGroupChipPWM(uint8_t grpId, uint8_t *pwmData)
{
    uint16_t i;
    uint16_t cnt;
    if(s_manager->cmd == YX027_MODEL_UART) {
        s_manager->sendCache1[0] = 0x55;
        s_manager->sendCache1[1] = 0x14;
        s_manager->sendCache1[2] = grpId & 0x1F;
        cnt = 3;
        for(i = 0; i < 6; i++) {
            s_manager->sendCache1[cnt] = pwmData[i];
            cnt = cnt + 1;
        }
        s_manager->sendCache1[cnt] = CRC8MaxIM(s_manager->sendCache1, cnt, 1);

        UartSend(&g_uart2, s_manager->sendCache1, cnt + 1);
        UartWaitSendDone(&g_uart2);
    }
    else if(s_manager->cmd == YX027_MODEL_SPI) {
        s_manager->sendCache1[0] = 0xFF;
        s_manager->sendCache1[1] = 0xFF;
        s_manager->sendCache1[2] = 0x14;
        s_manager->sendCache1[3] = grpId & 0x1F;
        cnt = 4;
        for(i = 0; i < 6; i++) {
            s_manager->sendCache1[cnt] = pwmData[i];
            cnt = cnt + 1;
        }
        s_manager->sendCache1[cnt] = spi_CRC8MaxIM(&(s_manager->sendCache1[2]), cnt - 2, 0);
        s_manager->sendCache1[cnt + 1] = 0x00;
        s_manager->sendCache1[cnt + 2] = 0x00;
        cnt = cnt + 3;
        SpiSend(&g_spi0, s_manager->sendCache1, cnt);
    }
	
	if (s_manager->with_vsync) {
		YX027PortSendVsync();
	}
	
    YX027PortClearCache1();
}

void YX027PortSetFrame(uint8_t sysReg, uint8_t grpReg, YX027PortReg *regList, uint8_t *pwmData, uint8_t *pwmData2, uint8_t chipNum)
{
    FrameFlag = 0;
    fManger->sysReg = sysReg;
    fManger->gropReg = grpReg;
    //fManger->regList =regList;
    fManger->pwmData = pwmData;
    fManger->chipNum = chipNum;
    fManger->pwmData2 = pwmData2;
    //UartPrintf(&g_uart1, "%2x", fManger->gropReg);
}

void YX027PortFrame(uint8_t sendSysReg)
{
    if(sendSysReg) {
        YX027PortBroadcastReg(0x00, 0x00, &(fManger->sysReg));
        delayUs(100);
    }
    YX027PortBroadcastReg(0x00, 0x01, &(fManger->gropReg));
    delayUs(100);

    //	for(uint8_t i = 0 ;i < regNum;i++){
    //		YX027PortSingleReg(0,fManger->regList->chipId,fManger->regList->regAdrr,&(fManger->regList->regValue));
    //		delayUs(100);
    //	}
    YX027PortSendSetID(0x00, 1);
    delayUs(2000);
    if(FrameFlag == 0) {
        //YX027PortMultiChipPWM(0, fManger->chipNum, fManger->pwmData);
        YX027PortBroadcastReg(5, 0x0A, fManger->pwmData);
        delayUs(200);
        FrameFlag = 1;
    }
    else if(FrameFlag == 1) {
        //YX027PortMultiChipPWM(0, fManger->chipNum, fManger->pwmData2);
        YX027PortBroadcastReg(5, 0x0A, fManger->pwmData2);
        delayUs(200);
        FrameFlag = 0;
    }

    YX027PortReadReg(0, 0, 0x12);
    delayUs(200);

    YX027PortSendVsync();
    delayUs(200);

}

void YX027PortRes(uint8_t res, uint8_t RStatue)
{
    switch (res) {
    case 0:
        CH0RX = RStatue;
        rManger->RX0 = RStatue;
        break;
    case 1:
        CH0TX = RStatue;
        rManger->TX0 = RStatue;
        break;
    case 2:
        CH1RX = RStatue;
        rManger->RX1 = RStatue;
        break;
    case 3:
        CH1TX = RStatue;
        rManger->TX1 = RStatue;
        break;
    default:
        UartPrintf(&g_uart1, "RES error\r\n");
        break;
    }
}

void showYX027PortResState(void)
{
    UartPrintf(&g_uart1, "CH0TX:%d\r\n", rManger->TX0);
    UartPrintf(&g_uart1, "CH0RX:%d\r\n", rManger->RX0);
    UartPrintf(&g_uart1, "CH1TX:%d\r\n", rManger->TX1);
    UartPrintf(&g_uart1, "CH1RX:%d\r\n", rManger->RX1);
}

void YX027PortTrimSqe1(uint8_t testModel)
{
    uint8_t regVal1 = 0xCC;

    YX027PortBroadcastReg(0, 0x7F, &(regVal1));
    delayUs(100);
    YX027PortSendSetID(0, 1);
    delayUs(50);
    YX027PortBroadcastReg(0, 0x7F, &(testModel));
    delayUs(100);
}

void YX027PortTrimSqe2(void)
{
    uint8_t regVal = 0x82;
    YX027PortBroadcastReg(0, 0x7F, &(regVal));
}

void YX027PortTrimSqe3(uint8_t *data, uint8_t start, uint8_t end)
{
	uint8_t reg;
	
    uint8_t satrtReg = 0x20 + start;
	
    TrimIO = 0;
    for(uint8_t i = 0 ; i < end ; i++ ) {
        reg = i + satrtReg;
        YX027PortBroadcastReg(0, 0x7A, &data[i]);
        YX027PortBroadcastReg(0, 0x7B, &reg);
        delayUs(200);
        TrimIO = 1;
        delayMs(1);
        delayUs(100);
        TrimIO = 0;
        delayUs(200);
    }
}

void YX027PortTrimRead(void)
{
    uint8_t regVal = 0x37;
    YX027PortSetCmd(YX027_MODEL_UART);
    YX027PortBroadcastReg(0, 0x7F, &(regVal));
    delayUs(500);
    YX027PortReadReg(7, 0, 0x28);
}
