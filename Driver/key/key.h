#ifndef __KEY_H
#define __KEY_H

#include "stdbool.h"
#include "delay.h"

#define KEY_EXTI_EN 1

void KeyInit(void);

#endif
