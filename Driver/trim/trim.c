#include "trim.h"
#include "delay.h"
#include "NuMicro.h"

#define TRIM_AD PB12
#define TRIM_CE PB13

#define TRIM_IIC_CLK PB12
#define TRIM_IIC_DAT PB13

#define TRIM_SPI_CLK	PB12
#define TRIM_SPI_MOSI	PB13

struct IICManger {
    uint16_t time;
};

struct TrimManager {
    uint16_t tas;
    uint16_t tpw;
    uint16_t tah;
    uint16_t tinterval;
	
	TrimPinStatusEnum pin_stat;
	
    struct IICManger iic;
};

static struct TrimManager *s_trim_manager = NULL;

/**
 * @brief	Trim 初始化
 */
void TrimInit(void)
{
    static struct TrimManager manager;

    GPIO_SetMode(PB, BIT12, GPIO_MODE_OUTPUT);
    GPIO_SetMode(PB, BIT13, GPIO_MODE_OUTPUT);
    GPIO_SetPullCtl(PB, BIT12, GPIO_PUSEL_DISABLE);
    GPIO_SetPullCtl(PB, BIT13, GPIO_PUSEL_DISABLE);
    TRIM_AD = 0;
    TRIM_CE = 0;

    manager.tas = 5;
    manager.tpw = 30;
    manager.tah = 5;
    manager.tinterval = 10;
	manager.pin_stat = TRIM_PIN_STAT_NORMAL;
    manager.iic.time = 4;
    s_trim_manager = &manager;
}

/**
 * @brief	Trim 引脚转为 IIC 状态
 * @note	IO 转为开漏输出
 */
void TrimToIIC(void)
{
	TRIM_IIC_CLK = 1;
	TRIM_IIC_DAT = 1;
	GPIO_SetMode(PB, BIT12, GPIO_MODE_OPEN_DRAIN);
	GPIO_SetMode(PB, BIT13, GPIO_MODE_OPEN_DRAIN);
	GPIO_SetPullCtl(PB, BIT12, GPIO_PUSEL_DISABLE);
    GPIO_SetPullCtl(PB, BIT13, GPIO_PUSEL_DISABLE);
	
	s_trim_manager->pin_stat = TRIM_PIN_STAT_IIC;
}

/**
 * @brief	Trim 引脚转为正常状态
 * @note	IO 转为浮空输出
 */
void TrimToNormal(void)
{
	TRIM_AD = 0;
	TRIM_CE = 0;
	GPIO_SetMode(PB, BIT12, GPIO_MODE_OUTPUT);
    GPIO_SetMode(PB, BIT13, GPIO_MODE_OUTPUT);
    GPIO_SetPullCtl(PB, BIT12, GPIO_PUSEL_DISABLE);
    GPIO_SetPullCtl(PB, BIT13, GPIO_PUSEL_DISABLE);
	
	s_trim_manager->pin_stat = TRIM_PIN_STAT_NORMAL;
}

void TrimWrite(uint32_t data, uint8_t bitNum)
{
    uint8_t i;
    uint32_t temp = data;

    TRIM_AD = 0;
    TRIM_CE = 0;
    for (i = 0; i < bitNum; i++) {
        TRIM_AD = 1;
        delayUs(s_trim_manager->tas);
        if (temp & 0x01) {
            TRIM_CE = 1;
        }
        else {
            TRIM_CE = 0;
        }
        delayUs(s_trim_manager->tpw);
        temp >>= 1;
        TRIM_CE = 0;
        delayUs(s_trim_manager->tah);
        TRIM_AD = 0;
        delayUs(s_trim_manager->tinterval);
    }
    TRIM_CE = 0;
}

void TrimSetTas(uint16_t tas)
{
    s_trim_manager->tas = tas;
}

void TrimSetTpw(uint16_t tpw)
{
    s_trim_manager->tpw = tpw;
}

void TrimSetTah(uint16_t tah)
{
    s_trim_manager->tah = tah;
}

void TrimSetIntervel(uint16_t intervel)
{
    s_trim_manager->tinterval = intervel;
}

uint16_t TrimGetTas(void)
{
    return s_trim_manager->tas;
}

uint16_t TrimGetTpw(void)
{
    return s_trim_manager->tpw;
}

uint16_t TrimGetTah(void)
{
    return s_trim_manager->tah;
}

uint16_t TrimGetIntervel(void)
{
    return s_trim_manager->tinterval;
}

void TrimIICGenStart(void)
{
    TRIM_IIC_DAT = 1;
    TRIM_IIC_CLK = 1;
    delayUs(s_trim_manager->iic.time);
    TRIM_IIC_DAT = 0;
    delayUs(s_trim_manager->iic.time);
    TRIM_IIC_CLK = 0;
}

void TrimIICGenStop(void)
{
    TRIM_IIC_CLK = 0;
    TRIM_IIC_DAT = 0;
    delayUs(s_trim_manager->iic.time);
    TRIM_IIC_CLK = 1;
    TRIM_IIC_DAT = 1;
    delayUs(s_trim_manager->iic.time);
}

/**
 * @brief	等待应答信号
 * @retval	0 = 收到应答
 *			1 = 未收到应答
 */
uint8_t TrimIICWaitAck(void)
{
    uint16_t errTime = 0;

    TRIM_IIC_DAT = 1;
    delayUs(1);
    TRIM_IIC_CLK = 1;
    delayUs(1);
    GPIO_SetMode(PB, BIT13, GPIO_MODE_INPUT);
    while (TRIM_IIC_DAT) {
        errTime++;
        if (errTime > 500) {
            GPIO_SetMode(PB, BIT13, GPIO_MODE_OPEN_DRAIN);
            TrimIICGenStop();
            return 1;
        }
    }
    GPIO_SetMode(PB, BIT13, GPIO_MODE_OPEN_DRAIN);
    TRIM_IIC_CLK = 0;

    return 0;
}

void TrimIICGenAck(void)
{
    TRIM_IIC_CLK = 0;
    TRIM_IIC_DAT = 0;
    delayUs(2);
    TRIM_IIC_CLK = 1;
    delayUs(2);
    TRIM_IIC_CLK = 0;
}

void TrimIICGenNoAck(void)
{
    TRIM_IIC_CLK = 0;
    TRIM_IIC_DAT = 1;
    delayUs(2);
    TRIM_IIC_CLK = 1;
    delayUs(2);
    TRIM_IIC_CLK = 0;
}

/**
 * @brief	通过 IIC 协议发送一个字节
 * @param	data 要发送的数据
 */
void TrimIICSendOneByte(uint8_t data)
{
    uint8_t t;

    TRIM_IIC_CLK = 0;
    for (t = 0; t < 8; t++) {
        TRIM_IIC_DAT = (data & 0x80) >> 7;
        data <<= 1;
        delayUs(2);
        TRIM_IIC_CLK = 1;
        delayUs(2);
        TRIM_IIC_CLK = 0;
        delayUs(2);
    }
}

/**
 * @breif	通过 IIC 协议读取一个字节
 * @param	ack 1 = 发送应答，0 = 发送非应答
 * @retval	读取到的数据
 */
uint8_t TrimIICReadOneByte(uint8_t ack)
{
    uint8_t i;
    uint8_t result = 0;

    GPIO_SetMode(PB, BIT13, GPIO_MODE_INPUT);
    for (i = 0; i < 8; i++) {
        TRIM_IIC_CLK = 0;
        delayUs(2);
        TRIM_IIC_CLK = 1;
        result <<= 1;
        if (TRIM_IIC_DAT) {
            result++;
        }
        delayUs(1);
    }
    GPIO_SetMode(PB, BIT13, GPIO_MODE_OPEN_DRAIN);
    if (ack) {
        TrimIICGenAck();
    }
    else {
        TrimIICGenNoAck();
    }
	
	return result;
}

uint8_t TrimIICWriteReg(uint8_t slave, uint8_t reg, uint8_t val)
{
	TrimIICGenStart();
	TrimIICSendOneByte(slave);
	if (TrimIICWaitAck()) {
		return 1;
	}
	TrimIICSendOneByte(reg);
	if (TrimIICWaitAck()) {
		return 2;
	}
	TrimIICSendOneByte(val);
	if (TrimIICWaitAck()) {
		return 3;
	}
	TrimIICGenStop();
	
	return 0;
}

uint8_t TrimIICWriteArray(uint8_t slave, uint8_t reg, uint8_t *array, uint16_t len)
{
	uint16_t i;
	
	TrimIICGenStart();
	TrimIICSendOneByte(slave);
	if (TrimIICWaitAck()) {
		return 1;
	}
	TrimIICSendOneByte(reg);
	if (TrimIICWaitAck()) {
		return 2;
	}
	for (i = 0; i < len; i++) {
		TrimIICSendOneByte(array[i]);
		if (TrimIICWaitAck()) {
			return 3;
		}
	}
	TrimIICGenStop();
	
	return 0;
}

uint8_t TrimIICReadReg(uint8_t slave, uint8_t reg, uint8_t *val)
{
	TrimIICGenStart();
	TrimIICSendOneByte(slave);
	if (TrimIICWaitAck()) {
		return 1;
	}
	TrimIICSendOneByte(reg);
	if (TrimIICWaitAck()) {
		return 2;
	}
	TrimIICGenStart();
	TrimIICSendOneByte(slave | 0x01);
	if (TrimIICWaitAck()) {
		return 3;
	}
	*val = TrimIICReadOneByte(0);
	TrimIICGenStop();
	
	return 0;
}

uint8_t TrimIICReadArray(uint8_t slave, uint8_t reg, uint8_t *array, uint16_t len)
{
	TrimIICGenStart();
	TrimIICSendOneByte(slave);
	if (TrimIICWaitAck()) {
		return 1;
	}
	TrimIICSendOneByte(reg);
	if (TrimIICWaitAck()) {
		return 2;
	}
	TrimIICGenStart();
	TrimIICSendOneByte(slave | 0x01);
	if (TrimIICWaitAck()) {
		return 3;
	}
	while (len) {
		if (len == 1) {
			*array = TrimIICReadOneByte(0);
		}
		else {
			*array = TrimIICReadOneByte(1);
		}
		len--;
		array++;
	}
	TrimIICGenStop();
	
	return 0;
}

void TrimSPISendBits(uint32_t data, uint8_t bits, uint16_t nopNum)
{
	uint8_t i;
	uint16_t d = nopNum / 2;
	uint32_t checkBit = 0x01 << (bits - 1);
	uint32_t temp = data;
	
	delayByNop(d / 2);
	for (i = 0; i < bits; i++) {
		if (temp & checkBit) {
			TRIM_SPI_MOSI = 1;
		}
		else {
			TRIM_SPI_MOSI = 0;
		}
		delayByNop(d);
		TRIM_SPI_CLK = 1;
		delayByNop(d);
		TRIM_SPI_CLK = 0;
		temp <<= 1;
	}
	TRIM_SPI_CLK = 0;
	TRIM_SPI_MOSI = 0;
}

void TrimSPISendByteArray(uint8_t *array, uint16_t len, uint16_t nopNum)
{
	uint16_t i;
	
	for (i = 0; i < len; i++) {
		TrimSPISendBits(array[i], 8, nopNum);
	}
}

void TrimSPISendHalfwordArray(uint16_t *array, uint16_t len, uint16_t nopNum)
{
	uint16_t i;
	
	for (i = 0; i < len; i++) {
		TrimSPISendBits(array[i], 16, nopNum);
	}
}

void TrimSPISend17BitsArray(uint32_t *array, uint16_t len, uint16_t nopNum)
{
	uint16_t i;
	
	for (i = 0; i < len; i++) {
		TrimSPISendBits(array[i], 17, nopNum);
	}
}

void TrimSPISendBitsFast(uint32_t data, uint8_t bits)
{
	uint8_t i;
	uint32_t checkBit = 0x01 << (bits - 1);
	uint32_t temp = data;
	
	for (i = 0; i < bits; i++) {
		if (temp & checkBit) {
			TRIM_SPI_MOSI = 1;
		}
		else {
			TRIM_SPI_MOSI = 0;
		}
		TRIM_SPI_CLK = 1;
		temp <<= 1;
		TRIM_SPI_CLK = 0;
	}
	TRIM_SPI_CLK = 0;
	TRIM_SPI_MOSI = 0;
}

void TrimSPISendByteArrayFast(uint8_t *array, uint16_t len)
{
	uint16_t i;
	
	for (i = 0; i < len; i++) {
		TrimSPISendBitsFast(array[i], 8);
	}
}

void TrimSPISendHalfwordArrayFast(uint16_t *array, uint16_t len)
{
	uint16_t i;
	
	for (i = 0; i < len; i++) {
		TrimSPISendBitsFast(array[i], 16);
	}
}

void TrimSPISend17BitsArrayFast(uint32_t *array, uint16_t len)
{
	uint16_t i;
	
	for (i = 0; i < len; i++) {
		TrimSPISendBitsFast(array[i], 17);
	}
}
