#ifndef __TRIM_CMD_H
#define __TRIM_CMD_H

#include "stdint.h"

#define TRIM_CMD_HALFCACHE_SIZE 1024
#define TRIM_CMD_WORDCACHE_SZIE 1024

void TrimCmdInit(void);

#endif
