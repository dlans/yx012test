#ifndef __TRIM_H
#define __TRIM_H

#include "stdint.h"

#define TrimToSPI TrimToNormal

typedef enum {
	TRIM_PIN_STAT_NORMAL,
	TRIM_PIN_STAT_IIC
} TrimPinStatusEnum;

void TrimInit(void);
void TrimToIIC(void);
void TrimToNormal(void);
void TrimWrite(uint32_t data, uint8_t bitNum);
void TrimSetTas(uint16_t tas);
void TrimSetTpw(uint16_t tpw);
void TrimSetTah(uint16_t tah);
void TrimSetIntervel(uint16_t intervel);
uint16_t TrimGetTas(void);
uint16_t TrimGetTpw(void);
uint16_t TrimGetTah(void);
uint16_t TrimGetIntervel(void);

void TrimIICGenStart(void);
void TrimIICGenStop(void);
uint8_t TrimIICWaitAck(void);
void TrimIICGenAck(void);
void TrimIICGenNoAck(void);
void TrimIICSendOneByte(uint8_t data);
uint8_t TrimIICReadOneByte(uint8_t ack);

uint8_t TrimIICWriteReg(uint8_t slave, uint8_t regAddr, uint8_t regVal);
uint8_t TrimIICWriteArray(uint8_t slave, uint8_t reg, uint8_t *array, uint16_t len);
uint8_t TrimIICReadReg(uint8_t slave, uint8_t reg, uint8_t *val);
uint8_t TrimIICReadArray(uint8_t slave, uint8_t reg, uint8_t *array, uint16_t len);

void TrimSPISendBits(uint32_t data, uint8_t bits, uint16_t nopNum);
void TrimSPISendByteArray(uint8_t *array, uint16_t len, uint16_t nopNum);
void TrimSPISendHalfwordArray(uint16_t *array, uint16_t len, uint16_t nopNum);
void TrimSPISend17BitsArray(uint32_t *array, uint16_t len, uint16_t nopNum);
void TrimSPISendBitsFast(uint32_t data, uint8_t bits);
void TrimSPISendByteArrayFast(uint8_t *array, uint16_t len);
void TrimSPISendHalfwordArrayFast(uint16_t *array, uint16_t len);
void TrimSPISend17BitsArrayFast(uint32_t *array, uint16_t len);

#endif
