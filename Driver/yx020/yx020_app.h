#ifndef __YX020_APP_H
#define __YX020_APP_H

#include "stdint.h"

#define YX020_LOG_CACHE_SIZE   1024
#define YX020_FRAME_CACHE_SIZE 512

typedef enum {
    YX020_BROAD_REG0 = 0x61,
    YX020_BROAD_REG1 = 0x63,
    YX020_BROAD_REG2 = 0x65,
    YX020_BROAD_REG3 = 0x67,
    YX020_BROAD_REG4 = 0x69,
    YX020_BROAD_REG5 = 0x6B,
    YX020_BROAD_REG6 = 0x6D,
    YX020_BROAD_REG7 = 0x6F
} YX020_BROAD_REG_E;

typedef enum {
    YX020_WSELECT_REG0 = 0x60,
    YX020_WSELECT_REG1 = 0x62,
    YX020_WSELECT_REG2 = 0x64,
    YX020_WSELECT_REG3 = 0x66,
    YX020_WSELECT_REG4 = 0x68,
    YX020_WSELECT_REG5 = 0x6A,
    YX020_WSELECT_REG6 = 0x6C,
    YX020_WSELECT_REG7 = 0x6E
} YX020_WSELECT_REG_E;

void YX020AppInit(void);
void YX020CreateTask(uint8_t priority, uint8_t priorityFrame);

void YX020BroadcastReg(YX020_BROAD_REG_E reg, uint8_t val);
void YX020BroadcastFrame(const uint8_t *regList, const uint16_t *brData, uint8_t tm, uint8_t btm);
void YX020WriteSelectReg(YX020_WSELECT_REG_E reg, uint8_t val, uint16_t id, uint8_t cmd2s);
void YX020InitAndSetup(uint8_t reg4, uint16_t chipNum, uint8_t btm, uint8_t reg7, uint8_t cmd2d);
void YX020Frame(const uint8_t *regList, const uint16_t *brData, uint16_t brLen, uint16_t startID, uint8_t tm, uint8_t btm);
void YX020HLBuadFrame(const uint8_t *regList, const uint16_t *brData, uint16_t brLen, uint16_t startID, uint8_t tm, uint8_t btm,uint8_t isF0);

#endif
