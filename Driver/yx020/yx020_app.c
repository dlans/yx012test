/**
 * @file	yx020_app.c
 * @version	V0.1
 * @brief	yx020相关指令
 */
#include "yx020_app.h"
#include "main.h"
#include "util_crc.h"
#include "util_cmd.h"
#include "util_str.h"
#include "delay.h"
#include "uart_peripheral.h"
#include "uart_cmd.h"
#include "stdio.h"
#include "string.h"

#define CACHE1_SIZE         64

#define INIT_BYTE_DELAY		30		/* 初始化字节与字节间延时 */
#define INIT_FRAGE_DELAY	50		/* 初始化段与段间延时 */
#define SET_ID_DELAY		50		/* 设置 ID 间延时 */
#define FRAME_HEAD_DELAY    50  	/* 帧中 [F5, 79, R4] 到 CMD2S 的延时 */
#define FRAME_CMD2S_DELAY   10  	/* 帧中 CMD2S 到 SetID 的延时 */
#define FRAME_VSYNC_DELAY   10  	/* 帧中 CMD2D 到 VSYNC 的延时 */
#define DELAY_OFFSET		(-3)	/* 补偿 Wait 函数的返回 */

#define ABB_VERSION 	0xC000
#define CHIP_IDX_MSK	0x0700

#define YX020_EVENT_FLASH	0x01

struct FrameTaskParameter {
    uint8_t regs[6];
    uint16_t len;
    uint16_t id;
    uint16_t time;
    uint16_t newTime;
	uint8_t *data8;
	uint16_t *data16;
};

/**
 * @brief	YX020APP 管理结构体
 * @details	critical	发送时是否进入临界段
 *			disableIRQ	发送时是否屏蔽中断
 *			sendCache1	发送缓冲，大小由 CACHE1_SIZE 设置
 *			echo		是否回显发送的数据
 *          pdTime      lowiq 的下拉时间, 单位 us
 *          lowBaud     低波特率
 *          highBaud    高波特率
 */
struct YX020ManagerType {
    uint8_t critical : 1;
    uint8_t disableIRQ : 1;
    uint8_t echo : 1;
    uint8_t bit8Mode : 1;
    uint8_t lowiq : 1;
    uint8_t isF0 : 1;
    uint8_t reversed : 2;

    uint16_t chipNum;
    uint16_t delayCMD2D;
    uint32_t pdTime;
    uint32_t lowBaud;
    uint32_t highBaud;
    float magin;
    uint8_t *sendCache1;
    uint8_t *sendCache2;
    uint16_t *rxCache1;
    uint8_t *rxCache2;
    char *logCache1;
    char *logCache2;
    char *logCache3;

    uint8_t const *cmd2sSeq;
    uint8_t cmd2sLen;
    uint8_t const *setIDSeq;
    uint8_t setIDLen;
    uint8_t const *vsyncSeq;
    uint8_t vsyncLen;
    uint8_t const *cmd2dSeq;
    uint8_t cmd2dLen;
    uint8_t const *setBaudSql;
    uint8_t setBaudLen;

    struct FrameTaskParameter frame;
};

static struct YX020ManagerType *sManager = NULL;

static char *sCmdYx020 = "yx020";
static char *sParInit = "init";
static char *sPar2ndTmno2d = "tmno2d";

static char *sParFrame = "frame";
static char *sPar2ndFrame0 = "frame0";
static char *sPar2ndBroadcast = "broadcast";
static char *sPar2ndOnlybr = "onlybr";
static char *sPar2ndRegs = "regs";
static char *sPar2ndData = "data";
static char *sPar2ndStartid = "startid";
static char *sPar2ndTime = "time";
static char *sPar2ndHLBaud = "hlbaud";
static char *sPar2ndZero = "zero";

static char *sParSetid = "setid";
static char *sParCritical = "critical";
static char *sParEcho = "echo";
static char *sParFree = "free";
static char *sParBrreg = "brreg";
static char *sParSereg = "sereg";
static char *sParReadreg = "readreg";
static char *sPar8bitmode = "8bitmode";
static char *sParLowiq = "lowiq";
static char *sParPdtime = "pdtime";

static char *sParTestmode = "testmode";
static char *sPar2ndEnter = "enter";
static char *sPar2ndEnter6 = "enter6";
static char *sPar2ndPreview = "preview";
static char *sPar2ndBkw10 = "bkw10";
static char *sPar2ndTestOSC = "OSC";

static char *sParVsync = "vsync";

static char *sParTrim = "trim";
static char *sPar2ndBurn = "burn";
static char *sPar2ndRead = "read";

static char *sParCacheBaud = "cacheBaud";

static TaskHandle_t logTaskHandle = NULL;
static TaskHandle_t frameTaskHandle = NULL;
static EventGroupHandle_t eventGroup = NULL;
static TimerHandle_t timerHandle = NULL;

/**
 * @brief   计算 CMD2D 延时时间
 */
static void calDelayCMD2D(void)
{
    float baud = UartGetBaud(UART_COM2);
    float t = 500000.0f / baud;
    sManager->delayCMD2D = t * sManager->chipNum * sManager->magin;
}

/**
 * @brief   当波特率通过 uart2 -baud 修改时的回调
 */
static void baudCallback(void)
{
    calDelayCMD2D();
}

/**
 * @brief   定时器回调, 用于自动帧时间
 */
static void timerCallback(TimerHandle_t xTimer)
{
    xEventGroupSetBits(eventGroup, YX020_EVENT_FLASH);
    if (sManager->frame.time != sManager->frame.newTime) {
        if (sManager->frame.newTime) {
            xTimerChangePeriod(timerHandle, sManager->frame.newTime, 0);
            sManager->frame.time = sManager->frame.newTime;
        }
        else {
            xTimerStop(timerHandle, 0);
            vTaskSuspend(frameTaskHandle);
        }
    }
}

/**
 * @brief   日志任务，发送完成后将挂起自身
 */
static void logTask(void *parameter)
{
    uint16_t i;

    while (1) {
        while (!UartCheckSendDone(UART_COM1)) {
            vTaskDelay(10);
        }
        UartPutsPDMA(UART_COM1, sManager->logCache1);

        while (!UartCheckSendDone(UART_COM1)) {
            vTaskDelay(10);
        }
        UartPutsPDMA(UART_COM1, sManager->logCache2);
        for (i = 0; i < YX020_LOG_CACHE_SIZE; i++) {
            sManager->logCache1[i] = 0x00;
        }

        while (!UartCheckSendDone(UART_COM1)) {
            vTaskDelay(10);
        }
        UartPutsPDMA(UART_COM1, sManager->logCache3);
        for (i = 0; i < YX020_LOG_CACHE_SIZE * 4; i++) {
            sManager->logCache2[i] = 0x00;
        }

        while (!UartCheckSendDone(UART_COM1)) {
            vTaskDelay(10);
        }
        for (i = 0; i < YX020_LOG_CACHE_SIZE; i++) {
            sManager->logCache3[i] = 0x00;
        }
        vTaskSuspend(logTaskHandle);
    }
}

static void frameTask(void *parameter)
{
    while (1) {
        xEventGroupWaitBits(eventGroup, YX020_EVENT_FLASH, pdTRUE, pdTRUE, portMAX_DELAY);
		if (sManager->bit8Mode) {
			YX020Frame(sManager->frame.regs, (uint16_t *)sManager->frame.data8, sManager->frame.len, sManager->frame.id, 0, 0);
		}
		else {
			YX020Frame(sManager->frame.regs, sManager->frame.data16, sManager->frame.len, sManager->frame.id, 0, 0);
		}
    }
}

static void freeCache(void)
{
    if (sManager->sendCache2 != NULL) {
        vPortFree(sManager->sendCache2);
        sManager->sendCache2 = NULL;
    }
    if (sManager->rxCache1 != NULL) {
        vPortFree(sManager->rxCache1);
        sManager->rxCache1 = NULL;
    }
    if (sManager->rxCache2 != NULL) {
        vPortFree(sManager->rxCache2);
        sManager->rxCache2 = NULL;
    }
	if (sManager->frame.data8 != NULL) {
		vPortFree(sManager->frame.data8);
		sManager->frame.data8 = NULL;
	}
}

/**
 * @brief   发送帧头
 * @param   reg4 REG4 的值
 * @param   reg7 REG7 的值
 * @param   tm   是否是板级测试
 */
static void sendFrameHead(uint8_t reg4, uint8_t reg7, uint8_t tm)
{
    UartRawSendByte(UART_COM2, 0xF5);
    UartWaitSendDone(UART_COM2);
    delayUs(INIT_BYTE_DELAY);
    UartRawSendByte(UART_COM2, 0x79);
    UartWaitSendDone(UART_COM2);

    if (tm) {
        delayUs(INIT_BYTE_DELAY);
        YX020BroadcastReg(YX020_BROAD_REG7, reg7);
        UartWaitSendDone(UART_COM2);
    }

    delayUs(INIT_BYTE_DELAY);
    YX020BroadcastReg(YX020_BROAD_REG4, reg4);
    UartWaitSendDone(UART_COM2);
}

static void sendCMD2S(void)
{
    UartRawSend(UART_COM2, sManager->cmd2sSeq, sManager->cmd2sLen);
    UartWaitSendDone(UART_COM2);
}

static void sendCMD2D(void)
{
    UartRawSend(UART_COM2, sManager->cmd2dSeq, sManager->cmd2dLen);
    UartWaitSendDone(UART_COM2);
}

static void sendVSYNC(void)
{
    UartRawSend(UART_COM2, sManager->vsyncSeq, sManager->vsyncLen);
    UartWaitSendDone(UART_COM2);
}

static void sendSetBaud(void)
{
    UartRawSend(UART_COM2, sManager->setBaudSql, sManager->setBaudLen);
    UartWaitSendDone(UART_COM2);
}

/**
 * @brief		发送设置 ID 指令
 * @param[in]	num 发送指令的数量
 */
static void sendSetID(uint16_t num)
{
    uint16_t i;

    sendCMD2S();
    delayUs(200 + DELAY_OFFSET);

    for (i = 0; i < num; i++) {
        UartRawSend(UART_COM2, sManager->setIDSeq, sManager->setIDLen);
        UartWaitSendDone(UART_COM2);
        delayUs(INIT_BYTE_DELAY + DELAY_OFFSET);
    }
}

/**
 * @brief		发送亮度
 * @param[in]	startID 起始 ID
 * @param[in]	brData  亮度数据数组
 * @param[in]	brLen   数据长度
 */
static void sendBrightness(uint16_t startID, const uint16_t *brData, uint16_t brLen)
{
    uint16_t i, j, cnt;
    uint16_t t;
    char log[32];

    uint16_t chipNum = brLen / 3;
    uint16_t repeat = chipNum / 8;
    uint8_t rest = chipNum % 8;
    uint8_t brRest = brLen % 3;

    sManager->sendCache1[0] = 0x55;
    /* 8 个一组 */
    for (i = 0; i < repeat; i++) {
        sManager->sendCache1[1] = 0x80 | (7<<1);
        sManager->sendCache1[2] = startID;
        if (APP_BIT_CHECK(startID, 0x01 << 8)) {
            sManager->sendCache1[1] |= 0x01;
        }
        cnt = 3;

        for (j = 0; j < 24; j++) {
            t = brData[i*24+j];
            sManager->sendCache1[cnt] = t >> 8;
            cnt++;
            sManager->sendCache1[cnt] = t;
            cnt++;
        }
        sManager->sendCache1[cnt] = CRC8MaxIM(sManager->sendCache1, cnt, 1);
        cnt++;
        UartRawSend(UART_COM2, sManager->sendCache1, cnt);

        startID += 8;

        if (sManager->echo) {
            strcat(sManager->logCache2, "Send brigtness:\r\n");
            for (j = 0; j < cnt; j++) {
                sprintf(log, "%02X ", sManager->sendCache1[j]);
                strcat(sManager->logCache2, log);
            }
            strcat(sManager->logCache2, "\r\n");
        }
    }

    if (rest) {
        sManager->sendCache1[1] = 0x80 | ((rest - 1) << 1);
        sManager->sendCache1[2] = startID;
        if (APP_BIT_CHECK(startID, 0x01 << 8)) {
            sManager->sendCache1[1] |= 0x01;
        }
        cnt = 3;
        for (j = 0; j < rest * 3; j++) {
            t = brData[i*24+j];
            sManager->sendCache1[cnt] = t >> 8;
            cnt++;
            sManager->sendCache1[cnt] = t;
            cnt++;
        }
        if (brRest != 0) {
            t = sManager->sendCache1[1];
            t &= 0x01;
            sManager->sendCache1[1] >>= 1;
            sManager->sendCache1[1]++;
            sManager->sendCache1[1] <<= 1;
            sManager->sendCache1[1] |= t;
            for (j = 0; j < brRest; j++) {
                t = brData[chipNum*3+j];
                sManager->sendCache1[cnt] = t >> 8;
                cnt++;
                sManager->sendCache1[cnt] = t;
                cnt++;
            }
            while (j < 3) {
                sManager->sendCache1[cnt++] = 0x00;
                sManager->sendCache1[cnt++] = 0x00;
                j++;
            }
        }

        sManager->sendCache1[cnt] = CRC8MaxIM(sManager->sendCache1, cnt, 1);
        cnt++;
        UartRawSend(UART_COM2, sManager->sendCache1, cnt);

        if (sManager->echo) {
            sprintf(log, "Send brigtness:\r\n");
            strcat(sManager->logCache2, log);
            for (j = 0; j < cnt; j++) {
                sprintf(log, "%02X ", sManager->sendCache1[j]);
                strcat(sManager->logCache2, log);
            }
            strcat(sManager->logCache2, "\r\n");
        }
    }
}

/**
 * @brief		发送亮度, 8bit模式
 * @param[in]	startID 起始 ID
 * @param[in]	brData  亮度数据数组
 * @param[in]	brLen   数据长度
 */
static void sendBrightness8bit(uint16_t startID, const uint8_t *brData, uint16_t brLen)
{
    uint16_t i, j, cnt;
    uint16_t t;
    char log[32];

    uint16_t chipNum = brLen / 3;
    uint16_t repeat = chipNum / 8;
    uint8_t rest = chipNum % 8;
    uint8_t brRest = brLen % 3;

    sManager->sendCache1[0] = 0x55;
    /* 8 个一组 */
    for (i = 0; i < repeat; i++) {
        sManager->sendCache1[1] = 0x80 | (7<<1);
        sManager->sendCache1[2] = startID;
        if (APP_BIT_CHECK(startID, 0x01 << 8)) {
            sManager->sendCache1[1] |= 0x01;
        }
        cnt = 3;

        for (j = 0; j < 24; j++) {
            t = brData[i*24+j];
            sManager->sendCache1[cnt] = t;
            cnt++;
        }
        sManager->sendCache1[cnt] = CRC8MaxIM(sManager->sendCache1, cnt, 1);
        cnt++;
        UartRawSend(UART_COM2, sManager->sendCache1, cnt);

        startID += 8;

        if (sManager->echo) {
            strcat(sManager->logCache2, "Send brigtness:\r\n");
            for (j = 0; j < cnt; j++) {
                sprintf(log, "%02X ", sManager->sendCache1[j]);
                strcat(sManager->logCache2, log);
            }
            strcat(sManager->logCache2, "\r\n");
        }
    }

    if (rest) {
        sManager->sendCache1[1] = 0x80 | ((rest - 1) << 1);
        sManager->sendCache1[2] = startID;
        if (APP_BIT_CHECK(startID, 0x01 << 8)) {
            sManager->sendCache1[1] |= 0x01;
        }
        cnt = 3;
        for (j = 0; j < rest * 3; j++) {
            t = brData[i*24+j];
            sManager->sendCache1[cnt] = t;
            cnt++;
        }
        if (brRest != 0) {
            t = sManager->sendCache1[1];
            t &= 0x01;
            sManager->sendCache1[1] >>= 1;
            sManager->sendCache1[1]++;
            sManager->sendCache1[1] <<= 1;
            sManager->sendCache1[1] |= t;
            for (j = 0; j < brRest; j++) {
                t = brData[chipNum*3+j];
                sManager->sendCache1[cnt] = t;
                cnt++;
            }
            while (j < 3) {
                sManager->sendCache1[cnt++] = 0x00;
                sManager->sendCache1[cnt++] = 0x00;
                j++;
            }
        }

        sManager->sendCache1[cnt] = CRC8MaxIM(sManager->sendCache1, cnt, 1);
        cnt++;
        UartRawSend(UART_COM2, sManager->sendCache1, cnt);

        if (sManager->echo) {
            sprintf(log, "Send brigtness:\r\n");
            strcat(sManager->logCache2, log);
            for (j = 0; j < cnt; j++) {
                sprintf(log, "%02X ", sManager->sendCache1[j]);
                strcat(sManager->logCache2, log);
            }
            strcat(sManager->logCache2, "\r\n");
        }
    }
}

static void broadcastBr(uint16_t *brData, uint8_t vsync)
{
    char log[8];
    uint16_t i;

    sManager->sendCache1[0] = 0x55;
    sManager->sendCache1[1] = 0x81;
    sManager->sendCache1[2] = 0xFF;
    sManager->sendCache1[3] = brData[0] >> 8;
    sManager->sendCache1[4] = brData[0];
    sManager->sendCache1[5] = brData[1] >> 8;
    sManager->sendCache1[6] = brData[1];
    sManager->sendCache1[7] = brData[2] >> 8;
    sManager->sendCache1[8] = brData[2];
    sManager->sendCache1[9] = CRC8MaxIM(sManager->sendCache1, 9, 1);
    UartRawSendPDMA(UART_COM2, (uint32_t)sManager->sendCache1, 10);

    if (sManager->echo) {
        sprintf(sManager->logCache1, "Broadcast Br:");
        for (i = 0; i < 10; i++) {
            sprintf(log, "%02X ", sManager->sendCache1[i]);
            strcat(sManager->logCache1, log);
        }
        strcat(sManager->logCache1, "\r\n");
    }

    if (vsync) {
        sendVSYNC();
        if (sManager->echo) {
            strcat(sManager->logCache1, "vsync sent 1 times\r\n");
        }
    }
}

static void readReg(uint16_t id, uint8_t reg)
{
    uint8_t i;
    char log[8];

    sManager->sendCache1[0] = 0x55;
    sManager->sendCache1[1] = 0xA0;
    sManager->sendCache1[1] |= reg << 1;
    if (APP_BIT_CHECK(id, 0x01 << 9)) {
        sManager->sendCache1[1] |= 0x01;
    }
    sManager->sendCache1[2] = id;
    sManager->sendCache1[3] = CRC8MaxIM(sManager->sendCache1, 3, 1);
    sManager->sendCache1[4] = 0x00;
    sManager->sendCache1[5] = 0x00;

    if (sManager->echo) {
        sprintf(sManager->logCache1, "Send: ");
        for (i = 0; i < 6; i++) {
            sprintf(log, "%02X ", sManager->sendCache1[i]);
            strcat(sManager->logCache1, log);
        }
    }

    sendCMD2S();
    UartRawSendPDMA(UART_COM2, (uint32_t)sManager->sendCache1, 6);
}

static void showState(void)
{
    UartPuts(UART_COM1, "State:\r\n");
    UartPrintf(UART_COM1, "critical = %d\r\n", sManager->critical);
    UartPrintf(UART_COM1, "echo     = %d\r\n", sManager->echo);
    UartPrintf(UART_COM1, "bit8mode = %d\r\n", sManager->bit8Mode);
    UartPrintf(UART_COM1, "lowiq    = %d\r\n", sManager->lowiq);
	UartPrintf(UART_COM1, "hbaud    = %d\r\n", sManager->highBaud);
	UartPrintf(UART_COM1, "lbaud    = %d\r\n", sManager->lowBaud);
    UartPrintf(UART_COM1, "pdtime   = %d us\r\n", sManager->pdTime);
}

static void testmodeBKWBoard(uint16_t chipNum, uint8_t reg0)
{
    uint8_t flag;
    uint16_t i;

    if (sManager->echo) {
        flag = 0x01;
        sManager->echo = 0;
    }

    for (i = 0; i < chipNum; i++) {
        YX020BroadcastReg(YX020_BROAD_REG0, reg0);
        UartWaitSendDone(UART_COM2);
        delayUs(100);

        YX020WriteSelectReg(YX020_WSELECT_REG7, 0x01, i, 0);
        UartWaitSendDone(UART_COM2);
        delayUs(100);

        sendVSYNC();
        delayUs(100);

        YX020WriteSelectReg(YX020_WSELECT_REG7, 0x10, i, 0);
        UartWaitSendDone(UART_COM2);
        delayUs(100);

        YX020BroadcastReg(YX020_BROAD_REG0, reg0);
        UartWaitSendDone(UART_COM2);
        delayUs(100);
        YX020BroadcastReg(YX020_BROAD_REG0, reg0);
        UartWaitSendDone(UART_COM2);
        delayUs(100);
        YX020BroadcastReg(YX020_BROAD_REG0, reg0);
        UartWaitSendDone(UART_COM2);
        delayUs(100);

        sendVSYNC();
        delayUs(100);
    }

    if (flag) {
        sManager->echo = 1;
    }

    if (sManager->echo) {
        UartPrintfPDMA(UART_COM1, "BKWBoard: chipNum = %d, reg0 = %02X\r\n", chipNum, reg0);
    }
}

/**
 * @brief   发送初始化时序
 */
static void parInitFunc(uint8_t *data)
{
    uint8_t reg4, reg7, chipNum;
    int16_t index;
    uint16_t offset;

    uint8_t branch = 0;

    index = StrGetCHIndex((char *)data, '-');
    /* yx020 -init reg4(Hex) chipNum(Dec) */
    if (index == -1) {
        index = StrGetNextNotSpace((char *)data);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "init: No param");
            return;
        }
        offset = index;
        reg4 = StrGetHex((char *)&data[offset], 0, 4);
        offset += 2;

        index = StrGetNextNotSpace((char *)&data[offset]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "init: No chip num");
            return;
        }
        offset += index;
        chipNum = StrGetDec((char *)&data[offset], 0, 8);

        YX020InitAndSetup(reg4, chipNum, 0, 0, 1);
    }
    else {
        /* yx020 -init -testmode reg4(Hex) reg7(Hex) chipNum(Dec) */
        if (strncmp((char *)&data[index + 1], sParTestmode, 8) == 0) {
            branch = 0x01;
            offset = index + 9;
        }
        /* yx020 -init -tmno2d reg4(Hex) reg7(Hex) chipNum(Dec) */
        else if (strncmp((char *)&data[index + 1], sPar2ndTmno2d, 6) == 0) {
            branch = 0x02;
            offset = index + 7;
        }
        else {
            UartPutsPDMA(UART_COM1, "init: No match 2nd param");
            branch = 0x00;
        }

        if ((branch == 0x01) || (branch == 0x02)) {
            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "init -testmode: No param");
                return;
            }
            offset += index;
            reg4 = StrGetHex((char *)&data[offset], 0, 4);
            offset += 2;

            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "init -testmode: No reg7");
                return;
            }
            offset += index;
            reg7 = StrGetHex((char *)&data[offset], 0, 4);
            offset += 2;

            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "init -testmode: No chipNum");
            }
            chipNum = StrGetDec((char *)&data[offset], 0, 8);
        }

        if (branch == 0x01) {
            YX020InitAndSetup(reg4, chipNum, 1, reg7, 1);
        }
        else if (branch == 0x02) {
            YX020InitAndSetup(reg4, chipNum, 1, reg7, 0);
        }
    }
}

static void parFrameData(uint8_t *data, uint16_t receiveLen)
{
    uint16_t ana;
    int16_t index;

    if (receiveLen / 4 > YX020_FRAME_CACHE_SIZE) {
        UartPrintfPDMA(UART_COM1, "data: %d out of range\r\n", receiveLen);
        return;
    }

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "data: No param\r\n");
        return;
    }

	if (sManager->bit8Mode) {
		if (sManager->frame.data8 != NULL) {
			vPortFree(sManager->frame.data8);
		}
		
		sManager->frame.data8 = (uint8_t *)pvPortMalloc(receiveLen);
		if (sManager->frame.data8 == NULL) {
			UartPutsPDMA(UART_COM1, "can't get memory\r\n");
			return;
		}
		
		ana = StrChar2HexByByte((char *)&data[index], 0, receiveLen, sManager->frame.data8, receiveLen);
		sManager->frame.len = ana / 2;
	}
	else {
		ana = StrChar2Hex((char *)&data[index], 0, receiveLen, sManager->frame.data16, YX020_FRAME_CACHE_SIZE);
		sManager->frame.len = ana / 4;
	}

    if (sManager->echo) {
		if (sManager->bit8Mode) {
			for (ana = 0; ana < sManager->frame.len; ana++) {
				UartPrintfPDMA(UART_COM1, "%02X ", sManager->frame.data8[ana]);
			}
		}
		else {
			for (ana = 0; ana < sManager->frame.len; ana++) {
				UartPrintf(UART_COM1, "%04X ", sManager->frame.data16[ana]);
			}
		}
        UartPutsPDMA(UART_COM1, "\r\nsave to frame cache\r\n");
    }
}

static void parFrameRegs(uint8_t *data, uint16_t receiveLen)
{
    int16_t index;
    uint16_t i;
    uint16_t offset = 0;

    for (i = 0; i < 6; i++) {
        index = StrGetNextNotSpace((char *)&data[offset]);
        if (index == -1) {
#ifdef USER_DEBUG_LOG_EN
            UartPutsPDMA(UART_COM1, "regs: Can't read reg\r\n");
#endif
            return;
        }
        sManager->frame.regs[i] = StrGetHex((char *)&data[offset], 0, 3);
        offset = offset + index + 2;
    }

    if (sManager->echo) {
        for (i = 0; i < 5; i++) {
            UartPrintf(UART_COM1, "reg%d = %02X\r\n", i, sManager->frame.regs[i]);
        }
		UartPrintf(UART_COM1, "reg6 = %02X\r\n", sManager->frame.regs[5]);
        UartPutsPDMA(UART_COM1, "save to frame cahce\r\n");
    }
}

/**
 * @brief   发送帧时序
 */
static void parFrameFunc(uint8_t *data, uint16_t receiveLen)
{
    uint8_t regs[7];
    uint8_t offset, i, btm;
    int16_t index;
    uint16_t id, ana;
    uint8_t isF0;

    index = StrGetCHIndex((char *)data, '-');
    /* yx020 -frame id reg0 reg1 reg2 reg3 reg4 reg6 brdata */
    if (index == -1) {
        offset = 0;
        btm = 0;
    }
    else {
        /* yx020 -frame -testmode id reg0 reg1 reg2 reg3 reg4 reg7 brdata */
        if (strncmp((char *)&data[index + 1], sParTestmode, 8) == 0) {
            offset = index + 9;
            btm = 2;
        }
        /* yx020 -frame -frame0 id reg4 reg7 brdata */
        else if (strncmp((char *)&data[index + 1], sPar2ndFrame0, 6) == 0) {
            offset = index + 7;
            btm = 1;
        }
        /* yx020 -fram -broadcast reg0 reg1 reg2 reg3 reg4 brdata */
        else if (strncmp((char *)&data[index + 1], sPar2ndBroadcast, 9) == 0) {
            offset = index + 10;
            btm = 3;
        }
        /* yx020 -frame -onlybr brdata */
        else if (strncmp((char *)&data[index + 1], sPar2ndOnlybr, 6) == 0) {
            offset = index + 7;
            freeCache();
            sManager->rxCache1 = (uint16_t *)pvPortMalloc(6);
            if (sManager->rxCache1 == NULL) {
                UartPutsPDMA(UART_COM1, "onlybr: can't get memory!");
                return;
            }
            StrChar2Hex((char *)&data[offset], 1, receiveLen - offset, sManager->rxCache1, receiveLen);
            broadcastBr(sManager->rxCache1, 1);

            if (sManager->echo) {
                vTaskResume(logTaskHandle);
            }

            return;
        }
        /* yx020 -frame -regs reg0 reg1 reg2 reg3 reg4 reg6 */
        else if (strncmp((char *)&data[index + 1], sPar2ndRegs, 4) == 0) {
            parFrameRegs(&data[index + 5], receiveLen - 18);
            return;
        }
        /* yx020 -frame -data brdata */
        else if (strncmp((char *)&data[index + 1], sPar2ndData, 4) == 0) {
            parFrameData(&data[index + 5], receiveLen - 18);
            return;
        }
        /* yx020 -frame -startid id */
        else if (strncmp((char *)&data[index + 1], sPar2ndStartid, 7) == 0) {
            offset = 8 + index;
            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "startid: No param\r\n");
                return;
            }
            sManager->frame.id = StrGetHex((char *)&data[offset], 0, 4);
            if (sManager->echo) {
                UartPrintfPDMA(UART_COM1, "id = %X, save to framecache\r\n", sManager->frame.id);
            }
            return;
        }
        /* yx020 -frame -time time */
        else if (strncmp((char *)&data[index + 1], sPar2ndTime, 4) == 0) {
            offset = 5 + index;
            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "time: No param\r\n");
                return;
            }

            offset = offset + index;
            sManager->frame.newTime = StrGetDec((char *)&data[offset], 0, 6);
            if (sManager->echo) {
                UartPrintfPDMA(UART_COM1, "time: time = %d\r\n", sManager->frame.newTime);
            }

            if (xTimerIsTimerActive(timerHandle) == pdFALSE) {
                sManager->frame.time = sManager->frame.newTime;
                xTimerChangePeriod(timerHandle, sManager->frame.time, 0);
                if (eTaskGetState(frameTaskHandle) == eSuspended) {
                    vTaskResume(frameTaskHandle);
                }
                sManager->echo = 0;
            }
            return;
        }
        /* yx020 -frame -hlbaud isF0 id reg0 reg1 reg2 reg3 reg4 brdata */
        else if (strncmp((char *)&data[index + 1], sPar2ndHLBaud, 6) == 0) {
            offset = index + 7;
            isF0 = StrGetHex((char *)&data[offset], 0, 2);
            sManager->isF0 = isF0;
            offset += 1;
            btm = 4;
        }
        /* yx020 -frame -zero */
        else if (strncmp((char *)&data[index + 1], sPar2ndZero, 4) == 0) {
            sendCMD2S();

            while(1) {
                UartRawSendByte(UART_COM2, 0xA5);
            }
        }
        else {
            UartPutsPDMA(UART_COM1, "frame: No match 2nd param");
            return;
        }
    }

    freeCache();
    if (sManager->bit8Mode) {
        sManager->rxCache2 = (uint8_t *)pvPortMalloc(receiveLen);
    }
    else {
        sManager->rxCache1 = (uint16_t *)pvPortMalloc(receiveLen);
    }
    if ((sManager->rxCache1 == NULL) && (sManager->rxCache2 == NULL)) {
        UartPutsPDMA(UART_COM1, "frame: can't get memory!");
        return;
    }

    /* read ID */
    if (btm != 3) {
        index = StrGetNextNotSpace((char *)&data[offset]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "frame: No param");
            return;
        }
        offset += index;
        id = StrGetHex((char *)&data[offset], 0, 6);
        offset += 4;
    }

    /* read REGs */
    if (btm == 2) {
        for (i = 0; i < 6; i++) {
            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "frame: Can't read reg\r\n");
                return;
            }
            regs[i] = StrGetHex((char *)&data[offset], 0, 3);
            offset = offset + index +2;
        }
    }
    else if (btm == 1) {
        for (i = 4; i < 6; i++) {
            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "frame: Can't read reg\r\n");
                return;
            }
            regs[i] = StrGetHex((char *)&data[offset], 0, 3);
            offset = offset + index +2;
        }
    }
    else {
        for (i = 0; i < 6; i++) {
            index = StrGetNextNotSpace((char *)&data[offset]);
            if (index == -1) {
                UartPutsPDMA(UART_COM1, "frame: Can't read reg\r\n");
                return;
            }
            regs[i] = StrGetHex((char *)&data[offset], 0, 3);
            offset = offset + index + 2;
        }
    }

    /* read brdata */
    if (sManager->bit8Mode) {
        ana = StrChar2HexByByte((char *)&data[offset], 1, receiveLen - offset, sManager->rxCache2, receiveLen);
        if (btm == 3) {
            YX020BroadcastFrame(regs, (uint16_t *)sManager->rxCache2, 0, 0);
        }
        else {
            YX020Frame(regs, (uint16_t *)sManager->rxCache2, ana / 2, id, 0, btm);
        }
    }
    else {
        ana = StrChar2Hex((char *)&data[offset], 1, receiveLen - offset, sManager->rxCache1, receiveLen);
        if (btm == 3) {
            YX020BroadcastFrame(regs, sManager->rxCache1, 0, 0);
        }
        else if (btm == 4) {
            UartPutsPDMA(UART_COM1, "HLBaud ");
            YX020HLBuadFrame(regs, sManager->rxCache1, ana / 4, id, 0, btm, sManager->isF0);
        }
        else {
            YX020Frame(regs, sManager->rxCache1, ana / 4, id, 0, btm);
        }
    }

    if (sManager->echo) {
        vTaskResume(logTaskHandle);
    }
}

/**
 * @brief   广播写寄存器
 */
static void parBrregFunc(uint8_t *data)
{
    int16_t index;
    uint16_t reg, val;
    YX020_BROAD_REG_E regEnum;

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "brreg: No param\r\n");
        return;
    }

    reg = StrGetHex((char *)&data[index], 0, 4);
    switch (reg) {
    case 0:
        regEnum = YX020_BROAD_REG0;
        break;
    case 1:
        regEnum = YX020_BROAD_REG1;
        break;
    case 2:
        regEnum = YX020_BROAD_REG2;
        break;
    case 3:
        regEnum = YX020_BROAD_REG3;
        break;
    case 4:
        regEnum = YX020_BROAD_REG4;
        break;
    case 5:
        regEnum = YX020_BROAD_REG5;
        break;
    case 6:
        regEnum = YX020_BROAD_REG6;
        break;
    case 7:
        regEnum = YX020_BROAD_REG7;
        break;
    default:
        UartPutsPDMA(UART_COM1, "brreg: Invalid reg\r\n");
        return;
    }

    val = StrGetHex((char *)&data[index+2], 0, 4);
    YX020BroadcastReg(regEnum, val);
    if (sManager->echo) {
        vTaskResume(logTaskHandle);
    }
}

/**
 * @brief   指定写寄存器
 */
static void parSeregFun(uint8_t *data)
{
    int16_t index;
    uint16_t reg, val, id;
    YX020_WSELECT_REG_E regEnum;

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "sereg: No param\r\n");
        return;
    }
    id = StrGetHex((char *)&data[index], 0, 6);
    reg = StrGetHex((char *)&data[index+4], 0, 4);
    switch (reg) {
    case 0:
        regEnum = YX020_WSELECT_REG0;
        break;
    case 1:
        regEnum = YX020_WSELECT_REG1;
        break;
    case 2:
        regEnum = YX020_WSELECT_REG2;
        break;
    case 3:
        regEnum = YX020_WSELECT_REG3;
        break;
    case 4:
        regEnum = YX020_WSELECT_REG4;
        break;
    case 5:
        regEnum = YX020_WSELECT_REG5;
        break;
    case 6:
        regEnum = YX020_WSELECT_REG6;
        break;
    case 7:
        regEnum = YX020_WSELECT_REG7;
        break;
    default:
        UartPutsPDMA(UART_COM1, "sereg: Invalid reg\r\n");
        return;
    }
    val = StrGetHex((char *)&data[index+8], 0, 4);
    YX020WriteSelectReg(regEnum, val, id, 1);
    if (sManager->echo) {
        vTaskResume(logTaskHandle);
    }
}

static void parReadregFun(uint8_t *data)
{
    int16_t index;
    uint16_t id;
    uint8_t reg;

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "readreg: No param\r\n");
    }
    id = StrGetHex((char *)&data[index], 0, 6);
    reg = StrGetHex((char *)&data[index+4], 0, 4);
    readReg(id, reg);

    if (sManager->echo) {
        vTaskResume(logTaskHandle);
    }
}

static void parTestmodeFun(uint8_t *data)
{
    uint8_t flag, i;
    uint8_t regs[] = {0x00, 0x00, 0x00, 0x00, 0x00};
    uint16_t br[] = {0xFFFF, 0xFFFF, 0xFFFF};
    uint16_t id;
    uint16_t chipNum;
    int16_t index, index1;

    index = StrGetCHIndex((char *)data, '-');
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "testmode: No param\r\n");
        return;
    }

    /* yx020 -testmode -enter6 id: 从 TX 进入测试模式 */
    if (strncmp((char *)&data[index+1], sPar2ndEnter6, 6) == 0) {
        if (sManager->echo) {
            sManager->echo = 0;
            flag = 0x01;
        }

        id = StrGetHex((char *)&data[index+7], 0, 6);

        UartRawSendByte(UART_COM2, 0xF5);
        UartWaitSendDone(UART_COM2);
        delayUs(10 + DELAY_OFFSET);
        UartRawSendByte(UART_COM2, 0x79);
        UartWaitSendDone(UART_COM2);
        delayUs(10 + DELAY_OFFSET);
        YX020BroadcastReg(YX020_BROAD_REG7, 0xCC);
        YX020BroadcastReg(YX020_BROAD_REG4, 0xC7);
        UartWaitSendDone(UART_COM2);
        delayUs(40 + DELAY_OFFSET);
		sendCMD2S();
        sendSetID(1);
        delayUs(SET_ID_DELAY + DELAY_OFFSET);

        YX020BroadcastReg(YX020_BROAD_REG0, 0x23);
        YX020BroadcastReg(YX020_BROAD_REG1, 0xFF);
        YX020BroadcastReg(YX020_BROAD_REG2, 0xFF);
        YX020BroadcastReg(YX020_BROAD_REG3, 0xFF);
        sendBrightness(id, br, 3);
        sendCMD2D();
        sendVSYNC();

        br[0] = 0x00;
        br[1] = 0x00;
        br[2] = 0x00;
        sendBrightness(id, br, 3);

        sendCMD2S();
        YX020BroadcastReg(YX020_BROAD_REG6, 0x00);
        YX020BroadcastReg(YX020_BROAD_REG7, 0x06);

        if (APP_BIT_CHECK(flag, 0x01)) {
            sManager->echo = 1;
        }

        UartPutsPDMA(UART_COM1, "testmode: Done\r\n");
    }
	/* yx020 -testmode -enter id: 进入测试模式 */
	else if (strncmp((char *)&data[index+1], sPar2ndEnter, 5) == 0) {
        if (sManager->echo) {
            sManager->echo = 0;
            flag = 0x01;
        }

        id = StrGetHex((char *)&data[index+6], 0, 6);

        UartRawSendByte(UART_COM2, 0xF5);
        UartWaitSendDone(UART_COM2);
        delayUs(10 + DELAY_OFFSET);
        UartRawSendByte(UART_COM2, 0x79);
        UartWaitSendDone(UART_COM2);
        delayUs(10 + DELAY_OFFSET);
        YX020BroadcastReg(YX020_BROAD_REG7, 0xCC);
        YX020BroadcastReg(YX020_BROAD_REG4, 0x0F);
        UartWaitSendDone(UART_COM2);
        delayUs(40 + DELAY_OFFSET);
        sendSetID(1);
        delayUs(SET_ID_DELAY + DELAY_OFFSET);

        YX020BroadcastReg(YX020_BROAD_REG0, 0x23);
        YX020BroadcastReg(YX020_BROAD_REG1, 0xFF);
        YX020BroadcastReg(YX020_BROAD_REG2, 0xFF);
        YX020BroadcastReg(YX020_BROAD_REG3, 0xFF);
        sendBrightness(id, br, 3);
        sendCMD2D();
        sendVSYNC();

        br[0] = 0x00;
        br[1] = 0x00;
        br[2] = 0x00;
        sendBrightness(id, br, 3);

        sendCMD2S();
        YX020BroadcastReg(YX020_BROAD_REG6, 0x00);
        YX020BroadcastReg(YX020_BROAD_REG7, 0x01);
        YX020BroadcastReg(YX020_BROAD_REG0, 0x33);

        if (APP_BIT_CHECK(flag, 0x01)) {
            sManager->echo = 1;
        }

        UartPutsPDMA(UART_COM1, "testmode: Done\r\n");
    }
    /* yx020 -testmode -preview id reg0 reg1 reg2 reg3 reg4 preview[15:0] preview[31:16] br2[15:0] */
    else if (strncmp((char *)&data[index+1], sPar2ndPreview, 7) == 0) {
        /* Get id */
        index = StrGetNextNotSpace((char *)&data[index+8]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "preview: Can't read id\r\n");
            return;
        }
        id = StrGetHex((char *)&data[8+index], 0, 5);
        /* Get reg0-4 */
        index += 4;
        for (i = 0; i < 5; i++) {
            index1 = StrGetNextNotSpace((char *)&data[8+index]);
            if (index1 == -1) {
                UartPutsPDMA(UART_COM1, "preview: Can't read regs\r\n");
                return;
            }
            regs[i] = StrGetHex((char *)&data[8+index+index1], 0, 3);
            index = index + index1 + 2;
        }
        /* Get preview data */
        StrChar2Hex((char *)&data[8+index], 0, 16, br, 3);
        YX020Frame(regs, br, 3, id, 1, 0);

        if (sManager->echo) {
            vTaskResume(logTaskHandle);
        }
    }
    /* yx020 -testmode -bkw10 chipNum reg0 */
    else if (strncmp((char *)&data[index + 1], sPar2ndBkw10, 5) == 0) {
        index = StrGetNextNotSpace((char *)&data[index + 6]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "bkw10: Can't read chipNum\r\n");
            return;
        }
        chipNum = StrGetDec((char *)&data[6 + index], 0, 6);
        index += 2;

        index1 = StrGetNextNotSpace((char *)&data[index + 6]);
        if (index1 == -1) {
            UartPutsPDMA(UART_COM1, "bkw10: Can't read reg0\r\n");
            return;
        }
        regs[0] = StrGetHex((char *)&data[6 + index + index1], 0, 4);
        testmodeBKWBoard(chipNum, regs[0]);
    }
    /* yx020 -testmode -OSC */
    else if (strncmp((char *)&data[index + 1], sPar2ndTestOSC, 3) == 0) {
        UartRawSendByte(UART_COM2, 0xF5);
        UartWaitSendDone(UART_COM2);
        delayUs(10 + DELAY_OFFSET);
        UartRawSendByte(UART_COM2, 0x79);
        UartWaitSendDone(UART_COM2);
        delayUs(10 + DELAY_OFFSET);
        YX020BroadcastReg(YX020_BROAD_REG7, 0xCC);
        YX020BroadcastReg(YX020_BROAD_REG4, 0x0F);
        UartWaitSendDone(UART_COM2);
        delayUs(40 + DELAY_OFFSET);
        sendSetID(1);
        delayUs(SET_ID_DELAY + DELAY_OFFSET);

        YX020BroadcastReg(YX020_BROAD_REG0, 0x23);
        YX020BroadcastReg(YX020_BROAD_REG6, 0x00);
        YX020BroadcastReg(YX020_BROAD_REG7, 0x03);
    }
    else {
        UartPutsPDMA(UART_COM1, "testmode: No match 2nd param\r\n");
    }
}

/**
 * @brief   Burn chip
 * @details command : yx020 -trim -burn Fuse[15:0] Fuse[31:16] burnChipNum
 */
static void parTrimBurnFunc(uint8_t *data)
{
    uint8_t flag;
    int16_t index;
    uint16_t br[3];
    uint8_t t;
    uint8_t reg4 = 0;
    uint32_t BurnChipNum;

    if (sManager->echo) {
        flag = 0x01;
        sManager->echo = 0;
    }

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "trim: No param\r\n");
        return;
    }

    br[0] = StrGetHex((char *)&data[index], 0, 6);		/* Efuse[15:0] */
    br[1] = StrGetHex((char *)&data[index+5], 0, 6);	/* Efuse[31:16] */
    br[2] = 0x0000;
    BurnChipNum = StrGetDec((char *)&data[index+10], 0, 4);
    if (BurnChipNum == 0) {
        BurnChipNum = 1;
    }
    for (int i=0 ; i< BurnChipNum ; i++) {
        UartRawSendByte(UART_COM2, 0xF5);
        UartWaitSendDone(UART_COM2);
        delayUs(30 - DELAY_OFFSET);
        UartRawSendByte(UART_COM2, 0x79);
        UartWaitSendDone(UART_COM2);
        delayUs(30 - DELAY_OFFSET);
        YX020BroadcastReg(YX020_BROAD_REG7, 0xCC);

        if ((br[1] & ABB_VERSION) == ABB_VERSION) {
            t = (br[1] & CHIP_IDX_MSK) >> 8;
            reg4 = t;
            reg4 |= t << 3;
        }
        delayUs(10- DELAY_OFFSET);
        YX020BroadcastReg(YX020_BROAD_REG4, 0x1B);
        UartWaitSendDone(UART_COM2);
        delayUs(50- DELAY_OFFSET);
    }
    delayUs(20 - DELAY_OFFSET);
    sendCMD2S();
    delayUs(100 - DELAY_OFFSET);
    sendSetID(BurnChipNum);
    delayUs(20 - DELAY_OFFSET);
    sendCMD2D();
    for(int id = BurnChipNum - 1; id >= 0; id--) {
        delayUs(10);
        sendBrightness(id, br, 3);
        UartWaitSendDone(UART_COM2);
        delayUs(10);
        YX020WriteSelectReg(YX020_WSELECT_REG7, 0x01, id, 0);
        UartWaitSendDone(UART_COM2);
        delayUs(10);
        YX020WriteSelectReg(YX020_WSELECT_REG7, 0x02, id, 0);
        UartWaitSendDone(UART_COM2);
        delayMs(10);
    }

    if (flag) {
        sManager->echo = 1;
    }
}

static void parTrimBurnReadFunc(uint8_t *data)
{
    int16_t index;
    uint16_t id;
    uint8_t reg4;

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "trim -read: No param\r\n");
        return;
    }
    id = StrGetHex((char *)&data[index], 0, 6);
    reg4 = StrGetHex((char *)&data[index+5], 0, 4);

    UartRawSendByte(UART_COM2, 0xF5);
    UartWaitSendDone(UART_COM2);
    delayUs(10 - DELAY_OFFSET);
    UartRawSendByte(UART_COM2, 0x79);
    UartWaitSendDone(UART_COM2);
    YX020BroadcastReg(YX020_BROAD_REG4, reg4);
    UartWaitSendDone(UART_COM2);
    delayUs(10 - DELAY_OFFSET);
    sendCMD2S();
    UartRawSend(UART_COM2, sManager->setIDSeq, sManager->setIDLen);
    YX020WriteSelectReg(YX020_WSELECT_REG6, 0x10, 0x0000, 1);
    // readReg(0x0000, 7);
    // vTaskDelay(10);
    // readReg(0x0000, 8);
    // vTaskDelay(10);
    // readReg(0x0000, 9);
    // vTaskDelay(10);
    readReg(id, 10);
}

static void parTrimFunc(uint8_t *data)
{
    uint8_t flag;
    int16_t index;

    if (sManager->echo) {
        flag = 0x01;
        sManager->echo = 0;
    }

    index = StrGetCHIndex((char *)data, '-');
    if (strncmp((char *)&data[index+1], sPar2ndBurn, 4) == 0) {
        parTrimBurnFunc(&data[index+5]);
        UartPutsPDMA(UART_COM1, "trim -burn: Done\r\n");
    }
    else if (strncmp((char *)&data[index+1], sPar2ndRead, 4) == 0) {
        parTrimBurnReadFunc(&data[index]);
        UartPutsPDMA(UART_COM1, "trim -read: Done\r\n");
    }

    if (flag) {
        sManager->echo = 1;
    }
}
/**
 * @brief   Set high and low baudrate
 * @details Baud and set uart2 baud -> low baud
 */
static void parCacheBaud(uint8_t *data)
{
    int16_t index;
    uint32_t highBaud, lowBaud;

    index = StrGetNextNotSpace((char *)data);
    if (index == -1) {
        UartPutsPDMA(UART_COM1, "sereg: No param\r\n");
        return;
    }
    highBaud = StrGetDec((char *)&data[index], 0, 8);
    lowBaud = StrGetDec((char *)&data[index+8], 0, 9);
    sManager->highBaud = highBaud;
    sManager->lowBaud = lowBaud;
    UartSetBaud(UART_COM2, sManager->lowBaud);
}

static void cmdYX020Callback(CmdParamTypeDef *param)
{
    int16_t index;
    uint16_t t1, t2;
    uint8_t *receiveArray = UartGetRxBuffer(UART_COM1);

    if (param == NULL) {
        taskENTER_CRITICAL();
        showState();
        taskEXIT_CRITICAL();

        return;
    }

    if (strcmp(param->name, sParInit) == 0) {
        parInitFunc(&receiveArray[param->index]);
    }
    else if (strcmp(param->name, sParFrame) == 0) {
        parFrameFunc(&receiveArray[param->index], UartGetReceiveLen(UART_COM1));
    }
    else if (strcmp(param->name, sParSetid) == 0) {
        index = StrGetNextNotSpace((char *)&receiveArray[param->index]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "setid: No param\r\n");
        }
        else {
            t1 = StrGetHex((char *)&receiveArray[param->index], 0, 6);
            sendSetID(t1);
            UartPrintfPDMA(UART_COM1, "SetID has been sent %04X times\r\n", t1);
        }
    }
    else if (strcmp(param->name, sParCritical) == 0) {
        index = StrGetNextNotSpace((char *)&receiveArray[param->index]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "critical: No param\r\n");
            return;
        }

        if (strncmp((char *)&receiveArray[param->index+index], strEn, 2) == 0) {
            sManager->critical = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+index], strUnen, 4) == 0) {
            sManager->critical = 0;
        }
        else {
            UartPutsPDMA(UART_COM1, "critical: No match param\r\n");
        }
    }
    else if (strcmp(param->name, sParEcho) == 0) {
        index = StrGetNextNotSpace((char *)&receiveArray[param->index]);
        if (index == -1) {
            UartPutsPDMA(UART_COM1, "echo: No param\r\n");
            return;
        }

        if (strncmp((char *)&receiveArray[param->index+index], strEn, 2) == 0) {
            sManager->echo = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+index], strUnen, 4) == 0) {
            sManager->echo = 0;
        }
        else {
            UartPutsPDMA(UART_COM1, "echo: No match param\r\n");
        }
    }
    else if (strcmp(param->name, sPar8bitmode) == 0) {
        index = StrGetNextNotSpace((char *)&receiveArray[param->index]);
        if (strncmp((char *)&receiveArray[param->index + index], strEn, 2) == 0) {
            sManager->bit8Mode = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index + index], strUnen, 4) == 0) {
            sManager->bit8Mode = 0;
        }
        else {
            UartPutsPDMA(UART_COM1, "8bitmode: No match param\r\n");
        }
    }
    else if (strcmp(param->name, sParLowiq) == 0) {
        index = StrGetNextNotSpace((char *)&receiveArray[param->index]);
        if (strncmp((char *)&receiveArray[param->index + index], strEn, 2) == 0) {
            sManager->lowiq = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index + index], strUnen, 4) == 0) {
            sManager->lowiq = 0;
        }
        else {
            UartPutsPDMA(UART_COM1, "lowiq: No match param\r\n");
        }
    }
    else if (strcmp(param->name, sParPdtime) == 0) {
        index = StrGetNextNotSpace((char *)&receiveArray[param->index]);
        sManager->pdTime = StrGetDec((char *)&receiveArray[param->index + index], 0, 8);
    }
    else if (strcmp(param->name, sParBrreg) == 0) {
        parBrregFunc(&receiveArray[param->index]);
    }
    else if (strcmp(param->name, sParSereg) == 0) {
        parSeregFun(&receiveArray[param->index]);
    }
    else if (strcmp(param->name, sParReadreg) == 0) {
        parReadregFun(&receiveArray[param->index]);
    }
    else if (strcmp(param->name, sParTestmode) == 0) {
        parTestmodeFun(&receiveArray[param->index]);
    }
    else if (strcmp(param->name, sParVsync) == 0) {
        /* yx020 -vsync num */
        t1 = StrGetDec((char *)&receiveArray[param->index+1], 0, 6);
        for (t2 = 0; t2 < t1; t2++) {
            sendVSYNC();
        }
    }
    else if (strcmp(param->name, sParTrim) == 0) {
        parTrimFunc(&receiveArray[param->index]);
    }
    else if (strcmp(param->name, sParCacheBaud) == 0 ) {
        parCacheBaud(&receiveArray[param->index]);
    }
    else {
        freeCache();
    }
}

void YX020AppInit(void)
{
    static struct YX020ManagerType manager;
    static uint8_t cache1[CACHE1_SIZE];
    static char logCache1[YX020_LOG_CACHE_SIZE];
    static char logCache2[YX020_LOG_CACHE_SIZE * 4];
    static char logCahce3[YX020_LOG_CACHE_SIZE];
    static uint16_t frameCache[YX020_FRAME_CACHE_SIZE];
    static uint8_t const cmd2sSeq[] = {0x55, 0xC0, 0xAA, 0x2B};
    static uint8_t const setIDSeq[] = {0x55, 0x20, 0x00, 0x8F, 0x00, 0x00, 0x00};
    static uint8_t const vsyncSeq[] = {0x55, 0x40, 0x00, 0xD5};
    static uint8_t const cmd2dSeq[] = {0x55, 0xC0, 0x55, 0x1E};
    static uint8_t const setbuadSeq[] = {0x55, 0xC0, 0x0A, 0x84};

    uint16_t i;

    for (i = 0; i < CACHE1_SIZE; i++) {
        cache1[i] = 0;
    }
    for (i = 0; i < YX020_LOG_CACHE_SIZE; i++) {
        logCache1[i] = 0;
        logCache2[i] = 0;
        logCahce3[i] = 0;
    }
    manager.lowBaud=1000000;
    manager.highBaud=1000000;

    manager.critical = 0;
    manager.disableIRQ = 0;
    manager.echo = 1;
    manager.bit8Mode = 0;
    manager.lowiq = 0;

    manager.chipNum = 0;
    manager.magin = 1.2;
    manager.delayCMD2D = 50;

    manager.pdTime = 100;
    manager.sendCache1 = cache1;
    manager.sendCache2 = NULL;
    manager.rxCache1 = NULL;
    manager.rxCache2 = NULL;
    manager.logCache1 = logCache1;
    manager.logCache2 = logCache2;
    manager.logCache3 = logCahce3;

    manager.cmd2sSeq = cmd2sSeq;
    manager.cmd2sLen = sizeof(cmd2sSeq) / sizeof(cmd2sSeq[0]);
    manager.setIDSeq = setIDSeq;
    manager.setIDLen = sizeof(setIDSeq) / sizeof(setIDSeq[0]);
    manager.vsyncSeq = vsyncSeq;
    manager.vsyncLen = sizeof(vsyncSeq) / sizeof(vsyncSeq[0]);
    manager.cmd2dSeq = cmd2dSeq;
    manager.cmd2dLen = sizeof(cmd2dSeq) / sizeof(cmd2dSeq[0]);

    manager.setBaudSql = setbuadSeq;
    manager.setBaudLen = sizeof(setbuadSeq) / sizeof(setbuadSeq[0]);
    for (i = 0; i < 5; i++) {
        manager.frame.regs[i] = 0;
    }
    manager.frame.len = 0;
    manager.frame.id = 0;
    manager.frame.time = 100;
    manager.frame.newTime = 100;
	manager.frame.data16 = frameCache;
	manager.frame.data8 = NULL;
    sManager = &manager;

    CmdAppend(sCmdYx020, cmdYX020Callback);
    CmdAppendParam(sCmdYx020, sParInit);
    CmdAppendParam(sCmdYx020, sParFrame);
    CmdAppendParam(sCmdYx020, sParSetid);
    CmdAppendParam(sCmdYx020, sParCritical);
    CmdAppendParam(sCmdYx020, sParEcho);
    CmdAppendParam(sCmdYx020, sParFree);
    CmdAppendParam(sCmdYx020, sParBrreg);
    CmdAppendParam(sCmdYx020, sParSereg);
    CmdAppendParam(sCmdYx020, sParReadreg);
    CmdAppendParam(sCmdYx020, sParTestmode);
    CmdAppendParam(sCmdYx020, sParVsync);
    CmdAppendParam(sCmdYx020, sParTrim);
    CmdAppendParam(sCmdYx020, sPar8bitmode);
    CmdAppendParam(sCmdYx020, sParLowiq);
    CmdAppendParam(sCmdYx020, sParPdtime);
    CmdAppendParam(sCmdYx020, sParCacheBaud);
    UartCmdRegisterBaudrateChangeCb(baudCallback);
    if (eventGroup == NULL) {
        eventGroup = xEventGroupCreate();
    }
}

void YX020CreateTask(uint8_t priorityLog, uint8_t priorityFrame)
{
    BaseType_t result = pdPASS;

    result = xTaskCreate(logTask, "listenTask", 256, NULL, priorityLog, &logTaskHandle);
    if (result != pdPASS) {
        UartPutsPDMA(UART_COM1, "listenTask create fail\r\n");
    }
    else {
        vTaskSuspend(logTaskHandle);
    }

    result = xTaskCreate(frameTask, "frameTask", 256, NULL, priorityFrame, &frameTaskHandle);
    if (result != pdPASS) {
        UartPutsPDMA(UART_COM1, "frameTask create fail\r\n");
    }
    else {
        vTaskSuspend(frameTaskHandle);
    }

    timerHandle = xTimerCreate("020TimerTask", sManager->frame.time, pdTRUE, (void *)2, timerCallback);
    xTimerStop(timerHandle, 0);
}

/**
 * @brief		广播写寄存器
 * @param[in]	reg 广播寄存器枚举值 @ref: YX020_BROAD_REG_E
 * @param[in]	val 寄存器值
 */
void YX020BroadcastReg(YX020_BROAD_REG_E reg, uint8_t val)
{
    uint8_t i;
    char log[8];

    sManager->sendCache1[0] = 0x55;
    sManager->sendCache1[1] = reg;
    sManager->sendCache1[2] = 0xFF;
    sManager->sendCache1[3] = val;
    sManager->sendCache1[4] = CRC8MaxIM(sManager->sendCache1, 4, 1);
    UartRawSend(UART_COM2, sManager->sendCache1, 5);

    if (sManager->echo) {
        strcat(sManager->logCache1, "Broadcast REG: ");
        for (i = 0; i < 5; i++) {
            sprintf(log, "%02X ", sManager->sendCache1[i]);
            strcat(sManager->logCache1, log);
        }
        strcat(sManager->logCache1, "\r\n");
    }
}

/**
 * @brief	广播写帧
 * @param	regList 包含寄存器 0-4 值的数组，如果在板级测试模式下，则包含 0-4,7 的值
 * @param	brData  亮度数据
 * @param	tm		是否在测试模式下
 * @param   btm     0 = 不在板级测试，1 = 板级测试 Frame0，2 = 板级测试 Frame1
 */
void YX020BroadcastFrame(const uint8_t *regList, const uint16_t *brData, uint8_t tm, uint8_t btm)
{
    uint16_t ms;
    uint16_t us;
    uint8_t i;
    char log[8];

    uint8_t *br = NULL;

    if (sManager->critical) {
        taskENTER_CRITICAL();
    }

    if (sManager->echo) {
        sprintf(sManager->logCache1, "BroadcastFrame:\r\n");
    }

    sendFrameHead(regList[4], 0, 0);
    delayUs(FRAME_HEAD_DELAY + DELAY_OFFSET);
    sendSetID(1);
    delayUs(SET_ID_DELAY + DELAY_OFFSET);
    if (sManager->echo) {
        strcat(sManager->logCache1, "CMD2S sent 1 times\r\n");
        strcat(sManager->logCache1, "SetID sent 1 times\r\n");
    }

    if (btm == 1) {
        YX020BroadcastReg(YX020_BROAD_REG7, regList[5]);
    }
    else if (btm == 2) {
        YX020BroadcastReg(YX020_BROAD_REG0, regList[0]);
        YX020BroadcastReg(YX020_BROAD_REG1, regList[1]);
        YX020BroadcastReg(YX020_BROAD_REG2, regList[2]);
        YX020BroadcastReg(YX020_BROAD_REG3, regList[3]);
        YX020BroadcastReg(YX020_BROAD_REG7, regList[5]);
    }
    else {
        YX020BroadcastReg(YX020_BROAD_REG0, regList[0]);
        YX020BroadcastReg(YX020_BROAD_REG1, regList[1]);
        YX020BroadcastReg(YX020_BROAD_REG2, regList[2]);
        YX020BroadcastReg(YX020_BROAD_REG3, regList[3]);
    }

    sManager->sendCache1[0] = 0x55;
    sManager->sendCache1[1] = 0x81;
    sManager->sendCache1[2] = 0xFF;
    if (sManager->bit8Mode) {
        br = (uint8_t *)brData;
        sManager->sendCache1[3] = br[0];
        sManager->sendCache1[4] = br[1];
        sManager->sendCache1[5] = br[2];
        sManager->sendCache1[6] = CRC8MaxIM(sManager->sendCache1, 6, 1);
        UartRawSend(UART_COM2, sManager->sendCache1, 6);
        if (sManager->echo) {
            strcat(sManager->logCache2, "Send brigtness:\r\n");
            for (i = 0; i < 6; i++) {
                sprintf(log, "%02X ", sManager->sendCache1[i]);
                strcat(sManager->logCache2, log);
            }
            strcat(sManager->logCache2, "\r\n");
        }
    }
    else {
        sManager->sendCache1[3] = brData[0] >> 8;
        sManager->sendCache1[4] = brData[0];
        sManager->sendCache1[5] = brData[1] >> 8;
        sManager->sendCache1[6] = brData[1];
        sManager->sendCache1[7] = brData[2] >> 8;
        sManager->sendCache1[8] = brData[2];
        sManager->sendCache1[9] = CRC8MaxIM(sManager->sendCache1, 9, 1);
        UartRawSend(UART_COM2, sManager->sendCache1, 10);
        if (sManager->echo) {
            strcat(sManager->logCache2, "Send brigtness:\r\n");
            for (i = 0; i < 9; i++) {
                sprintf(log, "%02X ", sManager->sendCache1[i]);
                strcat(sManager->logCache2, log);
            }
            strcat(sManager->logCache2, "\r\n");
        }
    }
    sendCMD2D();
    delayUs(FRAME_VSYNC_DELAY + DELAY_OFFSET);

    if (tm) {
        if (sManager->echo) {
            strcat(sManager->logCache2, "CMD2D has been sent\r\n");
        }
    }
    else {
        sendVSYNC();
        if (sManager->lowiq) {
            UartIOToCore(UART_COM2, 0);
            UartTxPullDown(UART_COM2);
            if (sManager->pdTime > 1000) {
                ms = sManager->pdTime / 1000;
                us = sManager->pdTime % 1000;
                vTaskDelay(ms);
                delayUs(us);
            }
            else {
                delayUs(sManager->pdTime);
            }
            UartIOToPeripheral(UART_COM2, 0);
        }
        if (sManager->echo) {
            strcat(sManager->logCache2, "CMD2D has been sent\r\n");
            strcat(sManager->logCache2, "VSYNC has been sent\r\n");
        }
    }

    if (sManager->critical) {
        taskEXIT_CRITICAL();
    }
}

/**
 * @brief		写指定 ID 的寄存器
 * @param[in]	reg 寄存器枚举值 @ref: YX020_WSELECT_REG_E
 * @param[in]	val 寄存器值
 * @param[in]	id  ID
 */
void YX020WriteSelectReg(YX020_WSELECT_REG_E reg, uint8_t val, uint16_t id, uint8_t cmd2s)
{
    uint8_t i;
    char log[8];

    sManager->sendCache1[0] = 0x55;
    sManager->sendCache1[1] = reg;
    if (APP_BIT_CHECK(id, (0x01 << 9))) {
        sManager->sendCache1[1] |= 0x01;
    }
    sManager->sendCache1[2] = id;
    sManager->sendCache1[3] = val;
    sManager->sendCache1[4] = CRC8MaxIM(sManager->sendCache1, 4, 1);
    if (cmd2s) {
        sendCMD2S();
    }
    UartRawSend(UART_COM2, sManager->sendCache1, 5);

    if (sManager->echo) {
        strcat(sManager->logCache1, "Write select REG: ");
        for (i = 0; i < 5; i++) {
            sprintf(log, "%02X ", sManager->sendCache1[i]);
            strcat(sManager->logCache1, log);
        }
        strcat(sManager->logCache1, "\r\n");
    }
}

/**
 * @brief		初始化和设置时序
 * @param[in]	reg4    寄存器 4 的值
 * @param[in]	chipNum 芯片的级联数量
 * @param[in]   btm     是否是板级测试
 * @param[in]   reg7    如果是板级测试，则传入 REG7 的值，否则可以传入任意值
 * @param[in]   cmd2d   是否跟随 CMD2D
 */
void YX020InitAndSetup(uint8_t reg4, uint16_t chipNum, uint8_t btm, uint8_t reg7, uint8_t cmd2d)
{
    uint16_t i;
    char log[64];

    if (sManager->critical)
        taskENTER_CRITICAL();

    sManager->chipNum = chipNum;
    calDelayCMD2D();
    if (sManager->echo) {
        sprintf(sManager->logCache1, "Init and setup:\r\n");
    }

    if (btm) {
        for (i = 0; i < chipNum; i++) {
            sendFrameHead(reg4, reg7, 1);
            delayUs(INIT_FRAGE_DELAY + DELAY_OFFSET);
        }
    }
    else {
        for (i = 0; i < chipNum; i++) {
            sendFrameHead(reg4, 0, 0);
            delayUs(INIT_FRAGE_DELAY + DELAY_OFFSET);
        }
    }

    delayUs(50 + DELAY_OFFSET);
    sendSetID(chipNum);
    delayUs(100 + DELAY_OFFSET);
    if (cmd2d) {
        sendCMD2D();
    }

    if (sManager->echo) {
        strcat(sManager->logCache1, "CMD2S has been sent 1 times\r\n");
        sprintf(log, "SetID has been sent %d times\r\n", chipNum);
        strcat(sManager->logCache1, log);
        if (cmd2d) {
            strcat(sManager->logCache1, "CMD2D has been sent 1 times\r\n");
        }
        UartPutsPDMA(UART_COM1, sManager->logCache1);
    }

    if (sManager->critical)
        taskEXIT_CRITICAL();
}

/**
 * @brief		发送帧时序
 * @param[in]	regList 包含寄存器 0-4 值的数组，如果在板级测试模式下，则包含 0-4,7 的值
 * @param[in]	brData  亮度数据
 * @param[in]	brLen   亮度数据长度
 * @param[in]   startID 起始 ID
 * @param[in]	tm		是否在测试模式下
 * @param[in]   btm     0 = 不在板级测试，1 = 板级测试 Frame0，2 = 板级测试 Frame1
 */
void YX020Frame(const uint8_t *regList, const uint16_t *brData, uint16_t brLen, uint16_t startID, uint8_t tm, uint8_t btm)
{
    uint16_t ms;
    uint16_t us;

    if (sManager->critical) {
        taskENTER_CRITICAL();
    }

    if (sManager->echo) {
        sprintf(sManager->logCache1, "Frame:\r\n");
    }

    sendFrameHead(regList[4], 0, 0);
    delayUs(FRAME_HEAD_DELAY + DELAY_OFFSET);
    sendSetID(1);
    delayUs(20);
    sendCMD2D();
    delayUs(200);

    if (sManager->echo) {
        strcat(sManager->logCache1, "CMD2S sent 1 times\r\n");
        strcat(sManager->logCache1, "SetID sent 1 times\r\n");
    }

    if (btm == 1) {
        YX020BroadcastReg(YX020_BROAD_REG7, regList[5]);
    }
    else if (btm == 2) {
        YX020BroadcastReg(YX020_BROAD_REG0, regList[0]);
        YX020BroadcastReg(YX020_BROAD_REG1, regList[1]);
        YX020BroadcastReg(YX020_BROAD_REG2, regList[2]);
        YX020BroadcastReg(YX020_BROAD_REG3, regList[3]);
        YX020BroadcastReg(YX020_BROAD_REG7, regList[5]);
    }
    else {
        YX020BroadcastReg(YX020_BROAD_REG0, regList[0]);
        YX020BroadcastReg(YX020_BROAD_REG1, regList[1]);
        YX020BroadcastReg(YX020_BROAD_REG2, regList[2]);
        YX020BroadcastReg(YX020_BROAD_REG3, regList[3]);
		YX020BroadcastReg(YX020_BROAD_REG6, regList[5]);
    }

    if (sManager->bit8Mode) {
        sendBrightness8bit(startID, (uint8_t *)brData, brLen);
    }
    else {
        sendBrightness(startID, brData, brLen);
    }

    if (tm) {
        if (sManager->echo) {
            strcat(sManager->logCache3, "CMD2D has been sent\r\n");
        }
    }
    else {
        sendVSYNC();
        if (sManager->lowiq) {
            UartIOToCore(UART_COM2, 0);
            UartTxPullDown(UART_COM2);
            if (sManager->pdTime > 1000) {
                ms = sManager->pdTime / 1000;
                us = sManager->pdTime % 1000;
                vTaskDelay(ms);
                delayUs(us);
            }
            else {
                delayUs(sManager->pdTime);
            }
            UartIOToPeripheral(UART_COM2, 0);
        }
        if (sManager->echo) {
            strcat(sManager->logCache3, "CMD2D has been sent\r\n");
            strcat(sManager->logCache3, "VSYNC has been sent\r\n");
        }
    }

    if (sManager->critical) {
        taskEXIT_CRITICAL();
    }
}

void YX020HLBuadFrame(const uint8_t *regList, const uint16_t *brData, uint16_t brLen, uint16_t startID, uint8_t tm, uint8_t btm, uint8_t isF0)
{
    uint16_t ms;
    uint16_t us;

    if (sManager->critical) {
        taskENTER_CRITICAL();
    }

    if (sManager->echo) {
        sprintf(sManager->logCache1, "Frame:\r\n");
    }
    if(isF0) {
        UartSetBaud(UART_COM2, sManager->lowBaud);
    }
    else {
        UartSetBaud(UART_COM2, sManager->highBaud);
    }
    delayUs(100);
    sendSetBaud();
    delayUs(100);

    UartSetBaud(UART_COM2, sManager->lowBaud);
    sendFrameHead(regList[4], 0, 0);
    delayUs(FRAME_HEAD_DELAY + DELAY_OFFSET);
    sendSetID(1);
    delayUs(100);
    sendCMD2D();
    delayUs(100);
    sendSetBaud();

    delayUs(100);
    UartSetBaud(UART_COM2, sManager->highBaud);
    delayUs(10);
    UartRawSendByte(UART_COM2, 0xF5);
    UartWaitSendDone(UART_COM2);
    delayUs(30);
    UartRawSendByte(UART_COM2, 0x79);
    UartWaitSendDone(UART_COM2);
    delayUs(30);
    if (sManager->echo) {
        strcat(sManager->logCache1, "CMD2S sent 1 times\r\n");
        strcat(sManager->logCache1, "SetID sent 1 times\r\n");
    }

    if (btm == 1) {
        YX020BroadcastReg(YX020_BROAD_REG7, regList[5]);
    }
    else if (btm == 2) {
        YX020BroadcastReg(YX020_BROAD_REG0, regList[0]);
        YX020BroadcastReg(YX020_BROAD_REG1, regList[1]);
        YX020BroadcastReg(YX020_BROAD_REG2, regList[2]);
        YX020BroadcastReg(YX020_BROAD_REG3, regList[3]);
        YX020BroadcastReg(YX020_BROAD_REG7, regList[5]);
    }
    else {
        YX020BroadcastReg(YX020_BROAD_REG0, regList[0]);
        YX020BroadcastReg(YX020_BROAD_REG1, regList[1]);
        YX020BroadcastReg(YX020_BROAD_REG2, regList[2]);
        YX020BroadcastReg(YX020_BROAD_REG3, regList[3]);
		YX020BroadcastReg(YX020_BROAD_REG6, regList[5]);
    }

    if (sManager->bit8Mode) {
        sendBrightness8bit(startID, (uint8_t *)brData, brLen);
    }
    else {
        sendBrightness(startID, brData, brLen);
    }

    if (tm) {
        if (sManager->echo) {
            strcat(sManager->logCache3, "CMD2D has been sent\r\n");
        }
    }
    else {
        sendVSYNC();
        if (sManager->lowiq) {
            UartIOToCore(UART_COM2, 0);
            UartTxPullDown(UART_COM2);
            if (sManager->pdTime > 1000) {
                ms = sManager->pdTime / 1000;
                us = sManager->pdTime % 1000;
                vTaskDelay(ms);
                delayUs(us);
            }
            else {
                delayUs(sManager->pdTime);
            }
            UartIOToPeripheral(UART_COM2, 0);
        }
        if (sManager->echo) {
            strcat(sManager->logCache3, "CMD2D has been sent\r\n");
            strcat(sManager->logCache3, "VSYNC has been sent\r\n");
        }
    }

    if (sManager->critical) {
        taskEXIT_CRITICAL();
    }
}
