## yx020

含义：显示内部控制结构体的一些状态

示例：`yx020` 

## yx020 -init

- yx020 -init

    含义：发送初始化时序

    语法：`yx020 -init reg4 chipNum(Dec)` 

    示例：`yx020 -init 0B 08` 

- yx020 -init -testmode

    含义：带 REG7 的值，进入测试模式的初始化时序

    语法：`yx020 -init -testmode reg4 reg7 chipNum(Dec)` 

    示例：`yx020 -init -testmode 0B CC 08` 

- yx020 -init -tmno2d

    含义：带 REG7 的值，进入测试模式的初始化时序，但时序中不包含 CMD2D 序列

    语法：`yx020 -init -tmno2d reg4 reg7 chipNum(Dec)` 

    示例：`yx020 -init -tmno2d 0B CC 08` 

## yx020 -frame

- yx020 -frame

    含义：发送帧时序

    语法：`yx020 -frame id reg0 reg1 reg2 reg3 reg4 reg6 brdata` 

    - `id`: 起始 ID
    - `reg0`: REG0 的值
    - `reg1`: REG1 的值
    - `reg2`: REG2 的值
    - `reg3`: REG3 的值
    - `reg4`: REG4 的值
    - `reg6`: REG6 的值
    - `brdata`: 亮度数据部分，2 个字节一组，**组数不是 3 的倍数时，自动以 0 补齐** 

    示例：`yx020 -frame 0000 20 10 10 10 00 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000` 

- yx020 -frame -testmode

    含义：发送板级测试模式下帧时序 1

    语法：`yx020 -frame id reg0 reg1 reg2 reg3 reg4 reg7 brdata` 

    示例：`yx020 -frame 0000 20 10 10 10 00 13 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000` 

- yx020 -frame -frame0

    含义：发送板级测试模式下帧时序 0

    语法：`yx020 -frame -frame0 id reg4 reg7 brdata` 

    示例：`yx020 -frame 0000 00 13 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000` 

- yx020 -frame -broadcast

    含义：发送广播帧时序

    语法：`yx020 -frame -broadcast reg0 reg1 reg2 reg3 reg4 brdata` 

    示例：`yx020 -frame -broadcast 20 10 10 10 00 8000 8000 8000` 

    - `brdata` 项只需提供 3 组

- yx020 -frame -onlybr

    含义：广播发送亮度

    语法：`yx020 -frame -onlybr brdata` 

    示例：`yx020 -frame -onlybr 8000 8000 8000` 

    - `brdata` 项只需提供 3 组
    - 不发送 VSYNC 序列

- yx020 -frame -regs

    含义：缓存 REG0-REG4、REG6 的值

    语法：`yx020 -frame -regs reg0 reg1 reg2 reg3 reg4 reg6` 

    示例：`yx020 -frame -regs 20 10 10 10 00` 

- yx020 -frame -data

    含义：缓存亮度数据

    语法：`yx020 -frame -data brdata` 

    示例：`yx020 -frame -data 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000` 

- yx020 -frame -startid

    含义：缓存起始 ID

    语法：`yx020 -frame -startid id` 

    示例：`yx020 -frame -startid 00` 

- yx020 -frame -time

    含义：设置自动帧时间，当设置值不为 0 时，将根据缓存的起始 ID、REG0-REG4、亮度数据自动发送帧时序

    语法：`yx020 -frame -time time(Dec)` 

    示例：`yx020 -frame -time 100` 

    - 自动帧时间单位为 ms
    - **确保时间间隔 > 发送数据所需的时间** 

- yx020 -frame -hlbaud

    含义：发送高低波特率帧时序

    语法：`yx020 -frame -hlbaud isF0 id reg0 reg1 reg2 reg3 reg4 reg6 brdata` 

    示例：`yx020 -frame -hlbaud 0 0000 20 10 10 10 00 0C 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000 8000` 

    - `isF0`: 是否是第 0 帧

- yx020 -frame -zero

    含义：**死循环发送 0xA5** 

    语法：`yx020 -frame -zero` 

    示例：`yx020 -frame -zero` 

## yx020 -setid

- yx020 -setid

    含义：发送 SetID 时序

    语法：`yx020 -setid num` 

    - `num`: 发送多少次

    示例：`yx020 -setid 05` 

## yx020 -critical

- yx020 -critical

    含义：设置状态，发送初始化时序和帧时序时是否进入临界段

    - 临界段：关闭调度和抢占优先级 >= 5 的中断

    语法：`yx020 -critical status` 

    - `status`: 可选 `en`、`unen` 

    示例：`yx020 -critical en` 

## yx020 -echo

- yx020 -echo

    含义：设置状态，是否回显

    语法：`yx020 -echo status` 

    - `status`: 可选 `en`、`unen` 

    示例：`yx020 -echo en` 

## yx020 -free

- yx020 -free

    含义：立即释放从堆中分配的内存

    - 不仅 yx020 模块会使用堆内存，其它模块也会使用堆内存

    语法：`yx020 -free` 

    示例：`yx020 -free` 

## yx020 -brreg

- yx020 -brreg

    含义：广播写寄存器

    语法：`yx020 -brreg reg val` 

    - `reg`: 范围 00-07

    示例：`yx020 -brreg 00 20` 

## yx020 -sereg

- yx020 -sereg

    含义：指定 ID 写寄存器

    语法：`yx020 -sereg id reg val` 

    示例：`yx020 -sereg 0000 01 20` 

## yx020 -readreg

- yx020 -readreg

    含义：读指定 ID 的寄存器

    - 由于芯片输出信号可能反相，在反相后 MCU UART 接口不能读取数据，且会导致接收失效

    语法：`yx020 -readreg id reg` 

    示例：`yx020 -readreg 0000 04` 

## yx020 -8bitmode

- yx020 -8bitmode

    含义：设置状态，亮度数据解释为 16bit 或 8bit

    语法：`yx020 -8bitmode status` 

    - `status`: 可选 `en`、`unen` 

    示例：`yx020 -8bitmode en` 

## yx020 -lowiq

- yx020 -lowiq

    含义：设置状态，帧时序中是否包括 lowiq 段

    语法：`yx020 -lowiq status` 

    - `status`: 可选 `en`、`unen` 

    示例：`yx020 -lowiq en` 

## yx020 -pdtime

- yx020 -pdtime

    含义：设置 lowiq 段的下拉时间

    语法：`yx020 -pdtime time(Dec)` 

    示例：`yx020 -lowiq 200` 

    - 时间单位为 us
    - 下拉时间内不能再调用 yx020 相关指令

## yx020 -testmode

- yx020 -testmode -enter

    含义：进入测试模式

    语法：`yx020 -testmode -enter id` 

    - `id`: 起始 ID，对 ABB 版本，id 应为烧录的 id

    示例：`yx020 -testmode -enter 00` 

- yx020 -testmode -enter6

    含义：从 TX 进入测试模式

    语法：`yx020 -testmode -enter6 id` 

    - `id`: 起始 ID，对 ABB 版本，id 应为烧录的 id

    示例：`yx020 -testmode -enter6 00` 

- yx020 -testmode -preview

    含义：预览 Efuse 效果

    语法：`yx020 -testmode -preview id reg0 reg1 reg2 reg3 reg4 preview[15:0] preview[31:16] br2[15:0]` 

    - `id`: 起始 ID，对 ABB 版本，id 应为烧录的 id
    - `br2[15:0]`: 保持为 0

    示例：`yx020 -testmode -preview 00 20 10 10 10 00 0101 0101 0000` 

- yx020 -testmode -bkw10

    含义：运行板级测试 10

    语法：`yx020 -testmode -bkw10 chipNum reg0` 

    示例：`yx020 -testmode -bkw10 10 20` 

- yx020 -testmode -OSC

    含义：输出烧录 Efuse 后的时钟

    语法：`yx020 -testmode -OSC` 

    示例：`yx020 -testmode -OSC` 

## yx020 -vsync

- yx020 -vsync

    含义：发送 VSYNC 序列

    语法：`yx020 -vsync num(Dec)` 

    - `num`: VYSNC 序列的个数

    示例：`yx020 -vsync 10` 

## yx020 -trim

- yx020 -trim -burn

    含义：烧录 Efuse

    语法：`yx020 -trim -burn Fuse[15:0] Fuse[31:16] burnChipNum` 

    示例：`yx020 -trim -burn 0200 0100 1` 

- yx020 -trim -read

    废弃

## yx020 -cacheBaud

- yx020 -cacheBaud

    含义：设置高低波特率

    语法：`yx020 -cacheBaud high(Dec) low(Dec)` 

    示例：`yx020 -cacheBaud 2000000 1000000` 

