#include "cmd.h"
#include "trim.h"
#include "main.h"
#include "string.h"
#include "util_str.h"

static char * const sc_cmd_pca9555 = "pca9555";
static char * const sc_info1 = "PCA9555 is not enable";

static uint8_t s_address = 0x40;
static uint8_t s_enable = 0;

static void cmdPca9555Callback(int argc, char *argv[], int index);
static void pca9555ShowStatus(void);
static void pca9555Set(uint8_t *receive, char *param, uint16_t recelen);
static void pca9555WriteProcess(uint8_t *receive, char *param, uint16_t recelen);
static void pca9555ReadProcess(uint8_t *receive, char *param, uint16_t recelen);

void CmdPca9555Init(void)
{
	CmdAppend(&g_cmd_uart_manager, sc_cmd_pca9555, cmdPca9555Callback);
}

static void cmdPca9555Callback(int argc, char *argv[], int index)
{
	uint16_t recelen = UartGetReceiveLen(&g_uart1);
	uint8_t *receive = UartGetRxBuffer(&g_uart1);
	
	if (argc == 0) {
		pca9555ShowStatus();
	}
	else if (argc == 1) {
		if (strcmp(argv[0], gc_cmd_par_set) == 0) {
			pca9555Set(&receive[index], NULL, recelen - index);
		}
		else if (strcmp(argv[0], gc_cmd_par_write) == 0) {
			if (s_enable) {
				pca9555WriteProcess(&receive[index], NULL, recelen - index);
			}
			else {
				UartPutsEndl(&g_uart1, sc_info1);
			}
		}
		else if (strcmp(argv[0], gc_cmd_par_read) == 0) {
			if (s_enable) {
				pca9555ReadProcess(&receive[index], NULL, recelen - index);
			}
			else {
				UartPutsEndl(&g_uart1, sc_info1);
			}
		}
		else {
			UartPutsEndl(&g_uart1, gc_cmd_warn_nmp);
		}
	}
	else if (argc == 2) {
		if (strcmp(argv[0], gc_cmd_par_set) == 0) {
			pca9555Set(&receive[index], argv[1], recelen - index);
		}
		else {
			UartPutsEndl(&g_uart1, gc_cmd_warn_nmp);
		}
	}
	else {
		UartPutsEndl(&g_uart1, gc_cmd_warn_nmp);
	}
}

static void pca9555ShowStatus(void)
{
	uint8_t i;
	
	uint8_t regs[8] = {0};
	
	UartPrintfEndl(&g_uart1, "Address: 0x%02X", s_address);
	if (s_enable) {
		for (i = 0; i < 8; i++) {
			TrimIICReadReg(s_address, i, &regs[i]);
			UartPrintfEndl(&g_uart1, "Reg[%d] = 0x%02X", i, regs[i]);
		}
	}
	else {
		UartPutsEndl(&g_uart1, sc_info1);
	}
}

static void pca9555Set(uint8_t *receive, char *param, uint16_t recelen)
{
	int16_t index;
	
	if (param == NULL) {
		index = StrGetNextNotSpace((char *)receive);
		if (index == -1) {
			UartPutsEndl(&g_uart1, gc_cmd_info_no_param);
		}
		else {
			if (strncmp((char *)&receive[index], gc_cmd_par_enable, 6) == 0) {
				TrimToIIC();
				s_enable = 1;
				UartPutsEndl(&g_uart1, gc_cmd_info_done);
			}
			else if (strncmp((char *)&receive[index], gc_cmd_par_disable, 7) == 0) {
				TrimToNormal();
				s_enable = 0;
				UartPutsEndl(&g_uart1, gc_cmd_info_done);
			}
			else {
				UartPutsEndl(&g_uart1, gc_cmd_warn_nmp);
			}
		}
	}
	else if (strcmp(param, gc_cmd_par_address) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		if (index == -1) {
			UartPutsEndl(&g_uart1, gc_cmd_info_no_param);
		}
		else {
			s_address = StrGetHex((char *)&receive[index], 0, 4);
			UartPrintfEndl(&g_uart1, "new address: 0x%02X", s_address);
		}
	}
}

static void pca9555WriteProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint8_t reg, value, ret;
	int16_t index, offset;
	
	index = StrGetNextNotSpace((char *)receive);
	offset = index;
	if (index == -1) {
		UartPutsEndl(&g_uart1, gc_cmd_warn_mp);
	}
	else {
		reg = StrGetHex((char *)&receive[offset], 0, 4);
		index = StrGetCHIndex((char *)&receive[offset], ' ');
		if (index == -1) {
			UartPutsEndl(&g_uart1, gc_cmd_warn_mp);
		}
		else {
			offset += index;
			value = StrGetHex((char *)&receive[offset], 0, 6);
			ret = TrimIICWriteReg(s_address, reg, value);
			if (ret) {
				UartPrintfEndl(&g_uart1, "fail, ret = %d", ret);
			}
			else {
				UartPutsEndl(&g_uart1, gc_cmd_info_done);
			}
		}
	}
}

static void pca9555ReadProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint8_t reg, value, ret;
	int16_t index, offset;
	
	index = StrGetNextNotSpace((char *)receive);
	offset = index;
	if (index == -1) {
		UartPutsEndl(&g_uart1, gc_cmd_warn_mp);
	}
	else {
		reg = StrGetHex((char *)&receive[offset], 0, 4);
		ret = TrimIICReadReg(s_address, reg, &value);
		if (ret) {
			UartPrintfEndl(&g_uart1, "fail, ret = %d", ret);
		}
		else {
			UartPrintfEndl(&g_uart1, "value: %X", value);
		}
	}
}
