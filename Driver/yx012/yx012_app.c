#include "yx012_app.h"
#include "yx012_port.h"
#include "util_cmd.h"
#include "util_str.h"
#include "util_crc.h"
#include "uart_nuvoton.h"
#include "hard_spi.h"
#include "tim.h"
#include "trim.h"
#include "delay.h"
#include "main.h"
#include "stdio.h"
#include "string.h"

#define CRC_EN_BIT (0x01 << 6)
#define MIN_VSYNC_ALLOW_FRE     20
#define MAX_VSYNC_ALLOW_FRE     500
#define MIN_VSYNC_ALLOW_PERIOD  2
#define MAX_VSYNC_ALLOW_PERIOD  50
#define MAX_VSYNC_ALLOW_WIDTH   500
#define MIN_ALLOW_CHANGE_TIME   500
#define MAX_ALLOW_CHANGE_TIME   10000

struct VsyncManagerType {
    uint8_t flow : 1;       /* Wheather to send a vsync signal following spi data */
    uint8_t polarity : 1;   /* The polarity of vsync. 1 = high; 0 = low */
    uint8_t signal : 1;     /* Wheather send a pulse in another pin when vsync period changes. 1 = yes; 0 = no */
    uint8_t mode : 1;       /* Vsync time unit, 1 = period. ms; 0 = frequency, Hz */
    uint8_t autoChange : 1;
    uint8_t autoSend : 1;
    uint8_t autoWith : 1;
    uint8_t position : 1;   /* 1 = befor, 0 = after */
    uint16_t interval;
    uint16_t min;
    uint16_t max;
    uint16_t step;
    uint16_t width;
    uint16_t changeTime;
};

struct TrimManagerType {
    const uint16_t *tmSeq1;
    uint8_t tmSeq1Len;
    const uint16_t *tmSeq2;
    uint8_t tmSeq2Len;
    const uint16_t *outSeq;
    uint8_t outSeqLen;
    const uint16_t *oscSeq;
    uint8_t oscSeqLen;
    const uint16_t *ifbSeq1;
    uint8_t ifbSeq1Len;
    const uint16_t *ifbSeq2;
    uint8_t ifbSeq2Len;
    const uint16_t *vfbSeq1;
    uint8_t vfbSeq1Len;
    const uint16_t *vfbSeq2;
    uint8_t vfbSeq2Len;
    const uint16_t *sramSeq1;
    uint8_t sramSeq1Len;
    const uint16_t *sramSeq2;
    uint8_t sramSeq2Len;
    const uint16_t *sramSeq3;
    uint8_t sramSeq3Len;
    const uint16_t *sramCurrectData;
    uint8_t sramCurrectDataLen;
    uint16_t *cache;
	uint32_t preview;
	uint8_t savepr : 1;
	uint8_t reverse : 7;
};

union CacheArray {
    uint8_t byteArray[YX012_CACHE_ARRAY_SIZE];
    uint16_t halfWorldArray[YX012_CACHE_ARRAY_SIZE/2];
    uint32_t wordArray[YX012_CACHE_ARRAY_SIZE/4];
};

struct YX012ManagerType {
    uint8_t crcen : 1;
    uint8_t valien : 1;
    uint8_t rxen : 1;
    uint8_t trace : 1;
	uint8_t crcOnOperat : 1;
	uint8_t crcOffOperat : 1;
	uint8_t readRegOperat : 1;
    uint8_t reversed : 1;
	uint16_t regNum;
    uint16_t *rxCache;
	uint16_t rxLen;
    uint16_t *sendCache;
    uint16_t sendCacheLen;
    struct VsyncManagerType vsyncManager;
    struct TrimManagerType trimManager;
    union CacheArray *cache;
};

EventGroupHandle_t YX012EventGroup = NULL;

static struct YX012ManagerType *sManager = NULL;
static TaskHandle_t vsyncTaskHandle = NULL;
static TaskHandle_t analyzeRxTaskHandle = NULL;
static TimerHandle_t sTimerHandle = NULL;
static const char *strVsyncTaskName = "vsyncTask";
static const char *strRxTaskName = "analyzeRxTask";
static const char *strErrlog1 = "param err\r\n";

static char *sCmdYx012 = "yx012";
static char *sParSend = "send";
static char *sParChar = "char";
static char *sParCrc = "crc";
static char *sParVali = "vali";
static char *sParRx = "rx";
static char *sParTrace = "trace";
static char *sParTestmode = "testmode";
static char *sParPreview = "preview";
static char *sParTrim = "trim";
static char *sParFree = "free";

const static char *previewIFBO = "IFBO";
const static char *previewVFBO = "VFBO";
const static char *previewOSC = "OSC";
const static char *previewSavepr = "savepr";
const static char *previewReport = "report";
const static char *previewSram = "SRAM";

static char *sCmdVsync = "vsync";
static char *sParFlow = "flow";
static char *sParPolarity = "polarity";
static char *sParSignal = "signal";
static char *sParMode = "mode";
static char *sParAutoChange = "autochange";
static char *sParAutosend = "autosend";
static char *sParAutowith = "autowith";
static char *sParPosition = "position";
static char *sParAutocode = "autocode";
static char *sParInterval = "interval";
static char *sParMin = "min";
static char *sParMax = "max";
static char *sParStep = "step";
static char *sParWidth = "width";
static char *sParChangetime = "changetime";

static void showYx012Status(void)
{
    UartPuts(UART_COM1, "Current status:\r\n");
    UartPrintf(UART_COM1, "crcen = %d\r\n", sManager->crcen);
    UartPrintf(UART_COM1, "valien = %d\r\n", sManager->valien);
    UartPrintf(UART_COM1, "rxen = %d\r\n", sManager->rxen);
    UartPrintf(UART_COM1, "trace = %d\r\n", sManager->trace);
    UartPrintf(UART_COM1, "sendCache = %d\r\n", sManager->sendCache);
    UartPrintf(UART_COM1, "sendCacheLen = %d\r\n", sManager->sendCacheLen);
}

static void showVsyncStatus(void)
{
    UartPrintf(UART_COM1, "flow = %d\r\n", sManager->vsyncManager.flow);
    UartPrintf(UART_COM1, "polarity = %d\r\n", sManager->vsyncManager.polarity);
    UartPrintf(UART_COM1, "signal = %d\r\n", sManager->vsyncManager.signal);
    UartPrintf(UART_COM1, "mode = %d\r\n", sManager->vsyncManager.mode);
    UartPrintf(UART_COM1, "autoChange = %d\r\n", sManager->vsyncManager.autoChange);
    UartPrintf(UART_COM1, "autoSend = %d\r\n", sManager->vsyncManager.autoSend);
    UartPrintf(UART_COM1, "autoWith = %d\r\n", sManager->vsyncManager.autoWith);
	UartPrintf(UART_COM1, "position = %d\r\n", sManager->vsyncManager.position);
    UartPrintf(UART_COM1, "min = %d\r\n", sManager->vsyncManager.min);
    UartPrintf(UART_COM1, "max = %d\r\n", sManager->vsyncManager.max);
    UartPrintf(UART_COM1, "step = %d\r\n", sManager->vsyncManager.step);
	UartPrintf(UART_COM1, "width = %d\r\n", sManager->vsyncManager.width);
	UartPrintf(UART_COM1, "changeTime = %d\r\n", sManager->vsyncManager.changeTime);
}

static void verifyData(uint16_t len)
{}

static void parSendFun(uint8_t *data)
{}
	
static void analyzeRxTask(void *parameter)
{
	uint16_t i;
	
	while (1) {
		xEventGroupWaitBits(YX012EventGroup, YX012_EVENT_RX_DONE, pdTRUE, pdTRUE, portMAX_DELAY);
		
		xEventGroupClearBits(APPEventGroup, APP_EVENT_YX012_APP_RX);
		taskENTER_CRITICAL();
		
	#ifdef USER_DEBUG_LOG_EN
		UartPrintf(UART_COM1, "%s ==> sManager->rxCache:", __FUNCTION__);
		for (i = 0; i < sManager->rxLen; i++) {
			if ((i % 10) == 0) {
				UartPuts(UART_COM1, "\r\n");
			}
			UartPrintf(UART_COM1, "0x%04X ", sManager->rxCache[i]);
		}
		UartPuts(UART_COM1, "\r\n");
	#endif
		
		if (sManager->crcOnOperat) {
			sManager->cache->halfWorldArray[0] = 0x0000;
			sManager->cache->halfWorldArray[1] = 0x0000;
			sManager->cache->halfWorldArray[2] = sManager->rxCache[2] | CRC_EN_BIT;
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)(sManager->cache->halfWorldArray), 3);
			
			/* 此任务由 trace 参数恢复，则重新进入挂起态 */
			if (sManager->rxen == 0) {
				vTaskSuspend(analyzeRxTaskHandle);
			}
			sManager->crcOnOperat = 0;
		}
		else if (sManager->crcOffOperat) {
			sManager->cache->halfWorldArray[0] = 0x0000;
			sManager->cache->halfWorldArray[1] = 0x0000;
			sManager->cache->halfWorldArray[2] = sManager->rxCache[2] & (~CRC_EN_BIT);
			sManager->cache->halfWorldArray[3] = CRC16XModem(sManager->cache->halfWorldArray, 3);
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)(sManager->cache->halfWorldArray), 3);
			
			if (sManager->rxen == 0) {
				vTaskSuspend(analyzeRxTaskHandle);
			}
			sManager->crcOffOperat = 0;
		}
		else {
			if (sManager->readRegOperat) {
				if (sManager->crcen) {
					for (i = 0; i < sManager->rxLen - 3; i++) {
						if ((i % 10) == 0) {
							UartPuts(UART_COM1, "\r\n");
						}
						UartPrintf(UART_COM1, "REG%03d=%04X ", sManager->regNum, sManager->rxCache[i+3]);
						sManager->regNum++;
					}
				}
				else {
					for (i = 0; i < sManager->rxLen - 2; i++) {
						if ((i % 10) == 0) {
							UartPuts(UART_COM1, "\r\n");
						}
						UartPrintf(UART_COM1, "REG%03d=%04X ", sManager->regNum, sManager->rxCache[i+2]);
						sManager->regNum++;
					}
				}
			}
		}
		
		taskEXIT_CRITICAL();
		sManager->regNum = 0;
		sManager->rxLen = 0;
	}
}

static void charRxen(uint16_t *data, uint16_t sendLen)
{
    uint16_t i;
    uint16_t crcVal;
	
	sManager->regNum = data[1];
	xEventGroupSetBits(APPEventGroup, APP_EVENT_YX012_APP_RX);
	
#ifdef USER_DEBUG_LOG_EN
	UartPrintf(UART_COM1, "%s ==> sendLen = %d\r\n", __FUNCTION__, sendLen);
#endif

    if (sManager->crcen) {
        if (APP_BIT_CHECK(data[0], 0x8000)) {
            crcVal = CRC16XModem(data, 2);
            for (i = sendLen; i > 1; i--) {
                data[i+1] = data[i];
            }
            data[2] = crcVal;
			sManager->rxLen = sendLen + 1;
			if (sManager->rxCache != NULL) {
				vPortFree(sManager->rxCache);
			}
			sManager->rxCache = (uint16_t *)pvPortMalloc(sManager->rxLen * 2);
			sManager->readRegOperat = 1;
            HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen+1, (uint32_t)(sManager->rxCache), sendLen+1);
        }
        else {
            crcVal = CRC16XModem(data, sendLen);
            data[sendLen] = crcVal;
			if (sManager->rxCache != NULL) {
				vPortFree(sManager->rxCache);
			}
			sManager->rxCache = (uint16_t *)pvPortMalloc(sManager->rxLen * 2);
            HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen+1, (uint32_t)(sManager->rxCache), sendLen+1);
			sManager->regNum = data[1];
        }
    }
    else {
		if (APP_BIT_CHECK(data[0], 0x8000)) {
			sManager->readRegOperat = 1;
		}
		sManager->rxLen = sendLen;
		if (sManager->rxCache != NULL) {
			vPortFree(sManager->rxCache);
		}
		sManager->rxCache = (uint16_t *)pvPortMalloc(sManager->rxLen * 2);
        HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen, (uint32_t)(sManager->rxCache), sendLen);
    }
}

static void charRxUnen(uint16_t *data, uint16_t sendLen)
{
    uint16_t i;
    uint16_t regNum, crcVal;

    if (sManager->crcen) {
        regNum = data[0];
        /* Read reg instruction */
        if (APP_BIT_CHECK(regNum, 0x8000)) {
            crcVal = CRC16XModem(data, 2);
            if (sendLen >= (YX012_CACHE_ARRAY_SIZE / 2)) {
                taskENTER_CRITICAL();

                HardSPISendOneHalfWord(HARD_SPI_CH0, data[0]);
                HardSPISendOneHalfWord(HARD_SPI_CH0, data[1]);
                HardSPISendOneHalfWord(HARD_SPI_CH0, crcVal);
                HardSPISendHalfWordArray(HARD_SPI_CH0, &data[2], YX012_CACHE_ARRAY_SIZE / 2);

                taskEXIT_CRITICAL();
            }
            else {
                for (i = sendLen; i > 1; i--) {
                    data[i+1] = data[i];
                }
                data[2] = crcVal;
                HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen+1);
            }
        }
        else {
            crcVal = CRC16XModem(data, sendLen);
            if (sendLen >= (YX012_CACHE_ARRAY_SIZE / 2)) {
                taskENTER_CRITICAL();

                HardSPISendHalfWordArray(HARD_SPI_CH0, data, YX012_CACHE_ARRAY_SIZE / 2);
                HardSPISendOneHalfWord(HARD_SPI_CH0, crcVal);

                taskEXIT_CRITICAL();
            }
            else {
                data[sendLen] = crcVal;
				if (sManager->vsyncManager.flow && (sManager->vsyncManager.position == 0)) {
					taskENTER_CRITICAL();
					HardSPISendHalfWordArray(HARD_SPI_CH0, data, sendLen+1);
					taskEXIT_CRITICAL();
				}
				else {
					HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen + 1);
				}
            }
            if (sManager->valien && (regNum >> 8 == 0)) {
                verifyData(sendLen);
            }
        }
    }
    else {
        regNum = data[0];
		/* VSYNC 跟随并且在位置在数据的后面 */
		if (sManager->vsyncManager.flow && (sManager->vsyncManager.position == 0)) {
			taskENTER_CRITICAL();
			/* 使用阻塞式发送，可以确保 VSYNC 信号在数据即将发完的时候再发送
			 * 因为开启了 SPI FIFO，所以是在即将发完的时候而不是在完全发完的时候 */
			HardSPISendHalfWordArray(HARD_SPI_CH0, data, sendLen);
			taskEXIT_CRITICAL();
		}
		else {
			/* FIX: 当大量数据直接使用 PDMA 发送而不接收时，导致后续不能再接收到数据 */
			/* HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen); */
			if (sManager->rxCache != NULL) {
				vPortFree(sManager->rxCache);
			}
			sManager->rxCache = (uint16_t *)pvPortMalloc(sendLen * 2);
			HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)data, sendLen, (uint32_t)(sManager->rxCache), sendLen);
		}
        if (sManager->valien && (regNum >> 8 == 0)) {
            verifyData(sendLen);
        }
    }
}

static void parCharFun(uint8_t *data, uint16_t index)
{
    uint16_t sendLen;

    sendLen = StrChar2Hex((char *)data, 0, UART1_RX_BUF_SIZE, sManager->cache->halfWorldArray, YX012_CACHE_ARRAY_SIZE / 2);
    if (sManager->rxen) {
        charRxen(sManager->cache->halfWorldArray, sendLen/4);
    }
    else {
        charRxUnen(sManager->cache->halfWorldArray, sendLen/4);
    }
}

static void setIFBO(uint16_t code)
{
    code = code & 0x001F;
	sManager->trimManager.preview &= (~0x001F);
	sManager->trimManager.preview |= code;
	if (sManager->trimManager.cache != NULL) {
		vPortFree(sManager->trimManager.cache);
	}
    sManager->trimManager.cache = (uint16_t *)pvPortMalloc(8);
    sManager->trimManager.cache[1] = 0x00C3;
	if (sManager->trimManager.savepr) {
		sManager->trimManager.cache[0] = 0x0001;
        sManager->trimManager.cache[2] = sManager->trimManager.preview;
        sManager->trimManager.cache[3] = sManager->trimManager.preview >> 16;
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.cache, 4);
	}
    else {
		sManager->trimManager.cache[0] = 0x0000;
        sManager->trimManager.cache[2] = code;
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.cache, 3);
    }
}

static void setVFBO(uint16_t code)
{
	code <<= 5;
    code = code & 0x03E0;
    sManager->trimManager.preview &= (~0x03E0);
    sManager->trimManager.preview |= code;
    if (sManager->trimManager.cache != NULL) {
		vPortFree(sManager->trimManager.cache);
	}
    sManager->trimManager.cache = (uint16_t *)pvPortMalloc(8);
    sManager->trimManager.cache[1] = 0x00C3;
    if (sManager->trimManager.savepr) {
		sManager->trimManager.cache[0] = 0x0001;
        sManager->trimManager.cache[2] = sManager->trimManager.preview;
        sManager->trimManager.cache[3] = sManager->trimManager.preview >> 16;
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.cache, 4);
    }
    else {
		sManager->trimManager.cache[0] = 0x0000;
        sManager->trimManager.cache[2] = code;
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.cache, 3);
    }
}

static void setOSC(uint16_t code)
{
	uint32_t t = code;
	
	code <<= 12;
	t >>= 4;
	code = code & 0xF000;
	t = t & 0x0007;
	sManager->trimManager.preview &= 0xFFF80FFF;
	sManager->trimManager.preview |= code;
	sManager->trimManager.preview |= (t << 16);
	if (sManager->trimManager.cache != NULL) {
		vPortFree(sManager->trimManager.cache);
	}
	sManager->trimManager.cache = (uint16_t *)pvPortMalloc(8);
    sManager->trimManager.cache[0] = 0x0001;
    sManager->trimManager.cache[1] = 0x00C3;
	if (sManager->trimManager.savepr) {
		sManager->trimManager.cache[2] = sManager->trimManager.preview;
		sManager->trimManager.cache[3] = (sManager->trimManager.preview >> 16);
	}
	else {
		sManager->trimManager.cache[2] = code;
		sManager->trimManager.cache[3] = t;
	}
	HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.cache, 4);
}

static void parPreview(uint8_t *receive)
{
	uint16_t code;
	int16_t index;
	
    if (strncmp((char *)receive, strEn, 2) == 0) {
        index = StrGetNextNotSpace((char *)receive);
        if (index == -1) {
            UartPuts(UART_COM1, "en: No param\r\n");
        }
        else if (strncmp((char *)&receive[index], previewIFBO, 4) == 0) {
            HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.ifbSeq1, sManager->trimManager.ifbSeq1Len);
            vTaskDelay(1);
            HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.ifbSeq2, sManager->trimManager.ifbSeq2Len);
			vTaskDelay(1);
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.outSeq, sManager->trimManager.outSeqLen);
        }
		else if (strncmp((char *)&receive[index], previewVFBO, 4) == 0) {
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.vfbSeq1, sManager->trimManager.vfbSeq1Len);
			vTaskDelay(1);
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.vfbSeq2, sManager->trimManager.vfbSeq2Len);
			vTaskDelay(1);
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.outSeq, sManager->trimManager.outSeqLen);
		}
		else if (strncmp((char *)&receive[index], previewOSC, 4) == 0) {
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.oscSeq, sManager->trimManager.oscSeqLen);
			vTaskDelay(1);
			HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.outSeq, sManager->trimManager.outSeqLen);
		}
		else {
			UartPuts(UART_COM1, "en: No match param\r\n");
		}
    }
	else if (strncmp((char *)receive, previewIFBO, 4) == 0) {
		StrChar2Hex((char *)receive, 4, 8, &code, 1);
		UartPrintf(UART_COM1, "IFBO = %02X\r\n", code);
		setIFBO(code);
	}
    else if (strncmp((char *)receive, previewVFBO, 4) == 0) {
        StrChar2Hex((char *)receive, 4, 8, &code, 1);
        UartPrintf(UART_COM1, "VFBO = %02X\r\n", code);
		setVFBO(code);
    }
    else if (strncmp((char *)receive, previewOSC, 3) == 0) {
		StrChar2Hex((char *)receive, 4, 8, &code, 1);
		UartPrintf(UART_COM1, "OSC = %02X\r\n", code);
		setOSC(code);
	}
	else if (strncmp((char *)receive, previewSavepr, 6) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		if (index == -1) {
			UartPuts(UART_COM1, "savepr: No param\r\n");
			return;
		}
		if (strncmp((char *)&receive[index], strEn, 2) == 0) {
			sManager->trimManager.savepr = 1;
		}
		else if (strncmp((char *)&receive[index], strUnen, 2) == 0) {
			sManager->trimManager.savepr = 0;
		}
		else {
			UartPuts(UART_COM1, "savepr: No match param\r\n");
		}
	}
    else if (strncmp((char *)receive, previewReport, 6) == 0) {
        UartPrintf(UART_COM1, "preview = %08X\r\n", sManager->trimManager.preview);
    }
	else if (strncmp((char *)receive, previewSram, 4) == 0) {
		HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.sramSeq1, sManager->trimManager.sramSeq1Len);
		vTaskDelay(1);
		HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.sramSeq2, sManager->trimManager.sramSeq2Len);
		vTaskDelay(1);
		if (sManager->trimManager.cache != NULL) {
			vPortFree(sManager->trimManager.cache);
			sManager->trimManager.cache = NULL;
		}
		sManager->trimManager.cache = (uint16_t *)pvPortMalloc(sManager->trimManager.sramSeq3Len * 2);
		HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.sramSeq3, sManager->trimManager.sramSeq3Len, (uint32_t)sManager->trimManager.cache, sManager->trimManager.sramSeq3Len);
		vTaskDelay(1);
		for (index = 0; index < sManager->trimManager.sramSeq3Len; index++) {
			if (sManager->trimManager.cache[index] != sManager->trimManager.sramCurrectData[index]) {
				break;
			}
		}
		if (index == sManager->trimManager.sramSeq3Len) {
			UartPuts(UART_COM1, "SRAM Result: Fail\r\n");
		}
		else {
			UartPuts(UART_COM1, "SRAM Result: Pass\r\n");
		}
	#ifdef USER_DEBUG_LOG_EN
		UartPuts(UART_COM1, "SRAM Receive Data:\r\n");
		for (index = 0; index < sManager->trimManager.sramSeq3Len; index++) {
			UartPrintf(UART_COM1, "0x%04X ", sManager->trimManager.cache[index]);
		}
	#endif
		vPortFree(sManager->trimManager.cache);
		sManager->trimManager.cache = NULL;
	}
    else {
        UartPuts(UART_COM1, "preview: No match param\r\n");
    }
}

static void parTrim(uint8_t *receive)
{
	uint32_t val;
	uint16_t *p = (uint16_t *)pvPortMalloc(4);
	
	StrChar2Hex((char *)receive, 0, 8, p, 2);
	val = p[0];
	val <<= 16;
	val |= p[1];
	UartPrintf(UART_COM1, "trim val = %08X\r\n", val);
	HardSPISendOneHalfWord(HARD_SPI_CH0, 0x0000);
	HardSPISendOneHalfWord(HARD_SPI_CH0, 0x00C5);
	HardSPISendOneHalfWord(HARD_SPI_CH0, 0x0002);
	vTaskDelay(1);
	TrimWrite(val, 32);
	UartPuts(UART_COM1, "trim: Finish\r\n");
}

static void cmdYx012Callback(CmdParamTypeDef *param)
{
    uint16_t crcVal;
    uint8_t *receiveArray = UartGetRxBuffer(UART_COM1);

    if (param == NULL) {
        taskENTER_CRITICAL();
        showYx012Status();
        taskEXIT_CRITICAL();
        return;
    }

    if (strcmp(param->name, sParSend) == 0) {
        if (sManager->vsyncManager.flow) {
			if (sManager->vsyncManager.position) {
				YX012PortGenVsync(sManager->vsyncManager.width);
				parSendFun(&receiveArray[param->index+1]);
			}
			else {
				parSendFun(&receiveArray[param->index+1]);
				YX012PortGenVsync(sManager->vsyncManager.width);
			}
		}
		else {
			parSendFun(&receiveArray[param->index+1]);
		}
    }
    else if (strcmp(param->name, sParChar) == 0) {
		if (sManager->vsyncManager.flow) {
			if (sManager->vsyncManager.position) {
				YX012PortGenVsync(sManager->vsyncManager.width);
				parCharFun(&receiveArray[param->index+1], param->index);
			}
			else {
				parCharFun(&receiveArray[param->index+1], param->index);
				YX012PortGenVsync(sManager->vsyncManager.width);
			}
		}
		else {
			parCharFun(&receiveArray[param->index+1], param->index);
		}
    }
	else if (strcmp(param->name, sParCrc) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->crcen = 1;
            if (sManager->trace) {
                sManager->cache->halfWorldArray[0] = 0x8000;
                sManager->cache->halfWorldArray[1] = 0x0000;
                sManager->cache->halfWorldArray[2] = 0x0000;
				if (sManager->rxCache != NULL) {
					vPortFree(sManager->rxCache);
				}
				sManager->rxCache = (uint16_t *)pvPortMalloc(6);
				sManager->crcOnOperat = 1;
				sManager->rxLen = 3;
				if (sManager->rxen == 0) {
					HardSPIEnPDMAInterrupt(HARD_SPI_CH0);
					vTaskResume(analyzeRxTaskHandle);
				}
				xEventGroupSetBits(APPEventGroup, APP_EVENT_YX012_APP_RX); /* 标记在中断中可切换至接收任务 */
                HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->cache->halfWorldArray, 3, (uint32_t)(sManager->rxCache), 3);
            }
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->crcen = 0;
            if (sManager->trace) {
                sManager->cache->halfWorldArray[0] = 0x8000;
                sManager->cache->halfWorldArray[1] = 0x0000;
                crcVal = CRC16XModem(sManager->cache->halfWorldArray, 2);
                sManager->cache->halfWorldArray[2] = crcVal;
                sManager->cache->halfWorldArray[3] = 0x0000;
				if (sManager->rxCache != NULL) {
					vPortFree(sManager->rxCache);
				}
				sManager->rxCache = (uint16_t *)pvPortMalloc(8);
				sManager->crcOffOperat = 1;
				sManager->rxLen = 4;
				if (sManager->rxen == 0) {
					HardSPIEnPDMAInterrupt(HARD_SPI_CH0);
					vTaskResume(analyzeRxTaskHandle);
				}
				xEventGroupSetBits(APPEventGroup, APP_EVENT_YX012_APP_RX);
                HardSPISendReceiveHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->cache->halfWorldArray, 4, (uint32_t)(sManager->rxCache), 4);
				
				/*
				vTaskDelay(1);
                APP_BIT_CLEAR(sManager->rxCache[3], CRC_EN_BIT);
                sManager->cache->halfWorldArray[2] = sManager->rxCache[3];
                crcVal = CRC16XModem(sManager->cache->halfWorldArray, 3);
                sManager->cache->halfWorldArray[3] = crcVal;
                HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->cache->halfWorldArray, 4);
				*/
            }
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParCrc, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParVali) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->valien = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->valien = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParVali, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParRx) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
			HardSPIEnPDMAInterrupt(HARD_SPI_CH0);
			vTaskResume(analyzeRxTaskHandle);
            sManager->rxen = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
			HardSPIDisPDMAInterrupt(HARD_SPI_CH0);
			vTaskSuspend(analyzeRxTaskHandle);
            sManager->rxen = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParRx, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParTrace) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->trace = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->trace = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParTrace, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParTestmode) == 0) {
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.tmSeq1, sManager->trimManager.tmSeq1Len);
        vTaskDelay(1);
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.tmSeq2, sManager->trimManager.tmSeq2Len);
        vTaskDelay(1);
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.tmSeq1, sManager->trimManager.tmSeq1Len);
        vTaskDelay(1);
        HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->trimManager.tmSeq2, sManager->trimManager.tmSeq2Len);
    }
    else if (strcmp(param->name, sParPreview) == 0) {
        parPreview(&receiveArray[param->index+1]);
    }
    else if (strcmp(param->name, sParTrim) == 0) {
        parTrim(&receiveArray[param->index+1]);
    }
	else if (strcmp(param->name, sParFree) == 0) {
		if (sManager->rxCache != NULL) {
			vPortFree(sManager->rxCache);
			sManager->rxCache = NULL;
		}
	}
}

static void vsyncTask(void *parameter)
{
    static uint8_t dir = 1;

    EventBits_t event;

    while (1) {
		/* 等待 VSYNC 生成事件 */
        event = xEventGroupWaitBits(APPEventGroup, APP_EVENT_VSYNC_GEN, pdTRUE, pdFALSE, portMAX_DELAY);
		/* 如果需要跟随发送数据则根据 position 的值决定在数据前还是在数据后生成 */
        if (sManager->vsyncManager.autoWith) {
            if (sManager->vsyncManager.position) {
                YX012PortGenVsync(sManager->vsyncManager.width);
				if (sManager->sendCache != NULL) {
					HardSPISendHalfWordArrayPDMA(HARD_SPI_CH0, (uint32_t)sManager->sendCache, sManager->sendCacheLen);
				}
            }
            else {
				if (sManager->sendCache != NULL) {
					taskENTER_CRITICAL();
					HardSPISendHalfWordArray(HARD_SPI_CH0, sManager->sendCache, sManager->sendCacheLen);
					taskEXIT_CRITICAL();
				}
                YX012PortGenVsync(sManager->vsyncManager.width);
            }
        }
        else {
            YX012PortGenVsync(sManager->vsyncManager.width);
        }
		/* 如果定时切换 VSYNC 周期或频率的功能开启，则再检查 APP_EVENT_VSYNC_SW 事件有没有发生
		 * APP_EVENT_VSYNC_SW 事件由软件定时器 sTimerHandle 产生 */
        if (sManager->vsyncManager.autoChange) {
            event = xEventGroupGetBits(APPEventGroup);
            if (APP_BIT_CHECK(event, APP_EVENT_VSYNC_SW)) {
				/* 设置 APP_EVENT_VSYNC_UPDATA 事件 */
				xEventGroupSetBits(APPEventGroup, APP_EVENT_VSYNC_UPDATA);
                xEventGroupClearBits(APPEventGroup, APP_EVENT_VSYNC_SW);
                if (sManager->vsyncManager.signal) {
                    YX012PortGenSWSignal(1);
                }
                if (dir) {
                    sManager->vsyncManager.interval += sManager->vsyncManager.step;
                    if (sManager->vsyncManager.interval > sManager->vsyncManager.max) {
                        sManager->vsyncManager.interval = sManager->vsyncManager.max;
                        dir = 0;
                    }
                }
                else {
                    sManager->vsyncManager.interval -= sManager->vsyncManager.step;
                    if (sManager->vsyncManager.interval < sManager->vsyncManager.min) {
                        sManager->vsyncManager.interval = sManager->vsyncManager.min;
                        dir = 1;
                    }
                }
            }
        }
		/* 检查是否产生了 VSYNC 周期或频率更新事件 */
		event = xEventGroupGetBits(APPEventGroup);
		if (APP_BIT_CHECK(event, APP_EVENT_VSYNC_UPDATA)) {
			xEventGroupClearBits(APPEventGroup, APP_EVENT_VSYNC_UPDATA);
			TimerClose(TIMER_PERIPHERAL_0);
			if (sManager->vsyncManager.mode) {
				TimerInitByPeriod(TIMER_PERIPHERAL_0, sManager->vsyncManager.interval * 1000);
			}
			else {
				TimerInitByFre(TIMER_PERIPHERAL_0, sManager->vsyncManager.interval);
			}
			TimerInterruptEnable(TIMER_PERIPHERAL_0, 13);
			TimerStart(TIMER_PERIPHERAL_0);
		}
    }
}

static void vsyncIntervalChangeCallback(TimerHandle_t xTimer)
{
    xEventGroupSetBits(APPEventGroup, APP_EVENT_VSYNC_SW);
}

static void cmdVsyncCallback(CmdParamTypeDef *param)
{
    int16_t index;
    uint16_t receiveLen, i, t;
    uint8_t *receiveArray = UartGetRxBuffer(UART_COM1);

    if (param == NULL) {
        taskENTER_CRITICAL();
        showVsyncStatus();
        taskEXIT_CRITICAL();
        return;
    }
    if (strcmp(param->name, sParFlow) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->vsyncManager.flow = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->vsyncManager.flow = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParFlow, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParPolarity) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strHigh, 4) == 0) {
            sManager->vsyncManager.polarity = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strLow, 3) == 0) {
            sManager->vsyncManager.polarity = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParPolarity, strErrlog1);
        }
        YX012PortSetVsyncPorarity(sManager->vsyncManager.polarity);
    }
    else if (strcmp(param->name, sParSignal) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->vsyncManager.signal = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->vsyncManager.signal = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParSignal, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParMode) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strPeriod, 6) == 0) {
            /* 如果是从频率模式切换过来的，才做处理，否则不做任何处理 */
            if (sManager->vsyncManager.mode == 0) {
                sManager->vsyncManager.interval = 1000 / sManager->vsyncManager.interval;
                sManager->vsyncManager.min = 1000 / sManager->vsyncManager.min;
                sManager->vsyncManager.max = 1000 / sManager->vsyncManager.max;
                sManager->vsyncManager.step = 1;
                sManager->vsyncManager.mode = 1;
            }
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strFrequency, 9) == 0) {
            /* 如果是从周期模式切换过来的，才做处理，否则不做任何处理 */
            if (sManager->vsyncManager.mode == 1) {
                sManager->vsyncManager.interval = 1000 / sManager->vsyncManager.interval;
                sManager->vsyncManager.min = 1000 / sManager->vsyncManager.min;
                sManager->vsyncManager.max = 1000 / sManager->vsyncManager.max;
                sManager->vsyncManager.step = 10;
                sManager->vsyncManager.mode = 0;
            }
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParMode, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParAutoChange) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 1) == 0) {
            if (sManager->vsyncManager.autoChange == 0) {
                if (sTimerHandle != NULL) {
                    xTimerDelete(sTimerHandle, 0);
                    sTimerHandle = NULL;
                }
            }
			
			sTimerHandle = xTimerCreate(sParAutoChange, sManager->vsyncManager.changeTime, pdTRUE, (void *)2, vsyncIntervalChangeCallback);
			if (sTimerHandle != NULL) {
				xTimerStart(sTimerHandle, 0);
				sManager->vsyncManager.autoChange = 1;
			}
			else {
				UartPuts(UART_COM1, "Err: Can't create timer\r\n");
			}
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->vsyncManager.autoChange = 0;
            if (sTimerHandle != NULL) {
                xTimerDelete(sTimerHandle, 0);
                sTimerHandle = NULL;
            }
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParAutoChange, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParAutosend) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->vsyncManager.autoSend = 1;
            vTaskResume(vsyncTaskHandle);
            TimerStop(TIMER_PERIPHERAL_0);
            if (sManager->vsyncManager.mode) {
                TimerInitByPeriod(TIMER_PERIPHERAL_0, sManager->vsyncManager.interval * 1000);
            }
            else {
                TimerInitByFre(TIMER_PERIPHERAL_0, sManager->vsyncManager.interval);
            }
			if (sTimerHandle == NULL) {
				sTimerHandle = xTimerCreate(sParAutoChange, sManager->vsyncManager.changeTime, pdTRUE, (void *)2, vsyncIntervalChangeCallback);
			}
			xTimerStart(sTimerHandle, 0);
			TimerInterruptEnable(TIMER_PERIPHERAL_0, 13);
            TimerStart(TIMER_PERIPHERAL_0);
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->vsyncManager.autoSend = 0;
            vTaskSuspend(vsyncTaskHandle);
            TimerStop(TIMER_PERIPHERAL_0);
			TimerInterruptDisable(TIMER_PERIPHERAL_0);
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParAutosend, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParAutowith) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strEn, 2) == 0) {
            sManager->vsyncManager.autoWith = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strUnen, 4) == 0) {
            sManager->vsyncManager.autoWith = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParAutowith, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParPosition) == 0) {
        if (strncmp((char *)&receiveArray[param->index+1], strBefore, 6) == 0) {
            sManager->vsyncManager.position = 1;
        }
        else if (strncmp((char *)&receiveArray[param->index+1], strAfter, 5) == 0) {
            sManager->vsyncManager.position = 0;
        }
        else {
            UartPrintf(UART_COM1, "%s: %s", sParPosition, strErrlog1);
        }
    }
    else if (strcmp(param->name, sParAutocode) == 0) {
        /* Second level parameters */
        index = StrGetCHIndex((char *)&receiveArray[param->index], '-');
        receiveLen = UartGetReceiveLen(UART_COM1);
        if (index == -1) {
            if (sManager->sendCache == NULL) {
                sManager->sendCache = (uint16_t *)pvPortMalloc(receiveLen);
            }
            if (sManager->sendCache == NULL) {
                UartPuts(UART_COM1, "Err: Can't ger mem\r\n");
                return;
            }
            for (i = 0; i < (receiveLen - param->index - 1) / 2; i++) {
                t = receiveArray[i];
                t <<= 8;
                t |= receiveArray[i+1];
                sManager->sendCache[i] = t;
            }
            sManager->sendCacheLen = i;
        }
        else {
            if (strncmp((char *)&receiveArray[param->index+1+index], sParChar, 4) == 0) {
                if (sManager->sendCache == NULL) {
                    sManager->sendCache = (uint16_t *)pvPortMalloc(receiveLen/4);
                }
                if (sManager->sendCache == NULL) {
                    UartPuts(UART_COM1, "Err: Can't ger mem\r\n");
                    return;
                }
                index = StrGetCHIndex((char *)&receiveArray[param->index+1], ' ');
                sManager->sendCacheLen = StrChar2Hex((char *)&receiveArray[param->index+1+index], 0, receiveLen - param->index - 1 - index, sManager->sendCache, receiveLen/4);
            }
            else if (strncmp((char *)&receiveArray[param->index+1+index], strUnen, 4) == 4) {
                vPortFree(sManager->sendCache);
                sManager->sendCache = NULL;
            }
        }
    }
    else if (strcmp(param->name, sParInterval) == 0) {
        t = StrGetDec((char *)&receiveArray[param->index], 0, 10);
        if (t > sManager->vsyncManager.min && t < sManager->vsyncManager.max) {
            sManager->vsyncManager.interval = t;
			/* 关闭自动切换 */
			sManager->vsyncManager.autoChange = 0;
			if (sTimerHandle != NULL) {
				xTimerStop(sTimerHandle, 0);
			}
			/* 更新事件标志 */
			xEventGroupSetBits(APPEventGroup, APP_EVENT_VSYNC_UPDATA);
        }
        else {
            UartPuts(UART_COM1, "interval: Out of range\r\n");
        }
    }
    else if (strcmp(param->name, sParMin) == 0) {
        t = StrGetDec((char *)&receiveArray[param->index], 0, 10);
		if (t < sManager->vsyncManager.max) {
			if (sManager->vsyncManager.mode) {
				if (t > MIN_VSYNC_ALLOW_PERIOD && t < MAX_VSYNC_ALLOW_PERIOD) {
					sManager->vsyncManager.min = t;
					return;
				}
			}
			else {
				if (t > MIN_VSYNC_ALLOW_FRE && t < MAX_VSYNC_ALLOW_FRE) {
					sManager->vsyncManager.min = t;
					return;
				}
			}
		}
		UartPuts(UART_COM1, "min: Out of range\r\n");
    }
    else if (strcmp(param->name, sParMax) == 0) {
        t = StrGetDec((char *)&receiveArray[param->index], 0, 10);
		if (t > sManager->vsyncManager.min) {
			if (sManager->vsyncManager.mode) {
				if (t > MIN_VSYNC_ALLOW_PERIOD && t < MAX_VSYNC_ALLOW_PERIOD) {
					sManager->vsyncManager.max = t;
					return;
				}
			}
			else {
				if (t > MIN_VSYNC_ALLOW_FRE && t < MAX_VSYNC_ALLOW_FRE) {
					sManager->vsyncManager.max = t;
					return;
				}
			}
		}
		UartPuts(UART_COM1, "max: Out of range\r\n");
    }
    else if (strcmp(param->name, sParStep) == 0) {
        t = StrGetDec((char *)&receiveArray[param->index], 0, 10);
        if (t > sManager->vsyncManager.min && t < sManager->vsyncManager.max) {
            sManager->vsyncManager.step = t;
        }
        else {
            UartPuts(UART_COM1, "step: Out of range\r\n");
        }
    }
    else if (strcmp(param->name, sParWidth) == 0) {
        t = StrGetDec((char *)&receiveArray[param->index], 0, 10);
        if (t < MAX_VSYNC_ALLOW_WIDTH) {
            sManager->vsyncManager.width = t;
        }
        else {
            UartPuts(UART_COM1, "width: Out of range\r\n");
        }
    }
    else if (strcmp(param->name, sParChangetime) == 0) {
        t = StrGetDec((char *)&receiveArray[param->index], 0, 10);
        if (t > MIN_ALLOW_CHANGE_TIME && t < MAX_ALLOW_CHANGE_TIME) {
            sManager->vsyncManager.changeTime = t;
			xTimerStop(sTimerHandle, 0);
			xTimerChangePeriod(sTimerHandle, t, 0);
			xTimerStart(sTimerHandle, 0);
        }
        else {
            UartPuts(UART_COM1, "changetime: Out of range\r\n");
        }
    }
}

void YX012AppInit(void)
{
    static struct YX012ManagerType manger;
    static union CacheArray cache;
    static const uint16_t tmSeq1[] = {0x0000, 0x00C5, 0x0008};
    static const uint16_t tmSeq2[] = {0x0000, 0x00C5, 0x0000};
    static const uint16_t outSeq[] = {0x0000, 0x00C5, 0x0011};
    static const uint16_t oscSeq[] = {0x0001, 0x0004, 0xC407, 0x07C5};
    static const uint16_t ifbSeq1[] = {0x0000, 0x000E, 0x0001};
    static const uint16_t ifbSeq2[] = {0x0002, 0x0010, 0x0FFF, 0x0FFF, 0x0FFF};
    static const uint16_t vfbSeq1[] = {0x0000, 0x000E, 0x0000};
    static const uint16_t vfbSeq2[] = {0x0002, 0x0010, 0x07FF, 0x07FF, 0x07FF};
    static const uint16_t sramSeq1[] = {0x0000, 0x0004, 0xC407};
    static const uint16_t sramSeq2[] = {0x0000, 0x00C5, 0x0003};
    static const uint16_t sramSeq3[] = {0x8005, 0x00FA, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
    static const uint16_t sramCurrect[] = {0x8005, 0x00FA, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x003F};

    uint16_t i;
    BaseType_t xReturn;

    if (sManager == NULL) {
        sManager = &manger;
    }
    sManager->crcen = 0;
    sManager->valien = 0;
    sManager->trace = 0;
	sManager->crcOnOperat = 0;
	sManager->crcOffOperat = 0;
	sManager->readRegOperat = 0;
    sManager->reversed = 0;
    sManager->rxen = 0;
	sManager->regNum = 0;
    sManager->cache = &cache;
    sManager->rxCache = NULL;
	sManager->rxLen = 0;
    sManager->sendCache = NULL;
    sManager->sendCacheLen = 0;

    sManager->vsyncManager.flow = 0;
    sManager->vsyncManager.polarity = 0;
    sManager->vsyncManager.signal = 0;
    sManager->vsyncManager.mode = 1;
    sManager->vsyncManager.autoChange = 1;
    sManager->vsyncManager.autoSend = 0;
    sManager->vsyncManager.autoWith = 0;
    sManager->vsyncManager.position = 1;
    sManager->vsyncManager.interval = 4;
    sManager->vsyncManager.min = 4;
    sManager->vsyncManager.max = 16;
    sManager->vsyncManager.step = 1;
    sManager->vsyncManager.width = 50;
    sManager->vsyncManager.changeTime = 5000;

    sManager->trimManager.tmSeq1 = tmSeq1;
    sManager->trimManager.tmSeq1Len = sizeof(tmSeq1) / sizeof(tmSeq1[0]);
    sManager->trimManager.tmSeq2 = tmSeq2;
    sManager->trimManager.tmSeq2Len = sizeof(tmSeq2) / sizeof(tmSeq2[0]);
    sManager->trimManager.outSeq = outSeq;
    sManager->trimManager.outSeqLen = sizeof(outSeq) / sizeof(outSeq[0]);
    sManager->trimManager.oscSeq = oscSeq;
    sManager->trimManager.oscSeqLen = sizeof(oscSeq) / sizeof(oscSeq[0]);
    sManager->trimManager.ifbSeq1 = ifbSeq1;
    sManager->trimManager.ifbSeq1Len = sizeof(ifbSeq1) / sizeof(ifbSeq1[0]);
    sManager->trimManager.ifbSeq2 = ifbSeq2;
    sManager->trimManager.ifbSeq2Len = sizeof(ifbSeq2) / sizeof(ifbSeq2[0]);
    sManager->trimManager.vfbSeq1 = vfbSeq1;
    sManager->trimManager.vfbSeq1Len = sizeof(vfbSeq1) / sizeof(vfbSeq1[0]);
    sManager->trimManager.vfbSeq2 = vfbSeq2;
    sManager->trimManager.vfbSeq2Len = sizeof(vfbSeq2) / sizeof(vfbSeq2[0]);
    sManager->trimManager.sramSeq1 = sramSeq1;
    sManager->trimManager.sramSeq1Len = sizeof(sramSeq1) / sizeof(sramSeq1[0]);
    sManager->trimManager.sramSeq2 = sramSeq2;
    sManager->trimManager.sramSeq2Len = sizeof(sramSeq2) / sizeof(sramSeq2[0]);
    sManager->trimManager.sramSeq3 = sramSeq3;
    sManager->trimManager.sramSeq3Len = sizeof(sramSeq3) / sizeof(sramSeq3[0]);
    sManager->trimManager.sramCurrectData = sramCurrect;
    sManager->trimManager.sramCurrectDataLen = sizeof(sramCurrect) / sizeof(sramCurrect[0]);
    sManager->trimManager.cache = NULL;
	sManager->trimManager.preview = 0;
	sManager->trimManager.savepr = 0;
	sManager->trimManager.reverse = 0;

    for (i = 0; i < YX012_CACHE_ARRAY_SIZE / 4; i++) {
        sManager->cache->wordArray[i] = 0x00;
    }

    CmdAppend(sCmdYx012, cmdYx012Callback);
    CmdAppendParam(sCmdYx012, sParSend);
    CmdAppendParam(sCmdYx012, sParChar);
	CmdAppendParam(sCmdYx012, sParCrc);
    CmdAppendParam(sCmdYx012, sParRx);
    CmdAppendParam(sCmdYx012, sParVali);
    CmdAppendParam(sCmdYx012, sParTrace);
    CmdAppendParam(sCmdYx012, sParTestmode);
	CmdAppendParam(sCmdYx012, sParPreview);
    CmdAppendParam(sCmdYx012, sParTrim);
	CmdAppendParam(sCmdYx012, sParFree);

    CmdAppend(sCmdVsync, cmdVsyncCallback);
    CmdAppendParam(sCmdVsync, sParFlow);
    CmdAppendParam(sCmdVsync, sParPolarity);
    CmdAppendParam(sCmdVsync, sParAutosend);
    CmdAppendParam(sCmdVsync, sParSignal);
    CmdAppendParam(sCmdVsync, sParMode);
    CmdAppendParam(sCmdVsync, sParPosition);
    CmdAppendParam(sCmdVsync, sParAutoChange);
    CmdAppendParam(sCmdVsync, sParAutocode);
    CmdAppendParam(sCmdVsync, sParMin);
    CmdAppendParam(sCmdVsync, sParMax);
    CmdAppendParam(sCmdVsync, sParStep);
    CmdAppendParam(sCmdVsync, sParWidth);
    CmdAppendParam(sCmdVsync, sParChangetime);

    YX012PortVsyncInit(sManager->vsyncManager.polarity);
    if (vsyncTaskHandle == NULL) {
        taskENTER_CRITICAL();

        xReturn = xTaskCreate(vsyncTask, strVsyncTaskName, 128, NULL, 4, &vsyncTaskHandle);
        if (xReturn != pdPASS) {
            UartPrintf(UART_COM1, "%s create fail\r\n", strVsyncTaskName);
        }
        else {
            vTaskSuspend(vsyncTaskHandle);
        }

        taskEXIT_CRITICAL();
    }
}

void YX012AppCreateAnalyzeRxTask(uint8_t priority)
{
	BaseType_t result = pdPASS;
	
	result = xTaskCreate(analyzeRxTask, strRxTaskName, 384, NULL, priority, &analyzeRxTaskHandle);
	if (result != pdPASS) {
		UartPrintf(UART_COM1, "analyzeRxTask create fail\r\n");
	}
	else {
		vTaskSuspend(analyzeRxTaskHandle);
	}
	YX012EventGroup = xEventGroupCreate();
}
