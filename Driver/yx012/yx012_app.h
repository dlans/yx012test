#ifndef __YX012_APP_H
#define __YX012_APP_H

#include "stdint.h"

#define YX012_CACHE_ARRAY_SIZE 1024

#define YX012_EVENT_RX_DONE	(0x01 << 0)

void YX012AppInit(void);
void YX012AppCreateAnalyzeRxTask(uint8_t priority);

#endif
