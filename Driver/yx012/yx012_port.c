#include "yx012_port.h"
#include "delay.h"
#include "NuMicro.h"

#define YX012_VSYNC 			PD10
#define YX012_VSYNC_SW_SIGNAL	PD11

static uint8_t porarity = 0;

void YX012PortVsyncInit(uint8_t porarity)
{
	SYS->GPD_MFPH &= ~SYS_GPD_MFPH_PD10MFP_Msk;
	SYS->GPD_MFPH |= SYS_GPD_MFPH_PD10MFP_GPIO;
    SYS->GPD_MFPH &= ~SYS_GPD_MFPH_PD11MFP_Msk;
    SYS->GPD_MFPH |= SYS_GPD_MFPH_PD11MFP_GPIO;
	
	GPIO_SetMode(PD, BIT10, GPIO_MODE_OUTPUT);
    GPIO_SetMode(PD, BIT11, GPIO_MODE_OUTPUT);
	
    if (porarity) {
	    YX012_VSYNC = 0;
    }
    else {
        YX012_VSYNC = 1;
    }
    YX012_VSYNC_SW_SIGNAL = 0;
}

void YX012PortGenVsync(uint16_t width)
{
    if (porarity) {
        YX012_VSYNC = 1;
        delayUs(width);
        YX012_VSYNC = 0;
    }
    else {
        YX012_VSYNC = 0;
        delayUs(width);
        YX012_VSYNC = 1;
    }
}

void YX012PortGenSWSignal(uint16_t width)
{
    YX012_VSYNC_SW_SIGNAL = 1;
    delayUs(width);
    YX012_VSYNC_SW_SIGNAL = 0;
}

void YX012PortSetVsyncPorarity(uint16_t po)
{
    if (po) {
        YX012_VSYNC = 0;
    }
    else {
        YX012_VSYNC = 1;
    }
    porarity = po;
}
