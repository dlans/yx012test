#ifndef __YX012_PORT_H
#define __YX012_PORT_H

#include "stdint.h"

void YX012PortVsyncInit(uint8_t porarity);
void YX012PortGenVsync(uint16_t width);
void YX012PortGenSWSignal(uint16_t width);
void YX012PortSetVsyncPorarity(uint16_t po);

#endif
