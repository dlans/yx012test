#ifndef __TEST_H
#define __TEST_H

void TestUtilCRC(void);
void TestSPITest(void);
void TestSpiMSTest(void);
void TestSpiSlaveTest(void);
void TestUartTest(void);

#endif
