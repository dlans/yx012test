#ifndef __CLOCK_H
#define __CLOCK_H

#include "util_cmd.h"
#include "uart_peripheral.h"
#include "spi_peripheral.h"
#include "bpwm_peripheral.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "event_groups.h"
#include "stdint.h"

#define SYSTEM_CONFIG_PULSE_EN		0
#define SYSTEM_UAER_MAIN_STACK_SIZE	1024
#define SYSTEM_DEFAULT_REC_SIZE		1024
#define SYSTEM_MIN_REC_SIZE			16

#define NVIC_PRIORITYGROUP_0         0x00000007U /*!< 0 bits for pre-emption priority 4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         0x00000006U /*!< 1 bits for pre-emption priority 3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         0x00000005U /*!< 2 bits for pre-emption priority 2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         0x00000004U /*!< 3 bits for pre-emption priority 1 bits for subpriority */
#define NVIC_PRIORITYGROUP_4         0x00000003U /*!< 4 bits for pre-emption priority 0 bits for subpriority */

#define SYSTEM_CLK_HIRC_NOT_STABLE (0x01 << 0)
#define SYSTEM_CLK_HXT_NOT_STABLE  (0x01 << 1)
#define SYSTEM_CLK_LXT_NOT_STABLE  (0x01 << 2)

#define SYSTEM_VALUE_SET(val,x)    (val |= x)
#define SYSTEM_VALUE_CLEAR(val,x)  (val &= (~x))
#define SYSTEM_VALUE_CHECK(val,x)  ((val & x) == x ? 1 : 0)

#define SYSTEM_EVENT_UART_GET				(0x01 << 0)
#define SYSTEM_EVENT_UART_REC_DMA_DONE		(0x01 << 1)
#define SYSTEM_EVENT_UART_SIZE_CHANGE		(0x01 << 2)

#define KEY_EVENT_KEY1_PRESS	(0x01 << 0)
#define KEY_EVENT_KEY2_PRESS	(0x01 << 1)
#define KEY_EVENT_KEY3_PRESS	(0x01 << 2)
#define KEY_EVENT_KEY4_PRESS	(0x01 << 3)
#define KEY_EVENT_KEY5_PRESS	(0x01 << 4)
#define KEY_EVENT_KEY6_PRESS	(0x01 << 5)
#define KEY_EVENT_KEY7_PRESS	(0x01 << 6)
#define KEY_EVENT_KEY8_PRESS	(0x01 << 7)

#define SYSTEM_NAME		"M483KGCAE2A"
#define SYSTEM_VERSION	"V0.1.0"
#define SYSTEM_BRANCH	"os"

extern EventGroupHandle_t g_app_events;
extern EventGroupHandle_t g_key_events;
extern CmdManagerType g_cmd_uart_manager;
extern UartPeripheral g_uart1;
extern UartPeripheral g_uart2;
extern SpiPeripheral g_spi0;
extern SpiPeripheral g_spi1;
extern BpwmPeripheral g_bpwm0;
extern BpwmPeripheral g_bpwm1;

uint8_t SystemClockConfig(void);
void SystemSetInterruptGroup(uint32_t PriorityGroup);
void SystemSetReceiveSize(uint16_t size);
uint16_t SystemGetReceiveSize(void);

#if SYSTEM_CONFIG_PULSE_EN
void SystemPulseInit(void);
void SystemPulseGen(uint32_t nop);
#endif

#endif
