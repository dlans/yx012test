#ifndef __UART_PERIPHERAL_H
#define __UART_PERIPHERAL_H

#include "stdint.h"
#include "NuMicro.h"

#define UART_MAX_BAUDRATE 7500000
#define UART_MIN_BAUDRATE 9600

#define UART_RETURN_OK		0
#define UART_RETURN_TIMEOUT	1
#define UART_RETURN_ERROR	2

#define UART_GPIO_TX_IN_CORE		0x00000001
#define UART_GPIO_TX_IN_PERIPHERAL	0x00000002
#define UART_GPIO_RX_IN_CORE		0x00000004
#define UART_GPIO_RX_IN_PERIPHERAL	0x00000008

typedef enum {
    UART_CB_ON_BAUD_CHANGE,
    UART_CB_ON_STOPBIT_CHANGE,
	UART_CB_ON_IT_SEND_DONE,
	UART_CB_ON_IT_REC_DONE,
    UART_CB_ON_DMA_TIMEOUT,
    UART_CB_ON_DMA_SEND_DONE,
	UART_CB_ON_DMA_REC_DONE
} UartCbEnum;

typedef struct {
    uint32_t stopbit;
    uint32_t baudrate;
    uint32_t width;
    uint32_t parity;
} UartInitType;

typedef union {
	uint16_t all;
	
	uint16_t abr_en : 1;
	uint16_t wk_en : 1;
	uint16_t lin_en : 1;
	uint16_t bufferr_en : 1;
	uint16_t rxto_en : 1;
	uint16_t modem_en : 1;
	uint16_t rls_en : 1;
	uint16_t thre_en : 1;
	uint16_t rda_en : 1;
	uint16_t reserved : 7;
} UartITSelect;

typedef struct {
	UartITSelect u;
	
    uint8_t priority;
    uint16_t cnt_send;
    uint16_t cnt_rec;
    uint16_t length_send;
    uint16_t length_rec;
    uint8_t *array_send;
    uint8_t *array_rec;
} UartInterruptType;

typedef struct {
	uint16_t length;
	PDMA_T *instance;
	uint32_t channel;
	uint32_t address;
    uint32_t timeout;
	uint32_t it_priotiry;
} UartDMAType;

typedef struct {
	GPIO_T *group;
	uint32_t pin;
	uint32_t pin_alia;
} UartGPIOType;

typedef struct UartPeripheral {
	uint8_t busy : 1;
    uint8_t it_send : 1;
    uint8_t it_receive : 1;
    uint8_t dma_send : 1;
    uint8_t dma_rec : 1;
    uint8_t block_send : 1;
    uint8_t block_rec : 1;
    uint8_t reversed : 1;
	
	uint16_t recelen;

	uint32_t wait_timeout;
	uint32_t gpio_sta;
	
    UART_T *instance;
    UartInitType init;
    UartInterruptType it;
    UartDMAType dma_tx;
	UartDMAType dma_rx;
	UartGPIOType gpio_tx;
	UartGPIOType gpio_rx;

	void (* cb_on_baud_change)(struct UartPeripheral *uart);
	void (* cb_on_stopbit_change)(struct UartPeripheral *uart);
	void (* cb_on_it_send_done)(struct UartPeripheral *uart);
	void (* cb_on_it_rec_done)(struct UartPeripheral *uart);
	void (* cb_on_dma_timeout)(struct UartPeripheral *uart);
	void (* cb_on_dma_send_done)(struct UartPeripheral *uart);
	void (* cb_on_dma_rec_done)(struct UartPeripheral *uart);
} UartPeripheral;

typedef enum {
	UART_IO_CTL_TX_TO_CORE,
	UART_IO_CTL_TX_TO_PERIPHERAL,
	UART_IO_CTL_RX_TO_CORE,
	UART_IO_CTL_RX_TO_PERIPHERAL,
	UART_IO_CTL_TXRX_TO_CORE,
	UART_IO_CTL_TXRX_TO_PERIPHERAL
} UartIOCtlEnum;

typedef void (* UartCbFunction)(UartPeripheral *uart);

/* UART Init function *****************************************************/
void UartInit(UartPeripheral *uart);
void UartMspInit(UartPeripheral *uart);

/* UART Output function ***************************************************/
uint8_t UartSendByte(UartPeripheral *uart, uint8_t b);
uint8_t UartSend(UartPeripheral *uart, const uint8_t *array, uint16_t len);
uint8_t UartSendIT(UartPeripheral *uart, uint8_t *array, uint16_t length);
uint8_t UartSendDMA(UartPeripheral *uart, uint8_t *array, uint16_t length);

uint8_t UartPuts(UartPeripheral *uart, const char *str);
uint8_t UartPutsEndl(UartPeripheral *uart, const char *str);
int UartPrintf(UartPeripheral *uart, const char *format, ...);
int UartPrintfEndl(UartPeripheral *uart, const char *format, ...);
uint8_t UartPutsDMA(UartPeripheral *uart, const char *str);
uint8_t UartPrintfDMA(UartPeripheral *uart, const char *format, ...);

uint8_t UartReceive(UartPeripheral *uart, uint8_t *array, uint16_t length);
uint8_t UartReceiveIT(UartPeripheral *uart, uint8_t *array, uint16_t length);
uint8_t UartReceiveDMA(UartPeripheral *uart, uint8_t *array, uint16_t length);
void UartReceiveDMAAbort(UartPeripheral *uart);

/* UART Object control function *******************************************/
UartCbFunction UartRegisterCb(UartPeripheral *uart, UartCbEnum cb_enum, UartCbFunction cb);
uint32_t UartGetStopbit(UartPeripheral *uart);
uint16_t UartGetReceiveLen(UartPeripheral *uart);
uint16_t UartGetRecDmaLength(UartPeripheral *uart);
uint32_t UartGetBaud(UartPeripheral *uart);
uint8_t * UartGetRxBuffer(UartPeripheral *uart);
uint32_t UartGetStatus(UartPeripheral *uart);

void UartClearBuffer(UartPeripheral *uart);

/* UART Peripheral control function ***************************************/
uint8_t UartSetSopbit(UartPeripheral *uart, uint8_t stopbit);
uint8_t UartSetBaud(UartPeripheral *uart, uint32_t baud);

void UartWaitSendDone(UartPeripheral *uart);
uint8_t UartCheckSendDone(UartPeripheral *uart);
void UartCheckAndClearError(UartPeripheral *uart);

/* UART IO conrol function ************************************************/
uint8_t UartIOToCore(UartPeripheral *uart, uint8_t withRx);
void UartIOToPeripheral(UartPeripheral *uart, uint8_t withRx);
void UartIOCtl(UartPeripheral *uart, UartIOCtlEnum io_ctl);
void UartTxPullUp(UartPeripheral *uart);
void UartTxPullDown(UartPeripheral *uart);
void UartRxPullUp(UartPeripheral *uart);
void UartRxPullDown(UartPeripheral *uart);

/* UART Object cb function ************************************************/
void UartInterruptHandle(UartPeripheral *uart);
void UartDMATimoutInterruptHandle(UartPeripheral *uart);
void UartDMADoneInterruptHandle(UartPeripheral *uart);

#ifdef USER_DEBUG_LOG
typedef struct UartRegs {
	uint32_t dat;
	uint32_t inten;
	uint32_t fifo;
	uint32_t line;
	uint32_t modem;
	uint32_t modemsts;
	uint32_t fifosts;
	uint32_t intsts;
	uint32_t tout;
	uint32_t baud;
	uint32_t irda;
	uint32_t altctl;
	uint32_t funcsel;
	uint32_t linctl;
	uint32_t brcomp;
	uint32_t wkctl;
	uint32_t wksts;
	uint32_t dwkcomp;
} UartRegs;

void UartDumpRegs(UartPeripheral *uart, UartRegs *regs);
#endif

#endif
