#ifndef __SPI_PERIPHERAL_H
#define __SPI_PERIPHERAL_H

#include "stdint.h"
#include "M480.h"

#define SPI_RETURN_OK			0x00
#define SPI_RETURN_TIMEOUT		0x01
#define SPI_RETURN_ERR			0x02
#define SPI_RETURN_PARAM_ERR    0x03

#define SPI_PIN_OCCUPY_CLK_MSK		0x01
#define SPI_PIN_OCCUPY_MOSI_MSK		0x02
#define SPI_PIN_OCCUPY_MISO_MSK		0x04
#define SPI_PIN_OCCUPY_NSS_MSK		0x08

#define SPI_AUTO_NSS_ENABLE		0x01
#define SPI_AUTO_NSS_DISABLE	0x00

typedef enum SpiTransStatus {
	SPI_TRANS_STAT_IDLE,
	SPI_TRANS_STAT_BLOCK_SEND,
	SPI_TRANS_STAT_BLOCK_REC,
	SPI_TRANS_STAT_BLOCK_SEND_REC,
	SPI_TRANS_STAT_IT_SEND,
	SPI_TRANS_STAT_IT_REC,
	SPI_TRANS_STAT_IT_SEND_REC,
	SPI_TRANS_STAT_DMA_SEND,
	SPI_TRANS_STAT_DMA_REC,
	SPI_TRANS_STAT_DMA_SEND_REC
} SpiTransStatus;

typedef enum {
	SPI_CB_ON_IT_SEND_DONE,
	SPI_CB_ON_IT_REC_DONE,
	SPI_CB_ON_IT_SEND_REC_DONE,
	SPI_CB_ON_DMA_SEND_DONE,
	SPI_CB_ON_DMA_REC_DONE,
	SPI_CB_ON_DMA_SEND_REC_DONE,
} SpiCbEnum;

typedef struct {
	uint32_t device_mode;
	uint32_t speed;
	uint32_t mode;
	uint32_t width;
	uint32_t autonss;
	uint32_t nss_level;
} SpiInitType;

typedef struct {
	uint8_t	priority;
	uint16_t cnt_send;
	uint16_t cnt_rec;
	uint16_t length_send;
	uint16_t length_rec;
	uint8_t *array_send;
	uint8_t *array_rec;
} SpiInterruptType;

typedef struct {
	uint16_t length;
	PDMA_T * instance;
	uint32_t channel;
	uint32_t it_priority;
	uint32_t address;
	uint32_t timeout;
} SpiDmaType;

typedef struct {
	GPIO_T * group;
	uint32_t pin;
} SpiGPIOType;

typedef struct SpiPeripheral {
	uint8_t sending : 1;
	uint8_t receiving : 1;
	uint8_t reserved : 6;
	
	uint8_t iomsk;
	uint32_t wait_timeout;
	
	SpiTransStatus trans_sta;
	SPI_T * instance;
	SpiInitType init;
	SpiInterruptType it;
	SpiDmaType dma_tx;
	SpiDmaType dma_rx;
	SpiGPIOType gpio_clk;
	SpiGPIOType gpio_mosi;
	SpiGPIOType gpio_miso;
	SpiGPIOType gpio_nss;
	
	void (* cb_on_it_send_done)(struct SpiPeripheral *spi);
	void (* cb_on_it_rec_done)(struct SpiPeripheral *spi);
	void (* cb_on_it_send_rec_done)(struct SpiPeripheral *spi);
	void (* cb_on_dma_send_done)(struct SpiPeripheral *spi);
	void (* cb_on_dma_rec_done)(struct SpiPeripheral *spi);
	void (* cb_on_dma_send_rec_done)(struct SpiPeripheral *spi);
} SpiPeripheral;

typedef enum {
	SPI_IO_CTL_CLK_TO_CORE,
	SPI_IO_CTL_CLK_TO_PERIPHERAL,
	SPI_IO_CTL_MOSI_TO_CORE,
	SPI_IO_CTL_MOSI_TO_PERIPHERAL,
	SPI_IO_CTL_MISO_TO_CORE,
	SPI_IO_CTL_MISO_TO_PERIPHERAL,
	SPI_IO_CTL_NSS_TO_CORE,
	SPI_IO_CTL_NSS_TO_PERIPHERAL,
	SPI_IO_CTL_MISO_EDGE_RAISE_DETECT,
	SPI_IO_CTL_MISO_EDGE_FALL_DETECT,
	SPI_IO_CTL_MISO_EDGE_BOTH_DETECT
} SpiIOCtlEnum;

typedef void (* SpiCallbackFunction)(SpiPeripheral *spi);

void SpiInit(SpiPeripheral *spi);
void SpiMspInit(SpiPeripheral *spi);

uint8_t SpiSendOneData(SpiPeripheral *spi, uint32_t data);
uint8_t SpiSend(SpiPeripheral *spi, uint8_t *array, uint16_t length);
uint8_t SpiReceive(SpiPeripheral *spi, uint8_t *array, uint16_t length);
uint8_t SpiSendReceive(SpiPeripheral *spi, uint8_t *arr_send, uint8_t *arr_rec, uint16_t length);

uint8_t SpiSendIT(SpiPeripheral *spi, uint8_t *array, uint16_t length);
uint8_t SpiReceiveIT(SpiPeripheral *spi, uint8_t *array, uint16_t length);
uint8_t SpiSendReceiveIT(SpiPeripheral *spi, uint8_t *arr_send, uint8_t *arr_rec, uint16_t length);
void SpiTransferITAbort(SpiPeripheral *spi);

uint8_t SpiSendDma(SpiPeripheral *spi, uint8_t *array, uint16_t length);
uint8_t SpiReceiveDma(SpiPeripheral *spi, uint8_t *array, uint16_t length);
uint8_t SpiSendReceiveDma(SpiPeripheral *spi, uint8_t *arr_send, uint8_t *arr_rec, uint16_t length);
uint8_t SpiWaitDmaDone(SpiPeripheral *spi);
void SpiTransferDmaAbort(SpiPeripheral *spi);
uint8_t SpiWaitTransDone(SpiPeripheral *spi);

SpiCallbackFunction SpiRegisterCb(SpiPeripheral *spi, SpiCbEnum cb_enum, SpiCallbackFunction cb);
uint8_t SpiSetSpeed(SpiPeripheral *spi, uint32_t speed);
uint8_t SpiSetPSC(SpiPeripheral *spi, uint32_t psc);
uint8_t SpiSetWidth(SpiPeripheral *spi, uint32_t width);
uint8_t SpiSetDeviceMode(SpiPeripheral *spi, uint32_t device_mode);
uint32_t SpiGetMode(SpiPeripheral *spi);
uint32_t SpiGetSpeed(SpiPeripheral *spi);
uint32_t SpiGetWidth(SpiPeripheral *spi);
uint16_t SpiGetRecDmaLength(SpiPeripheral *spi);

uint8_t SpiIOToCore(SpiPeripheral *spi, uint8_t iomsk);
void SpiIOToPeripheral(SpiPeripheral *spi, uint8_t iomsk);
uint8_t SpiIOCtl(SpiPeripheral *spi, SpiIOCtlEnum ioctl);
void SpiMisoExitMspConfig(SpiPeripheral *spi, SpiIOCtlEnum ioctl);
void SpiMisoExitStartDetec(SpiPeripheral *spi);
void SpiMisoExitStopDetec(SpiPeripheral *spi);
uint8_t SpiIOOutput(SpiPeripheral *spi, uint8_t iomsk, uint8_t iostat);

void SpiInterruptHandle(SpiPeripheral *spi);
void SpiDmaDoneHandle(SpiPeripheral *spi);

#endif
