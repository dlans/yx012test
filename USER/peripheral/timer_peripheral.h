#ifndef __TIMER_PERIPHERAL_H
#define __TIMER_PERIPHERAL_H

#include "stdint.h"

#define TIMER_TIMESTAMP_EN	0

#define TIMER2_TIME_BASE 20

typedef enum {
	TIMER_PERIPHERAL_0,
	TIMER_PERIPHERAL_1,
	TIMER_PERIPHERAL_2,
} TIMER_AVAILABLE_LIST;

void TimerInitByFre(TIMER_AVAILABLE_LIST tim, uint16_t fre);
void TimerInitByPeriod(TIMER_AVAILABLE_LIST tim, uint16_t period);
void TimerInterruptEnable(TIMER_AVAILABLE_LIST tim, uint32_t priority);
void TimerInterruptDisable(TIMER_AVAILABLE_LIST tim);
void TimerClose(TIMER_AVAILABLE_LIST tim);
void TimerStart(TIMER_AVAILABLE_LIST tim);
void TimerStop(TIMER_AVAILABLE_LIST tim);

#if TIMER_TIMESTAMP_EN
void TimerTimestampInit(void);
void TimerTimestampRun(void);
void TimerTimestampStop(void);
uint32_t TimerTimestampGetCNT(void);
#endif

#endif
