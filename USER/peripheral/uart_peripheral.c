#include "uart_peripheral.h"
#include "stdarg.h"
#include "string.h"

static char * const sc_endl = "\r\n";

static void uartTxDMAConfig(UartPeripheral *uart);
static void uartRxDMAConfig(UartPeripheral *uart);
static uint8_t waitSendUnlock(UartPeripheral *uart);
static uint8_t waitReceiveUnlock(UartPeripheral *uart);

static void uartTxDMAConfig(UartPeripheral *uart)
{	
	UART_PDMA_DISABLE(uart->instance, UART_INTEN_TXPDMAEN_Msk);
	PDMA_Open(PDMA, 0x01U << uart->dma_tx.channel);
    if (uart->instance == UART1) {
        PDMA_SetTransferMode(PDMA, uart->dma_tx.channel, PDMA_UART1_TX, 0, 0);
    }
    else if (uart->instance == UART2) {
        PDMA_SetTransferMode(PDMA, uart->dma_tx.channel, PDMA_UART2_TX, 0, 0);
    }
	PDMA_SetTransferCnt(PDMA, uart->dma_tx.channel, PDMA_WIDTH_8, uart->dma_tx.length);
	PDMA_SetTransferAddr(PDMA, uart->dma_tx.channel, uart->dma_tx.address, PDMA_SAR_INC, (uint32_t)uart->instance, PDMA_DAR_FIX);
	PDMA_SetBurstType(PDMA, uart->dma_tx.channel, PDMA_REQ_SINGLE, 0);
	PDMA_EnableInt(PDMA, uart->dma_tx.channel, PDMA_INT_TRANS_DONE);
	if (uart->dma_tx.channel < 2) {
		PDMA_SetTimeOut(PDMA, uart->dma_tx.channel, 1, uart->dma_tx.timeout);
		PDMA_EnableInt(PDMA, uart->dma_tx.channel, PDMA_INT_TIMEOUT);
	}
	else {
		PDMA_SetTimeOut(PDMA, uart->dma_tx.channel, 0, uart->dma_tx.timeout);
		PDMA_DisableInt(PDMA, uart->dma_tx.channel, PDMA_INT_TIMEOUT);
	}
	UART_PDMA_ENABLE(uart->instance, UART_INTEN_TXPDMAEN_Msk);
}

static void uartRxDMAConfig(UartPeripheral *uart)
{	
	PDMA_Open(PDMA, 0x1U << uart->dma_rx.channel);
	if (uart->instance == UART1) {
		PDMA_SetTransferMode(PDMA, uart->dma_rx.channel, PDMA_UART1_RX, FALSE, 0);
	}
	else {
		PDMA_SetTransferMode(PDMA, uart->dma_rx.channel, PDMA_UART2_RX, FALSE, 0);
	}
	PDMA_SetTransferCnt(PDMA, uart->dma_rx.channel, PDMA_WIDTH_8, uart->dma_rx.length);
	PDMA_SetTransferAddr(PDMA, uart->dma_rx.channel, (uint32_t)uart->instance, PDMA_SAR_FIX, uart->dma_rx.address, PDMA_DAR_INC);
	PDMA_SetBurstType(PDMA, uart->dma_rx.channel, PDMA_REQ_SINGLE, 0);
	PDMA_EnableInt(PDMA, uart->dma_rx.channel, PDMA_INT_TRANS_DONE);
	if (uart->dma_rx.channel < 2) {
		PDMA_SetTimeOut(PDMA, uart->dma_rx.channel, 1, uart->dma_rx.timeout);
		PDMA_EnableInt(PDMA, uart->dma_rx.channel, PDMA_INT_TIMEOUT);
	}
	else {
		PDMA_SetTimeOut(PDMA, uart->dma_rx.channel, 0, uart->dma_rx.timeout);
		PDMA_DisableInt(PDMA, uart->dma_rx.channel, PDMA_INT_TIMEOUT);
	}
	UART_PDMA_ENABLE(uart->instance, UART_INTEN_RXPDMAEN_Msk);
}

static uint8_t waitSendUnlock(UartPeripheral *uart)
{
	uint32_t timeout = uart->wait_timeout;
	
	while (timeout) {
		if (uart->block_send || uart->it_send || uart->dma_send) {
			timeout--;
		}
		else {
			break;
		}
	}
	if (timeout) {
		return UART_RETURN_OK;
	}
	else {
		return UART_RETURN_TIMEOUT;
	}
}

static uint8_t waitReceiveUnlock(UartPeripheral *uart)
{
	uint32_t timeout = uart->wait_timeout;
	
	while (timeout) {
		if (uart->block_rec || uart->it_receive || uart->dma_rec) {
			timeout--;
		}
		else {
			break;
		}
	}
	if (timeout) {
		return UART_RETURN_OK;
	}
	else {
		return UART_RETURN_TIMEOUT;
	}
}

/**
 * @brief	Uart Init
 * @param   uart UartPeripheral struct
 * @param   baud Baudrate
 */
void UartInit(UartPeripheral *uart)
{
    uart->busy = 0;
    uart->it_send = 0;
    uart->it_receive = 0;
    uart->dma_send = 0;
    uart->dma_rec = 0;
    uart->block_send = 0;
    uart->block_rec = 0;

    uart->recelen = 0;
	uart->gpio_sta = 0;
	uart->gpio_sta |= UART_GPIO_TX_IN_PERIPHERAL;
	uart->gpio_sta |= UART_GPIO_RX_IN_PERIPHERAL;
	
	uart->cb_on_baud_change = NULL;
	uart->cb_on_stopbit_change = NULL;
	uart->cb_on_it_send_done = NULL;
	uart->cb_on_it_rec_done = NULL;
	uart->cb_on_dma_timeout = NULL;
	uart->cb_on_dma_send_done = NULL;
	uart->cb_on_dma_rec_done = NULL;
	
	UartMspInit(uart);
	
	if (uart->it.u.abr_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_ABRIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_ABRIEN_Msk);
	}
	if (uart->it.u.wk_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_WKIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_WKIEN_Msk);
	}
	if (uart->it.u.lin_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_LINIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_LINIEN_Msk);
	}
	if (uart->it.u.bufferr_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_BUFERRIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_BUFERRIEN_Msk);
	}
	if (uart->it.u.rxto_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_RXTOIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_RXTOIEN_Msk);
	}
	if (uart->it.u.modem_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_MODEMIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_MODEMIEN_Msk);
	}
	if (uart->it.u.rls_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_RLSIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_RLSIEN_Msk);
	}
	if (uart->it.u.thre_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_THREIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_THREIEN_Msk);
	}
	if (uart->it.u.rda_en) {
		UART_ENABLE_INT(uart->instance, UART_INTEN_RDAIEN_Msk);
	}
	else {
		UART_DISABLE_INT(uart->instance, UART_INTEN_RDAIEN_Msk);
	}
	
	UART_SetLineConfig(uart->instance, uart->init.baudrate, uart->init.width, uart->init.parity, uart->init.stopbit);
	UART_Open(uart->instance, uart->init.baudrate);
}

__weak void UartMspInit(UartPeripheral *uart)
{
	uint32_t prioritygroup = 0x00U;
	
	SYS_UnlockReg();
	
	if (uart->instance == UART0) {
		CLK_EnableModuleClock(UART0_MODULE);
		CLK_SetModuleClock(UART0_MODULE, CLK_CLKSEL1_UART0SEL_PLL, CLK_CLKDIV0_UART0(2));
		SystemCoreClockUpdate();
		
		SYS->GPC_MFPH &= (~SYS_GPC_MFPH_PC12MFP_Msk);
		SYS->GPC_MFPH |= SYS_GPC_MFPH_PC12MFP_UART0_TXD;
		SYS->GPC_MFPH &= (~SYS_GPC_MFPH_PC11MFP_Msk);
		SYS->GPC_MFPH |= SYS_GPC_MFPH_PC11MFP_UART0_RXD;
		
		NVIC_EnableIRQ(UART0_IRQn);
		prioritygroup = NVIC_GetPriorityGrouping();
		NVIC_SetPriority(UART0_IRQn, NVIC_EncodePriority(prioritygroup, uart->it.priority, 0));
	}
	else if (uart->instance == UART1) {
		CLK_EnableModuleClock(UART1_MODULE);
		CLK_SetModuleClock(UART1_MODULE, CLK_CLKSEL1_UART1SEL_PLL, CLK_CLKDIV0_UART1(2));
		SystemCoreClockUpdate();
		
		SYS->GPB_MFPL &= ~(SYS_GPB_MFPL_PB2MFP_Msk | SYS_GPB_MFPL_PB3MFP_Msk);
		SYS->GPB_MFPL |= (SYS_GPB_MFPL_PB2MFP_UART1_RXD | SYS_GPB_MFPL_PB3MFP_UART1_TXD);
		
		NVIC_EnableIRQ(UART1_IRQn);
		prioritygroup = NVIC_GetPriorityGrouping();
		NVIC_SetPriority(UART1_IRQn, NVIC_EncodePriority(prioritygroup, uart->it.priority, 0));
	}
	else if (uart->instance == UART2) {
		CLK_EnableModuleClock(UART2_MODULE);
		CLK_SetModuleClock(UART2_MODULE, CLK_CLKSEL3_UART2SEL_PLL, CLK_CLKDIV4_UART2(2));
		SystemCoreClockUpdate();
		
		SYS->GPC_MFPH &= ~SYS_GPC_MFPH_PC13MFP_Msk;
		SYS->GPC_MFPH |= SYS_GPC_MFPH_PC13MFP_UART2_TXD;
		SYS->GPD_MFPH &= ~SYS_GPD_MFPH_PD12MFP_Msk;
		SYS->GPD_MFPH |= SYS_GPD_MFPH_PD12MFP_UART2_RXD;
		
		NVIC_EnableIRQ(UART2_IRQn);
		prioritygroup = NVIC_GetPriorityGrouping();
		NVIC_SetPriority(UART2_IRQn, NVIC_EncodePriority(prioritygroup, uart->it.priority, 0));
	}
	
	if (uart->dma_rx.instance == PDMA) {
		CLK_EnableModuleClock(PDMA_MODULE);
		SystemCoreClockUpdate();
		
		if (NVIC_GetActive(PDMA_IRQn) == 0) {
			NVIC_EnableIRQ(PDMA_IRQn);
			prioritygroup = NVIC_GetPriorityGrouping();
			NVIC_SetPriority(PDMA_IRQn, NVIC_EncodePriority(prioritygroup, uart->dma_tx.it_priotiry, 0));
		}
	}
	
	SYS_LockReg();
}

uint8_t UartSendByte(UartPeripheral *uart, uint8_t b)
{
	uint32_t timeout;
	
	if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}

    uart->block_send = 1;
	timeout = uart->wait_timeout;
	while (uart->instance->FIFOSTS & UART_FIFOSTS_TXFULL_Msk) {
		timeout--;
		if (timeout == 0) {
			uart->block_send = 0;
			return UART_RETURN_TIMEOUT;
		}
	}
	uart->instance->DAT = b;
    uart->block_send = 0;
	
	return UART_RETURN_OK;
}

/**
 * @brief	UART directly send
 * @param   com  Com list
 * @param   data The data be send
 * @param   len  The length of data
 */
uint8_t UartSend(UartPeripheral *uart, const uint8_t *array, uint16_t len)
{
	uint16_t i;
    uint32_t timeout;
	
	if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	uart->block_send = 1;
	timeout = uart->wait_timeout;
	for (i = 0; i < len; i++) {
		while (uart->instance->FIFOSTS & UART_FIFOSTS_TXFULL_Msk) {
			timeout--;
			if (timeout == 0) {
				uart->block_send = 0;
				return UART_RETURN_TIMEOUT;
			}
		}
		uart->instance->DAT = array[i];
	}
	uart->block_send = 0;
	
	return UART_RETURN_OK;
}

uint8_t UartSendIT(UartPeripheral *uart, uint8_t *array, uint16_t length)
{
    if ((array == NULL) || (length == 0)) {
		return UART_RETURN_ERROR;
	}

    if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->it_send = 1;
    uart->it.array_send = array;
    uart->it.length_send = length;
    uart->it.cnt_send = 1;
    uart->instance->DAT = array[0];
	UART_ENABLE_INT(uart->instance, UART_INTEN_THREIEN_Msk);
	
	return UART_RETURN_OK;
}

uint8_t UartSendDMA(UartPeripheral *uart, uint8_t *array, uint16_t length)
{
    if ((length == 0) || (array == NULL)) {
        return UART_RETURN_ERROR;
    }

    if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->dma_send = 1;
	uart->dma_tx.address = (uint32_t)array;
	uart->dma_tx.length = length;
    uartTxDMAConfig(uart);
	
	return UART_RETURN_OK;
}

/**
 * @brief   UART send string, like puts()
 * @param   com Comlist
 * @param   str The string be send
 */
uint8_t UartPuts(UartPeripheral *uart, const char *str)
{
    uint16_t i;
	uint32_t timeout;

    if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->block_send = 1;
	timeout = uart->wait_timeout;
    for (i = 0; str[i]; i++) {
        while (uart->instance->FIFOSTS & UART_FIFOSTS_TXFULL_Msk) {
			timeout--;
			if (timeout == 0) {
				uart->block_send = 0;
				return UART_RETURN_TIMEOUT;
			}
		}
        uart->instance->DAT = str[i];
    }
    uart->block_send = 0;
	
	return UART_RETURN_OK;
}

uint8_t UartPutsEndl(UartPeripheral *uart, const char *str)
{
	uint8_t ret;
	
	if (str != NULL) {
		ret = UartPuts(uart, str);
	}
	else {
		ret = UART_RETURN_OK;
	}
	if (ret == UART_RETURN_OK) {
		ret = UartPuts(uart, sc_endl);
	}
	
	return ret;
}

/**
 * @brief   Send format string, like printf()
 * @param   com    Com list
 * @param   format The Format string
 * @retval  The length of send string
 * @note    The length of format string must less then 128 bytes
 */
int UartPrintf(UartPeripheral *uart, const char *format, ...)
{
	static char buffer[128];
	int length;
	uint8_t i;
	uint32_t timeout;
	
    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	uart->block_send = 1;
	timeout = uart->wait_timeout;
	length = strlen(buffer);
    for (i = 0; i < length; i++) {
        while (uart->instance->FIFOSTS & UART_FIFOSTS_TXFULL_Msk) {
			timeout--;
			if (timeout == 0) {
				uart->block_send = 0;
				return UART_RETURN_TIMEOUT;
			}
		}
        uart->instance->DAT = buffer[i];
    }
    uart->block_send = 0;

    return length;
}

int UartPrintfEndl(UartPeripheral *uart, const char *format, ...)
{
	static char buffer[130];
	int length;
	uint8_t i;
	uint32_t timeout;
	
	va_list args;
	va_start(args, format);
	vsprintf(buffer, format, args);
	va_end(args);
	
	if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	uart->block_send = 1;
	timeout = uart->wait_timeout;
	length = strlen(buffer);
	buffer[length++] = '\r';
	buffer[length++] = '\n';
	for (i = 0; i < length; i++) {
		while (uart->instance->FIFOSTS & UART_FIFOSTS_TXFULL_Msk) {
			timeout--;
			if (timeout == 0) {
				uart->block_send = 0;
				return UART_RETURN_ERROR;
			}
		}
		uart->instance->DAT = buffer[i];
	}
	uart->block_send = 0;
	
	return length;
}

uint8_t UartPutsDMA(UartPeripheral *uart, const char *str)
{
    uint16_t length = strlen(str);

    if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->dma_send = 1;
    uart->dma_tx.address = (uint32_t)str;
	uart->dma_tx.length = length;
    uartTxDMAConfig(uart);
	
	return UART_RETURN_OK;
}

uint8_t UartPrintfDMA(UartPeripheral *uart, const char *format, ...)
{
    static char buffer[128];

    uint16_t len;

    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    len = strlen(buffer);

    if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->dma_send = 1;
    uart->dma_tx.address = (uint32_t)buffer;
	uart->dma_tx.length = len;
    uartTxDMAConfig(uart);
	
	return UART_RETURN_OK;
}

uint8_t UartReceive(UartPeripheral *uart, uint8_t *array, uint16_t length)
{
	uint16_t i;
	uint32_t fifo, timeout;

    if (waitReceiveUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->block_rec = 1;
    fifo = uart->instance->FIFO;
    fifo &= UART_FIFO_RFITL_Msk;
	uart->instance->FIFO &= ~(UART_FIFO_RFITL_Msk);
	timeout = uart->wait_timeout;
	for (i = 0; i < length; i++) {
		while (UART_IS_RX_READY(uart->instance) == 0) {
			timeout--;
			if (timeout == 0) {
				return UART_RETURN_TIMEOUT;
			}
		}
		array[i] = UART_READ(uart->instance);
	}
	uart->instance->FIFO |= fifo;
    uart->block_rec = 0;
	
	return UART_RETURN_OK;
}

uint8_t UartReceiveIT(UartPeripheral *uart, uint8_t *array, uint16_t length)
{
    if (waitReceiveUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    uart->it_receive = 1;
    uart->it.array_rec = array;
    uart->it.cnt_rec = 0;
    uart->it.length_rec = length;
	
	UART_ENABLE_INT(uart->instance, UART_INTEN_RDAIEN_Msk);
	
	return UART_RETURN_OK;
}

uint8_t UartReceiveDMA(UartPeripheral *uart, uint8_t *array, uint16_t length)
{
	if (waitReceiveUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	uart->dma_rec = 1;
	uart->dma_rx.address = (uint32_t)array;
	uart->dma_rx.length = length;
	
	uart->instance->FIFOSTS |= UART_FIFOSTS_PEF_Msk;
	uart->instance->FIFOSTS |= UART_FIFOSTS_FEF_Msk;
	uart->instance->FIFOSTS |= UART_FIFOSTS_BIF_Msk;
	
	uartRxDMAConfig(uart);
	
	return UART_RETURN_OK;
}

void UartReceiveDMAAbort(UartPeripheral *uart)
{
	UART_PDMA_DISABLE(uart->instance, UART_INTEN_RXPDMAEN_Msk);
	PDMA_STOP(PDMA, uart->dma_rx.channel);
	PDMA_DisableInt(PDMA, uart->dma_rx.channel, PDMA_INT_TRANS_DONE);
	if (uart->dma_rx.channel < 2) {
		PDMA_DisableInt(PDMA, uart->dma_rx.channel, PDMA_INT_TIMEOUT);
	}
	uart->dma_rec = 0;
}

UartCbFunction UartRegisterCb(UartPeripheral *uart, UartCbEnum cb_enum, UartCbFunction cb)
{
	UartCbFunction old = NULL;
	
	switch (cb_enum) {
		case UART_CB_ON_BAUD_CHANGE:
			old = uart->cb_on_baud_change;
			uart->cb_on_baud_change = cb;
			break;
		case UART_CB_ON_STOPBIT_CHANGE:
			old = uart->cb_on_stopbit_change;
			uart->cb_on_stopbit_change = cb;
			break;
		case UART_CB_ON_IT_SEND_DONE:
			old = uart->cb_on_it_send_done;
			uart->cb_on_it_send_done = cb;
			break;
		case UART_CB_ON_IT_REC_DONE:
			old = uart->cb_on_it_rec_done;
			uart->cb_on_it_rec_done = cb;
			break;
		case UART_CB_ON_DMA_TIMEOUT:
			old = uart->cb_on_dma_timeout;
			uart->cb_on_dma_timeout = cb;
			break;
		case UART_CB_ON_DMA_SEND_DONE:
			old = uart->cb_on_dma_send_done;
			uart->cb_on_dma_send_done = cb;
			break;
		case UART_CB_ON_DMA_REC_DONE:
			old = uart->cb_on_dma_rec_done;
			uart->cb_on_dma_rec_done = cb;
			break;
		default:
			break;
	}
	
	return old;
}

uint32_t UartGetStopbit(UartPeripheral *uart)
{
    return uart->init.stopbit;
}

/**
 * @brief	Get receive length, only for case as follow:
 * 			- Use UartReceiveIT
 * 			- Use UartReceiveDMA and DMA channel < 2
 */
uint16_t UartGetReceiveLen(UartPeripheral *uart)
{
    return uart->recelen;
}

uint16_t UartGetRecDmaLength(UartPeripheral *uart)
{
	uint32_t dsct_ctl = PDMA->DSCT[uart->dma_rx.channel].CTL;
	uint16_t reclen = dsct_ctl >> PDMA_DSCT_CTL_TXCNT_Pos;

	reclen = uart->dma_rx.length - reclen;
	reclen--;

	return reclen;
}

uint32_t UartGetBaud(UartPeripheral *uart)
{
    return uart->init.baudrate;
}

uint8_t * UartGetRxBuffer(UartPeripheral *uart)
{
    return (uint8_t *)uart->dma_rx.address;
}

uint32_t UartGetStatus(UartPeripheral *uart)
{
	uint32_t status = uart->busy;
	
	status |= (uart->it_send << 1);
	status |= (uart->it_receive << 2);
	status |= (uart->dma_send << 3);
	status |= (uart->dma_rec << 4);
	status |= (uart->block_send << 5);
	status |= (uart->block_rec << 6);
	
	return status;
}

void UartClearBuffer(UartPeripheral *uart)
{
    uint16_t i;

    uint8_t *array = (uint8_t *)uart->dma_rx.address;

    for (i = 0; i < uart->dma_rx.length; i++) {
        array[i] = 0;
    }
}

uint8_t UartSetSopbit(UartPeripheral *uart, uint8_t stopbit)
{
	if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	if (waitReceiveUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	uart->instance->LINE &= (~UART_LINE_NSB_Msk);
	if (stopbit == 1) {
	}
	else if (stopbit == 2) {
		uart->instance->LINE |= UART_LINE_NSB_Msk;
	}
	else {
		return UART_RETURN_ERROR;
	}
	uart->init.stopbit = stopbit;
	
	return UART_RETURN_OK;
}

uint8_t UartSetBaud(UartPeripheral *uart, uint32_t baud)
{
	if (waitSendUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
	if (waitReceiveUnlock(uart) == UART_RETURN_TIMEOUT) {
		return UART_RETURN_TIMEOUT;
	}
	
    UART_Close(uart->instance);
	UART_Open(uart->instance, baud);
	uart->init.baudrate = baud;
	if (uart->init.stopbit == 2) {
		UartSetSopbit(uart, 2);
	}
	
	return UART_RETURN_OK;
}

void UartWaitSendDone(UartPeripheral *uart)
{
	while (!(uart->instance->FIFOSTS & UART_FIFOSTS_TXEMPTYF_Msk)) {}
}

uint8_t UartCheckSendDone(UartPeripheral *uart)
{
    if (uart->instance->FIFOSTS & UART_FIFOSTS_TXEMPTYF_Msk) {
        return 1;
    }
    else {
        return 0;
    }
}

void UartCheckAndClearError(UartPeripheral *uart)
{
	uint32_t fifosts = uart->instance->FIFOSTS;
	
	if (((fifosts & UART_FIFOSTS_FEF_Msk) == UART_FIFOSTS_FEF_Msk) ||
		((fifosts & UART_FIFOSTS_BIF_Msk) == UART_FIFOSTS_BIF_Msk) ||
		((fifosts & UART_FIFOSTS_RXOVIF_Msk) == UART_FIFOSTS_RXOVIF_Msk)) {
		UART2->FIFOSTS |= UART_FIFOSTS_RXOVIF_Msk;
		UART2->FIFO |= UART_FIFO_RXRST_Msk;
	}
}

/**
 * @brief   IO 切换到内核控制
 * @param   com    端口号
 * @param   withRx RX IO 也进行切换
 */
uint8_t UartIOToCore(UartPeripheral *uart, uint8_t withRx)
{
	uint32_t status = UartGetStatus(uart);
	if (status != 0) {
		return 1;
	}
	uart->busy = 1;
	
    if (uart->instance == UART1) {
        GPIO_SetPullCtl(PB, BIT3, GPIO_PUSEL_DISABLE);
        GPIO_SetMode(PB, BIT3, GPIO_MODE_OUTPUT);
        PB3 = 1;
        SYS->GPB_MFPL &= (~SYS_GPB_MFPL_PB3MFP_Msk);
        if (withRx) {
            GPIO_SetPullCtl(PB, BIT2, GPIO_PUSEL_DISABLE);
            GPIO_SetMode(PB, BIT2, GPIO_MODE_OUTPUT);
            PB2 = 1;
            SYS->GPB_MFPL &= (~SYS_GPB_MFPL_PB2MFP_Msk);
        }
    }
    else if (uart->instance == UART2) {
        GPIO_SetSlewCtl(PC, BIT13, GPIO_PUSEL_DISABLE);
        GPIO_SetMode(PC, BIT13, GPIO_MODE_OUTPUT);
        PC13 = 1;
        SYS->GPC_MFPH &= (~SYS_GPC_MFPH_PC13MFP_Msk);
        if (withRx) {
            GPIO_SetSlewCtl(PD, BIT12, GPIO_PUSEL_DISABLE);
            GPIO_SetMode(PD, BIT12, GPIO_MODE_OUTPUT);
            PD12 = 1;
            SYS->GPD_MFPH &= (~SYS_GPD_MFPH_PD12MFP_Msk);
        }
    }
	
	return 0;
}

void UartIOToPeripheral(UartPeripheral *uart, uint8_t withRx)
{
    if (uart->instance == UART1) {
        SYS->GPB_MFPL &= (~SYS_GPB_MFPL_PB3MFP_Msk);
        SYS->GPB_MFPL |= SYS_GPB_MFPL_PB3MFP_UART1_TXD;
        if (withRx) {
            SYS->GPB_MFPL &= (~SYS_GPB_MFPL_PB2MFP_Msk);
            SYS->GPB_MFPL |= (~SYS_GPB_MFPL_PB2MFP_UART1_RXD);
        }
    }
    else if (uart->instance == UART2) {
        SYS->GPC_MFPH &= ~SYS_GPC_MFPH_PC13MFP_Msk;
        SYS->GPC_MFPH  |= SYS_GPC_MFPH_PC13MFP_UART2_TXD;
        if (withRx) {
            SYS->GPD_MFPH &= ~SYS_GPD_MFPH_PD12MFP_Msk;
            SYS->GPD_MFPH |= SYS_GPD_MFPH_PD12MFP_UART2_RXD;
        }
    }
	
	uart->busy = 0;
}

__weak void UartIOCtl(UartPeripheral *uart, UartIOCtlEnum io_ctl)
{
	if (io_ctl == UART_IO_CTL_TX_TO_CORE) {
		if (uart->instance == UART1) {
			SYS->GPB_MFPL &= (~SYS_GPB_MFPL_PB3MFP_Msk);
			SYS->GPB_MFPL |= SYS_GPB_MFPL_PB3MFP_GPIO;
		}
		else {
			SYS->GPC_MFPH &= (~SYS_GPC_MFPH_PC13MFP_Msk);
			SYS->GPC_MFPH |= SYS_GPC_MFPH_PC13MFP_GPIO;
		}
		GPIO_SetMode(uart->gpio_tx.group, uart->gpio_tx.pin, GPIO_MODE_OUTPUT);
		GPIO_SetSlewCtl(uart->gpio_tx.group, uart->gpio_tx.pin, GPIO_PUSEL_DISABLE);
	}
	else if (io_ctl == UART_IO_CTL_TX_TO_PERIPHERAL) {
		if (uart->instance == UART1) {
			SYS->GPB_MFPL &= (~SYS_GPB_MFPL_PB3MFP_Msk);
			SYS->GPB_MFPL |= SYS_GPB_MFPL_PB3MFP_UART1_TXD;
		}
		else {
			SYS->GPC_MFPH &= ~SYS_GPC_MFPH_PC13MFP_Msk;
			SYS->GPC_MFPH  |= SYS_GPC_MFPH_PC13MFP_UART2_TXD;
		}
	}
}

void UartTxPullUp(UartPeripheral *uart)
{
    if (uart->instance == UART1) {
        PB3 = 1;
    }
    else if (uart->instance == UART2) {
        PC13 = 1;
    }
}

void UartTxPullDown(UartPeripheral *uart)
{
    if (uart->instance == UART1) {
        PB3 = 0;
    }
    else if (uart->instance == UART2) {
        PC13 = 0;
    }
}

void UartRxPullUp(UartPeripheral *uart)
{
    if (uart->instance == UART1) {
        PB2 = 1;
    }
    else if (uart->instance == UART2) {
        PD12 = 1;
    }
}

void UartRxPullDown(UartPeripheral *uart)
{
    if (uart->instance == UART1) {
        PB2 = 0;
    }
    else if (uart->instance == UART2) {
        PD12 = 0;
    }
}

void UartInterruptHandle(UartPeripheral *uart)
{
    uint32_t fifosts;

    if (UART_GET_INT_FLAG(uart->instance, UART_INTEN_RDAIEN_Msk)) {
        UART_ClearIntFlag(uart->instance, UART_INTEN_RDAIEN_Msk);
        if (uart->it_receive) {
            uart->it.array_rec[uart->it.cnt_rec] = UART_READ(uart->instance);
            uart->it.cnt_rec++;
            if (uart->it.cnt_rec == uart->it.length_rec) {
                UART_DISABLE_INT(uart->instance, UART_INTEN_RDAIEN_Msk);
                if (uart->cb_on_it_rec_done != NULL) {
                    uart->cb_on_it_rec_done(uart);
                }
                uart->it_receive = 0;
            }
        }
		else {
			(void)UART_READ(uart->instance);
		}
    }
    else if (UART_GET_INT_FLAG(uart->instance, UART_INTSTS_BUFERRINT_Msk)) {
        UART_ClearIntFlag(uart->instance, UART_INTSTS_BUFERRINT_Msk);
    }
    else if (UART_GET_INT_FLAG(uart->instance, UART_INTSTS_THREIF_Msk)) {
		UART_DISABLE_INT(uart->instance, UART_INTEN_THREIEN_Msk);
        if (uart->it_send) {
            UART_WRITE(uart->instance, uart->it.array_send[uart->it.cnt_send]);
            uart->it.cnt_send++;
            if (uart->it.cnt_send == uart->it.length_send) {
                if (uart->cb_on_it_send_done != NULL) {
                    uart->cb_on_it_send_done(uart);
                }
                uart->it_send = 0;
            }
			else {
				UART_ENABLE_INT(uart->instance, UART_INTEN_THREIEN_Msk);
			}
        }
    }
	else if (UART_GET_INT_FLAG(uart->instance, UART_INTSTS_RLSIF_Msk)) {
		UART_ClearIntFlag(uart->instance, UART_INTSTS_RLSINT_Msk);
	}

    fifosts = uart->instance->FIFOSTS;
    if ((fifosts & UART_FIFOSTS_RXOVIF_Msk) == UART_FIFOSTS_RXOVIF_Msk) {
        uart->instance->FIFOSTS |= UART_FIFOSTS_RXOVIF_Msk;
        uart->instance->FIFO |= UART_FIFO_RXRST_Msk;
    }
	if ((fifosts & UART_FIFOSTS_FEF_Msk) == UART_FIFOSTS_FEF_Msk) {
		uart->instance->FIFOSTS |= UART_FIFOSTS_FEF_Msk;
		uart->instance->FIFO |= UART_FIFO_RXRST_Msk;
	}
	if ((fifosts & UART_FIFOSTS_BIF_Msk) == UART_FIFOSTS_BIF_Msk) {
		uart->instance->FIFOSTS |= UART_FIFOSTS_BIF_Msk;
		uart->instance->FIFO |= UART_FIFO_RXRST_Msk;
	}
}

void UartDMATimoutInterruptHandle(UartPeripheral *uart)
{
    uint32_t dsct_ctl;
	
	if (uart->dma_send) {
		PDMA_SetTimeOut(PDMA, uart->dma_tx.channel, 0, uart->dma_tx.timeout);
		PDMA_STOP(PDMA, uart->dma_tx.channel);
		PDMA_CLR_TMOUT_FLAG(PDMA, uart->dma_tx.channel);
		if (uart->cb_on_dma_timeout != NULL) {
			uart->cb_on_dma_timeout(uart);
		}
		uart->dma_send = 0;
	}
	if (uart->dma_rec) {
		PDMA_SetTimeOut(PDMA, uart->dma_rx.channel, 0, uart->dma_rx.timeout);
		PDMA_STOP(PDMA, uart->dma_rx.channel);
		PDMA_CLR_TMOUT_FLAG(PDMA, uart->dma_rx.channel);
		dsct_ctl = PDMA->DSCT[uart->dma_rx.channel].CTL;
		uart->recelen = dsct_ctl >> PDMA_DSCT_CTL_TXCNT_Pos;
		uart->recelen = uart->dma_rx.length - uart->recelen;
		uart->recelen--;
		if (uart->cb_on_dma_timeout != NULL) {
			uart->cb_on_dma_timeout(uart);
		}
		uart->dma_rec = 0;
	}
}

void UartDMADoneInterruptHandle(UartPeripheral *uart)
{
	if (uart->dma_send) {
		PDMA->CHCTL &= ~((uint32_t)(0x1U << uart->dma_tx.channel));
		uart->dma_send = 0;
		if (uart->cb_on_dma_send_done != NULL) {
			uart->cb_on_dma_send_done(uart);
		}
	}
	if (uart->dma_rec) {
		PDMA->CHCTL &= ~((uint32_t)(0x1U << uart->dma_rx.channel));
		uart->recelen = uart->dma_rx.length;
		uart->dma_rec = 0;
		if (uart->cb_on_dma_rec_done != NULL) {
			uart->cb_on_dma_rec_done(uart);
		}
	}
}

#ifdef USER_DEBUG_LOG
void UartDumpRegs(UartPeripheral *uart, UartRegs *regs)
{
	regs->dat = uart->instance->DAT;
	regs->inten = uart->instance->INTEN;
	regs->fifo = uart->instance->FIFO;
	regs->line = uart->instance->LINE;
	regs->modem = uart->instance->MODEM;
	regs->modemsts = uart->instance->MODEMSTS;
	regs->fifosts = uart->instance->FIFOSTS;
	regs->intsts = uart->instance->INTSTS;
	regs->tout = uart->instance->TOUT;
	regs->baud = uart->instance->BAUD;
	regs->irda = uart->instance->IRDA;
	regs->altctl = uart->instance->ALTCTL;
	regs->funcsel = uart->instance->FUNCSEL;
	regs->linctl = uart->instance->LINCTL;
	regs->brcomp = uart->instance->BRCOMP;
	regs->wkctl = uart->instance->WKCTL;
	regs->wksts = uart->instance->WKSTS;
	regs->dwkcomp = uart->instance->DWKCOMP;
}
#endif
