#ifndef __BPWM_PERIPHERAL_H
#define __BPWM_PERIPHERAL_H

#include "NuMicro.h"
#include "stdint.h"
#include "stdio.h"

typedef enum {
	BPWM_PERIPHERAL_0,
	BPWM_PERIPHERAL_1
} BPWM_AVAILABLE_LIST;

typedef struct BpwmPeripheral {
	uint8_t ch_en_msk;

	BPWM_T *instance;
	uint32_t fre_target;
	uint32_t fre_real;
	uint8_t duty[BPWM_CHANNEL_NUM];
} BpwmPeripheral;

void BpwmInit(BpwmPeripheral *bpwm);
void BpwmMspInit(BpwmPeripheral *bpwm);

void BPWMSetEnable(BPWM_AVAILABLE_LIST bpwm, uint8_t newStatus);
void BPWMSetFre(BPWM_AVAILABLE_LIST bpwm, uint32_t fre);
void BPWMSetDuty(BPWM_AVAILABLE_LIST bpwm, uint16_t duty, uint8_t ch);
uint32_t BPWMGetFre(BPWM_AVAILABLE_LIST bpwm);
double BPWMGetFreReal(BPWM_AVAILABLE_LIST bpwm);
uint16_t BPWMGetDuty(BPWM_AVAILABLE_LIST bpwm, uint16_t ch);
uint8_t BPWMGetOnOff(BPWM_AVAILABLE_LIST bpwm);

#endif
