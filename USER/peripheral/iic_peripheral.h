#ifndef __IIC_PERIPHERAL_H
#define __IIC_PERIPHERAL_H

#include "M480.h"
#include "stdint.h"

#define HARD_IIC_CH0_PDMA_CH 3

typedef struct {
	uint32_t speed;
} IICInitType;

typedef struct {
	PDMA_T *instance;
	uint32_t channel;
	uint32_t address;
	uint32_t length;
} IICDmaType;

typedef struct {
	uint8_t priority;
	uint16_t cnt_send;
	uint16_t cnt_rec;
	uint16_t length_send;
	uint16_t length_rec;
	uint8_t *array_send;
	uint8_t *array_rec;
} IICITType;

typedef struct IICPeripheral {
	uint8_t busy : 1;
	uint8_t block_send : 1;
	uint8_t block_receive : 1;
	uint8_t it_send : 1;
	uint8_t it_receive : 1;
	uint8_t dma_send : 1;
	uint8_t dma_receive : 1;
	uint8_t reversed : 1;
	
	I2C_T *instance;
	IICDmaType dma;
	IICITType it;
} IICPeripheral;

typedef enum {
	HARD_IIC_CH0
} HardIICCHEnum;

void HardIICDeInit(HardIICCHEnum iicch, uint32_t speed);
void HardIICEnableInt(HardIICCHEnum iicch);
void HardIICDisableInt(HardIICCHEnum iicch);

void HardIICSendOneData(HardIICCHEnum iicch, uint8_t data);
uint8_t HardIICWriteOneByte(HardIICCHEnum iicch, uint8_t slAddr, uint8_t data);
uint32_t HardIICWriteByteArray(HardIICCHEnum iicch, uint8_t slAddr, uint8_t *data, uint16_t len);
uint8_t HardIICWriteOneReg(HardIICCHEnum iicch, uint8_t slAddr, uint8_t valAddr, uint8_t data);
uint32_t HardIICWriteArray(HardIICCHEnum iicch, uint8_t slAddr, uint8_t valAddr, uint8_t *data, uint16_t len);
uint8_t HardIICWrite16bitReg(HardIICCHEnum iicch, uint8_t slAddr, uint16_t valAddr, uint8_t data);
uint32_t HardIICWriteArrayTo16BitAddr(HardIICCHEnum iicch, uint8_t slAddr, uint16_t valAddr, uint8_t *data, uint16_t len);

uint8_t HardIICReadOneByte(HardIICCHEnum iicch, uint8_t slAddr);
uint32_t HardIICReadByteArray(HardIICCHEnum iicch, uint8_t slAddr, uint8_t *receive, uint16_t len);
uint8_t HardIICReadOneReg(HardIICCHEnum iicch, uint8_t slAddr, uint8_t valAddr);
uint32_t HardIICReadByteArrayOneReg(HardIICCHEnum iicch, uint8_t slAddr, uint8_t valAddr, uint8_t *receive, uint16_t len);
uint8_t HardIICReadByteOne16bitReg(HardIICCHEnum iicch, uint8_t slAddr, uint16_t valAddr);
uint32_t HardIICReadArrayOne16bitReg(HardIICCHEnum iicch, uint8_t slAddr, uint16_t valAddr, uint8_t *receive, uint16_t len);

void HardIICSendByteArrayPDMA(HardIICCHEnum iicch, uint8_t *data, uint16_t len);

uint8_t HardIICGetPDMAFinish(HardIICCHEnum iicch);

void HardIICCallbackInPDMAISR(HardIICCHEnum iicch);
void HardIICIRQHandle(HardIICCHEnum iicch);

#endif
