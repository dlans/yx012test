#include "spi_peripheral.h"

static void spiSendDmaConfig(SpiPeripheral *spi);
static void spiReceiveDmaConfig(SpiPeripheral *spi);
static void spiSendReceiveDmaConfig(SpiPeripheral *spi);

static void spiSendDmaConfig(SpiPeripheral *spi)
{
	PDMA_Open(PDMA, 1 << spi->dma_tx.channel);
	if (spi->init.width == 8) {
		PDMA_SetTransferCnt(PDMA, spi->dma_tx.channel, PDMA_WIDTH_8, spi->dma_tx.length);
	}
	else if (spi->init.width == 16) {
		PDMA_SetTransferCnt(PDMA, spi->dma_tx.channel, PDMA_WIDTH_16, spi->dma_tx.length);
	}
	else {
		PDMA_SetTransferCnt(PDMA, spi->dma_tx.channel, PDMA_WIDTH_32, spi->dma_tx.length);
	}
	PDMA_SetTransferAddr(PDMA, spi->dma_tx.channel, spi->dma_tx.address, PDMA_SAR_INC, (uint32_t)&(spi->instance->TX), PDMA_DAR_FIX);
	if (spi->instance == SPI0) {
		PDMA_SetTransferMode(PDMA, spi->dma_tx.channel, PDMA_SPI0_TX, FALSE, 0);
	}
	else if (spi->instance == SPI1) {
		PDMA_SetTransferMode(PDMA, spi->dma_tx.channel, PDMA_SPI1_TX, FALSE, 0);
	}
	else {
		PDMA_SetTransferMode(PDMA, spi->dma_tx.channel, PDMA_SPI2_TX, FALSE, 0);
	}
	PDMA_SetBurstType(PDMA, spi->dma_tx.channel, PDMA_REQ_SINGLE, 0);
	PDMA_EnableInt(PDMA, spi->dma_tx.channel, PDMA_INT_TRANS_DONE);
	if (spi->dma_tx.channel < 2) {
		PDMA_EnableInt(PDMA, spi->dma_tx.channel, PDMA_INT_TIMEOUT);
	}
	
	SPI_TRIGGER_TX_PDMA(spi->instance);
}

static void spiReceiveDmaConfig(SpiPeripheral *spi)
{
	PDMA_Open(PDMA, 1 << spi->dma_rx.channel);
	if (spi->init.width == 8) {
		PDMA_SetTransferCnt(PDMA, spi->dma_rx.channel, PDMA_WIDTH_8, spi->dma_rx.length);
	}
	else if (spi->init.width == 16) {
		PDMA_SetTransferCnt(PDMA, spi->dma_rx.channel, PDMA_WIDTH_16, spi->dma_rx.length);
	}
	else {
		PDMA_SetTransferCnt(PDMA, spi->dma_rx.channel, PDMA_WIDTH_32, spi->dma_rx.length);
	}
	PDMA_SetTransferAddr(PDMA, spi->dma_rx.channel, (uint32_t)&(spi->instance->RX), PDMA_SAR_FIX, spi->dma_rx.address, PDMA_DAR_INC);
	if (spi->instance == SPI0) {
		PDMA_SetTransferMode(PDMA, spi->dma_rx.channel, PDMA_SPI0_RX, FALSE, 0);
	}
	else if (spi->instance == SPI1) {
		PDMA_SetTransferMode(PDMA, spi->dma_rx.channel, PDMA_SPI1_RX, FALSE, 0);
	}
	else {
		PDMA_SetTransferMode(PDMA, spi->dma_rx.channel, PDMA_SPI2_RX, FALSE, 0);
	}
    PDMA_SetBurstType(PDMA, spi->dma_rx.channel, PDMA_REQ_SINGLE, 0);
    PDMA->DSCT[spi->dma_rx.channel].CTL |= PDMA_DSCT_CTL_TBINTDIS_Msk;
	PDMA_EnableInt(PDMA, spi->dma_rx.channel, PDMA_INT_TRANS_DONE);
	if (spi->dma_tx.channel < 2) {
		PDMA_EnableInt(PDMA, spi->dma_rx.channel, PDMA_INT_TIMEOUT);
	}
	
	SPI_TRIGGER_RX_PDMA(spi->instance);
}

static void spiSendReceiveDmaConfig(SpiPeripheral *spi)
{
	/* Rx dma config */
	PDMA_Open(PDMA, 1 << spi->dma_rx.channel);
	if (spi->init.width == 8) {
		PDMA_SetTransferCnt(PDMA, spi->dma_rx.channel, PDMA_WIDTH_8, spi->dma_rx.length);
	}
	else if (spi->init.width == 16) {
		PDMA_SetTransferCnt(PDMA, spi->dma_rx.channel, PDMA_WIDTH_16, spi->dma_rx.length);
	}
	else {
		PDMA_SetTransferCnt(PDMA, spi->dma_rx.channel, PDMA_WIDTH_32, spi->dma_rx.length);
	}
	PDMA_SetTransferAddr(PDMA, spi->dma_rx.channel, (uint32_t)&(spi->instance->RX), PDMA_SAR_FIX, spi->dma_rx.address, PDMA_DAR_INC);
	if (spi->instance == SPI0) {
		PDMA_SetTransferMode(PDMA, spi->dma_rx.channel, PDMA_SPI0_RX, FALSE, 0);
	}
	else if (spi->instance == SPI1) {
		PDMA_SetTransferMode(PDMA, spi->dma_rx.channel, PDMA_SPI1_RX, FALSE, 0);
	}
	else {
		PDMA_SetTransferMode(PDMA, spi->dma_rx.channel, PDMA_SPI2_RX, FALSE, 0);
	}
    PDMA_SetBurstType(PDMA, spi->dma_rx.channel, PDMA_REQ_SINGLE, 0);
    PDMA->DSCT[spi->dma_rx.channel].CTL |= PDMA_DSCT_CTL_TBINTDIS_Msk;
	PDMA_EnableInt(PDMA, spi->dma_rx.channel, PDMA_INT_TRANS_DONE);
	if (spi->dma_tx.channel < 2) {
		PDMA_EnableInt(PDMA, spi->dma_rx.channel, PDMA_INT_TIMEOUT);
	}
	
	/* Tx dma config */
	PDMA_Open(PDMA, 1 << spi->dma_tx.channel);
	if (spi->init.width == 8) {
		PDMA_SetTransferCnt(PDMA, spi->dma_tx.channel, PDMA_WIDTH_8, spi->dma_tx.length);
	}
	else if (spi->init.width == 16) {
		PDMA_SetTransferCnt(PDMA, spi->dma_tx.channel, PDMA_WIDTH_16, spi->dma_tx.length);
	}
	else {
		PDMA_SetTransferCnt(PDMA, spi->dma_tx.channel, PDMA_WIDTH_32, spi->dma_tx.length);
	}
	PDMA_SetTransferAddr(PDMA, spi->dma_tx.channel, spi->dma_tx.address, PDMA_SAR_INC, (uint32_t)&(spi->instance->TX), PDMA_DAR_FIX);
	if (spi->instance == SPI0) {
		PDMA_SetTransferMode(PDMA, spi->dma_tx.channel, PDMA_SPI0_TX, FALSE, 0);
	}
	else if (spi->instance == SPI1) {
		PDMA_SetTransferMode(PDMA, spi->dma_tx.channel, PDMA_SPI1_TX, FALSE, 0);
	}
	else {
		PDMA_SetTransferMode(PDMA, spi->dma_tx.channel, PDMA_SPI2_TX, FALSE, 0);
	}
	PDMA_SetBurstType(PDMA, spi->dma_tx.channel, PDMA_REQ_SINGLE, 0);
	PDMA->DSCT[spi->dma_tx.channel].CTL |= PDMA_DSCT_CTL_TBINTDIS_Msk;
	PDMA_EnableInt(PDMA, spi->dma_tx.channel, PDMA_INT_TRANS_DONE);
	if (spi->dma_tx.channel < 2) {
		PDMA_EnableInt(PDMA, spi->dma_tx.channel, PDMA_INT_TIMEOUT);
	}
	
	SPI_TRIGGER_TX_RX_PDMA(spi->instance);
}

void SpiInit(SpiPeripheral *spi)
{
	spi->trans_sta = SPI_TRANS_STAT_IDLE;
	spi->cb_on_it_send_done = NULL;
	spi->cb_on_it_rec_done = NULL;
	spi->cb_on_it_send_rec_done = NULL;
	spi->cb_on_dma_send_done = NULL;
	spi->cb_on_dma_rec_done = NULL;
	spi->cb_on_dma_send_rec_done = NULL;
	
	SpiMspInit(spi);
	
	SPI_Close(spi->instance);
	SPI_Open(spi->instance, spi->init.device_mode, spi->init.mode, spi->init.width, spi->init.speed);
	SPI_SetFIFO(spi->instance, 0, 0);
	if (spi->init.autonss) {
		SPI_EnableAutoSS(spi->instance, SPI_SS, spi->init.nss_level);
	}
	else {
		SPI_DisableAutoSS(spi->instance);
	}
	spi->init.speed = SPI_GetBusClock(spi->instance);
}

__weak void SpiMspInit(SpiPeripheral *spi)
{
	uint32_t prioritygroup = NVIC_GetPriorityGrouping();
	
	SYS_UnlockReg();
	if (spi->instance == SPI0) {
		CLK_SetModuleClock(SPI0_MODULE, CLK_CLKSEL2_SPI0SEL_PCLK1, MODULE_NoMsk);
		CLK_EnableModuleClock(SPI0_MODULE);
		SystemCoreClockUpdate();
		
		SYS->GPF_MFPH &= ~SYS_GPF_MFPH_PF8MFP_Msk;
		SYS->GPF_MFPH |= SYS_GPF_MFPH_PF8MFP_SPI0_CLK;
		SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF6MFP_Msk;
		SYS->GPF_MFPL |= SYS_GPF_MFPL_PF6MFP_SPI0_MOSI;
		SYS->GPF_MFPH &= ~SYS_GPF_MFPH_PF9MFP_Msk;
		SYS->GPF_MFPH |= SYS_GPF_MFPH_PF9MFP_SPI0_SS;
		SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF7MFP_Msk;
		SYS->GPF_MFPL |= SYS_GPF_MFPL_PF7MFP_SPI0_MISO;
		
		GPIO_SetSlewCtl(PF, BIT8, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PF, BIT6, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PF, BIT9, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PF, BIT7, GPIO_SLEWCTL_FAST);
		
		NVIC_EnableIRQ(SPI0_IRQn);
		NVIC_SetPriority(SPI0_IRQn, NVIC_EncodePriority(prioritygroup, spi->it.priority, 0));
	}
	else if (spi->instance == SPI1) {
		CLK_SetModuleClock(SPI1_MODULE, CLK_CLKSEL2_SPI1SEL_PCLK0, MODULE_NoMsk);
		CLK_EnableModuleClock(SPI1_MODULE);
		SystemCoreClockUpdate();
		
		SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC1MFP_Msk;
		SYS->GPC_MFPL |= SYS_GPC_MFPL_PC1MFP_SPI1_CLK;
		SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC6MFP_Msk;
		SYS->GPC_MFPL |= SYS_GPC_MFPL_PC6MFP_SPI1_MOSI;
		SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC3MFP_Msk;
		SYS->GPC_MFPL |= SYS_GPC_MFPL_PC3MFP_SPI1_MISO;
		SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC0MFP_Msk;
		SYS->GPC_MFPL |= SYS_GPC_MFPL_PC0MFP_SPI1_SS;
		
		GPIO_SetSlewCtl(PC, BIT1, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PC, BIT6, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PC, BIT3, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PC, BIT0, GPIO_SLEWCTL_FAST);
		
		NVIC_EnableIRQ(SPI1_IRQn);
		NVIC_SetPriority(SPI1_IRQn, NVIC_EncodePriority(prioritygroup, spi->it.priority, 0));
	}
	else if (spi->instance == SPI2) {
		CLK_SetModuleClock(SPI2_MODULE, CLK_CLKSEL2_SPI2SEL_PCLK1, MODULE_NoMsk);
		CLK_EnableModuleClock(SPI2_MODULE);
		SystemCoreClockUpdate();
		
		SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE8MFP_Msk;
		SYS->GPE_MFPH |= SYS_GPE_MFPH_PE8MFP_SPI2_CLK;
		SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE10MFP_Msk;
		SYS->GPE_MFPH |= SYS_GPE_MFPH_PE10MFP_SPI2_MOSI;
		SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE9MFP_Msk;
		SYS->GPE_MFPH |= SYS_GPE_MFPH_PE9MFP_SPI2_MISO;
		SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE11MFP_Msk;
		SYS->GPE_MFPH |= SYS_GPE_MFPH_PE11MFP_SPI2_SS;
		
		GPIO_SetSlewCtl(PE, BIT8, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PE, BIT10, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PE, BIT9, GPIO_SLEWCTL_FAST);
		GPIO_SetSlewCtl(PE, BIT11, GPIO_SLEWCTL_FAST);
		
		NVIC_EnableIRQ(SPI2_IRQn);
		NVIC_SetPriority(SPI2_IRQn, NVIC_EncodePriority(prioritygroup, spi->it.priority, 0));
	}
	
	if (spi->dma_tx.instance == PDMA) {
		CLK_EnableModuleClock(PDMA_MODULE);
		SystemCoreClockUpdate();
		
		NVIC_EnableIRQ(PDMA_IRQn);
		NVIC_SetPriority(PDMA_IRQn, NVIC_EncodePriority(prioritygroup, spi->dma_tx.it_priority, 0));
	}
	if (spi->dma_rx.instance == PDMA) {
		CLK_EnableModuleClock(PDMA_MODULE);
		SystemCoreClockUpdate();
		
		NVIC_EnableIRQ(PDMA_IRQn);
		NVIC_SetPriority(PDMA_IRQn, NVIC_EncodePriority(prioritygroup, spi->dma_rx.it_priority, 0));
	}
	SYS_LockReg();
}

uint8_t SpiSendOneData(SpiPeripheral *spi, uint32_t data)
{
	volatile uint32_t ctl = spi->instance->CTL;
	
	if ((ctl & SPI_CTL_RXONLY_Msk) == SPI_CTL_RXONLY_Msk) {
		return SPI_RETURN_ERR;
	}
	
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	spi->sending = 1;
	spi->trans_sta = SPI_TRANS_STAT_BLOCK_SEND;
	SPI_WRITE_TX(spi->instance, data);
	spi->sending = 0;
	spi->trans_sta = SPI_TRANS_STAT_IDLE;
	
	return SPI_RETURN_OK;
}

uint8_t SpiSend(SpiPeripheral *spi, uint8_t *array, uint16_t length)
{
	uint16_t i;
	
	uint16_t *array16 = NULL;
	uint32_t *array32 = NULL;
	uint32_t timeout = spi->wait_timeout;
	volatile uint32_t ctl = spi->instance->CTL;
	
	if ((ctl & SPI_CTL_RXONLY_Msk) == SPI_CTL_RXONLY_Msk) {
		return SPI_RETURN_ERR;
	}
	
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	spi->sending = 1;
	spi->trans_sta = SPI_TRANS_STAT_BLOCK_SEND;
	if ((spi->init.width > 8) && (spi->init.width < 17)) {
		array16 = (uint16_t *)array;
		SPI_SetFIFO(spi->instance, 7, 7);
		for (i = 0; i < length; i++) {
			while (SPI_GET_TX_FIFO_FULL_FLAG(spi->instance)) {
				timeout--;
				if (timeout == 0) {
					spi->sending = 0;
					spi->trans_sta = SPI_TRANS_STAT_IDLE;
					return SPI_RETURN_TIMEOUT;
				}
			}
			SPI_WRITE_TX(spi->instance, array16[i]);
		}
	}
	else if ((spi->init.width > 16) && (spi->init.width < 33)) {
		array32 = (uint32_t *)array;
		SPI_SetFIFO(spi->instance, 3, 3);
		for (i = 0; i < length; i++) {
			while (SPI_GET_TX_FIFO_FULL_FLAG(spi->instance)) {
				timeout--;
				if (timeout == 0) {
					spi->sending = 0;
					spi->trans_sta = SPI_TRANS_STAT_IDLE;
					return SPI_RETURN_TIMEOUT;
				}
			}
			SPI_WRITE_TX(spi->instance, array32[i]);
		}
	}
	else {
		SPI_SetFIFO(spi->instance, 7, 7);
		for (i = 0; i < length; i++) {
			while (SPI_GET_TX_FIFO_FULL_FLAG(spi->instance)) {
				timeout--;
				if (timeout == 0) {
					spi->sending = 0;
					spi->trans_sta = SPI_TRANS_STAT_IDLE;
					return SPI_RETURN_TIMEOUT;
				}
			}
			SPI_WRITE_TX(spi->instance, array[i]);
		}
	}
	spi->sending = 0;
	spi->trans_sta = SPI_TRANS_STAT_IDLE;
	
	return SPI_RETURN_OK;
}

uint8_t SpiReceive(SpiPeripheral *spi, uint8_t *array, uint16_t length)
{
	uint16_t i;
	
	uint16_t *array16 = NULL;
	uint32_t *array32 = NULL;
	uint32_t timeout = spi->wait_timeout;

	if (spi->init.device_mode == SPI_MASTER) {
		return SPI_RETURN_ERR;
	}
	
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	spi->receiving = 1;
	spi->trans_sta = SPI_TRANS_STAT_BLOCK_REC;
	if (spi->init.width == 8) {
		SPI_SetFIFO(spi->instance, 7, 7);
		for (i = 0; i < length; i++) {
			while (SPI_GetStatus(spi->instance, SPI_RX_EMPTY_MASK)) {
				timeout--;
				if (timeout == 0) {
					return SPI_RETURN_TIMEOUT;
				}
			}
			array[i] = SPI_READ_RX(spi->instance);
		}
	}
	else if ((spi->init.width > 8) && (spi->init.width < 17)) {
		SPI_SetFIFO(spi->instance, 7, 7);
		array16 = (uint16_t *)array;
		for (i = 0; i < length; i++) {
			while (SPI_GetStatus(spi->instance, SPI_RX_EMPTY_MASK)) {
				timeout--;
				if (timeout == 0) {
					return SPI_RETURN_TIMEOUT;
				}
			}
			array16[i] = SPI_READ_RX(spi->instance);
		}
	}
	else {
		SPI_SetFIFO(spi->instance, 3, 3);
		array32 = (uint32_t *)array;
		for (i = 0; i < length; i++) {
			while (SPI_GetStatus(spi->instance, SPI_RX_EMPTY_MASK)) {
				timeout--;
				if (timeout == 0) {
					return SPI_RETURN_TIMEOUT;
				}
			}
			array32[i] = SPI_READ_RX(spi->instance);
		}
	}
	spi->receiving = 0;
	spi->trans_sta = SPI_TRANS_STAT_IDLE;

	return SPI_RETURN_OK;
}

uint8_t SpiSendReceive(SpiPeripheral *spi, uint8_t *arr_send, uint8_t *arr_rec, uint16_t length)
{
	uint16_t i;

	uint16_t *arr16_send = NULL;
	uint16_t *arr16_rec = NULL;
	uint32_t *arr32_send = NULL;
	uint32_t *arr32_rec = NULL;
	uint32_t timeout = spi->wait_timeout;

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->sending = 1;
	spi->receiving = 1;
	spi->trans_sta = SPI_TRANS_STAT_BLOCK_SEND_REC;
	if (spi->init.width == 8) {
		SPI_SetFIFO(spi->instance, 7, 7);
		for (i = 0; i < length; i++) {
			SPI_WRITE_TX(spi->instance, arr_send[i]);
			while (SPI_GetStatus(spi->instance, SPI_RX_EMPTY_MASK)) {
				timeout--;
				if (timeout == 0) {
					return SPI_RETURN_TIMEOUT;
				}
			}
			arr_rec[i] = SPI_READ_RX(spi->instance);
		}
	}
	else if ((spi->init.width > 8) && (spi->init.width < 17)) {
		SPI_SetFIFO(spi->instance, 7, 7);
		arr16_send = (uint16_t *)arr_send;
		arr16_rec = (uint16_t *)arr_rec;
		for (i = 0; i < length; i++) {
			SPI_WRITE_TX(spi->instance, arr16_send[i]);
			while (SPI_GetStatus(spi->instance, SPI_RX_EMPTY_MASK)) {
				timeout--;
				if (timeout == 0) {
					return SPI_RETURN_TIMEOUT;
				}
			}
			arr16_rec[i] = SPI_READ_RX(spi->instance);
		}
	}
	else {
		SPI_SetFIFO(spi->instance, 3, 3);
		arr32_send = (uint32_t *)arr_send;
		arr32_rec = (uint32_t *)arr_rec;
		for (i = 0; i < length; i++) {
			SPI_WRITE_TX(spi->instance, arr32_send[i]);
			while (SPI_GetStatus(spi->instance, SPI_RX_EMPTY_MASK)) {
				timeout--;
				if (timeout == 0) {
					return SPI_RETURN_TIMEOUT;
				}
			}
			arr32_rec[i] = SPI_READ_RX(spi->instance);
		}
	}
	spi->sending = 0;
	spi->receiving = 0;
	spi->trans_sta = SPI_TRANS_STAT_IDLE;

	return SPI_RETURN_OK;
}

uint8_t SpiSendIT(SpiPeripheral *spi, uint8_t *array, uint16_t length)
{
	uint16_t *array16 = NULL;
	uint32_t *array32 = NULL;

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->sending = 1;
	spi->trans_sta = SPI_TRANS_STAT_IT_SEND;
	spi->it.array_send = array;
	spi->it.cnt_send = 1;
	spi->it.length_send = length;

	SPI_SetFIFO(spi->instance, 0, 0);
	SPI_EnableInt(spi->instance, SPI_FIFO_TXTH_INT_MASK);
	if (spi->init.width == 8) {
		SPI_WRITE_TX(spi->instance, array[0]);
	}
	else if ((spi->init.width > 8) && (spi->init.width < 17)) {
		array16 = (uint16_t *)array;
		SPI_WRITE_TX(spi->instance, array16[0]);
	}
	else {
		array32 = (uint32_t *)array;
		SPI_WRITE_TX(spi->instance, array32[0]);
	}

	return SPI_RETURN_OK;
}

uint8_t SpiReceiveIT(SpiPeripheral *spi, uint8_t *array, uint16_t length)
{
	if (spi->init.device_mode == SPI_MASTER) {
		return SPI_RETURN_ERR;
	}

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->receiving = 1;
	spi->trans_sta = SPI_TRANS_STAT_IT_REC;
	spi->it.array_rec = array;
	spi->it.cnt_rec = 0;
	spi->it.length_rec = length;

	if (spi->init.width < 17) {
		SPI_SetFIFO(spi->instance, 7, 7);
	}
	else {
		SPI_SetFIFO(spi->instance, 3, 3);
	}
	SPI_EnableInt(spi->instance, SPI_FIFO_RXTH_INT_MASK);

	return SPI_RETURN_OK;
}

uint8_t SpiSendReceiveIT(SpiPeripheral *spi, uint8_t *arr_send, uint8_t *arr_rec, uint16_t length)
{
	uint16_t *array16 = NULL;
	uint32_t *array32 = NULL;

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->sending = 1;
	spi->receiving = 1;
	spi->trans_sta = SPI_TRANS_STAT_IT_SEND_REC;
	spi->it.array_send = arr_send;
	spi->it.cnt_send = 1;
	spi->it.length_send = length;
	spi->it.array_rec = arr_rec;
	spi->it.cnt_rec = 0;
	spi->it.length_rec = length;

	SPI_SetFIFO(spi->instance, 0, 0);
	SPI_EnableInt(spi->instance, SPI_FIFO_RXTH_INT_MASK);
	if (spi->init.width == 8) {
		SPI_WRITE_TX(spi->instance, arr_send[0]);
	}
	else if ((spi->init.width > 8) && (spi->init.width < 17)) {
		array16 = (uint16_t *)arr_send;
		SPI_WRITE_TX(spi->instance, array16[0]);
	}
	else {
		array32 = (uint32_t *)arr_send;
		SPI_WRITE_TX(spi->instance, array32[0]);
	}

	return SPI_RETURN_OK;
}

void SpiTransferITAbort(SpiPeripheral *spi)
{
	SPI_DisableInt(spi->instance, SPI_FIFO_RXTH_INT_MASK);

	spi->sending = 0;
	spi->receiving = 0;
	spi->trans_sta = SPI_TRANS_STAT_IDLE;
}

uint8_t SpiSendDma(SpiPeripheral *spi, uint8_t *array, uint16_t length)
{
	if ((length == 0) || (array == NULL)) {
		return SPI_RETURN_ERR;
	}

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->sending = 1;
	spi->trans_sta = SPI_TRANS_STAT_DMA_SEND;
	spi->dma_tx.length = length;

	PDMA_CLR_TD_FLAG(PDMA, spi->dma_tx.channel);
	spi->dma_tx.address = (uint32_t)array;
	spiSendDmaConfig(spi);

	return SPI_RETURN_OK;
}

uint8_t SpiReceiveDma(SpiPeripheral *spi, uint8_t *array, uint16_t length)
{
	if ((length == 0) || (array == NULL)) {
		return SPI_RETURN_ERR;
	}

	if (spi->init.device_mode == SPI_MASTER) {
		return SPI_RETURN_ERR;
	}

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->receiving = 1;
	spi->trans_sta = SPI_TRANS_STAT_DMA_REC;
	spi->dma_rx.length = length;
	spi->dma_rx.address = (uint32_t)array;
	SPI_SetFIFO(spi->instance, 0, 0);
	spiReceiveDmaConfig(spi);

	return SPI_RETURN_OK;
}

uint8_t SpiSendReceiveDma(SpiPeripheral *spi, uint8_t *arr_send, uint8_t *arr_rec, uint16_t length)
{
	if ((arr_send == NULL) || (arr_rec == NULL) || (length == 0)) {
		return SPI_RETURN_ERR;
	}

	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}

	spi->sending = 1;
	spi->receiving = 1;
	spi->trans_sta = SPI_TRANS_STAT_DMA_SEND_REC;
	spi->dma_tx.length = length;
	spi->dma_rx.length = length;

	if (spi->init.width < 17) {
		SPI_SetFIFO(spi->instance, 7, 7);
	}
	else {
		SPI_SetFIFO(spi->instance, 3, 3);
	}
	SPI_DISABLE_TX_RX_PDMA(spi->instance);
	spi->instance->PDMACTL |= SPI_PDMACTL_PDMARST_Msk;
	PDMA_CLR_TD_FLAG(PDMA, 1 << spi->dma_tx.channel);
	PDMA_CLR_TD_FLAG(PDMA, 1 << spi->dma_rx.channel);
	spi->dma_tx.address = (uint32_t)arr_send;
	spi->dma_rx.address = (uint32_t)arr_rec;
	spiSendReceiveDmaConfig(spi);

	return SPI_RETURN_OK;
}

uint8_t SpiWaitDmaDone(SpiPeripheral *spi)
{
	uint32_t timeout = spi->wait_timeout;
	
	while (timeout) {
		if ((spi->trans_sta == SPI_TRANS_STAT_DMA_SEND) ||
			(spi->trans_sta == SPI_TRANS_STAT_DMA_REC)  || 
			(spi->trans_sta == SPI_TRANS_STAT_DMA_SEND_REC)) {
			timeout--;
		}
		else {
			break;
		}
	}
	if (timeout) {
		return SPI_RETURN_OK;
	}
	else {
		return SPI_RETURN_TIMEOUT;
	}
}

void SpiTransferDmaAbort(SpiPeripheral *spi)
{
	SPI_DISABLE_TX_RX_PDMA(spi->instance);
	PDMA_STOP(PDMA, spi->dma_tx.channel);
	PDMA_STOP(PDMA, spi->dma_rx.channel);
	PDMA->CHCTL &= ~((uint32_t)(1 << spi->dma_tx.channel));
	PDMA->CHCTL &= ~((uint32_t)(1 << spi->dma_rx.channel));
}

uint8_t SpiWaitTransDone(SpiPeripheral *spi)
{
	uint32_t timeout = spi->wait_timeout;
	
	while (timeout) {
		if (SPI_IS_BUSY(spi->instance)) {
			timeout--;
		}
		else {
			break;
		}
	}
	
	while (timeout) {
		if (spi->trans_sta != SPI_TRANS_STAT_IDLE) {
			timeout--;
		}
		else {
			break;
		}
	}
	
	if (timeout) {
		return SPI_RETURN_OK;
	}
	else {
		return SPI_RETURN_TIMEOUT;
	}
}

SpiCallbackFunction SpiRegisterCb(SpiPeripheral *spi, SpiCbEnum cb_enum, SpiCallbackFunction cb)
{
	SpiCallbackFunction old = NULL;
	
	switch (cb_enum) {
		case SPI_CB_ON_IT_SEND_DONE:
			old = spi->cb_on_it_send_done;
			spi->cb_on_it_send_done = cb;
			break;
		case SPI_CB_ON_IT_REC_DONE:
			old = spi->cb_on_it_rec_done;
			spi->cb_on_it_rec_done = cb;
			break;
		case SPI_CB_ON_IT_SEND_REC_DONE:
			old = spi->cb_on_it_send_rec_done;
			spi->cb_on_it_send_rec_done = cb;
			break;
		case SPI_CB_ON_DMA_SEND_DONE:
			old = spi->cb_on_dma_send_done;
			spi->cb_on_dma_send_done = cb;
			break;
		case SPI_CB_ON_DMA_REC_DONE:
			old = spi->cb_on_dma_rec_done;
			spi->cb_on_dma_rec_done = cb;
			break;
		case SPI_CB_ON_DMA_SEND_REC_DONE:
			old = spi->cb_on_dma_send_rec_done;
			spi->cb_on_dma_send_rec_done = cb;
			break;
		default:
			break;
	}
	
	return old;
}

uint8_t SpiSetSpeed(SpiPeripheral *spi, uint32_t speed)
{
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	SPI_SetBusClock(spi->instance, speed);
	spi->init.speed = SPI_GetBusClock(spi->instance);
	
	return SPI_RETURN_OK;
}

uint8_t SpiSetPSC(SpiPeripheral *spi, uint32_t psc)
{
	uint32_t clksrc;
	
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	spi->instance->CLKDIV = psc;
	
	if (spi->instance == SPI0) {
		if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI0SEL_Msk) == CLK_CLKSEL2_SPI0SEL_HXT) {
			clksrc = __HXT;
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI0SEL_Msk) == CLK_CLKSEL2_SPI0SEL_PLL) {
			clksrc = CLK_GetPLLClockFreq();
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI0SEL_Msk) == CLK_CLKSEL2_SPI0SEL_PCLK1) {
			clksrc = CLK_GetPCLK1Freq();
		}
		else {
			clksrc = __HIRC;
		}
	}
	else if (spi->instance == SPI1) {
		if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI1SEL_Msk) == CLK_CLKSEL2_SPI1SEL_HXT) {
			clksrc = __HXT;
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI1SEL_Msk) == CLK_CLKSEL2_SPI1SEL_PLL) {
			clksrc = CLK_GetPLLClockFreq();
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI1SEL_Msk) == CLK_CLKSEL2_SPI1SEL_PCLK0) {
			clksrc = CLK_GetPCLK0Freq();
		}
		else {
			clksrc = __HIRC;
		}
	}
	else if (spi->instance == SPI2) {
		if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI2SEL_Msk) == CLK_CLKSEL2_SPI2SEL_HXT) {
			clksrc = __HXT;
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI2SEL_Msk) == CLK_CLKSEL2_SPI2SEL_PLL) {
			clksrc = CLK_GetPLLClockFreq();
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI2SEL_Msk) == CLK_CLKSEL2_SPI2SEL_PCLK1) {
			clksrc = CLK_GetPCLK1Freq();
		}
		else {
			clksrc = __HIRC;
		}
	}
	else {
		if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI3SEL_Msk) == CLK_CLKSEL2_SPI3SEL_HXT) {
			clksrc = __HXT;
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI3SEL_Msk) == CLK_CLKSEL2_SPI3SEL_PLL) {
			clksrc = CLK_GetPLLClockFreq();
		}
		else if((CLK->CLKSEL2 & CLK_CLKSEL2_SPI3SEL_Msk) == CLK_CLKSEL2_SPI3SEL_PCLK0) {
			clksrc = CLK_GetPCLK0Freq();
		}
		else {
			clksrc = __HIRC;
		}
	}
	
	spi->init.speed = clksrc / (psc + 1);
	
	return SPI_RETURN_OK;
}

uint8_t SpiSetWidth(SpiPeripheral *spi, uint32_t width)
{
	if (width < 8) {
		width = 8;
	}
	if (width > 32) {
		width = 32;
	}
	
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	spi->init.width = width;
	SPI_SET_DATA_WIDTH(spi->instance, width);
	
	return SPI_RETURN_OK;
}

uint8_t SpiSetDeviceMode(SpiPeripheral *spi, uint32_t device_mode)
{
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	spi->instance->CTL &= (~SPI_CTL_SLAVE_Msk);
	spi->instance->CTL |= device_mode;
	spi->init.device_mode = device_mode;
	
	return SPI_RETURN_OK;
}

uint32_t SpiGetMode(SpiPeripheral *spi)
{
	return spi->init.device_mode;
}

uint32_t SpiGetSpeed(SpiPeripheral *spi)
{
	return spi->init.speed;
}

uint32_t SpiGetWidth(SpiPeripheral *spi)
{
	uint32_t width = spi->instance->CTL;
	width &= SPI_CTL_DWIDTH_Msk;
	width >>= SPI_CTL_DWIDTH_Pos;
	if (width == 0) {
		width = 32;
	}
	spi->init.width = width;
	
	return width;
}

uint16_t SpiGetRecDmaLength(SpiPeripheral *spi)
{
	uint32_t dsct_ctl = PDMA->DSCT[spi->dma_rx.channel].CTL;
	uint16_t reclen = dsct_ctl >> PDMA_DSCT_CTL_TXCNT_Pos;
	
	if (spi->dma_rx.length == 0) {
		return 0;
	}
	
	reclen = spi->dma_rx.length - reclen;
	reclen--;
	
	return reclen;
}

__weak uint8_t SpiIOToCore(SpiPeripheral *spi, uint8_t iomsk)
{
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	if (iomsk & SPI_PIN_OCCUPY_CLK_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPH &= ~SYS_GPF_MFPH_PF8MFP_Msk;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC1MFP_Msk;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE8MFP_Msk;
		}
		GPIO_SetMode(spi->gpio_clk.group, spi->gpio_clk.pin, GPIO_MODE_OUTPUT);
	}
	if (iomsk & SPI_PIN_OCCUPY_MOSI_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF6MFP_Msk;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC6MFP_Msk;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE10MFP_Msk;
		}
		GPIO_SetMode(spi->gpio_mosi.group, spi->gpio_mosi.pin, GPIO_MODE_OUTPUT);
	}
	if (iomsk & SPI_PIN_OCCUPY_MISO_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF7MFP_Msk;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC3MFP_Msk;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE9MFP_Msk;
		}
		GPIO_SetMode(spi->gpio_miso.group, spi->gpio_miso.pin, GPIO_MODE_OUTPUT);
	}
	if (iomsk & SPI_PIN_OCCUPY_NSS_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPH &= ~SYS_GPF_MFPH_PF9MFP_Msk;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC0MFP_Msk;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE11MFP_Msk;
		}
		GPIO_SetMode(spi->gpio_nss.group, spi->gpio_nss.pin, GPIO_MODE_OUTPUT);
	}
	spi->iomsk |= iomsk;
	
	return SPI_RETURN_OK;
}

__weak void SpiIOToPeripheral(SpiPeripheral *spi, uint8_t iomsk)
{
	// TODO: Check whether pin should be configured  as input mode
	if (iomsk & SPI_PIN_OCCUPY_CLK_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPH &= ~SYS_GPF_MFPH_PF8MFP_Msk;
			SYS->GPF_MFPH |= SYS_GPF_MFPH_PF8MFP_SPI0_CLK;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC1MFP_Msk;
			SYS->GPC_MFPL |= SYS_GPC_MFPL_PC1MFP_SPI1_CLK;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE8MFP_Msk;
			SYS->GPE_MFPH |= SYS_GPE_MFPH_PE8MFP_SPI2_CLK;
		}
	}
	if (iomsk & SPI_PIN_OCCUPY_MOSI_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF6MFP_Msk;
			SYS->GPF_MFPL |= SYS_GPF_MFPL_PF6MFP_SPI0_MOSI;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC6MFP_Msk;
			SYS->GPC_MFPL |= SYS_GPC_MFPL_PC6MFP_SPI1_MOSI;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE10MFP_Msk;
			SYS->GPE_MFPH |= SYS_GPE_MFPH_PE10MFP_SPI2_MOSI;
		}
	}
	if (iomsk & SPI_PIN_OCCUPY_MISO_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF7MFP_Msk;
			SYS->GPF_MFPL |= SYS_GPF_MFPL_PF7MFP_SPI0_MISO;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC3MFP_Msk;
			SYS->GPC_MFPL |= SYS_GPC_MFPL_PC3MFP_SPI1_MISO;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE9MFP_Msk;
			SYS->GPE_MFPH |= SYS_GPE_MFPH_PE9MFP_SPI2_MISO;
		}
	}
	if (iomsk & SPI_PIN_OCCUPY_NSS_MSK) {
		if (spi->instance == SPI0) {
			SYS->GPF_MFPH &= ~SYS_GPF_MFPH_PF9MFP_Msk;
			SYS->GPF_MFPH |= SYS_GPF_MFPH_PF9MFP_SPI0_SS;
		}
		else if (spi->instance == SPI1) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC0MFP_Msk;
			SYS->GPC_MFPL |= SYS_GPC_MFPL_PC0MFP_SPI1_SS;
		}
		else {
			SYS->GPE_MFPH &= ~SYS_GPE_MFPH_PE11MFP_Msk;
			SYS->GPE_MFPH |= SYS_GPE_MFPH_PE11MFP_SPI2_SS;
		}
	}
	spi->iomsk &= (~iomsk);
}

uint8_t SpiIOCtl(SpiPeripheral *spi, SpiIOCtlEnum ioctl)
{
	if (SpiWaitTransDone(spi) == SPI_RETURN_TIMEOUT) {
		return SPI_RETURN_TIMEOUT;
	}
	
	switch (ioctl) {
		case SPI_IO_CTL_CLK_TO_CORE:
			SpiIOToCore(spi, SPI_PIN_OCCUPY_CLK_MSK);
			break;
		case SPI_IO_CTL_CLK_TO_PERIPHERAL:
			SpiIOToPeripheral(spi, SPI_PIN_OCCUPY_CLK_MSK);
			break;
		case SPI_IO_CTL_MOSI_TO_CORE:
			SpiIOToCore(spi, SPI_PIN_OCCUPY_MOSI_MSK);
			break;
		case SPI_IO_CTL_MOSI_TO_PERIPHERAL:
			SpiIOToPeripheral(spi, SPI_PIN_OCCUPY_MOSI_MSK);
			break;
		case SPI_IO_CTL_MISO_TO_CORE:
			SpiIOToCore(spi, SPI_PIN_OCCUPY_MISO_MSK);
			break;
		case SPI_IO_CTL_MISO_TO_PERIPHERAL:
			SpiIOToPeripheral(spi, SPI_PIN_OCCUPY_MISO_MSK);
			break;
		case SPI_IO_CTL_NSS_TO_CORE:
			SpiIOToCore(spi, SPI_PIN_OCCUPY_NSS_MSK);
			break;
		case SPI_IO_CTL_NSS_TO_PERIPHERAL:
			SpiIOToPeripheral(spi, SPI_PIN_OCCUPY_NSS_MSK);
			break;
		case SPI_IO_CTL_MISO_EDGE_RAISE_DETECT:
		case SPI_IO_CTL_MISO_EDGE_FALL_DETECT:
		case SPI_IO_CTL_MISO_EDGE_BOTH_DETECT:
			SpiMisoExitMspConfig(spi, ioctl);
			break;
		default:
			break;
	}
	
	return SPI_RETURN_OK;
}

__weak void SpiMisoExitMspConfig(SpiPeripheral *spi, SpiIOCtlEnum ioctl)
{
	if (spi->instance == SPI0) {
		GPIO_DisableInt(PF, 7);
	}
	else if (spi->instance == SPI1) {
		GPIO_DisableInt(PC, 3);
	}
	else {
		GPIO_DisableInt(PE, 9);
	}
}

__weak void SpiMisoExitStartDetec(SpiPeripheral *spi)
{
	if (spi->instance == SPI0) {}
	else if (spi->instance == SPI1) {}
	else {}
}

__weak void SpiMisoExitStopDetec(SpiPeripheral *spi)
{
}

__weak uint8_t SpiIOOutput(SpiPeripheral *spi, uint8_t iomsk, uint8_t iostat)
{
	return 0;
}

void SpiInterruptHandle(SpiPeripheral *spi)
{
	uint16_t *arr16_rec = NULL;
	uint16_t *arr16_send = NULL;
	uint32_t *arr32_rec = NULL;
	uint32_t *arr32_send = NULL;
	uint32_t status = spi->instance->STATUS;

	if ((status & SPI_STATUS_UNITIF_Msk) == SPI_STATUS_UNITIF_Msk) {
		SPI_ClearIntFlag(spi->instance, SPI_UNIT_INT_MASK);
	}
	if ((status & SPI_STATUS_SSACTIF_Msk) == SPI_STATUS_SSACTIF_Msk) {
		SPI_ClearIntFlag(spi->instance, SPI_SSACT_INT_MASK);
	}
	if ((status & SPI_STATUS_SSINAIF_Msk) == SPI_STATUS_SSINAIF_Msk) {
		SPI_ClearIntFlag(spi->instance, SPI_SSINACT_INT_MASK);
	}
	if ((status & SPI_STATUS_SLVURIF_Msk) == SPI_STATUS_SLVURIF_Msk) {
		SPI_ClearIntFlag(spi->instance, SPI_SLVUR_INT_MASK);
	}
	if ((status & SPI_STATUS_SLVBEIF_Msk) == SPI_STATUS_SLVBEIF_Msk) {
		SPI_ClearIntFlag(spi->instance, SPI_SLVBE_INT_MASK);
	}
	if ((status & SPI_STATUS_TXUFIF_Msk) == SPI_STATUS_TXUFIF_Msk) {
		SPI_ClearIntFlag(spi->instance, SPI_TXUF_INT_MASK);
	}
	if ((status & SPI_STATUS_TXTHIF_Msk) == SPI_STATUS_TXTHIF_Msk) {
		if (spi->trans_sta == SPI_TRANS_STAT_IT_SEND) {
			if (spi->init.width == 8) {
				SPI_WRITE_TX(spi->instance, spi->it.array_send[spi->it.cnt_send]);
			}
			else if ((spi->init.width > 8) && (spi->init.width < 17)) {
				arr16_send = (uint16_t *)spi->it.array_send;
				SPI_WRITE_TX(spi->instance, arr16_send[spi->it.cnt_send]);
			}
			else {
				arr32_send = (uint32_t *)spi->it.array_send;
				SPI_WRITE_TX(spi->instance, arr32_send[spi->it.cnt_send]);
			}
			spi->it.cnt_send++;
			if (spi->it.cnt_rec == spi->it.length_send) {
				spi->sending = 0;
				spi->trans_sta = SPI_TRANS_STAT_IDLE;
				if (spi->cb_on_it_send_done != NULL) {
					spi->cb_on_it_send_done(spi);
				}
			}
		}
	}
	if ((status & SPI_STATUS_RXTHIF_Msk) == SPI_STATUS_RXTHIF_Msk) {
		if (spi->trans_sta == SPI_TRANS_STAT_IT_REC) {
			if (spi->init.width == 8) {
				spi->it.array_rec[spi->it.cnt_rec] = SPI_READ_RX(spi->instance);
			}
			else if ((spi->init.width > 8) && (spi->init.width < 17)) {
				arr16_rec = (uint16_t *)spi->it.array_rec;
				arr16_rec[spi->it.cnt_rec] = SPI_READ_RX(spi->instance);
			}
			else {
				arr32_rec = (uint32_t *)spi->it.array_rec;
				arr32_rec[spi->it.cnt_rec] = SPI_READ_RX(spi->instance);
			}
			spi->it.cnt_rec++;
			if (spi->it.cnt_rec == spi->it.length_rec) {
				spi->receiving = 0;
				spi->trans_sta = SPI_TRANS_STAT_IDLE;
				SPI_DisableInt(spi->instance, SPI_FIFO_RXTH_INT_MASK);
				if (spi->cb_on_it_rec_done != NULL) {
					spi->cb_on_it_rec_done(spi);
				}
			}
		}
		if (spi->trans_sta == SPI_TRANS_STAT_IT_SEND_REC) {
			if (spi->init.width == 8) {
				spi->it.array_rec[spi->it.cnt_rec] = SPI_READ_RX(spi->instance);
				SPI_WRITE_TX(spi->instance, spi->it.array_send[spi->it.cnt_send]);
			}
			else if ((spi->init.width > 8) && (spi->init.width < 17)) {
				arr16_rec = (uint16_t *)spi->it.array_rec;
				arr16_send = (uint16_t *)spi->it.array_send;
				arr16_rec[spi->it.cnt_rec] = SPI_READ_RX(spi->instance);
				SPI_WRITE_TX(spi->instance, arr16_send[spi->it.cnt_send]);
			}
			else {
				arr32_rec = (uint32_t *)spi->it.array_rec;
				arr32_send = (uint32_t *)spi->it.array_send;
				arr32_rec[spi->it.cnt_rec] = SPI_READ_RX(spi->instance);
				SPI_WRITE_TX(spi->instance, arr32_send[spi->it.cnt_send]);
			}
			spi->it.cnt_rec++;
			spi->it.cnt_send++;
			if (spi->it.cnt_rec == spi->it.length_rec) {
				spi->sending = 0;
				spi->receiving = 0;
				spi->trans_sta = SPI_TRANS_STAT_IDLE;
				SPI_DisableInt(spi->instance, SPI_FIFO_RXTH_INT_MASK);
				if (spi->cb_on_it_send_rec_done != NULL) {
					spi->cb_on_it_send_rec_done(spi);
				}
			}
		}
	}
}

void SpiDmaDoneHandle(SpiPeripheral *spi)
{
	volatile uint32_t regval1, regval2;
	
	if (spi->trans_sta == SPI_TRANS_STAT_DMA_SEND) {
		PDMA_CLR_TD_FLAG(PDMA, 1 << spi->dma_tx.channel);
		PDMA_STOP(PDMA, spi->dma_tx.channel);
		SPI_DISABLE_TX_PDMA(spi->instance);
		spi->sending = 0;
		spi->trans_sta = SPI_TRANS_STAT_IDLE;
		if (spi->cb_on_dma_send_done != NULL) {
			spi->cb_on_dma_send_done(spi);
		}
	}
	else if (spi->trans_sta == SPI_TRANS_STAT_DMA_REC) {
		PDMA_CLR_TD_FLAG(PDMA, 1 << spi->dma_rx.channel);
		PDMA_STOP(PDMA, spi->dma_rx.channel);
		SPI_DISABLE_RX_PDMA(spi->instance);
		spi->receiving = 0;
		spi->trans_sta = SPI_TRANS_STAT_IDLE;
		if (spi->cb_on_dma_rec_done != NULL) {
			spi->cb_on_dma_rec_done(spi);
		}
	}
	else {
		regval1 = PDMA->DSCT[spi->dma_tx.channel].CTL >> PDMA_DSCT_CTL_TXCNT_Pos;
		regval2 = PDMA->DSCT[spi->dma_rx.channel].CTL >> PDMA_DSCT_CTL_TXCNT_Pos;
		if ((regval1 == 0) && (regval2 == 0)) {
			SPI_DISABLE_TX_RX_PDMA(spi->instance);
			PDMA->CHCTL &= ~((uint32_t)(1 << spi->dma_tx.channel));
			PDMA->CHCTL &= ~((uint32_t)(1 << spi->dma_rx.channel));
			spi->sending = 0;
			spi->receiving = 0;
			spi->trans_sta = SPI_TRANS_STAT_IDLE;
			if (spi->cb_on_dma_send_rec_done != NULL) {
				spi->cb_on_dma_send_rec_done(spi);
			}
		}
	}
}
