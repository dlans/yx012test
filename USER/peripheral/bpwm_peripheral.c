#include "bpwm_peripheral.h"
#include "NuMicro.h"

#define BPWM_ENABLE_MASK 0x05

struct BPWMStruct {
	uint32_t freTarget;
	double freReal;
	uint16_t duty0;
	uint16_t duty1;
};

struct BPWMManager {
	uint8_t bpwm0En : 1;
	uint8_t bpwm1En : 1;
	uint8_t reversed : 6;
	struct BPWMStruct bpwm0;
	struct BPWMStruct bpwm1;
};

static struct BPWMManager *sManager = NULL;

static void caculateFreReal(BpwmPeripheral *bpwm)
{
	uint32_t clk, prescale, period;
	
	if (bpwm->instance == BPWM0) {
		if ((CLK->CLKSEL2 & CLK_CLKSEL2_BPWM0SEL_Msk) == 0) {
			clk = CLK_GetPLLClockFreq();
		}
		else {
			clk = CLK_GetPCLK0Freq();
		}
		prescale = BPWM0->CLKPSC + 1;
		period = BPWM0->PERIOD + 1;
	}
	else {
		if ((CLK->CLKSEL2 & CLK_CLKSEL2_BPWM1SEL_Msk) == 0) {
			clk = CLK_GetPLLClockFreq();
		}
		else {
			clk = CLK_GetPCLK1Freq();
		}
		prescale = BPWM1->CLKPSC + 1;
		period = BPWM1->PERIOD + 1;
	}

	bpwm->fre_real = clk / (prescale * period);
}

void BpwmInit(BpwmPeripheral *bpwm)
{
	BpwmMspInit(bpwm);
	caculateFreReal(bpwm);
}

void BpwmMspInit(BpwmPeripheral *bpwm)
{
	SYS_UnlockReg();
	if (bpwm->instance == BPWM0) {
		CLK_EnableModuleClock(BPWM0_MODULE);
		CLK_SetModuleClock(BPWM0_MODULE, CLK_CLKSEL2_BPWM0SEL_PLL, 0);
		SystemCoreClockUpdate();
		if ((bpwm->ch_en_msk & BPWM_CH_0_MASK) == BPWM_CH_0_MASK) {
			SYS->GPG_MFPH &= ~SYS_GPG_MFPH_PG14MFP_Msk;
			SYS->GPG_MFPH |= SYS_GPG_MFPH_PG14MFP_BPWM0_CH0;
			GPIO_SetSlewCtl(PG, BIT14, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM0, 0, bpwm->fre_target, bpwm->duty[0]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_1_MASK) == BPWM_CH_1_MASK) {
			SYS->GPA_MFPL &= ~SYS_GPA_MFPL_PA1MFP_Msk;
			SYS->GPA_MFPL |= SYS_GPA_MFPL_PA1MFP_BPWM0_CH1;
			GPIO_SetSlewCtl(PA, BIT1, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM0, 1, bpwm->fre_target, bpwm->duty[1]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_2_MASK) == BPWM_CH_2_MASK) {
			SYS->GPG_MFPH &= ~SYS_GPG_MFPH_PG12MFP_Msk;
			SYS->GPG_MFPH |= SYS_GPG_MFPH_PG12MFP_BPWM0_CH2;
			GPIO_SetSlewCtl(PG, BIT12, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM0, 2, bpwm->fre_target, bpwm->duty[2]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_3_MASK) == BPWM_CH_3_MASK) {
			SYS->GPA_MFPL &= ~SYS_GPA_MFPL_PA3MFP_Msk;
			SYS->GPA_MFPL |= SYS_GPA_MFPL_PA3MFP_BPWM0_CH3;
			GPIO_SetSlewCtl(PA, BIT3, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM0, 3, bpwm->fre_target, bpwm->duty[3]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_4_MASK) == BPWM_CH_4_MASK) {
			SYS->GPA_MFPL &= ~SYS_GPA_MFPL_PA4MFP_Msk;
			SYS->GPA_MFPL |= SYS_GPA_MFPL_PA4MFP_BPWM0_CH4;
			GPIO_SetSlewCtl(PA, BIT4, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM0, 4, bpwm->fre_target, bpwm->duty[4]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_5_MASK) == BPWM_CH_5_MASK) {
			SYS->GPA_MFPL &= ~SYS_GPA_MFPL_PA5MFP_Msk;
			SYS->GPA_MFPL |= SYS_GPA_MFPL_PA5MFP_BPWM0_CH5;
			GPIO_SetSlewCtl(PA, BIT5, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM0, 5, bpwm->fre_target, bpwm->duty[5]);
		}
	}
	else if (bpwm->instance == BPWM1) {
		CLK_EnableModuleClock(BPWM1_MODULE);
		CLK_SetModuleClock(BPWM1_MODULE, CLK_CLKSEL2_BPWM1SEL_PCLK1, 0);
		SystemCoreClockUpdate();
		if ((bpwm->ch_en_msk & BPWM_CH_0_MASK) == BPWM_CH_0_MASK) {
			SYS->GPC_MFPL &= ~SYS_GPC_MFPL_PC7MFP_Msk;
			SYS->GPC_MFPL |= SYS_GPC_MFPL_PC7MFP_BPWM1_CH0;
			GPIO_SetSlewCtl(PC, BIT7, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM1, 0, bpwm->fre_target, bpwm->duty[0]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_1_MASK) == BPWM_CH_1_MASK) {
			SYS->GPF_MFPL &= ~SYS_GPF_MFPL_PF1MFP_Msk;
			SYS->GPF_MFPL |= SYS_GPF_MFPL_PF1MFP_BPWM1_CH1;
			GPIO_SetSlewCtl(PF, BIT1, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM1, 1, bpwm->fre_target, bpwm->duty[1]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_2_MASK) == BPWM_CH_2_MASK) {
			SYS->GPA_MFPH &= ~SYS_GPA_MFPH_PA12MFP_Msk;
			SYS->GPA_MFPH |= SYS_GPA_MFPH_PA12MFP_BPWM1_CH2;
			GPIO_SetSlewCtl(PA, BIT12, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM1, 2, bpwm->fre_target, bpwm->duty[2]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_3_MASK) == BPWM_CH_3_MASK) {
			SYS->GPA_MFPH &= ~SYS_GPA_MFPH_PA13MFP_Msk;
			SYS->GPA_MFPH |= SYS_GPA_MFPH_PA13MFP_BPWM1_CH3;
			GPIO_SetSlewCtl(PA, BIT13, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM1, 3, bpwm->fre_target, bpwm->duty[3]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_4_MASK) == BPWM_CH_4_MASK) {
			SYS->GPA_MFPH &= ~SYS_GPA_MFPH_PA14MFP_Msk;
			SYS->GPA_MFPH |= SYS_GPA_MFPH_PA14MFP_BPWM1_CH4;
			GPIO_SetSlewCtl(PA, BIT14, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM1, 4, bpwm->fre_target, bpwm->duty[4]);
		}
		else if ((bpwm->ch_en_msk & BPWM_CH_5_MASK) == BPWM_CH_5_MASK) {
			SYS->GPA_MFPH &= ~SYS_GPA_MFPH_PA15MFP_Msk;
			SYS->GPA_MFPH |= SYS_GPA_MFPH_PA15MFP_BPWM1_CH5;
			GPIO_SetSlewCtl(PA, BIT15, GPIO_SLEWCTL_FAST);
			BPWM_ConfigOutputChannel(BPWM1, 5, bpwm->fre_target, bpwm->duty[5]);
		}
	}
	SYS_LockReg();
}

void BPWMSetEnable(BPWM_AVAILABLE_LIST bpwm, uint8_t newStatus)
{
	if (newStatus) {
		if (bpwm == BPWM_PERIPHERAL_0) {
			BPWM_Start(BPWM0, BPWM_ENABLE_MASK);
			sManager->bpwm0En = 1;
		}
		else {
			BPWM_Start(BPWM1, BPWM_ENABLE_MASK);
			sManager->bpwm1En = 1;
		}
	}
	else {
		if (bpwm == BPWM_PERIPHERAL_0) {
			BPWM_ForceStop(BPWM0, BPWM_ENABLE_MASK);
			sManager->bpwm0En = 0;
		}
		else {
			BPWM_ForceStop(BPWM1, BPWM_ENABLE_MASK);
			sManager->bpwm1En = 0;
		}
	}
}

void BPWMSetFre(BPWM_AVAILABLE_LIST bpwm, uint32_t fre)
{
	if (bpwm == BPWM_PERIPHERAL_0) {
		sManager->bpwm0.freTarget = fre;
		BPWM_ConfigOutputChannel(BPWM0, 0, sManager->bpwm0.freTarget, sManager->bpwm0.duty0);
		BPWM_ConfigOutputChannel(BPWM0, 2, sManager->bpwm0.freTarget, sManager->bpwm0.duty0);
	}
	else {
		sManager->bpwm1.freTarget = fre;
		BPWM_ConfigOutputChannel(BPWM1, 0, sManager->bpwm1.freTarget, sManager->bpwm1.duty0);
		BPWM_ConfigOutputChannel(BPWM1, 2, sManager->bpwm1.freTarget, sManager->bpwm1.duty0);
	}
}

void BPWMSetDuty(BPWM_AVAILABLE_LIST bpwm, uint16_t duty, uint8_t ch)
{
	if (bpwm == BPWM_PERIPHERAL_0) {
		if (duty == 0 || duty > 100) {}
		else {
			if (ch == BPWM_CH_0_MASK) {
				sManager->bpwm0.duty0 = duty;
				BPWM_ConfigOutputChannel(BPWM0, 0, sManager->bpwm0.freTarget, sManager->bpwm0.duty0);
			}
			else if (ch == BPWM_CH_1_MASK) {
				sManager->bpwm0.duty1 = duty;
				BPWM_ConfigOutputChannel(BPWM0, 0, sManager->bpwm0.freTarget, sManager->bpwm0.duty1);
			}
		}
	}
	else {
		if (duty == 0 || duty > 100) {}
		else {
			if (ch == BPWM_CH_0_MASK) {
				sManager->bpwm1.duty0 = duty;
				BPWM_ConfigOutputChannel(BPWM1, 0, sManager->bpwm1.freTarget, sManager->bpwm1.duty0);
			}
			else if (ch == BPWM_CH_1_MASK) {
				sManager->bpwm0.duty1 = duty;
				BPWM_ConfigOutputChannel(BPWM1, 0, sManager->bpwm1.freTarget, sManager->bpwm1.duty1);
			}
		}
	}
}

uint32_t BPWMGetFre(BPWM_AVAILABLE_LIST bpwm)
{
	if (bpwm == BPWM_PERIPHERAL_0) {
		return sManager->bpwm0.freTarget;
	}
	else {
		return sManager->bpwm1.freTarget;
	}
}

double BPWMGetFreReal(BPWM_AVAILABLE_LIST bpwm)
{
	if (bpwm == BPWM_PERIPHERAL_0) {
		return sManager->bpwm0.freReal;
	}
	else {
		return sManager->bpwm1.freReal;
	}
}

uint16_t BPWMGetDuty(BPWM_AVAILABLE_LIST bpwm, uint16_t ch)
{
	if (bpwm == BPWM_PERIPHERAL_0) {
		if (ch == BPWM_CH_0_MASK) {
			return sManager->bpwm0.duty0;
		}
		else if (ch == BPWM_CH_1_MASK) {
			return sManager->bpwm0.duty1;
		}
		else {
			return 0;
		}
	}
	else {
		if (ch == BPWM_CH_0_MASK) {
			return sManager->bpwm1.duty0;
		}
		else if (ch == BPWM_CH_1_MASK) {
			return sManager->bpwm1.duty1;
		}
		else {
			return 0;
		}
	}
}

uint8_t BPWMGetOnOff(BPWM_AVAILABLE_LIST bpwm)
{
	if (bpwm == BPWM_PERIPHERAL_0) {
		return sManager->bpwm0En;
	}
	else {
		return sManager->bpwm1En;
	}
}
