#include "iic_peripheral.h"
#include "NuMicro.h"

struct __HardIIC {
	uint8_t txConfiged : 1;
	uint8_t rxConfiged : 1;
    uint8_t pdmaChTxUse : 1;
    uint8_t pdmaChRxUse : 1;
    uint8_t pdmaFinish : 1;
	uint8_t reversed : 3;
	uint8_t width;
	uint8_t masterSlave;
	uint16_t slaveAddr;
	uint32_t speed;
	uint32_t pdmaTxOldAddr;
	uint32_t pdmaRxOldAddr;
};

struct __HardIICManager {
	struct __HardIIC iic0;
};

static struct __HardIICManager *sManager = NULL;

static void iic0Init(uint32_t speed)
{
	
	SYS_UnlockReg();
	
	CLK_EnableModuleClock(I2C0_MODULE);
	SystemCoreClockUpdate();
	
	/* PB5->IIC0_SCL, PB4->IIC0_SDA */
	SYS->GPB_MFPL &= ~SYS_GPB_MFPL_PB5MFP_Msk;
	SYS->GPB_MFPL |= SYS_GPB_MFPL_PB5MFP_I2C0_SCL;
	SYS->GPB_MFPL &= ~SYS_GPB_MFPL_PB4MFP_Msk;
	SYS->GPB_MFPL |= SYS_GPB_MFPL_PB4MFP_I2C0_SDA;
	/* 开启输入施密特触发器 */
	PB->SMTEN |= GPIO_SMTEN_SMTEN4_Msk | GPIO_SMTEN_SMTEN5_Msk;
	GPIO_SetMode(PB, BIT5, GPIO_MODE_OPEN_DRAIN);
	GPIO_SetMode(PB, BIT4, GPIO_MODE_OPEN_DRAIN);
	
	SYS_LockReg();
	
	I2C_Open(I2C0, speed);
}

static void iic0PDMATxConfig(uint32_t srcAddr, uint16_t txSize)
{
    uint32_t prioritygroup = 0x00U;

    PDMA_Open(PDMA, 1 << HARD_IIC_CH0_PDMA_CH);
    PDMA_SetTransferMode(PDMA, HARD_IIC_CH0_PDMA_CH, PDMA_I2C0_TX, FALSE, 0);
    PDMA_SetTransferCnt(PDMA, HARD_IIC_CH0_PDMA_CH, PDMA_WIDTH_8, txSize);
    PDMA_SetTransferAddr(PDMA, HARD_IIC_CH0_PDMA_CH, srcAddr, PDMA_SAR_INC, (uint32_t)&(I2C0->DAT), PDMA_DAR_FIX);
    PDMA_SetBurstType(PDMA, HARD_IIC_CH0_PDMA_CH, PDMA_REQ_SINGLE, 0);
    if (!NVIC_GetActive(PDMA_IRQn)) {
        NVIC_EnableIRQ(PDMA_IRQn);
		prioritygroup = NVIC_GetPriorityGrouping();
		NVIC_SetPriority(PDMA_IRQn, NVIC_EncodePriority(prioritygroup, 15, 0));
    }
    PDMA_EnableInt(PDMA, HARD_IIC_CH0_PDMA_CH, PDMA_INT_TRANS_DONE);
}

static void iic0PDMATxRetrigger(uint16_t txSize)
{
    PDMA_SetTransferCnt(PDMA, HARD_IIC_CH0_PDMA_CH, PDMA_WIDTH_8, txSize);
    PDMA_SetTransferMode(PDMA, HARD_IIC_CH0_PDMA_CH, PDMA_I2C0_TX, FALSE, 0);
}

void HardIICDeInit(HardIICCHEnum iicch, uint32_t speed)
{
	static struct __HardIICManager manager;
	
	if (sManager == NULL) {
		manager.iic0.txConfiged = 0;
		manager.iic0.rxConfiged = 0;
        manager.iic0.pdmaChTxUse = 0;
        manager.iic0.pdmaChRxUse = 0;
        manager.iic0.pdmaFinish = 0;
		manager.iic0.reversed = 0;
		manager.iic0.width = 8;
		manager.iic0.masterSlave = 1;
		manager.iic0.slaveAddr = 0x00;
		manager.iic0.speed = speed;
		manager.iic0.pdmaTxOldAddr = 0;
		manager.iic0.pdmaRxOldAddr = 0;
		
		sManager = &manager;
	}
	
	if (iicch == HARD_IIC_CH0) {
		iic0Init(speed);
	}
}

/**
 * @brief	开启中断
 * @param[in]	iicch 通道枚举值
 */
void HardIICEnableInt(HardIICCHEnum iicch)
{
	if (iicch == HARD_IIC_CH0) {
		I2C_EnableInt(I2C0);
		NVIC_EnableIRQ(I2C0_IRQn);
	}
}

/**
 * @brief	关闭中断
 * @param[in]	iicch 通道枚举值
 */
void HardIICDisableInt(HardIICCHEnum iicch)
{
	if (iicch == HARD_IIC_CH0) {
		I2C_DisableInt(I2C0);
		NVIC_DisableIRQ(I2C0_IRQn);
	}
}

/**
 * @brief	向写一个数据
 * @param[in]	iicch 通道枚举值
 * @param[in]	data  数据
 */
void HardIICSendOneData(HardIICCHEnum iicch, uint8_t data)
{
	if (iicch == HARD_IIC_CH0) {
		I2C_START(I2C0);
		I2C_SET_DATA(I2C0, data);
	}
}

/**
 * @brief	向指定地址写入一个数据
 * @param[in]	iicch  通道枚举值
 * @param[in]	slAddr 从机地址
 * @param[in]	data   数据
 * @retval	0	成功
 *			1	失败，或总线错误，或写超时
 */
uint8_t HardIICWriteOneByte(HardIICCHEnum iicch, uint8_t slAddr, uint8_t data)
{
	if (iicch == HARD_IIC_CH0) {
		return I2C_WriteByte(I2C0, slAddr, data);
	}

    return 1;
}

/**
 * @brief	向指定地址写指定长度的数据
 * @param[in]	iicch  通道枚举值
 * @param[in]	slAddr 从机地址
 * @param[in]	data   数据
 * @param[in]   len    数据长度
 * @retval	发送的字节数
 */
uint32_t HardIICWriteByteArray(HardIICCHEnum iicch, uint8_t slAddr, uint8_t *data, uint16_t len)
{
	if (iicch == HARD_IIC_CH0) {
		return I2C_WriteMultiBytes(I2C0, slAddr, data, len);
	}

    return 0;
}

/**
 * @brief	向指定地址的指定数据地址写入一个数据
 * @param[in]	iicch   通道枚举值
 * @param[in]	slAddr  从机地址
 * @param[in]	valAddr 数据地址
 * @param[in]	data    数据
 * @retval	0	成功
 *			1	失败，或总线错误，或写超时
 */
uint8_t HardIICWriteOneReg(HardIICCHEnum iicch, uint8_t slAddr, uint8_t valAddr, uint8_t regVal)
{
	if (iicch == HARD_IIC_CH0) {
		return I2C_WriteByteOneReg(I2C0, slAddr, valAddr, regVal);
	}

    return 0;
}

/**
 * @brief	向指定地址的指定数据地址写指定长度的数据
 * @param[in]	iicch   通道枚举值
 * @param[in]	slAddr  从机地址
 * @param[in]	valAddr 数据地址
 * @param[in]	data    数据
 * @param[in]   len     数据长度
 * @retval	发送的字节数
 */
uint32_t HardIICWriteArray(HardIICCHEnum iicch, uint8_t slAddr, uint8_t valAddr, uint8_t *data, uint16_t len)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_WriteMultiBytesOneReg(I2C0, slAddr, valAddr, data, len);
    }

    return 0;
}

/**
 * @brief	向指定地址的指定数据地址(16bit)写入一个数据
 * @param[in]	iicch   通道枚举值
 * @param[in]	slAddr  从机地址
 * @param[in]	regAddr 寄存器地址(2 Bytes)
 * @param[in]	data    数据
 * @retval	0	成功
 *			1	失败，或总线错误，或写超时
 */
uint8_t HardIICWrite16bitReg(HardIICCHEnum iicch, uint8_t slAddr, uint16_t regAddr, uint8_t regVal)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_WriteByteTwoRegs(I2C0, slAddr, regAddr, regVal);
    }

    return 1;
}

/**
 * @brief	向指定地址的指定数据地址(16bit)写指定长度的数据
 * @param[in]	iicch   通道枚举值
 * @param[in]	slAddr  从机地址
 * @param[in]	valAddr 数据地址
 * @param[in]	data    数据
 * @param[in]   len     数据长度
 * @retval	发送的字节数
 */
uint32_t HardIICWriteArrayTo16BitAddr(HardIICCHEnum iicch, uint8_t slAddr, uint16_t valAddr, uint8_t *data, uint16_t len)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_WriteMultiBytesTwoRegs(I2C0, slAddr, valAddr, data, len);
    }

    return 0;
}

/**
 * @brief	从指定的地址读取一个字节
 * @param	iicch  IIC 通道
 * @param	slAddr 从机地址
 * @retval	读取到的值
 */
uint8_t HardIICReadOneByte(HardIICCHEnum iicch, uint8_t slAddr)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_ReadByte(I2C0, slAddr);
    }

    return 1;
}

/**
 * @brief	直接读取指定长度的数据
 * @param	iicch   IIC 通道
 * @param	slAddr  从机地址
 * @param	receive 用于接收数据的数组
 * @param	len     接收的数据长度
 * @retval	实际接收到的数据长度
 */
uint32_t HardIICReadByteArray(HardIICCHEnum iicch, uint8_t slAddr, uint8_t *receive, uint16_t len)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_ReadMultiBytes(I2C0, slAddr, receive, len);
    }

    return 0;
}

/**
 * @breif	读取器件的一个寄存器
 * @param	iicch   IIC 通道
 * @param	slAddr  从机地址
 * @param	regAddr 寄存器地址
 * @retval	读取到的值
 */
uint8_t HardIICReadOneReg(HardIICCHEnum iicch, uint8_t slAddr, uint8_t regAddr)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_ReadByteOneReg(I2C0, slAddr, regAddr);
    }

    return 1;
}

/**
 * @breif	从指定寄存器地址读取指定长度的数据
 * @param	iicch   IIC 通道
 * @param	slAddr  从机地址
 * @param	regAddr 寄存器地址
 * @param	receive 用于接收数据的数组
 * @param	len     接收的数据长度
 * @retval	实际接收到的数据长度
 */
uint32_t HardIICReadByteArrayOneReg(HardIICCHEnum iicch, uint8_t slAddr, uint8_t regAddr, uint8_t *receive, uint16_t len)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_ReadMultiBytesOneReg(I2C0, slAddr, regAddr, receive, len);
    }

    return 0;
}

/**
 * @brief	从指定寄存器地址读取数据
 * @param	iicch   IIC 通道
 * @param	slAddr  从机地址
 * @param	regAddr 寄存器地址(2 Bytes)
 * @retval	读取到的值
 */
uint8_t HardIICReadByteOne16bitReg(HardIICCHEnum iicch, uint8_t slAddr, uint16_t regAddr)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_ReadByteTwoRegs(I2C0, slAddr, regAddr);
    }

    return 0;
}

/**
 * @brief	从指定寄存器读取指定长度的数据
 * @param	iicch   IIC 通道
 * @param	slAddr  从机地址
 * @param	regAddr 寄存器地址
 * @param	receive 用于接收数据的数组
 * @param	len     接收的数据长度
 * @retval	实际接收到的数据长度
 */
uint32_t HardIICReadArrayOne16bitReg(HardIICCHEnum iicch, uint8_t slAddr, uint16_t regAddr, uint8_t *receive, uint16_t len)
{
    if (iicch == HARD_IIC_CH0) {
        return I2C_ReadMultiBytesTwoRegs(I2C0, slAddr, regAddr, receive, len);
    }

    return 0;
}

void HardIICSendByteArrayPDMA(HardIICCHEnum iicch, uint8_t *data, uint16_t len)
{
    if (iicch == HARD_IIC_CH0) {
        while (!HardIICGetPDMAFinish(iicch));
        sManager->iic0.pdmaFinish = 0;
        if (sManager->iic0.pdmaChRxUse) {
            sManager->iic0.pdmaChRxUse = 0;
            sManager->iic0.pdmaChTxUse = 1;
            sManager->iic0.pdmaTxOldAddr = (uint32_t)data;
            sManager->iic0.txConfiged = 1;
            iic0PDMATxConfig((uint32_t)data, len);
            I2C0->CTL1 = I2C_CTL1_TXPDMAEN_Msk;
            I2C_START(I2C0);
        }
        else {
            if (sManager->iic0.pdmaTxOldAddr != (uint32_t)data) {
                sManager->iic0.txConfiged = 0;
            }
            if (sManager->iic0.txConfiged == 0) {
                iic0PDMATxConfig((uint32_t)data, len);
                sManager->iic0.txConfiged = 1;
                sManager->iic0.pdmaTxOldAddr = (uint32_t)data;
                I2C0->CTL1 = I2C_CTL1_TXPDMAEN_Msk;
                I2C_START(I2C0);
            }
            else {
                iic0PDMATxRetrigger(len);
                I2C_START(I2C0);
            }
        }
    }
}

uint8_t HardIICGetPDMAFinish(HardIICCHEnum iicch)
{
    if (iicch == HARD_IIC_CH0) {
        return sManager->iic0.pdmaFinish;
    }

    return 0;
}

void HardIICCallbackInPDMAISR(HardIICCHEnum iicch)
{
    if (iicch == HARD_IIC_CH0) {
        PDMA->TDSTS = 0x01 << HARD_IIC_CH0_PDMA_CH;
        sManager->iic0.pdmaFinish = 1;
        if (sManager->iic0.pdmaChTxUse) {
            I2C0->CTL1 &= ~(1 << I2C_CTL1_TXPDMAEN_Msk);
        }
        else {
            I2C0->CTL1 &= ~(1 << I2C_CTL1_RXPDMAEN_Msk);
        }
    }
}

/**
 * @brief	中断处理
 * @param[in]	iicch 通道枚举值
 */
void HardIICIRQHandle(HardIICCHEnum iicch)
{
    uint32_t status;

	if (iicch == HARD_IIC_CH0) {
		if (I2C_GET_TIMEOUT_FLAG(I2C0)) {
			I2C_ClearTimeoutFlag(I2C0);
		}

        status = I2C_GET_STATUS(I2C0);
        /* 在主机模式下
         * 0x08: 开始信号已经发送；PDMA 传输下，该中断不会触发
         * 0x10: 开始信号已经重复发送
         * 0x18: 从机地址和写比特已发送，收到应答信号；PDMA 传输下，该中断不会触发
         * 0x20: 从机地址和写比特已发送，收到非应答信号
         * 0x28: 数据已发送，收到应答信号；PDMA 传输下，该中断不会触发
         * 0x40: 从机地址和读比特已发送，收到应答信号
         * 0x50: 数据已接收，收到应答信号；PDMA 传输下，该中断不会触发
         * 0x58: 数据已接收，收到非应答信号 */
        if (status == 0x08) {}
        else if (status == 0x10) {}
        else if (status == 0x18) {}
        else if (status == 0x20) {}
        else if (status == 0x28) {}
        else if (status == 0x40) {}
        else if (status == 0x50) {}
        else if (status == 0x58) {}
        /* 在从机模式下
         * 0x60: 收到自身地址和写比特，已返回应答信号
         * 0x80: 先前的地址和自身地址已接收，已返回应答信号
         * 0x88: 先前的地址和自身地址已接收，返回非应答信号
         * 0xA0: 已接收到停止或重复的开始信号，但仍寻址为从机
         * 0xA8: 收到自身的地址和读比特，已返回应答信号；PDMA 传输下，该中断不会触发
         * 0xB8: 在发送寄存器中的数据已发送，已发送应答信号；PDMA 传输下，该中断不会触发
         * 0xC0: 在发送寄存器中的数据已发送，已发送非应答信号；PDMA 传输下，该中断不会触发
         * 0x88: 先前的地址为自身的地址，收到非应答信号 */
        else {}
	}
}
