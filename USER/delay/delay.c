#include "delay.h"
#include "NuMicro.h"

static uint32_t US_COE;
static uint32_t MS_COE;
static uint32_t MAX_DELAY_TIME;

/**
  * @brief		Systick初始化
  * @param[in]	系统时钟，单位MHz
  */
void delayInit(uint32_t SysCLK)
{
#if USE_FREE_RTOS
	uint32_t inputClk;
#endif
	
	SYS_UnlockReg();
	
#if USE_FREE_RTOS
	CLK_SetModuleClock(TMR3_MODULE, CLK_CLKSEL1_TMR3SEL_PCLK1, 20);
	CLK_EnableModuleClock(TMR3_MODULE);
	SystemCoreClockUpdate();
	
	inputClk = TIMER_GetModuleClock(TIMER3);
	TIMER3->CTL = TIMER_PERIODIC_MODE;
	US_COE = inputClk / 1000000;
	MS_COE = US_COE * 1000;
#else
	CLK_SetSysTickClockSrc(CLK_CLKSEL0_STCLKSEL_HCLK_DIV2);
	SystemCoreClockUpdate();
	
	US_COE = SysCLK / 2;
	MS_COE = US_COE * 1000;
#endif
	
	MAX_DELAY_TIME = 16777216 / US_COE / 1000;
	SYS_LockReg();
}

/**
 * @brief		延迟 num 个空指令
 * @param[in]	空指令的个数
 */
void delayByNop(uint32_t num)
{
	uint32_t i;
	
	for (i = 0; i < num; i++) {
		__nop();
	}
}

/**
  * @brief		延迟nus
  * @param[in]	ns数
  */
void delayUs(uint32_t nus)
{
	volatile uint32_t temp;

#if USE_FREE_RTOS
	TIMER3->CNT = 0;
	if (nus == 0) {
		TIMER_SET_CMP_VALUE(TIMER3, US_COE / 2);
	}
	else {
		TIMER_SET_CMP_VALUE(TIMER3, nus * US_COE);
	}
	TIMER_ClearIntFlag(TIMER3);
	TIMER_Start(TIMER3);
	temp = TIMER_GetIntFlag(TIMER3);
	while (temp != 0x01) {
		temp = TIMER_GetIntFlag(TIMER3);
	}
	TIMER_Stop(TIMER3);
#else
	if (nus == 0)
		SysTick->LOAD = US_COE / 2;
	else
		SysTick->LOAD = nus * US_COE;
	SysTick->VAL = 0;
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
	do {
		temp = SysTick->CTRL;
	}
	while ((temp&0x01) && !(temp&(1<<16)));
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
	SysTick->VAL = 0x00;
#endif
}

/**
  * @brief		延迟nms
  * @param[in]	ms数
  * @note		上限约为174ms
  */
void delayMs(uint16_t nms)
{
	uint32_t temp;
	
#if USE_FREE_RTOS
	TIMER3->CNT = 0;
	TIMER_SET_CMP_VALUE(TIMER3, nms * MS_COE);
	TIMER_ClearIntFlag(TIMER3);
	TIMER_Start(TIMER3);
	temp = TIMER_GetIntFlag(TIMER3);
	while (temp != 0x01) {
		temp = TIMER_GetIntFlag(TIMER3);
	}
	TIMER_Stop(TIMER3);
#else	
	SysTick->LOAD = (uint32_t)nms * MS_COE;
	SysTick->VAL = 0;
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
	do {
		temp = SysTick->CTRL;
	}
	while ((temp&0x01) && !(temp&(1<<16)));
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
	SysTick->VAL = 0x00;
#endif
}

/**
  * @brief		延迟ms
  * @param[in]	ms数
  * @note		上限为 uint32_t 的最大值
  */
void delayXms(uint32_t nms)
{
	uint8_t repeat = nms / MAX_DELAY_TIME;
	uint16_t remain = nms % MAX_DELAY_TIME;
	
	while (repeat) {
		delayMs(MAX_DELAY_TIME);
		repeat--;
	}
	if (remain) {
		delayMs(remain);
	}
}
