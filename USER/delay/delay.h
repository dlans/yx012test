#ifndef __DELAY_H
#define __DELAY_H

#include "stdint.h"

#define USE_FREE_RTOS 1

void delayInit(uint32_t SysCLK);
void delayByNop(uint32_t num);
void delayUs(uint32_t nus);
void delayMs(uint16_t nms);
void delayXms(uint32_t nms);

#endif
