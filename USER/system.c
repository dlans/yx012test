/****************************************************************************
 * @file	system.c
 * @version	V0.1
 * @brief	Nuvoton M483
 * @detail	Pin Info
 *				PA6  ==> KEY2
 *				PA7  ==> KEY1
 *				PA8  ==> KEY5
 *				PA10 ==> KEY3
 *				PA11 ==> KEY4
 *				PA12 ==> BPWM1_CH2
 *
 *				PB2  ==> UART1_RX
 *				PB3  ==> UART1_TX
 *				PB4  ==> IIC0_DAT
 *				PB5  ==> IIC0_CLK
 *				PB7  ==> KEY6
 *				PB8  ==> KEY7
 *				PB9  ==> KEY8
 *				PB12 ==> TRIM_AD/TRIM_IIC_CLK/TRIM_SPI_CLK
 *				PB13 ==> TRIM_CE/TRIM_IIC_DAT/TRIM_SPI_MOSI
 *
 *				PC0  ==> SPI1_CS
 *				PC1  ==> SPI1_CLK
 *				PC3  ==> SPI1_MISO
 *				PC6  ==> SPI1_MOSI
 *				PC7  ==> BPWM1_CH0
 *				PC13 ==> UART2_TX
 *
 *				PD12 ==> UART2_RX
 *
 *				PF2  ==> XT1_Out
 *				PF3  ==> XT1_In
 *				PF4  ==> X32_Out
 *				PF5  ==> X32_In
 *				PF6  ==> SPI0_MOSI
 *				PF7  ==> SPI0_MISO
 *				PF8  ==> SPI0_CLK
 *				PF9  ==> SPI0_CS
 *
 *				PG12 ==> BPWM0_CH2
 *				PG14 ==> BPWM0_CH0
 *
 *				PH4  ==> LED0
 *				PH5  ==> LED1
 *				PH6  ==> LED2
*****************************************************************************/
#include "system.h"
#include "delay.h"
#include "timer_peripheral.h"
#include "iic_peripheral.h"
#include "util_str.h"
#include "cmd.h"
#include "key.h"
#include "led.h"
#include "trim.h"
#include "string.h"

#include "test.h"

extern void UserMain(void);

static uint8_t *s_receive_cache = NULL;
static uint16_t s_receive_size;

static TaskHandle_t s_start_handle = NULL;
static TaskHandle_t s_led_handle = NULL;
static TaskHandle_t s_cmd_check_handle = NULL;
static TaskHandle_t s_user_main_handle = NULL;
static CmdUartHandler *s_uart_handler[CMD_UART_NUMBER] = {&g_uart2};
static CmdSpiHandler *s_spi_master_handler[CMD_SPI_MASTER_NUMBER] = {&g_spi0};
static CmdSpiHandler *s_spi_slave_handler[CMD_SPI_SLAVE_NUMBER] = {&g_spi1};

EventGroupHandle_t g_app_events = NULL;
EventGroupHandle_t g_key_events = NULL;
CmdManagerType g_cmd_uart_manager;
UartPeripheral g_uart1;
UartPeripheral g_uart2;
SpiPeripheral g_spi0;
SpiPeripheral g_spi1;
BpwmPeripheral g_bpwm0;
BpwmPeripheral g_bpwm1;

static void peripheralSpiInit(void);
static void peripheralUartInit(void);
static void peripheralBpwmInie(void);
static void startTask(void *param);
static void ledTask(void *param);
static void cmdCheckTask(void *param);
static void userTask(void *param);
static void uart1DmaTimeout(UartPeripheral *uart);
static void uart1DmaRecDone(UartPeripheral *uart);
static void switchClockToLXT(void);
static void switchClkockToLIRC(void);
static void fixCoreClock(void);

int main(void)
{
    BaseType_t result = pdPASS;
	s_receive_size = SYSTEM_DEFAULT_REC_SIZE;

	SystemSetInterruptGroup(NVIC_PRIORITYGROUP_4);
	SystemClockConfig();

	delayInit(192);
	peripheralSpiInit();
	peripheralUartInit();
	peripheralBpwmInie();
	HardIICDeInit(HARD_IIC_CH0, 400000);
	HardIICDisableInt(HARD_IIC_CH0);
	LedInit();
	KeyInit();
	TrimInit();
	
	CmdInit(&g_cmd_uart_manager);
	CmdSystemInit();
	CmdUartInit(s_uart_handler);
	CmdSpiInit(s_spi_master_handler, s_spi_slave_handler);
	CmdPca9555Init();
	
	UartPuts(&g_uart1, "Run...");
    LedOff(LED_00);

    result = xTaskCreate(startTask, "startTask", 512, NULL, 2, &s_start_handle);
    if (result != pdPASS) {
        UartPuts(&g_uart1, "FreeRTOS start fail\r\n");
    }
    else {
        vTaskStartScheduler();
    }

    while(1) {}
}

void vApplicationIdleHook(void)
{
	UartCheckAndClearError(&g_uart1);
	UartCheckAndClearError(&g_uart2);
}

/**
 * @brief		系统时钟配置
 */
uint8_t SystemClockConfig(void)
{
	uint32_t result;
	uint8_t retVal = 0;
	
	SYS_UnlockReg();
	
	/* Enable HIRC clock */
    CLK_EnableXtalRC(CLK_PWRCTL_HIRCEN_Msk);
	/* Wait for HIRC clock ready */
    result = CLK_WaitClockReady(CLK_STATUS_HIRCSTB_Msk);
	if (result == 0) {
		retVal |= SYSTEM_CLK_HIRC_NOT_STABLE;
	}
	
	/* Set XT1_OUT(PF.2) and XT1_IN(PF.3) to input mode */
	PF->MODE &= ~(GPIO_MODE_MODE2_Msk | GPIO_MODE_MODE3_Msk);
	/* Enable External XTAL (4~24 MHz) */
    CLK_EnableXtalRC(CLK_PWRCTL_HXTEN_Msk);
	/* Waiting for 12MHz clock ready */
    result = CLK_WaitClockReady(CLK_STATUS_HXTSTB_Msk);
	if (result == 0) {
		retVal |= SYSTEM_CLK_HXT_NOT_STABLE;
	}
	
	/* 设置LXT时钟引脚 */
	PF->MODE &= ~(GPIO_MODE_MODE3_Msk | GPIO_MODE_MODE4_Msk);
	/* 启用LXT时钟 */
	CLK_EnableXtalRC(CLK_PWRCTL_LXTEN_Msk);
	/* 等待LXT时钟稳定 */
	result = CLK_WaitClockReady(CLK_STATUS_LXTSTB_Msk);
	if (result == 0) {
		retVal |= SYSTEM_CLK_LXT_NOT_STABLE;
	}
	
	if (CLK->STATUS & CLK_STATUS_HXTSTB_Msk) {
		/* Set core clock as PLL_CLOCK from PLL */
		CLK_SetCoreClock(FREQ_192MHZ);
	}
	else if (CLK->STATUS & CLK_STATUS_HIRCSTB_Msk) {
		CLK_DisableXtalRC(CLK_PWRCTL_HXTEN_Msk);
		CLK_SetCoreClock(FREQ_192MHZ);
	}
	else if (CLK->STATUS & CLK_STATUS_LXTSTB_Msk) {
		CLK_DisableXtalRC(CLK_PWRCTL_HIRCEN_Msk);
		switchClockToLXT();
	}
	else {
		CLK_EnableXtalRC(CLK_PWRCTL_LIRCEN_Msk);
		CLK_WaitClockReady(CLK_STATUS_LIRCSTB_Msk);
		if (CLK->STATUS & CLK_STATUS_LIRCSTB_Msk) {
			switchClkockToLIRC();
		}
		else {
			fixCoreClock();
		}
	}
	
	/* Set PCLK0/PCLK1 to HCLK/2 */
    CLK->PCLKDIV = (CLK_PCLKDIV_APB0DIV_DIV2 | CLK_PCLKDIV_APB1DIV_DIV2);
	/* Update System Core Clock */
    /* User can use SystemCoreClockUpdate() to calculate SystemCoreClock. */
    SystemCoreClockUpdate();
	
	SYS_LockReg();
	
	return retVal;
}

void SystemSetInterruptGroup(uint32_t PriorityGroup)
{
	NVIC_SetPriorityGrouping(PriorityGroup);
}

void SystemSetReceiveSize(uint16_t size)
{
	if (size < SYSTEM_MIN_REC_SIZE) {
		size = SYSTEM_MIN_REC_SIZE;
	}
	
	s_receive_size = size;
	xEventGroupSetBits(g_app_events, SYSTEM_EVENT_UART_SIZE_CHANGE);
}

uint16_t SystemGetReceiveSize(void)
{
	return s_receive_size;
}

#if SYSTEM_CONFIG_PULSE_EN
void SystemPulseInit(void)
{
	GPIO_SetMode(PG, BIT2, GPIO_MODE_OUTPUT);
	PG2 = 0;
}

void SystemPulseGen(uint32_t nop)
{
	PG2 = 1;
	while (nop--) {
	}
	PG2 = 0;
}
#endif

static void peripheralSpiInit(void)
{
	g_spi0.instance = SPI0;
	g_spi0.wait_timeout = SystemCoreClock / 10;
	g_spi0.init.autonss = SPI_AUTO_NSS_ENABLE;
	g_spi0.init.mode = SPI_MODE_0;
	g_spi0.init.device_mode = SPI_MASTER;
	g_spi0.init.nss_level = SPI_SS_ACTIVE_LOW;
	g_spi0.init.speed = 1000000;
	g_spi0.init.width = 8;
	g_spi0.it.priority = 14;
	g_spi0.dma_tx.instance = PDMA;
	g_spi0.dma_tx.channel = 15;
	g_spi0.dma_tx.it_priority = 13;
	g_spi0.dma_tx.timeout = 0x55;
	g_spi0.dma_rx.instance = PDMA;
	g_spi0.dma_rx.channel = 14;
	g_spi0.dma_rx.it_priority = 13;
	g_spi0.dma_rx.timeout = 0x55;
	SpiInit(&g_spi0);
	
	g_spi1.instance = SPI1;
	g_spi1.wait_timeout = SystemCoreClock / 10;
	g_spi1.init.autonss = SPI_AUTO_NSS_DISABLE;
	g_spi1.init.mode = SPI_MODE_0;
	g_spi1.init.device_mode = SPI_SLAVE;
	g_spi1.init.nss_level = SPI_SS_ACTIVE_LOW;
	g_spi1.init.speed = 0;
	g_spi1.init.width = 8;
	g_spi1.it.priority = 14;
	g_spi1.dma_tx.instance = PDMA;
	g_spi1.dma_tx.channel = 13;
	g_spi1.dma_tx.it_priority = 13;
	g_spi1.dma_tx.timeout = 0x55;
	g_spi1.dma_rx.instance = PDMA;
	g_spi1.dma_rx.channel = 12;
	g_spi1.dma_rx.it_priority = 13;
	g_spi1.dma_rx.timeout = 0x55;
	SpiInit(&g_spi1);
}

static void peripheralUartInit(void)
{
	g_uart1.instance = UART1;
	g_uart1.wait_timeout = SystemCoreClock / 1000;
	g_uart1.init.baudrate = 460800;
	g_uart1.init.parity = UART_PARITY_NONE;
	g_uart1.init.stopbit = UART_STOP_BIT_1;
	g_uart1.init.width = UART_WORD_LEN_8;
	g_uart1.it.priority = 15;
	g_uart1.it.u.all = 0;
	g_uart1.it.u.bufferr_en = 1;
	g_uart1.dma_rx.instance = PDMA;
	g_uart1.dma_rx.channel = 0;
	g_uart1.dma_rx.timeout = 0x55;
	g_uart1.dma_tx.instance = PDMA;
	g_uart1.dma_tx.channel = 2;
	g_uart1.dma_tx.timeout = 0x55;
	g_uart1.gpio_tx.group = PB;
	g_uart1.gpio_tx.pin = BIT3;
	g_uart1.gpio_tx.pin_alia = PB3;
	g_uart1.gpio_rx.group = PB;
	g_uart1.gpio_rx.pin = BIT2;
	g_uart1.gpio_rx.pin_alia = PB2;
	UartInit(&g_uart1);
	
	g_uart2.instance = UART2;
	g_uart2.wait_timeout = SystemCoreClock / 1000;
	g_uart2.init.baudrate = 115200;
	g_uart2.init.parity = UART_PARITY_NONE;
	g_uart2.init.stopbit = UART_STOP_BIT_1;
	g_uart2.init.width = UART_WORD_LEN_8;
	g_uart2.it.priority = 14;
	g_uart2.it.u.all = 0;
	g_uart2.it.u.bufferr_en = 1;
	g_uart2.it.u.rls_en = 1;
	g_uart2.dma_rx.instance = PDMA;
	g_uart2.dma_rx.channel = 3;
	g_uart2.dma_rx.timeout = 0x55;
	g_uart2.dma_rx.it_priotiry = 13;
	g_uart2.dma_tx.instance = PDMA;
	g_uart2.dma_tx.channel = 4;
	g_uart2.dma_tx.timeout = 0x55;
	g_uart2.dma_tx.it_priotiry = 13;
	g_uart2.gpio_tx.group = PC;
	g_uart2.gpio_tx.pin = BIT13;
	g_uart2.gpio_tx.pin_alia = PC13;
	g_uart2.gpio_rx.group = PD;
	g_uart2.gpio_rx.pin = BIT12;
	g_uart2.gpio_rx.pin_alia = PD12;
	UartInit(&g_uart2);
}

static void peripheralBpwmInie(void)
{
	uint8_t i;

	g_bpwm0.instance = BPWM0;
	g_bpwm0.ch_en_msk = (BPWM_CH_0_MASK | BPWM_CH_2_MASK);
	g_bpwm0.fre_target = 100000;

	g_bpwm1.instance = BPWM1;
	g_bpwm1.ch_en_msk = (BPWM_CH_0_MASK | BPWM_CH_2_MASK);
	g_bpwm1.fre_target = 100000;

	for (i = 0; i < BPWM_CHANNEL_NUM; i++) {
		g_bpwm0.duty[0] = 50;
		g_bpwm1.duty[1] = 50;
	}

	BpwmInit(&g_bpwm0);
	BpwmInit(&g_bpwm1);
}

static void startTask(void *param)
{
    taskENTER_CRITICAL();
	
	g_app_events = xEventGroupCreate();
	g_key_events = xEventGroupCreate();

    xTaskCreate(ledTask, "ledTask", 128, NULL, 26, &s_led_handle);
	xTaskCreate(cmdCheckTask, "cmdCheckTask", 512, NULL, 29, &s_cmd_check_handle);
	xTaskCreate(userTask, "userTask", SYSTEM_UAER_MAIN_STACK_SIZE, NULL, 20, &s_user_main_handle);
	
	CmdSpiCreateSendTask(28);
	CmdSpiCreateExslaveTask(27);
	CmdUartCreateSendTask(28);
	CmdUartCreateReceiveTask(27);

    vTaskDelete(s_start_handle);

    taskEXIT_CRITICAL();
}

static void ledTask(void *param)
{
	while (1) {
		LedToggle(LED_00);
		vTaskDelay(500);
	}
}

static void cmdCheckTask(void *param)
{
	uint8_t result;
	uint16_t i;
	EventBits_t events;
	
	EventBits_t events_wait = SYSTEM_EVENT_UART_GET | SYSTEM_EVENT_UART_REC_DMA_DONE | SYSTEM_EVENT_UART_SIZE_CHANGE;
	
	UartRegisterCb(&g_uart1, UART_CB_ON_DMA_TIMEOUT, uart1DmaTimeout);
	UartRegisterCb(&g_uart1, UART_CB_ON_DMA_REC_DONE, uart1DmaRecDone);
	
	while (s_receive_size >= SYSTEM_MIN_REC_SIZE) {
		s_receive_cache = (uint8_t *)pvPortMalloc(s_receive_size);
		if (s_receive_cache == NULL) {
			s_receive_size = s_receive_size / 2;
		}
		else {
			break;
		}
	}
	if (s_receive_cache == NULL) {
		UartPutsEndl(&g_uart1, "cmdCheckTask fail");
		vTaskDelete(s_cmd_check_handle);
	}
	else {
		UartReceiveDMA(&g_uart1, s_receive_cache, s_receive_size);
	}
	
	while (1) {
		events = xEventGroupWaitBits(g_app_events, events_wait, pdTRUE, pdFALSE, portMAX_DELAY);
		
		if (SYSTEM_VALUE_CHECK(events, SYSTEM_EVENT_UART_GET) || SYSTEM_VALUE_CHECK(events, SYSTEM_EVENT_UART_REC_DMA_DONE)) {
			if (SYSTEM_VALUE_CHECK(events, SYSTEM_EVENT_UART_REC_DMA_DONE)) {
				UartPutsEndl(&g_uart1, "Warning: receive length equals to receive cache size");
			}
			else {
				UartReceiveDMAAbort(&g_uart1);
			}
			
			result = CmdCheck(&g_cmd_uart_manager, (char *)s_receive_cache);
			if (result != CMD_STAT_OK) {
				UartPrintfEndl(&g_uart1, "check error, return %d", result);
				if (result == CMD_ERR_CHAR_FAIL) {
					s_receive_cache[s_receive_size - 1] = '\0';
					CmdCheck(&g_cmd_uart_manager, (char *)s_receive_cache);
				}
			}
			
			for (i = 0; i < g_uart1.recelen; i++) {
				s_receive_cache[i] = 0;
			}
			UartReceiveDMA(&g_uart1, s_receive_cache, s_receive_size);
		}
		else if (SYSTEM_VALUE_CHECK(events, SYSTEM_EVENT_UART_SIZE_CHANGE)) {
			taskENTER_CRITICAL();
			
			UartReceiveDMAAbort(&g_uart1);
			vPortFree(s_receive_cache);
			while (s_receive_size >= SYSTEM_MIN_REC_SIZE) {
				s_receive_cache = (uint8_t *)pvPortMalloc(s_receive_size);
				if (s_receive_cache == NULL) {
					s_receive_size = s_receive_size / 2;
				}
				else {
					break;
				}
			}
			if (s_receive_cache == NULL) {
				UartPutsEndl(&g_uart1, "cmdCheckTask fail");
				vTaskDelete(s_cmd_check_handle);
			}
			else {
				UartReceiveDMA(&g_uart1, s_receive_cache, s_receive_size);
				UartPrintfEndl(&g_uart1, "new size: %d", s_receive_size);
			}
			
			taskEXIT_CRITICAL();
		}
	}
}

static void userTask(void *param)
{
	UserMain();
	vTaskDelete(s_user_main_handle);
	UartPutsEndl(&g_uart1, "UserMain delete");
}

static void uart1DmaTimeout(UartPeripheral *uart)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	xEventGroupSetBitsFromISR(g_app_events, SYSTEM_EVENT_UART_GET, &xHigherPriorityTaskWoken);
	
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void uart1DmaRecDone(UartPeripheral *uart)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	xEventGroupSetBitsFromISR(g_app_events, SYSTEM_EVENT_UART_REC_DMA_DONE, &xHigherPriorityTaskWoken);
	
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void switchClockToLXT(void)
{
}

static void switchClkockToLIRC(void)
{
}

static void fixCoreClock(void)
{
}

/*** (C) End Of File ***/
