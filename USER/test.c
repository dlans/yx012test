#include "test.h"
#include "system.h"
#include "delay.h"
#include "uart_peripheral.h"
#include "spi_peripheral.h"
#include "util_crc.h"
#include "stdio.h"

static uint8_t s_spi_send_array1[20] = {
	0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0,
	0xB0, 0xC0, 0xD0, 0xE0, 0xF0, 0x10, 0x20, 0x30, 0x40, 0x50
};
static uint16_t s_spi_send_array2[20] = {
	0x1010, 0x2020, 0x3030, 0x4040, 0x5050, 0x6060, 0x7070, 0x8080, 0x9090, 0xA0A0,
	0xB0B0, 0xC0C0, 0xD0D0, 0xE0E0, 0xF0F0, 0x1010, 0x2020, 0x3030, 0x4040, 0x5050
};
static uint32_t s_spi_send_array3[20] = {
	0x10101010, 0x20202020, 0x30303030, 0x40404040, 0x50505050, 0x60606060, 0x70707070, 0x80808080, 0x90909090, 0xAAAA0000,
	0x0000BBBB, 0x0000CCCC, 0xDDDD0000, 0xEEEE0000, 0xFFFF0000, 0x10101010, 0x20202020, 0x30303030, 0x40404040, 0x50505050
};

static uint32_t s_spi_send_array3_1[20] = {
	0x0000BBBB, 0x0000CCCC, 0xDDDD0000, 0xEEEE0000, 0xFFFF0000, 0x10101010, 0x20202020, 0x30303030, 0x40404040, 0x50505050,
	0x10101010, 0x20202020, 0x30303030, 0x40404040, 0x50505050, 0x60606060, 0x70707070, 0x80808080, 0x90909090, 0xAAAA0000
};

static uint8_t s_uart_send_array1[20] = {
	0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0,
	0xB0, 0xC0, 0xD0, 0xE0, 0xF0, 0x10, 0x20, 0x30, 0x40, 0x50
};

static uint8_t s_uart_it_rec_done;
static uint8_t s_uart_dma_rec_done;

static uint8_t s_spi_rec_array1[20];
static uint16_t s_spi_rec_array2[20];
static uint32_t s_spi_rec_array3[20];
static uint32_t s_spi_rec_array3_1[20];
static uint32_t s_spi_rec_array3_2[20];

static uint8_t s_uart_rec_array1[20];

static void uartOnITSendCb(UartPeripheral *uart)
{
	if (uart->instance == UART2) {
		UartPuts(&g_uart1, "uratOnITSendCb ==> instance == UART_COM2\r\n");
	}
}

static void uartOnDMASendDone(UartPeripheral *uart)
{
	if (uart->instance == UART2) {
		UartPuts(&g_uart1, "uartOnDMASendDone ==> instance == UART_COM2\r\n");
	}
}

static void uartOnITRecDone(UartPeripheral *uart)
{
	s_uart_it_rec_done = 1;
}

static void uartOnDMARecDone(UartPeripheral *uart)
{
	s_uart_dma_rec_done = 1;
}

void TestUtilCRC(void)
{
	static uint8_t data[] = {
		0x55, 0x12, 0x98, 0x34, 0x88, 0x12
	};
	
	uint16_t i;
	
	UartPuts(&g_uart1, "TestUtilCRC ==> Run...");
	UartPuts(&g_uart1, "TestUtilCRC ==> Source data:\r\n");
	for (i = 0; i < sizeof(data); i++) {
		UartPrintf(&g_uart1, "0x%02X", data[i]);
		if ((i % 10 == 0) && (i != 0)) {
			UartPuts(&g_uart1, "\r\n");
		}
		else {
			UartPuts(&g_uart1, " ");
		}
	}
	UartPuts(&g_uart1, "\r\n");
	
	UartPrintf(&g_uart1, "CRC4itu = 0x%X\r\n", CRC4itu(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC5epc = 0x%X\r\n", CRC5epc(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC5itu = 0x%X\r\n", CRC5itu(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC5usb = 0x%X\r\n", CRC5usb(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC6itu = 0x%X\r\n", CRC6itu(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC7mmc = 0x%X\r\n", CRC7mmc(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC8 = 0x%X\r\n", CRC8(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC8itu = 0x%X\r\n", CRC8itu(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC8rohc = 0x%X\r\n", CRC8rohc(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC8maxim = 0x%X\r\n", CRC8maxim(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC12cdma2000 = 0x%X\r\n", CRC12cdma2000(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16ibm = 0x%X\r\n", CRC16ibm(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16maxim = 0x%X\r\n", CRC16maxim(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16usb = 0x%X\r\n", CRC16usb(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16modbus = 0x%X\r\n", CRC16modbus(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16ccitt = 0x%X\r\n", CRC16ccitt(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16ccittFalse = 0x%X\r\n", CRC16ccittFalse(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16x25 = 0x%X\r\n", CRC16x25(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16ymodem = 0x%X\r\n", CRC16ymodem(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC16dnp = 0x%X\r\n", CRC16dnp(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC32 = 0x%X\r\n", CRC32(data, sizeof(data)));
	UartPrintf(&g_uart1, "CRC32mpeg2 = 0x%X\r\n", CRC32mpeg2(data, sizeof(data)));
	
	UartPuts(&g_uart1, "TestUtilCRC ==> New API done\r\n");
}

void TestSPITest(void)
{
	uint16_t i;
	
	uint16_t length1 = sizeof(s_spi_send_array1) / sizeof(s_spi_send_array1[0]);
	uint16_t length2 = sizeof(s_spi_send_array2) / sizeof(s_spi_send_array2[0]);
	uint16_t length3 = sizeof(s_spi_send_array3) / sizeof(s_spi_send_array3[0]);
	
	UartPuts(&g_uart1, "TestSPITest ==> Start SPI Test\r\n");
	UartPuts(&g_uart1, "\tSPI0_CLK  --> PF8\r\n");
	UartPuts(&g_uart1, "\tSPI0_MOSI --> PF6\r\n");
	UartPuts(&g_uart1, "\tSPI0_MISO --> PF7\r\n");
	UartPuts(&g_uart1, "\tSPI0_SS   --> PF9\r\n");
	
	/* Test: SpiSendOneData() when width is 8/16/32 */
	SpiSetWidth(&g_spi0, 8);
	for (i = 0; i < length1; i++) {
		SpiSendOneData(&g_spi0, s_spi_send_array1[i]);
	}
	delayMs(5);
	
	SpiSetWidth(&g_spi0, 16);
	for (i = 0; i < length2; i++) {
		SpiSendOneData(&g_spi0, s_spi_send_array2[i]);
	}
	delayMs(5);
	
	SpiSetWidth(&g_spi0, 32);
	for (i = 0; i < length3; i++) {
		SpiSendOneData(&g_spi0, s_spi_send_array3[i]);
	}
	
	/* Test: SpiSend() when width is 8/16/32 */
	delayXms(20);
	SpiSetWidth(&g_spi0, 8);
	SpiSend(&g_spi0, s_spi_send_array1, length1);
    delayMs(5);
	
	SpiSetWidth(&g_spi0, 16);
	SpiSend(&g_spi0, (uint8_t *)s_spi_send_array2, length2);
	delayMs(5);
	
	SpiSetWidth(&g_spi0, 32);
	SpiSend(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	delayMs(5);
	
	/* Test: SpiSend() continue called */
	delayXms(20);
	SpiSend(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	SpiSend(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	SpiSend(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	
	/* Test: SpiSendDma() when width is 8/16/32 */
	delayXms(10);
	SpiSetWidth(&g_spi0, 8);
	SpiSendDma(&g_spi0, s_spi_send_array1, length1);
	
    delayMs(5);
	SpiSetWidth(&g_spi0, 16);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array2, length2);
	
    delayMs(5);
	SpiSetWidth(&g_spi0, 32);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	
	/* Test: SpiSendDma() use the same array */
	delayMs(5);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	SpiWaitDmaDone(&g_spi0);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	
	/* Test: SpiSendDma() continue called */
	delayXms(5);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	SpiSendDma(&g_spi0, (uint8_t *)s_spi_send_array3, length3);
	
	UartPutsDMA(&g_uart1, "TestSPITest ==> SPI only send test done!\r\n");
	
	/* Test: SpiSendReceive() when width is 8/16/32 */
	delayXms(100);
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
		s_spi_rec_array2[i] = 0;
		s_spi_rec_array3[i] = 0;
	}
	SpiSetWidth(&g_spi0, 8);
	SpiSendReceive(&g_spi0, s_spi_send_array1, s_spi_rec_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	delayXms(5);
	SpiSetWidth(&g_spi0, 16);
	SpiSendReceive(&g_spi0, (uint8_t *)s_spi_send_array2, (uint8_t *)s_spi_rec_array2, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%04X ", s_spi_rec_array2[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	delayXms(5);
	SpiSetWidth(&g_spi0, 32);
	SpiSendReceive(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3[i]);
	}
	
	/* Test: SpiSendReceiveDma() when width is 8/16/32 */
	delayXms(100);
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
		s_spi_rec_array2[i] = 0;
		s_spi_rec_array3[i] = 0;
	}
	SpiSetWidth(&g_spi0, 8);
	SpiSendReceiveDma(&g_spi0, s_spi_send_array1, s_spi_rec_array1, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	SpiSetWidth(&g_spi0, 16);
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array2, (uint8_t *)s_spi_rec_array2, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%04X ", s_spi_rec_array2[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	SpiSetWidth(&g_spi0, 32);
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3[i]);
	}
	
	/* Test: SpiSendReceiveDma() continue called */
	delayXms(10);
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3, 20);
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3, 20);
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3[i]);
	}
	
	/* Test: SpiSendReceiveDma() use the same send_array and different rec_array */
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3_1, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3_1[i]);
	}
	
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3_2, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3_2[i]);
	}
	
	/* Test: SpiSendReceiveDma() use different send_array and the same rec_array */
	delayXms(10);
	for (i = 0; i < 20; i++) {
		s_spi_rec_array3_1[i] = 0;
		s_spi_rec_array3_1[i] = 0;
		s_spi_rec_array3[i] = 0;
	}
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3, (uint8_t *)s_spi_rec_array3, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3[i]);
	}
	SpiSendReceiveDma(&g_spi0, (uint8_t *)s_spi_send_array3_1, (uint8_t *)s_spi_rec_array3, 20);
	delayMs(5);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%08X ", s_spi_rec_array3[i]);
	}
}

void TestSpiMSTest(void)
{
	uint16_t i;
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
		s_spi_rec_array2[i] = 0;
		s_spi_rec_array3[i] = 0;
		s_spi_rec_array3_1[i] = 0;
		s_spi_rec_array3_2[i] = 0;
	}
	SpiSetWidth(&g_spi0, 8);
	SpiSetWidth(&g_spi1, 8);
	SpiReceiveIT(&g_spi1, s_spi_rec_array1, 20);
	SpiSend(&g_spi0, s_spi_send_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
	}
	
	delayXms(150);
	SpiReceiveIT(&g_spi1, s_spi_rec_array1, 20);
	SpiSend(&g_spi0, s_spi_send_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
	}
	
	delayXms(150);
	SpiReceiveIT(&g_spi1, s_spi_rec_array1, 20);
	SpiSend(&g_spi0, s_spi_send_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
	}
	
	/* Test: SPI1 start DMA receive and SPI0 block send, lost first data */
	delayXms(150);
	SpiReceiveDma(&g_spi1, s_spi_rec_array1, 20);
	SpiSend(&g_spi0, s_spi_send_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
	}
	
	delayXms(150);
	SpiReceiveDma(&g_spi1, s_spi_rec_array1, 20);
	SpiSend(&g_spi0, s_spi_send_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
	}
	
	delayXms(150);
	SpiReceiveDma(&g_spi1, s_spi_rec_array1, 20);
	SpiSend(&g_spi0, s_spi_send_array1, 20);
	UartPuts(&g_uart1, "TestSPITest ==> Get:");
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
	}
}

void TestSpiSlaveTest(void)
{
	uint8_t ret, i;
	
	for (i = 0; i < 20; i++) {
		s_spi_rec_array1[i] = 0;
		s_spi_rec_array2[i] = 0;
		s_spi_rec_array3[i] = 0;
	}
	
	/* Test: SpiReceive() */
	SpiSetWidth(&g_spi1, 8);
	ret = SpiReceive(&g_spi1, s_spi_rec_array1, 20);
	while (ret != SPI_RETURN_OK) {
		ret = SpiReceive(&g_spi1, s_spi_rec_array1, 20);
	}
	UartPrintf(&g_uart1, "%s ==>\r\n", __FUNCTION__);
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%02X ", s_spi_rec_array1[i]);
	}
	UartPuts(&g_uart1, "\r\n");
	
	SpiSetWidth(&g_spi1, 16);
	ret = SpiReceive(&g_spi1, s_spi_rec_array1, 20);
	while (ret != SPI_RETURN_OK) {
		ret = SpiReceive(&g_spi1, s_spi_rec_array1, 20);
	}
	UartPrintf(&g_uart1, "%s ==>\r\n", __FUNCTION__);
	for (i = 0; i < 20; i++) {
		if (i % 10 == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "0x%04X ", s_spi_rec_array2[i]);
	}
	UartPuts(&g_uart1, "\r\n");
}

void TestUartTest(void)
{
	uint16_t i;
	
	/* Test: UartSendByte() */
	for (i = 0; i < 20; i++) {
		UartSendByte(&g_uart2, s_uart_send_array1[i]);
	}
	
	/* Test: UartSend() */
	delayMs(3);
	UartSend(&g_uart2, s_uart_send_array1, 20);
	
	/* Test: UartSend() continue send */
	delayMs(3);
	UartSend(&g_uart2, s_uart_send_array1, 20);
	UartSend(&g_uart2, s_uart_send_array1, 20);
	UartSend(&g_uart2, s_uart_send_array1, 20);
	
	/* Test: UartSendIT() */
	delayMs(3);
	UartSendIT(&g_uart2, s_uart_send_array1, 20);
	
	/* Test: UartSendIT() continue send and register UART_CB_ON_IT_SEND_DONE */
	delayMs(3);
	/* Register callback */
	UartRegisterCb(&g_uart2, UART_CB_ON_IT_SEND_DONE, uartOnITSendCb);
	UartSendIT(&g_uart2, s_uart_send_array1, 20);
	UartSendIT(&g_uart2, s_uart_send_array1, 20);
	UartSendIT(&g_uart2, s_uart_send_array1, 20);
	/* Unregister callback */
	UartRegisterCb(&g_uart2, UART_CB_ON_IT_SEND_DONE, NULL);
	
	/* Test: UartSendDMA() */
	delayMs(3);
	UartSendDMA(&g_uart2, s_uart_send_array1, 20);
	/* Register callback */
	UartWaitSendDone(&g_uart2);
	UartRegisterCb(&g_uart2, UART_CB_ON_DMA_SEND_DONE, uartOnDMASendDone);
	UartSendDMA(&g_uart2, s_uart_send_array1, 20);
	UartSendDMA(&g_uart2, s_uart_send_array1, 20);
	UartSendDMA(&g_uart2, s_uart_send_array1, 20);
	/* Unregister callback */
	UartRegisterCb(&g_uart2, UART_CB_ON_DMA_SEND_DONE, NULL);
	
	/* Test: UartReceive() */
	delayMs(3);
	UartReceive(&g_uart2, s_uart_rec_array1, 20);
	UartPuts(&g_uart1, "UART2 Get:");
	for (i = 0; i < 20; i++) {
		if ((i % 10) == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "%02X ", s_uart_rec_array1[i]);
	}
	
	/* Test: UartReceiveIT() */
	for (i = 0; i < 20; i++) {
		s_uart_rec_array1[i] = 0;
	}
	UartRegisterCb(&g_uart2, UART_CB_ON_IT_REC_DONE, uartOnITRecDone);
	UartReceiveIT(&g_uart2, s_uart_rec_array1, 20);
	while (s_uart_it_rec_done == 0);
	for (i = 0; i < 20; i++) {
		if ((i % 10) == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "%02X ", s_uart_rec_array1[i]);
	}
	UartRegisterCb(&g_uart2, UART_CB_ON_IT_REC_DONE, NULL);
	
	/* UartReceiveDMA() */
	for (i = 0; i < 20; i++) {
		s_uart_rec_array1[i] = 0;
	}
	UartRegisterCb(&g_uart2, UART_CB_ON_DMA_REC_DONE, uartOnDMARecDone);
	UartReceiveDMA(&g_uart2, s_uart_rec_array1, 20);
	while (s_uart_dma_rec_done == 0);
	for (i = 0; i < 20; i++) {
		if ((i % 10) == 0) {
			UartPuts(&g_uart1, "\r\n");
		}
		UartPrintf(&g_uart1, "%02X ", s_uart_rec_array1[i]);
	}
	UartRegisterCb(&g_uart2, UART_CB_ON_DMA_REC_DONE, NULL);
}
