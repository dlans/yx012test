#include "M480_it.h"
#include "system.h"
#include "led.h"
#include "key.h"
#include "timer_peripheral.h"
#include "uart_peripheral.h"
#include "spi_peripheral.h"

void HardFault_Handler(void)
{
#ifdef __CC_ARM
	uint32_t msp, pc, psr;
	uint32_t exceptionNum;
	uint32_t *stack = NULL;
	
	msp = __get_MSP();
	__disable_irq();
	
	UartPuts(&g_uart1, "In HardFault Handler!!!\r\n");
	UartPuts(&g_uart1, "ALL IRQn close\r\n");
	UartPrintf(&g_uart1, "MSP(R13) = 0x%08X\r\n", msp);
	
	stack = (uint32_t *)msp;
	pc = stack[6];
	psr = stack[7];
	UartPrintf(&g_uart1, "R0 = 0x%08X\r\n", stack[0]);
	UartPrintf(&g_uart1, "R1 = 0x%08X\r\n", stack[1]);
	UartPrintf(&g_uart1, "R2 = 0x%08X\r\n", stack[2]);
	UartPrintf(&g_uart1, "R3 = 0x%08X\r\n", stack[3]);
	UartPrintf(&g_uart1, "R12 = 0x%08X\r\n", stack[4]);
	UartPrintf(&g_uart1, "LR(R14) = 0x%08X\r\n", stack[5]);
	UartPrintf(&g_uart1, "PC(R15) = 0x%08X\r\n", stack[6]);
	UartPrintf(&g_uart1, "PSR = 0x%08X\r\n", psr);
	
	if ((psr & (1 << 24)) == 0) {
		UartPrintf(&g_uart1, "PSR T bit is 0. HardFault caused by changing to ARM mode!\r\n");
	}
	
	exceptionNum = psr & xPSR_ISR_Msk;
	if (exceptionNum > 0) {
		UartPrintf(&g_uart1, "HardFault caused by IRQ #%d\r\n", exceptionNum - 16);
	}
	
	UartPrintf(&g_uart1, "HardFault location is at 0x%08X\r\n", pc);
#endif
	
	while (1) {}
}

void TMR0_IRQHandler(void)
{
    BaseType_t ulReturn;

    ulReturn = taskENTER_CRITICAL_FROM_ISR();

	TIMER_ClearIntFlag(TIMER0);

    taskEXIT_CRITICAL_FROM_ISR(ulReturn);
}

void TMR1_IRQHandler(void)
{
	TIMER_ClearIntFlag(TIMER1);
}

void TMR2_IRQHandler(void)
{
	TIMER_ClearIntFlag(TIMER2);
}

void TMR3_IRQHandler(void)
{
	TIMER_ClearIntFlag(TIMER3);
}

void GPA_IRQHandler(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	if (GPIO_GET_INT_FLAG(PA, BIT7)) {
		GPIO_CLR_INT_FLAG(PA, BIT7);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY1_PRESS, &xHigherPriorityTaskWoken);
	}
	
	if (GPIO_GET_INT_FLAG(PA, BIT6)) {
		GPIO_CLR_INT_FLAG(PA, BIT6);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY2_PRESS, &xHigherPriorityTaskWoken);
	}
	
	if (GPIO_GET_INT_FLAG(PA, BIT10)) {
		GPIO_CLR_INT_FLAG(PA, BIT10);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY3_PRESS, &xHigherPriorityTaskWoken);
	}
	
	if (GPIO_GET_INT_FLAG(PA, BIT11)) {
		GPIO_CLR_INT_FLAG(PA, BIT11);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY4_PRESS, &xHigherPriorityTaskWoken);
	}
	
	if (GPIO_GET_INT_FLAG(PA, BIT8)) {
		GPIO_CLR_INT_FLAG(PA, BIT8);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY5_PRESS, &xHigherPriorityTaskWoken);
	}
}

void GPB_IRQHandler(void)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (GPIO_GET_INT_FLAG(PB, BIT7)) {
		GPIO_CLR_INT_FLAG(PB, BIT7);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY6_PRESS, &xHigherPriorityTaskWoken);
	}
	
	if (GPIO_GET_INT_FLAG(PB, BIT8)) {
		GPIO_CLR_INT_FLAG(PB, BIT8);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY7_PRESS, &xHigherPriorityTaskWoken);
	}
	
	if (GPIO_GET_INT_FLAG(PB, BIT9)) {
		GPIO_CLR_INT_FLAG(PB, BIT9);
		xEventGroupSetBitsFromISR(g_key_events, KEY_EVENT_KEY8_PRESS, &xHigherPriorityTaskWoken);
	}
}

void UART1_IRQHandler(void)
{
	BaseType_t ulReturn = taskENTER_CRITICAL_FROM_ISR();
	
	UartInterruptHandle(&g_uart1);
	
	taskEXIT_CRITICAL_FROM_ISR(ulReturn);
}

void UART2_IRQHandler(void)
{
	BaseType_t ulReturn = taskENTER_CRITICAL_FROM_ISR();
	
	UartInterruptHandle(&g_uart2);
	
	taskEXIT_CRITICAL_FROM_ISR(ulReturn);
}

void SPI0_IRQHandler(void)
{
    SpiInterruptHandle(&g_spi0);
}

void SPI1_IRQHandler(void)
{
	SpiInterruptHandle(&g_spi1);
}

void I2C0_IRQHandler(void)
{
}

void PDMA_IRQHandler(void)
{
	BaseType_t ulReturn;
	
	uint32_t status = PDMA_GET_INT_STATUS(PDMA);
    uint32_t tdsts = PDMA->TDSTS;
	
	ulReturn = taskENTER_CRITICAL_FROM_ISR();

	/* PDMA AHB Bus Error */
	if (status & 0x01) {
		PDMA_CLR_ABORT_FLAG(PDMA, PDMA_GET_ABORT_STS(PDMA));
	}
	/* PDMA Transfer done */
	else if (status & 0x02) {
		PDMA_CLR_TD_FLAG(PDMA, PDMA_GET_TD_STS(PDMA));
	}
	/* CH0 timeout */
	else if (status & 0x100) {
        UartDMATimoutInterruptHandle(&g_uart1);
	}
	/* CH1 Timeout */
	else if (status & 0x200) {
	}

	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_uart1.dma_rx.channel)) {
		UartDMADoneInterruptHandle(&g_uart1);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_uart1.dma_tx.channel)) {
		UartDMADoneInterruptHandle(&g_uart1);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_uart2.dma_rx.channel)) {
		UartDMADoneInterruptHandle(&g_uart2);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_uart2.dma_tx.channel)) {
		UartDMADoneInterruptHandle(&g_uart2);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_spi1.dma_tx.channel)) {
		SpiDmaDoneHandle(&g_spi1);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_spi1.dma_rx.channel)) {
		SpiDmaDoneHandle(&g_spi1);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_spi0.dma_rx.channel)) {
		SpiDmaDoneHandle(&g_spi0);
	}
	if (SYSTEM_VALUE_CHECK(tdsts, 0x01 << g_spi0.dma_tx.channel)) {
		SpiDmaDoneHandle(&g_spi0);
	}
	
	taskEXIT_CRITICAL_FROM_ISR(ulReturn);
}
