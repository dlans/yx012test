/*
 * FreeRTOS V202212.01
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://github.com/FreeRTOS
 *
 */


#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 *
 * See http://www.freertos.org/a00110.html
 *----------------------------------------------------------*/

/* Ensure stdint is only used by the compiler, and not the assembler. */
/* 对不同的编译器使用不同的 <dtdint.h> */
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
	#include <stdint.h>
	extern uint32_t SystemCoreClock;
#endif

/**
 * @brief	FreeRTOS 基础配置选项
 * @note	- 调度器选择：
 *				- 0 = 使用协作式调度器
 *				- 1 = 使用抢占式调度器
 *			- 计算前导 0：
 *				- 0 = 不使用特殊方法
 *				- 1 = 使用特殊方法
 *				FreeRTOS 提供两种方法选择下一个要执行的任务：
 *				- 通用方法：
 *					1. 纯 C 语言实现，效率略低
 *					2. 可以用于所有 FreeRTOS 支持的硬件
 *					3. 不强制要求限制最大可用优先级数目
 *				- 特殊方法：
 *					1. 依赖特定汇编指令，一般为计算前导 0 指令 CLZ
 *					2. 效率略高
 *					3. 强制限制最大可用优先级数目为 32
 *			- 低功耗 tickless 模式
 *				- 可通过空闲函数钩子将 MCU 处于低功耗状态下，此时 SystickTimer 关闭
 *				- 不是所有 FreeRTOS 移植都实现此功能
 *			- 系统节拍计数器变量数据类型
 *				- 0 = 32位无符号整形
 *				- 1 = 16位无符号整形
 */
#define configUSE_PREEMPTION					1					/* 调度器方式选择 */
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 0					/* 是否使用计算前导 0 指令 */
#define configUSE_TICKLESS_IDLE                 0					/* 是否使能低功耗 tickless 模式 */
#define configCPU_CLOCK_HZ						(SystemCoreClock)	/* CPU 时钟频率 */
#define configTICK_RATE_HZ						((TickType_t)1000)	/* 系统节拍中断频率，一秒钟中断的次数 */
#define configMAX_PRIORITIES					(32)				/* 可使用的最大优先级 */
#define configMINIMAL_STACK_SIZE				(128)				/* 空闲任务堆栈大小 */
#define configMAX_TASK_NAME_LEN					(10)				/* 任务名字字符串长度 */
#define configUSE_16_BIT_TICKS					0					/* 系统节拍计数器变量数据类型 */
#define configIDLE_SHOULD_YIELD					1					/* 空闲任务是否放弃 CPU 使用权给其它同优先级任务 */
#define configUSE_MUTEXES						1					/* 是否使用互斥量 */
#define configQUEUE_REGISTRY_SIZE				8					/* 设置可以注册的信号量和消息队列个数 */
#define configUSE_RECURSIVE_MUTEXES				1					/* 是否使用递归互斥信号量 */
#define configUSE_COUNTING_SEMAPHORES			1					/* 是否使用计数信号量 */
#define configUSE_APPLICATION_TASK_TAG			0
#define configUSE_TRACE_FACILITY				1


/**
 * @brief FreeRTOS 中同内存分配有关的选项
 */
#define configSUPPORT_STATIC_ALLOCATION 0						/* 是否允许静态内存分配 */
#define configTOTAL_HEAP_SIZE			((size_t)(32 * 1024))	/* 系统所有的堆大小 */

/**
 * @brief	FreeRTOS 中同钩子函数有关的选项
 * @note	- 空闲钩子函数在每个空闲任务周期都会被调用，该函数需要由用户实现
 *			- 时间片钩子函数在时间片中断中被周期调用，需要用户实现，钩子函数必须非常短小
 *			  不能大量使用堆栈，不能调用以 FromISR 或 From_ISR 结尾的函数
 *			- 堆栈检测算法中：
 *				- 0 = 不使用堆栈检测
 *				- 1 = 使用堆栈检测算法1
 *				- 2 = 使用堆栈检测算法2
 *			  必须提供一个栈溢出钩子函数
 */
#define configUSE_IDLE_HOOK							1	/* 空闲钩子函数 */
#define configUSE_TICK_HOOK							0	/* 时间片钩子 */
#define configCHECK_FOR_STACK_OVERFLOW				0	/* 堆栈检测算法选择 */
#define configUSE_MALLOC_FAILED_HOOK				0	/* 内存申请失败钩子函数 */
#define configAPPLICATION_ALLOCATED_HEAP			0	/* 是否由编写者提供堆，可让编写者决定堆在内存中的位置，名称固定：uint8_t ucHeap[configTOTAL_HEAP_SIZE] */
#define configSTACK_ALLOCATION_FROM_SEPARATE_HEAP	0	/* 是否使用自定义的线程安全 pvPortMallocStack 和 vPortFreeStack 函数 */
#define configENABLE_HEAP_PROTECTOR					0	/* 是否启用 heap_4.c 和 heap_5.c 的堆指针边界检查和混淆 */

/**
 * @brief	FreeRTOS 与运行时间和任务状态收集有关的配置
 */
#define configGENERATE_RUN_TIME_STATS	0	/* 运行时间统计功能 */
#define configUSE_TRACE_FACILITY		1	/* 可视化跟踪调试 */

/**
 * @brief	FreeRTOS 与协程有关的选项
 * @note	- configUSE_CO_ROUTINES 开启后必须编译 croutine.c 文件
 */
#define configUSE_CO_ROUTINES			0	/* 是否使用协程 */
#define configMAX_CO_ROUTINE_PRIORITIES 1	/* 协程的有效数量 */

/**
 * @brief	FreeRTOS 与软件定时器有关的配置选项
 */
#define configUSE_TIMERS				1	/* 是否使用软件定时器 */
#define configTIMER_TASK_PRIORITY		(30) /* 软件定时器优先级 */
#define configTIMER_QUEUE_LENGTH		10	/* 定时器队列长度 */
#define configTIMER_TASK_STACK_DEPTH	(configMINIMAL_STACK_SIZE * 2)	/* 定时器任务堆栈大小 */

/**
 * @brief	FreeRTOS 可选函数配置
 */
#define INCLUDE_vTaskPrioritySet			1
#define INCLUDE_uxTaskPriorityGet			1
#define INCLUDE_vTaskDelete					1
#define INCLUDE_vTaskCleanUpResources		0
#define INCLUDE_vTaskSuspend				1
#define INCLUDE_vTaskDelayUntil				1
#define INCLUDE_vTaskDelay					1
#define INCLUDE_xEventGroupSetBitFromISR	1
#define INCLUDE_xTimerPendFunctionCall		1

/* Cortex-M specific definitions. 同中断有关 */
#ifdef __NVIC_PRIO_BITS
	/* __BVIC_PRIO_BITS will be specified when CMSIS is being used. */
	#define configPRIO_BITS       		__NVIC_PRIO_BITS
#else
	#define configPRIO_BITS       		4        /* 15 priority levels */
#endif

/* The lowest interrupt priority that can be used in a call to a "set priority" function. */
/* 在调用“设置优先级”时可以使用的最低中断优先级函数 */
#define configLIBRARY_LOWEST_INTERRUPT_PRIORITY			0x0F

/* The highest interrupt priority that can be used by any interrupt service
routine that makes calls to interrupt safe FreeRTOS API functions.  DO NOT CALL
INTERRUPT SAFE FREERTOS API FUNCTIONS FROM ANY INTERRUPT THAT HAS A HIGHER
PRIORITY THAN THIS! (higher priorities are lower numeric values. */
/* 系统可管理的最高中断优先级 */
#define configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY	5

/* Interrupt priorities used by the kernel port layer itself.  These are generic
to all Cortex-M ports, and do not rely on any particular library functions. */
#define configKERNEL_INTERRUPT_PRIORITY 		( configLIBRARY_LOWEST_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )
/* !!!! configMAX_SYSCALL_INTERRUPT_PRIORITY must not be set to zero !!!!
See http://www.FreeRTOS.org/RTOS-Cortex-M3-M4.html. */
#define configMAX_SYSCALL_INTERRUPT_PRIORITY 	( configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )

/* Normal assert() semantics without relying on the provision of an assert.h header file. */
#define configASSERT( x ) if( ( x ) == 0 ) { taskDISABLE_INTERRUPTS(); for( ;; ); }

/* Definitions that map the FreeRTOS port interrupt handlers to their CMSIS
standard names. */
#define vPortSVCHandler SVC_Handler
#define xPortPendSVHandler PendSV_Handler
#define xPortSysTickHandler SysTick_Handler

#endif /* FREERTOS_CONFIG_H */

