#ifndef __M480_IT_H
#define __M480_IT_H

#include "NuMicro.h"

void UaerHardFault_Handler(uint32_t lr, uint32_t msp, uint32_t psp);

#endif
