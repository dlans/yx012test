#ifndef __CMD_H
#define __CMD_H

#include "stdint.h"
#include "cmd_port.h"

#define CMD_RETURN_OK			0x0U
#define CMD_RETURN_ERR_MEMRY	0x1U

#define CMD_UART_NUMBER			1
#define CMD_SPI_MASTER_NUMBER	1
#define CMD_SPI_SLAVE_NUMBER	1

extern char * const gc_cmd_cmd_adc;
extern char * const gc_cmd_cmd_rz;
extern char * const gc_cmd_cmd_spi;
extern char * const gc_cmd_cmd_srz;
extern char * const gc_cmd_cmd_system;
extern char * const gc_cmd_cmd_timer;
extern char * const gc_cmd_cmd_uart;
extern char * const gc_cmd_cmd_urz;

extern char * const gc_cmd_par_address;
extern char * const gc_cmd_par_baud;
extern char * const gc_cmd_par_byte;
extern char * const gc_cmd_par_char;
extern char * const gc_cmd_par_clk0;
extern char * const gc_cmd_par_clock;
extern char * const gc_cmd_par_disable;
extern char * const gc_cmd_par_enable;
extern char * const gc_cmd_par_endian;
extern char * const gc_cmd_par_fre;
extern char * const gc_cmd_par_fs;
extern char * const gc_cmd_par_get;
extern char * const gc_cmd_par_hex;
extern char * const gc_cmd_par_icdiv;
extern char * const gc_cmd_par_idle;
extern char * const gc_cmd_par_lsb;
extern char * const gc_cmd_par_memory;
extern char * const gc_cmd_par_ms;
extern char * const gc_cmd_par_msb;
extern char * const gc_cmd_par_name;
extern char * const gc_cmd_par_pulen;
extern char * const gc_cmd_par_pulse;
extern char * const gc_cmd_par_pulwidth;
extern char * const gc_cmd_par_read;
extern char * const gc_cmd_par_read2;
extern char * const gc_cmd_par_readf;
extern char * const gc_cmd_par_readv;
extern char * const gc_cmd_par_receive;
extern char * const gc_cmd_par_regs;
extern char * const gc_cmd_par_reset;
extern char * const gc_cmd_par_rxen;
extern char * const gc_cmd_par_rxhigh;
extern char * const gc_cmd_par_rxlow;
extern char * const gc_cmd_par_rxmode;
extern char * const gc_cmd_par_set;
extern char * const gc_cmd_par_size;
extern char * const gc_cmd_par_speed;
extern char * const gc_cmd_par_spis;
extern char * const gc_cmd_par_th0;
extern char * const gc_cmd_par_th1;
extern char * const gc_cmd_par_tl0;
extern char * const gc_cmd_par_tl1;
extern char * const gc_cmd_par_txhigh;
extern char * const gc_cmd_par_txlow;
extern char * const gc_cmd_par_us;
extern char * const gc_cmd_par_version;
extern char * const gc_cmd_par_width;
extern char * const gc_cmd_par_write;

extern char * const gc_cmd_info_done;
extern char * const gc_cmd_warn_nd;
extern char * const gc_cmd_info_no;
extern char * const gc_cmd_info_no_param;
extern char * const gc_cmd_info_ok;
extern char * const gc_cmd_info_yes;

extern char * const gc_cmd_warn_mp;
extern char * const gc_cmd_warn_nmp;
extern char * const gc_cmd_warn_time;

extern char * const gc_cmd_err_error;
extern char * const gc_cmd_err_fail;
extern char * const gc_cmd_err_memory;
extern char * const gc_cmd_err_nsp;
extern char * const gc_cmd_err_range;
extern char * const gc_cmd_err_limit;

typedef enum CmdSendSeqEnum {
	CMD_SEND_SEQ_UNUSE,
	CMD_SEND_SEQ_US,
	CMD_SEND_SEQ_MS,
	CMD_SEND_SEQ_BYTE,
	CMD_SEND_SEQ_HWORD,
	CMD_SEND_SEQ_WORD,
	CMD_SEND_SEQ_TX_PULLDOWN,
	CMD_SEND_SEQ_TX_PULLUP,
	CMD_SEND_SEQ_RX_PULLDOWN,
	CMD_SEND_SEQ_RX_PULLUP
} CmdSendSeqEnum;

typedef struct CmdSendStruct {
	struct CmdSendStruct *next;
	CmdSendSeqEnum seq_enum;
	uint16_t length;
	uint8_t *stream;
} CmdSendStruct;

void CmdAddSendStruct(CmdSendStruct *head, CmdSendStruct *node);
void CmdDeleteSendStruct(CmdSendStruct *head);
void CmdCleanSendStruct(CmdSendStruct *head);

#ifdef USER_DEBUG_LOG
	void CmdPrintSendStruct(CmdSendStruct *first);
#endif

void CmdSystemInit(void);

void CmdUartInit(CmdUartHandler *handler_array[CMD_UART_NUMBER]);
void CmdUartCreateSendTask(UBaseType_t priority);
void CmdUartCreateReceiveTask(UBaseType_t priority);

void CmdSpiInit(CmdSpiHandler *master[CMD_SPI_MASTER_NUMBER], CmdSpiHandler *slave[CMD_SPI_SLAVE_NUMBER]);
void CmdSpiCreateSendTask(UBaseType_t priority);
void CmdSpiCreateExslaveTask(UBaseType_t priority);

void CmdAdcInit(void);

void CmdPca9555Init(void);

#endif
