#include "cmd.h"
#include "main.h"
#include "util_str.h"
#include "arm_math.h"
#include "string.h"

#define OUTPUT_FFT_RESULT 0

static char * const sc_info1 = "ADC state as follow";
static char * const sc_info2 = "ADC function: ";
static char * const sc_info3 = "FreCounter function: ";
static char * const sc_info4 = "FreCounter already enable";

static char * const sc_warn1 = "ADC is not enable";
static char * const sc_warn2 = "FreCounter is not enable";

struct CmdAdcStruct {
	uint16_t sample_n;
	uint32_t fs;
	float vref;
	
	uint16_t *sample_value;
	float32_t *cfft_input;
	float32_t *cfft_output;
	arm_cfft_instance_f32 *cfft;
};

static struct CmdAdcStruct *s_manager = NULL;

static void cmdAdcCallback(int argc, char *argv[], int index);
static void cmdAdcShowState(void);
static void parReadProcess(void);
static void parReadvProcess(void);
static void parReadfProcess(uint8_t *receive, uint16_t recelen);
static void parSet1Process(uint8_t *receive, char *param, uint16_t recelen);
static void parSet2Process(uint8_t *receive, char *param, uint16_t recelen);
static void reportFrequency(uint8_t mode);

void CmdAdcInit(void)
{
	static struct CmdAdcStruct manager;
	
	if (s_manager == NULL) {
		manager.sample_n = 1024;
		manager.fs = 102400;
		manager.vref = 3.3f;
		
		manager.sample_value = NULL;
		manager.cfft_input = NULL;
		manager.cfft_output = NULL;
		manager.cfft = NULL;
		
		s_manager = &manager;
	}
	
	CmdAppend(&g_cmd_uart_manager, gc_cmd_cmd_adc, cmdAdcCallback);
}

static void cmdAdcCallback(int argc, char *argv[], int index)
{
	uint16_t recelen = UartGetReceiveLen(&g_uart0);
	uint8_t *receive = UartGetRxBuffer(&g_uart0);
	
	if (argc == 0) {
		cmdAdcShowState();
		
		return;
	}
	
	if (argc == 1) {
		if (strcmp(argv[0], gc_cmd_par_set) == 0) {
			parSet1Process(&receive[index], argv[1], recelen - index);
		}
		else if (strcmp(argv[0], gc_cmd_par_read) == 0) {
			parReadProcess();
		}
		else if (strcmp(argv[0], gc_cmd_par_readv) == 0) {
			parReadvProcess();
		}
		else if (strcmp(argv[0], gc_cmd_par_readf) == 0) {
			parReadfProcess(&receive[index], recelen - index);
		}
		else {
			UartPuts(&g_uart0, gc_cmd_warn1);
		}
	}
	else if (argc == 2) {
		if (strcmp(argv[0], gc_cmd_par_set) == 0) {
			parSet2Process(&receive[index], argv[1], recelen - index);
		}
		else {
			UartPuts(&g_uart0, gc_cmd_warn1);
		}
	}
	else {
		UartPuts(&g_uart0, gc_cmd_warn1);
	}
}

static void cmdAdcShowState(void)
{
	UartPutsEndl(&g_uart0, sc_info1);
	
	UartPuts(&g_uart0, sc_info2);
	if (ADCIsRunning(&g_adc1)) {
		UartPutsEndl(&g_uart0, gc_cmd_par_enable);
	}
	else {
		UartPutsEndl(&g_uart0, gc_cmd_par_disable);
	}
	
	UartPuts(&g_uart0, sc_info3);
	if (ADCIsRunning(&g_adc2)) {
		UartPutsEndl(&g_uart0, gc_cmd_par_enable);
	}
	else {
		UartPutsEndl(&g_uart0, gc_cmd_par_disable);
	}
	
	UartPrintfEndl(&g_uart0, "sample points: %d", s_manager->sample_n);
	UartPrintfEndl(&g_uart0, "fs: %d", s_manager->fs);
	UartPrintfEndl(&g_uart0, "vref: %.2f", s_manager->vref);
}

static void parReadProcess(void)
{
	uint16_t result;
	
	if (ADCIsRunning(&g_adc1)) {
		result = ADCSoftwareTriggerAndRead(&g_adc1);
		UartPrintfEndl(&g_uart0, "ADC Value: %d", result);
	}
	else {
		UartPutsEndl(&g_uart0, sc_warn1);
	}
}

static void parReadvProcess(void)
{
	uint16_t result;
	float coe;
	
	if (ADCIsRunning(&g_adc1)) {
		result = ADCSoftwareTriggerAndRead(&g_adc1);
		coe = ADCGetCalculationCoe(&g_adc1);
		UartPrintfEndl(&g_uart0, "Voltage: %f", (float)result / coe * s_manager->vref);
	}
	else {
		UartPutsEndl(&g_uart0, sc_warn1);
	}
}

static void parReadfProcess(uint8_t *receive, uint16_t recelen)
{
	uint16_t option;
	
	int16_t index = StrGetNextNotSpace((char *)receive);
	
	if (index == -1) {
		reportFrequency(0);
	}
	else {
		option = StrGetDec((char *)receive, 0, 4);
		if (option == 1) {
			reportFrequency(1);
		}
		else if (option == 2) {
			reportFrequency(2);
		}
		else if (option == 3) {
			reportFrequency(3);
		}
		else if (option == 4) {
			reportFrequency(4);
		}
	}
}

static void parSet1Process(uint8_t *receive, char *param, uint16_t recelen)
{
	int16_t index = StrGetNextNotSpace((char *)receive);
	
	if (index == -1) {
		UartPuts(&g_uart0, gc_cmd_info1);
	}
	else {
		if (strncmp((char *)&receive[index], gc_cmd_par_enable, 6) == 0) {
			ADCEnable(&g_adc1);
			UartPutsEndl(&g_uart0, gc_cmd_par_enable);
		}
		else if (strncmp((char *)&receive[index], gc_cmd_par_disable, 7) == 0) {
			ADCDisable(&g_adc1);
			UartPutsEndl(&g_uart0, gc_cmd_par_disable);
		}
		else {
			UartPuts(&g_uart0, gc_cmd_warn1);
		}
	}
}

static void parSet2Process(uint8_t *receive, char *param, uint16_t recelen)
{
	int16_t index;
	uint32_t fs, result;
	
	uint8_t error = 0;
	
	if (strcmp(param, gc_cmd_par_fs) == 0) {
		fs = StrGetDec((char *)receive, 0, 8);
		if (ADCIsRunning(&g_adc2)) {
			result = TimerSetFrequency(&g_timer4, fs);
		}
		else {
			result = fs;
		}
		s_manager->fs = result;
		UartPrintfEndl(&g_uart0, "New fs is %d", result);
	}
	else if (strcmp(param, gc_cmd_par_fre) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		if (index == -1) {
			UartPuts(&g_uart0, gc_cmd_info1);
		}
		else {
			if (strncmp((char *)&receive[index], gc_cmd_par_enable, 6) == 0) {
				if (ADCIsRunning(&g_adc2)) {
					UartPutsEndl(&g_uart0, sc_info4);
				}
				else {
					s_manager->cfft = (arm_cfft_instance_f32 *)pvPortMalloc(sizeof(arm_cfft_instance_f32));
					if (s_manager->cfft == NULL) {
						error++;
					}
					
					if (error == 0) {
						s_manager->sample_value = (uint16_t *)pvPortMalloc(sizeof(uint16_t) * s_manager->sample_n);
						if (s_manager->sample_value == NULL) {
							vPortFree(s_manager->cfft);
							s_manager->cfft = NULL;
							
							error++;
						}
					}
					
					if (error == 0) {
						s_manager->cfft_input = (float32_t *)pvPortMalloc(sizeof(float32_t) * s_manager->sample_n * 2);
						if (s_manager->cfft_input == NULL) {
							vPortFree(s_manager->cfft);
							s_manager->cfft = NULL;
							
							vPortFree(s_manager->sample_value);
							s_manager->sample_value = NULL;
							
							error++;
						}
					}
					
					if (error == 0) {
						s_manager->cfft_output = (float32_t *)pvPortMalloc(sizeof(float32_t) * s_manager->sample_n);
						if (s_manager->cfft_output == NULL) {
							vPortFree(s_manager->cfft);
							s_manager->cfft = NULL;
							
							vPortFree(s_manager->sample_value);
							s_manager->sample_value = NULL;
							
							vPortFree(s_manager->cfft_input);
							s_manager->cfft_input = NULL;
							
							error++;
						}
					}
					
					if (error == 0) {
						arm_cfft_init_f32(s_manager->cfft, s_manager->sample_n);
						g_adc2.dma.dma_en = 1;
						g_adc2.dma.dma_it_en = 0;
						g_adc2.dma.circulation_en = 1;
						g_adc2.dma.address = (uint32_t)s_manager->sample_value;
						g_adc2.dma.length = s_manager->sample_n;
						/* Must be reinitialized, otherwise DMA frequency error */
						ADCInit(&g_adc2);
						ADCEnable(&g_adc2);
						result = TimerSetFrequency(&g_timer4, s_manager->fs);
						TimerEnable(&g_timer4);
						s_manager->fs = result;
						UartPutsEndl(&g_uart0, gc_cmd_par_enable);
					}
					else {
						UartPuts(&g_uart0, gc_cmd_error_memory);
					}
				}
			}
			else if (strncmp((char *)&receive[index], gc_cmd_par_disable, 7) == 0) {
				TimerDisable(&g_timer4);
				ADCDisable(&g_adc2);
				
				vPortFree(s_manager->cfft);
				s_manager->cfft = NULL;
				
				vPortFree(s_manager->sample_value);
				s_manager->sample_value = NULL;
				
				vPortFree(s_manager->cfft_input);
				s_manager->cfft_input = NULL;
				
				vPortFree(s_manager->cfft_output);
				s_manager->cfft_output = NULL;
				
				UartPutsEndl(&g_uart0, gc_cmd_par_disable);
			}
			else {
				UartPuts(&g_uart0, gc_cmd_warn1);
			}
		}
	}
}

/**
 * @brief	Check command "adc -readf"
 * @param	mode
 *	\arg		0 Use ADC2 sample result
 *	\arg		1 Use ADC2 sample and report FFT result
 *	\arg		2 Use fixed polynmials
 *	\arg		3 Use fixed polynmials and report FFT result
 *	\arg		4 Report ADC2 sample value
 */
static void reportFrequency(uint8_t mode)
{
	uint16_t i;
	float32_t max_value, frequency;
	uint32_t max_value_index;
	
	if (ADCIsRunning(&g_adc2) == 0) {
		UartPutsEndl(&g_uart0, sc_warn2);
		return;
	}
	
	TimerDisable(&g_timer4);
	
	if ((mode == 0) || (mode == 1)) {
		for (i = 0; i < s_manager->sample_n; i++) {
			s_manager->cfft_input[i*2] = (float32_t)s_manager->sample_value[i];
			s_manager->cfft_input[i*2+1] = 0.0f;
		}
	}
	else if ((mode == 2) || (mode == 3)) {
		for (i = 0; i < s_manager->sample_n; i++) {
			s_manager->cfft_input[i*2] = 100 + 10 * arm_sin_f32(2 * PI * i / s_manager->sample_n) + 30 * arm_sin_f32(2 * PI * i * 4 / s_manager->sample_n) + 50 * arm_cos_f32(2 * PI * i * 8 / s_manager->sample_n);
			s_manager->cfft_input[i*2+1] = 0.0f;
		}
	}
	else {
		UartPutsEndl(&g_uart0, "ADC2 Vlaue:");
		for (i = 0; i < s_manager->sample_n; i++) {
			UartPrintfEndl(&g_uart0, "sample_value[%d] = %d", i, s_manager->sample_value[i]);
			UartWaitSendDone(&g_uart0);
		}
		TimerEnable(&g_timer4);
		
		return;
	}
	TimerEnable(&g_timer4);
	
	arm_cfft_f32(s_manager->cfft, s_manager->cfft_input, 0, 1);
	arm_cmplx_mag_f32(s_manager->cfft_input, s_manager->cfft_output, s_manager->sample_n);
	s_manager->cfft_output[0] = 0;
	for (i = s_manager->sample_n / 2; i < s_manager->sample_n; i++) {
		s_manager->cfft_output[i] = 0;
	}
	arm_max_f32(s_manager->cfft_output, s_manager->sample_n, &max_value, &max_value_index);
	frequency = s_manager->fs / s_manager->sample_n * max_value_index;
	UartPrintfEndl(&g_uart0, "Frequency: %f", frequency);
	
	if ((mode == 1) || (mode == 3)) {
		UartPutsEndl(&g_uart0, "FFT Result:");
		for (i = 0; i < s_manager->sample_n; i++) {
			UartPrintfEndl(&g_uart0, "cfft_output[%d] = %f", i, s_manager->cfft_output[i]);
			UartWaitSendDone(&g_uart0);
		}
	}
	
	for (i = 0; i < s_manager->sample_n; i++) {
		s_manager->cfft_input[i*2] = 0;
		s_manager->cfft_input[i*2+1] = 0;
		s_manager->cfft_output[i] = 0;
	}
}
