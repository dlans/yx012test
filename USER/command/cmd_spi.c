#include "cmd.h"
#include "delay.h"
#include "main.h"
#include "string.h"
#include "util_str.h"

static char * const sc_info1 = "spi peripheral status:";
static char * const sc_info2 = "spi receive:";
static char * const sc_info3 = "exspi: ";
static char * const sc_info4 = "exspi receive:";
static char * const sc_err1 = "fail, exspi close";
static char * const sc_info_exspi = "exspi status:";

struct CmdSpiExslave {
	uint8_t enable : 1;
	uint8_t reserved : 7;
	
	uint16_t length;
	
	uint8_t *rcache8;
	uint16_t *rcache16;
	uint32_t *rcache32;
};

typedef enum {
	CMD_SPI_REC_MODE_EDGE_DETEC,
	CMD_SPI_REC_MODE_EDGE_ONCE,
	CMD_SPI_REC_MODE_REPORT,
} CmdSpiReceiveModeEnum;

typedef struct CmdSpiReceiveList {
	uint8_t *rec_cache;
	uint16_t size;
	struct CmdSpiReceiveList *next;
} CmdSpiReceiveList;

struct CmdSpiManager {
	uint8_t use_receive : 1;
	uint8_t reversed : 7;
	
	uint16_t edge_cnt;
	uint16_t rec_list_max_size;
	uint16_t receive_cache_size;
	uint8_t *receive_cache8;
	uint16_t *receive_cache16;
	uint32_t *receive_cache32;
	
	CmdSpiReceiveList rec_list_head;
	CmdSpiReceiveModeEnum rec_mode;
	CmdSendStruct send_seq;
	struct CmdSpiExslave slave;
};

static TaskHandle_t s_send_handle = NULL;
static TaskHandle_t s_exsalve_handle = NULL;
static struct CmdSpiManager *s_spi_manager = NULL;
static CmdSpiHandler *s_master[CMD_SPI_MASTER_NUMBER] = {NULL};
static CmdSpiHandler *s_slave[CMD_SPI_SLAVE_NUMBER] = {NULL};

static void cmdSpiCallback(int argc, char *argv[], int index);
static void cmdSpiShowStatus(void);
static void cmdSpiSendTask(void *param);
static void cmdSpiExsalveTask(void *param);
static void cbOnSpiExslaveDmaRecDone(SpiPeripheral *spi);

static void spiSetProcess(uint8_t *receive, char *param, uint16_t recelen);
static void spiSetSpisProcess(uint8_t *receive, char *param, uint16_t recelen);
static void spiReadProcess(uint8_t *receive, char *param, uint16_t recelen);
static uint8_t spiByteProcess(uint8_t *receive, uint16_t recelen);
static uint8_t spiHexProcess(uint8_t *receive, uint16_t recelen);
static uint8_t spiUsProcess(uint8_t *receive, uint16_t recelen);
static uint8_t spiMsProcess(uint8_t *receive, uint16_t recelen);

void CmdSpiInit(CmdSpiHandler *master[CMD_SPI_MASTER_NUMBER], CmdSpiHandler *slave[CMD_SPI_SLAVE_NUMBER])
{
	static struct CmdSpiManager manager;
	
	uint16_t i;
	
	if (s_spi_manager == NULL) {
		manager.use_receive = 0;
		
		manager.edge_cnt = 0;
		manager.rec_list_max_size = 256;
		manager.receive_cache_size = 0;
		manager.receive_cache8 = NULL;
		manager.receive_cache16 = NULL;
		manager.receive_cache32 = NULL;

		manager.rec_mode = CMD_SPI_REC_MODE_REPORT;
		
		manager.rec_list_head.next = NULL;
		manager.rec_list_head.rec_cache = NULL;
		manager.rec_list_head.size = 0;
		manager.send_seq.next = NULL;
		manager.send_seq.seq_enum = CMD_SEND_SEQ_UNUSE;
		manager.send_seq.length = 0;
		manager.send_seq.stream = NULL;
		
		manager.slave.enable = 0;
		manager.slave.length = 10;
		manager.slave.rcache8 = NULL;
		manager.slave.rcache16 = NULL;
		manager.slave.rcache32 = NULL;
		
		s_spi_manager = &manager;
		
		CmdAppend(&g_cmd_uart_manager, gc_cmd_cmd_spi, cmdSpiCallback);
	}
	for (i = 0; i < CMD_SPI_MASTER_NUMBER; i++) {
		s_master[i] = master[i];
	}
	for (i = 0; i < CMD_SPI_SLAVE_NUMBER; i++) {
		s_slave[i] = slave[i];
	}
}

void CmdSpiCreateSendTask(UBaseType_t priority)
{
	xTaskCreate(cmdSpiSendTask, "cmdSpiSendTask", 256, NULL, priority, &s_send_handle);
	vTaskSuspend(s_send_handle);
}

void CmdSpiCreateExslaveTask(UBaseType_t priority)
{
	xTaskCreate(cmdSpiExsalveTask, "cmdSpiExsalveTask", 256, NULL, priority, &s_exsalve_handle);
	vTaskSuspend(s_exsalve_handle);
}

static void cmdSpiCallback(int argc, char *argv[], int index)
{
	int i;
	int16_t offset_new, offset_old;
	
	uint8_t resume_task = 1;
	uint16_t recelen = CmdPortGetCommunicateLength();
	uint8_t *receive = CmdPortGetCommunicateCache();

	if (argc == 0) {
		cmdSpiShowStatus();
		return;
	}
	
	if (strcmp(argv[0], gc_cmd_par_set) == 0) {
		if (argc == 1) {
			CmdPortPutsEndl(gc_cmd_info_no_param);
		}
		else if (argc == 2) {
			spiSetProcess(&receive[index], argv[1], recelen - index);
		}
		else if (argc == 3) {
			if (strcmp(argv[1], gc_cmd_par_spis) == 0) {
				spiSetSpisProcess(&receive[index], argv[2], recelen - index);
			}
			else {
				CmdPortPutsEndl(gc_cmd_warn_nmp);
			}
		}
		else {
			CmdPortPutsEndl(gc_cmd_warn_nmp);
		}
		
		resume_task = 0;
	}
	else if (strcmp(argv[0], gc_cmd_par_read) == 0) {
		spiReadProcess(&receive[index], NULL, recelen - index);
		resume_task = 0;
	}
	else {
		offset_old = 0;
		for (i = 0; i < argc; i++) {
			offset_new = StrGetCHIndex((char *)&receive[offset_old], '-');
			offset_new += offset_old;
			
			if (strcmp(argv[i], gc_cmd_par_byte) == 0) {
				spiByteProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_hex) == 0) {
				spiHexProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_us) == 0) {
				spiUsProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_ms) == 0) {
				spiMsProcess(&receive[offset_new], recelen - offset_new);
			}
			
			offset_old = offset_new + 1;
		}
	}
	
	if (resume_task) {
		vTaskResume(s_send_handle);
	}
}

static void cmdSpiShowStatus(void)
{
	/* Main SPI info, default is master, use SPI0 */
	CmdPortPutsEndl(sc_info1);
	CmdPortPuts("device mode: ");
	if (CmdPortSpiGetMode(s_master[0]) == SPI_MASTER) {
		CmdPortPutsEndl("master");
		CmdPortPrintfEndl("speed: %d", CmdPortSpiGetSpeed(s_master[0]));
	}
	else {
		CmdPortPutsEndl("slave");
	}
	
	CmdPortPrintfEndl("width: %d", CmdPortSpiGetWidth(s_master[0]));
	CmdPortPrintfEndl("use receive: %d", s_spi_manager->use_receive);
	CmdPortPuts("receive mode: ");
	if (s_spi_manager->rec_mode == CMD_SPI_REC_MODE_EDGE_DETEC) {
		CmdPortPutsEndl("edge detec");
	}
	else if (s_spi_manager->rec_mode == CMD_SPI_REC_MODE_EDGE_ONCE) {
		CmdPortPutsEndl("edge once");
	}
	else {
		CmdPortPutsEndl("report");
	}
	CmdPortPrintfEndl("rec list max size: %d", s_spi_manager->rec_list_max_size);
	
	/* Extern SPI info, default is slave, use SPI1 */
	CmdPortPutsEndl(sc_info_exspi);
	CmdPortPuts(sc_info3);
	if (s_spi_manager->slave.enable) {
		CmdPortPutsEndl(gc_cmd_par_enable);
	}
	else {
		CmdPortPutsEndl(gc_cmd_par_disable);
	}
	CmdPortPrintfEndl("length: %d", s_spi_manager->slave.length);
	CmdPortPrintfEndl("width: %d", CmdPortSpiGetWidth(s_slave[0]));
}

static void cmdSpiSendTask(void *param)
{
	uint16_t i;
	uint32_t remainder, quotient;
	
	uint16_t rec_size_all = 0;
	uint16_t *arr16 = NULL;
	uint32_t *arr32 = NULL;
	CmdSendStruct *send_struct = NULL;
	CmdSpiReceiveList *rec_list_new = NULL;
	CmdSpiReceiveList *rec_list_old = NULL;
	uint32_t width = CmdPortSpiGetWidth(s_master[0]);
	
	while (1) {
		send_struct = s_spi_manager->send_seq.next;
		if (send_struct == NULL) {
			vTaskSuspend(s_send_handle);
		}

		if (s_spi_manager->use_receive) {
			if (s_spi_manager->rec_mode != CMD_SPI_REC_MODE_REPORT) {
				CmdPortSpiMisoToEdgeRaise(s_master[0]);
			}
			else {
				rec_list_new = &s_spi_manager->rec_list_head;
				rec_list_old = &s_spi_manager->rec_list_head;
			}
		}
		
		/* TODO: CMD_SEND_SEQ_BYTE/CMD_SEND_SEQ_HWORD/CMD_SEND_SEQ_WORD Their send logic can be conbined into a function */
		while (send_struct != NULL) {
			if (send_struct->seq_enum == CMD_SEND_SEQ_US) {
				if (send_struct->length > 999) {
					quotient = send_struct->length / 1000;
					remainder = send_struct->length % 1000;
					vTaskDelay(quotient);
					delayUs(remainder);
				}
				else {
					delayUs(send_struct->length);
				}
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_MS) {
				vTaskDelay(send_struct->length);
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_BYTE) {
				if (width != 8) {
					CmdPortSpiSetWidth(s_master[0], 8);
				}
				if (s_spi_manager->use_receive) {
					if (s_spi_manager->rec_mode == CMD_SPI_REC_MODE_REPORT) {
						rec_size_all += send_struct->length;
						if (rec_size_all > s_spi_manager->rec_list_max_size) {
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}

						rec_list_new = (CmdSpiReceiveList *)pvPortMalloc(sizeof(CmdSpiReceiveList));
						if (rec_list_new == NULL) {
						#ifdef USER_DEBUG_LOG
							CmdPortPutsEndl(gc_cmd_err_memory);
						#endif
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}
						else {
							rec_list_new->next = NULL;
							rec_list_new->rec_cache = NULL;
							rec_list_new->size = send_struct->length;
						}

						rec_list_new->rec_cache = (uint8_t *)pvPortMalloc(rec_list_new->size);
						if (rec_list_new->rec_cache == NULL) {
						#ifdef USER_DEBUG_LOG
							CmdPortPutsEndl(gc_cmd_err_memory);
						#endif
							/*  System don't have memory to receive data, only send data */
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}

						CmdPortSpiSendRecSync(s_master[0], send_struct->stream, rec_list_new->rec_cache, send_struct->length);
						rec_list_old->next = rec_list_new;
						rec_list_old = rec_list_new;
						s_spi_manager->rec_list_head.size++;
					}
					else {
						CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
					}
				}
				else {
					CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
				}
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_HWORD) {
				if ((width < 9) || (width > 16)) {
					CmdPortSpiSetWidth(s_master[0], 16);
				}
				if (s_spi_manager->use_receive) {
					if (s_spi_manager->rec_mode == CMD_SPI_REC_MODE_REPORT) {
						rec_size_all = rec_size_all + send_struct->length * 2;
						if (rec_size_all > s_spi_manager->rec_list_max_size) {
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}

						rec_list_new = (CmdSpiReceiveList *)pvPortMalloc(sizeof(CmdSpiReceiveList));
						if (rec_list_new == NULL) {
						#ifdef USER_DEBUG_LOG
							CmdPortPutsEndl(gc_cmd_err_memory);
						#endif
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}
						else {
							rec_list_new->next = NULL;
							rec_list_new->rec_cache = NULL;
							rec_list_new->size = send_struct->length;
						}

						rec_list_new->rec_cache = (uint8_t *)pvPortMalloc(rec_list_new->size * 2);
						if (rec_list_new->rec_cache == NULL) {
						#ifdef USER_DEBUG_LOG
							CmdPortPutsEndl(gc_cmd_err_memory);
						#endif
							/*  System don't have memory to receive data, only send data */
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}

						CmdPortSpiSendRecSync(s_master[0], send_struct->stream, rec_list_new->rec_cache, send_struct->length);
						rec_list_old->next = rec_list_new;
						rec_list_old = rec_list_new;
						s_spi_manager->rec_list_head.size++;
					}
					else {
						CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
					}
				}
				else {
					CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
				}
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_WORD) {
				if ((width < 17) || (width > 32)) {
					CmdPortSpiSetWidth(s_master[0], width);
				}
				if (s_spi_manager->use_receive) {
					if (s_spi_manager->rec_mode == CMD_SPI_REC_MODE_REPORT) {
						rec_size_all = rec_size_all + send_struct->length * 4;
						if (rec_size_all > s_spi_manager->rec_list_max_size) {
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}

						rec_list_new = (CmdSpiReceiveList *)pvPortMalloc(sizeof(CmdSpiReceiveList));
						if (rec_list_new == NULL) {
						#ifdef USER_DEBUG_LOG
							CmdPortPutsEndl(gc_cmd_err_memory);
						#endif
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}
						else {
							rec_list_new->next = NULL;
							rec_list_new->rec_cache = NULL;
							rec_list_new->size = send_struct->length;
						}

						rec_list_new->rec_cache = (uint8_t *)pvPortMalloc(rec_list_new->size * 4);
						if (rec_list_new->rec_cache == NULL) {
						#ifdef USER_DEBUG_LOG
							CmdPortPutsEndl(gc_cmd_err_memory);
						#endif
							/*  System don't have memory to receive data, only send data */
							CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
							send_struct = send_struct->next;
							continue;
						}

						CmdPortSpiSendRecSync(s_master[0], send_struct->stream, rec_list_new->rec_cache, send_struct->length);
						rec_list_old->next = rec_list_new;
						rec_list_old = rec_list_new;
						s_spi_manager->rec_list_head.size++;
					}
					else {
						CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
					}
				}
				else {
					CmdPortSpiSendSync(s_master[0], send_struct->stream, send_struct->length);
				}
			}

			send_struct = send_struct->next;
		}

		if (s_spi_manager->use_receive) {
			if (s_spi_manager->rec_mode == CMD_SPI_REC_MODE_REPORT) {
				rec_list_new = s_spi_manager->rec_list_head.next;
				rec_list_old = s_spi_manager->rec_list_head.next;
				CmdPortPutsEndl(sc_info2);
				while (rec_list_new != NULL) {
					if (width == 8) {
						for (i = 0; i < rec_list_new->size; i++) {
							CmdPortPrintf("%02X ", rec_list_new->rec_cache[i]);
						}
					}
					else if ((width > 8) && (width < 17)) {
						arr16 = (uint16_t *)rec_list_new->rec_cache;
						for (i = 0; i < rec_list_new->size; i++) {
							CmdPortPrintf("%04X ", arr16[i]);
						}
					}
					else {
						arr32 = (uint32_t *)rec_list_new->rec_cache;
						for (i = 0; i < rec_list_new->size; i++) {
							CmdPortPrintf("%08X ", arr32[i]);
						}
					}
					CmdPortPutsEndl(NULL);

					rec_list_new = rec_list_new->next;
					vPortFree(rec_list_old->rec_cache);
					vPortFree(rec_list_old);
					rec_list_old = rec_list_new;
				}
				s_spi_manager->rec_list_head.next = NULL;
			}
			else {
				CmdPortSpiMisoStopDetec(s_master[0]);
				CmdPortPrintfEndl("get edges: %d", s_spi_manager->edge_cnt);
				CmdPortSpiMisoToSpi(s_master[0]);
			}
		}
		
		CmdCleanSendStruct(&s_spi_manager->send_seq);
		CmdPortPutsEndl(gc_cmd_info_done);
		vTaskSuspend(s_send_handle);
	}
}

static void cmdSpiExsalveTask(void *param)
{
	uint16_t i;
	uint32_t width;
	
	while (1) {
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		
		CmdPortPutsEndl(sc_info4);
		width = SpiGetWidth(&g_spi1);
		if (width == 8) {
			for (i = 0; i < s_spi_manager->slave.length; i++) {
				CmdPortPrintf("%02X ", s_spi_manager->slave.rcache8[i]);
				s_spi_manager->slave.rcache8[i] = 0;
			}
			SpiReceiveDma(&g_spi1, s_spi_manager->slave.rcache8, s_spi_manager->slave.length);
		}
		else if ((width > 8) && (width < 17)) {
			for (i = 0; i < s_spi_manager->slave.length; i++) {
				CmdPortPrintf("%04X ", s_spi_manager->slave.rcache16[i]);
				s_spi_manager->slave.rcache16[i] = 0;
			}
			SpiReceiveDma(&g_spi1, (uint8_t *)s_spi_manager->slave.rcache16, s_spi_manager->slave.length);
		}
		else {
			for (i = 0; i < s_spi_manager->slave.length; i++) {
				CmdPortPrintf("%08X ", s_spi_manager->slave.rcache32[i]);
				s_spi_manager->slave.rcache32[i] = 0;
			}
			SpiReceiveDma(&g_spi1, (uint8_t *)s_spi_manager->slave.rcache32, s_spi_manager->slave.length);
		}
	}
}

static void cbOnSpiExslaveDmaRecDone(SpiPeripheral *spi)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	vTaskNotifyGiveFromISR(s_exsalve_handle, &xHigherPriorityTaskWoken);
}

static void spiSetProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint32_t tmp;
	
	int16_t index = StrGetNextNotSpace((char *)receive);
	if (index == -1) {
		CmdPortPutsEndl(gc_cmd_info_no_param);
		return;
	}
	
	if (strcmp(param, gc_cmd_par_speed) == 0) {
		tmp = StrGetDec((char *)&receive[index], 0, 8);
		CmdPortSpiSetSpeed(s_master[0], tmp);
		tmp = CmdPortSpiGetSpeed(s_master[0]);
		CmdPortPrintfEndl("actually speed: %d", tmp);
	}
	else if (strcmp(param, gc_cmd_par_width) == 0) {
		tmp = StrGetDec((char *)&receive[index], 0, 4);
		CmdPortSpiSetWidth(s_master[0], tmp);
		tmp = CmdPortSpiGetWidth(s_master[0]);
		CmdPortPrintfEndl("actually width: %d", tmp);
	}
	else if (strcmp(param, gc_cmd_par_spis) == 0) {
		if (strncmp((char *)&receive[index], gc_cmd_par_enable, 6) == 0) {
			vTaskResume(s_exsalve_handle);
			tmp = SpiGetWidth(&g_spi1);
			if (tmp == 8) {
				s_spi_manager->slave.rcache8 = (uint8_t *)pvPortMalloc(s_spi_manager->slave.length);
				if (s_spi_manager->slave.rcache8 == NULL) {
					CmdPortPutsEndl(gc_cmd_err_memory);
				}
				else {
					SpiRegisterCb(&g_spi1, SPI_CB_ON_DMA_REC_DONE, cbOnSpiExslaveDmaRecDone);
					SpiReceiveDma(&g_spi1, s_spi_manager->slave.rcache8, s_spi_manager->slave.length);
					s_spi_manager->slave.enable = 1;
				}
			}
			else if ((tmp > 8) && (tmp << 17)) {
				s_spi_manager->slave.rcache16 = (uint16_t *)pvPortMalloc(s_spi_manager->slave.length * sizeof(uint16_t));
				if (s_spi_manager->slave.rcache16 == NULL) {
					CmdPortPutsEndl(gc_cmd_err_memory);
				}
				else {
					SpiRegisterCb(&g_spi1, SPI_CB_ON_DMA_REC_DONE, cbOnSpiExslaveDmaRecDone);
					SpiReceiveDma(&g_spi1, (uint8_t *)s_spi_manager->slave.rcache16, s_spi_manager->slave.length);
					s_spi_manager->slave.enable = 1;
				}
			}
			else {
				s_spi_manager->slave.rcache32 = (uint32_t *)pvPortMalloc(s_spi_manager->slave.length * sizeof(uint32_t));
				if (s_spi_manager->slave.rcache32 == NULL) {
					CmdPortPutsEndl(gc_cmd_err_memory);
				}
				else {
					SpiRegisterCb(&g_spi1, SPI_CB_ON_DMA_REC_DONE, cbOnSpiExslaveDmaRecDone);
					SpiReceiveDma(&g_spi1, (uint8_t *)s_spi_manager->slave.rcache32, s_spi_manager->slave.length);
					s_spi_manager->slave.enable = 1;
				}
			}
			CmdPortPutsEndl(gc_cmd_par_enable);
		}
		else if (strncmp((char *)&receive[index], gc_cmd_par_disable, 7) == 0) {
			SpiTransferDmaAbort(&g_spi1);
			vTaskSuspend(s_exsalve_handle);
			
			vPortFree(s_spi_manager->slave.rcache8);
			vPortFree(s_spi_manager->slave.rcache16);
			vPortFree(s_spi_manager->slave.rcache32);
			s_spi_manager->slave.rcache8 = NULL;
			s_spi_manager->slave.rcache16 = NULL;
			s_spi_manager->slave.rcache32 = NULL;
			
			s_spi_manager->slave.enable = 0;
			
			CmdPortPutsEndl(gc_cmd_par_disable);
		}
		else {
			CmdPortPutsEndl(gc_cmd_warn_nmp);
		}
	}
	else if (strcmp(param, gc_cmd_par_rxen) == 0) {
		if (strncmp((char *)&receive[index], gc_cmd_par_enable, 6) == 0) {
			s_spi_manager->use_receive = 1;
			CmdPortPutsEndl(gc_cmd_par_enable);
		}
		else if (strncmp((char *)&receive[index], gc_cmd_par_disable, 7) == 0) {
			s_spi_manager->use_receive = 0;
			CmdPortPutsEndl(gc_cmd_par_disable);
		}
		else {
			CmdPortPutsEndl(gc_cmd_warn_nmp);
		}
	}
	else {
		CmdPortPutsEndl(gc_cmd_warn_nmp);
	}
}

static void spiSetSpisProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint32_t val, width;
	
	uint8_t error = 0;
	int16_t index = StrGetNextNotSpace((char *)receive);
	if (index == -1) {
		CmdPortPutsEndl(gc_cmd_info_no_param);
		return;
	}
	
	if (strcmp(param, gc_cmd_par_width) == 0) {
		width = StrGetDec((char *)&receive[index], 0, 4);
		SpiSetWidth(&g_spi1, width);
		width = SpiGetWidth(&g_spi1);
		CmdPortPrintfEndl("new width: %d", width);
	}
	else if (strcmp(param, gc_cmd_par_size) == 0) {
		val = StrGetDec((char *)&receive[index], 0, 8);
		if (s_spi_manager->slave.enable) {
			SpiTransferDmaAbort(&g_spi1);
			if (s_spi_manager->slave.rcache8 != NULL) {
				vPortFree(s_spi_manager->slave.rcache8);
				s_spi_manager->slave.rcache8 = NULL;
				
				s_spi_manager->slave.rcache8 = (uint8_t *)pvPortMalloc(val * sizeof(uint8_t));
				if (s_spi_manager->slave.rcache8 == NULL) {
					error++;
				}
				else {
					SpiReceiveDma(&g_spi1, s_spi_manager->slave.rcache8, val);
				}
			}
			else if (s_spi_manager->slave.rcache16 != NULL) {
				vPortFree(s_spi_manager->slave.rcache16);
				s_spi_manager->slave.rcache16 = NULL;
				
				s_spi_manager->slave.rcache16 = (uint16_t *)pvPortMalloc(val * sizeof(uint16_t));
				if (s_spi_manager->slave.rcache16 == NULL) {
					error++;
				}
				else {
					SpiReceiveDma(&g_spi1, (uint8_t *)s_spi_manager->slave.rcache16, val);
				}
			}
			else {
				vPortFree(s_spi_manager->slave.rcache32);
				s_spi_manager->slave.rcache32 = NULL;
				
				s_spi_manager->slave.rcache32 = (uint32_t *)pvPortMalloc(val * sizeof(uint32_t));
				if (s_spi_manager->slave.rcache32 == NULL) {
					error++;
				}
				else {
					SpiReceiveDma(&g_spi1, (uint8_t *)s_spi_manager->slave.rcache32, val);
				}
			}
			
			if (error == 0) {
				s_spi_manager->slave.length = val;
				CmdPortPrintfEndl("new size: %d", s_spi_manager->slave.length);
			}
			else {
				CmdPortPutsEndl(sc_err1);
			}
		}
		else {
			s_spi_manager->slave.length = val;
			CmdPortPrintfEndl("new size: %d", s_spi_manager->slave.length);
		}
	}
}

static void spiReadProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint16_t length, i;
    uint32_t width;
	
	int16_t index = StrGetNextNotSpace((char *)receive);
	
	if (index == -1) {
		CmdPortPutsEndl(gc_cmd_info_no_param);
		return;
	}
	
	if (strncmp((char *)&receive[index], gc_cmd_par_spis, 4) == 0) {
		length = SpiGetRecDmaLength(&g_spi1);
		if (length == 0) {
			CmdPortPutsEndl(gc_cmd_warn_nd);
		}
		else {
            CmdPortPutsEndl(sc_info4);
            width = SpiGetWidth(&g_spi1);
			if (width == 8) {
                for (i = 0; i < length; i++) {
                    CmdPortPrintf("%02X ", s_spi_manager->slave.rcache8[i]);
                }
            }
            else if ((width > 8) && (width < 17)) {
                for (i = 0; i < length; i++) {
                    CmdPortPrintf("%04X ", s_spi_manager->slave.rcache16[i]);
                }
            }
            else {
                for (i = 0; i < length; i++) {
                    CmdPortPrintf("%08X ", s_spi_manager->slave.rcache32[i]);
                }
            }
		}
	}
}

static uint8_t spiByteProcess(uint8_t *receive, uint16_t recelen)
{
	uint16_t i;
	
	uint8_t *rec_data = &receive[1];
	uint32_t width = CmdPortSpiGetWidth(s_master[0]);
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	CmdPortReturnIfNULL(send_struct, CMD_RETURN_ERR_MEMRY);
	
	send_struct->next = NULL;
	send_struct->length = receive[0];
	if (width == 8) {
		send_struct->seq_enum = CMD_SEND_SEQ_BYTE;
		send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length);
		
		if (send_struct->stream == NULL) {
			vPortFree(send_struct);
			return CMD_RETURN_ERR_MEMRY;
		}
		
		for (i = 0; i < send_struct->length; i++) {
			send_struct->stream[i] = rec_data[i];
		}
	}
	else  if ((width > 8) && (width < 17)) {
		send_struct->seq_enum = CMD_SEND_SEQ_HWORD;
		send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length * 2);
		
		if (send_struct->stream == NULL) {
			vPortFree(send_struct);
			return CMD_RETURN_ERR_MEMRY;
		}
		
		for (i = 0; i < send_struct->length; i++) {
			send_struct->stream[i*2] = rec_data[i*2+1];
			send_struct->stream[i*2+1] = rec_data[i*2];
		}
	}
	else {
		send_struct->seq_enum = CMD_SEND_SEQ_WORD;
		send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length * 4);

		if (send_struct->stream == NULL) {
			vPortFree(send_struct);
			return CMD_RETURN_ERR_MEMRY;
		}

		for (i = 0; i < send_struct->length; i++) {
			send_struct->stream[i*4] = rec_data[i*4+3];
			send_struct->stream[i*4+1] = rec_data[i*4+2];
			send_struct->stream[i*4+2] = rec_data[i*4+1];
			send_struct->stream[i*4+3] = rec_data[i*4];
		}
	}
	CmdAddSendStruct(&s_spi_manager->send_seq, send_struct);
	
	return CMD_RETURN_OK;
}

static uint8_t spiHexProcess(uint8_t *receive, uint16_t recelen)
{
	int16_t index, offset;
	
	uint32_t width = CmdPortSpiGetWidth(s_master[0]);
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	CmdPortReturnIfNULL(send_struct, CMD_RETURN_ERR_MEMRY);
	
	send_struct->next = NULL;
	index = StrGetNextNotSpace((char *)receive);
	offset = index;
	send_struct->length = StrGetHex((char *)&receive[index], 0, 6);
	index = StrGetCHIndex((char *)&receive[offset], ' ');
	offset += index;
	if (width == 8) {
		send_struct->seq_enum = CMD_SEND_SEQ_BYTE;
		send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length);
		
		if (send_struct->stream == NULL) {
			vPortFree(send_struct);
			return CMD_RETURN_ERR_MEMRY;
		}
		
		StrString2Hex((char *)&receive[offset], recelen, send_struct->stream, send_struct->length, 2);
	}
	else if ((width > 8) && (width < 17)) {
		send_struct->seq_enum = CMD_SEND_SEQ_HWORD;
		send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length * 2);
		
		if (send_struct->stream == NULL) {
			vPortFree(send_struct);
			return CMD_RETURN_ERR_MEMRY;
		}
		
		if ((width % 4) == 0) {
			StrString2Hex((char *)&receive[offset], recelen, send_struct->stream, send_struct->length, width / 4);
		}
		else {
			StrString2Hex((char *)&receive[offset], recelen, send_struct->stream, send_struct->length, width / 4 + 1);
		}
	}
	else {
		send_struct->seq_enum = CMD_SEND_SEQ_WORD;
		send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length * 4);
		
		if (send_struct->stream == NULL) {
			vPortFree(send_struct);
			return CMD_RETURN_ERR_MEMRY;
		}
		if ((width % 4) == 0) {
			StrString2Hex((char *)&receive[offset], recelen, send_struct->stream, send_struct->length, width / 4);
		}
		else {
			StrString2Hex((char *)&receive[offset], recelen, send_struct->stream, send_struct->length, width / 4 + 1);
		}
	}
	
	CmdAddSendStruct(&s_spi_manager->send_seq, send_struct);
	
	return CMD_RETURN_OK;
}

static uint8_t spiUsProcess(uint8_t *receive, uint16_t recelen)
{
	int16_t index;
	
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	CmdPortReturnIfNULL(send_struct, CMD_RETURN_ERR_MEMRY);
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_US;
	send_struct->stream = NULL;
	index = StrGetNextNotSpace((char *)receive);
	send_struct->length = StrGetHex((char *)&receive[index], 0, 6);
	CmdAddSendStruct(&s_spi_manager->send_seq, send_struct);
	
	return CMD_RETURN_OK;
}

static uint8_t spiMsProcess(uint8_t *receive, uint16_t recelen)
{
	int16_t index;
	
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	CmdPortReturnIfNULL(send_struct, CMD_RETURN_ERR_MEMRY);
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_MS;
	send_struct->stream = NULL;
	index = StrGetNextNotSpace((char *)receive);
	send_struct->length = StrGetHex((char *)&receive[index], 0, 6);
	CmdAddSendStruct(&s_spi_manager->send_seq, send_struct);
	
	return CMD_RETURN_OK;
}
