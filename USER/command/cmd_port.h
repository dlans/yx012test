#ifndef __CMD_PORT_H
#define __CMD_PORT_H

#include "FreeRTOS.h"

/* Platform include file */
#include "system.h"

#define CMD_PORT_COMMUNICATE_SPEED_MIN	UART_MIN_BAUDRATE
#define CMD_PORT_COMMUNICATE_SPEED_MAX	UART_MAX_BAUDRATE

/* cmd_*.c port define */
/* Define how to get the length of receiving data from communication port */
#define CmdPortGetCommunicateLength()	UartGetReceiveLen(&g_uart1)

/* Define how to get the all receiving data from communication port */
#define CmdPortGetCommunicateCache()	UartGetRxBuffer(&g_uart1)

#define CmdPortSetCommunicateSpeed(s)	UartSetBaud(&g_uart1, s);

/* Define puts() function */
#define CmdPortPuts(str)				UartPuts(&g_uart1, str)

/* Like 'CmdPortPuts(str)' but end with a flag of end line
 * 	str: a string, if it's NULL, that mean only send a flag of end line */
#define CmdPortPutsEndl(str)			UartPutsEndl(&g_uart1, str)

#define CmdPortPrintf(str, ...)			UartPrintf(&g_uart1, str, ##__VA_ARGS__)
#define CmdPortPrintfEndl(str, ...)		UartPrintfEndl(&g_uart1, str, ##__VA_ARGS__)

#define CmdPortReturnIfNULL(ptr, ...)	do {\
											if (ptr == NULL) {\
												return __VA_ARGS__;\
											}\
										} while (0)
/* End of cmd_*.c port define */

/* cmd_uart.c port define */
typedef UartPeripheral CmdUartHandler;

#define CMD_PORT_UART_STOPBIT_1		UART_STOP_BIT_1
#define CMD_PORT_UART_STOPBIT_1_5	UART_STOP_BIT_1_5
#define CMD_PORT_UART_STOPBIT_2		UART_STOP_BIT_2

#define CMD_PORT_UART_REC_ASYNC_FRAME	UART_CB_ON_DMA_TIMEOUT
#define CMD_PORT_UART_REC_ASYNC_DONE	UART_CB_ON_DMA_REC_DONE

#define CmdPortUartSendSync(h, stream, length)	UartSend(h, stream, length)
#define CmdPortUartRecAsync(h, stream, length)	UartReceiveDMA(h, stream, length)
#define CmdPortUartRecAsyncAbort(h)				UartReceiveDMAAbort(h)
#define CmdPortUartGetRecLength(h)				UartGetReceiveLen(h)
#define CmdPortUartGetRecAsyncLength(h)			UartGetRecDmaLength(h)
#define CmdPortUartGetStopbit(h)				UartGetStopbit(h)
#define CmdPortUartGetBaudrate(h)				UartGetBaud(h)
#define CmdPortUartSetBaudrate(h, baud)			UartSetBaud(h, baud)
#define CmdPortUartWaitSendDone(h)				UartWaitSendDone(h)
#define CmdPortUartRegisterCb(h, e, f)			UartRegisterCb(h, e, f)
#define CmdPortUartTXPullDown(h)				UartTxPullDown(h)
#define CmdPortUartTXPullUp(h)					UartTxPullUp(h)

#define CmdPortUartTXToUart(x)			do {\
											if (SYSTEM_VALUE_CHECK(g_uart2.gpio_sta, UART_GPIO_TX_IN_CORE)) {\
												UartIOCtl(&g_uart2, UART_IO_CTL_TX_TO_PERIPHERAL);\
											}\
										} while(0)

#define CmdPortUartTXToCore(x)			do {\
											if (SYSTEM_VALUE_CHECK(g_uart2.gpio_sta, UART_GPIO_TX_IN_PERIPHERAL)) {\
												UartIOCtl(&g_uart2, UART_IO_CTL_TX_TO_CORE);\
											}\
										} while (0)
/* End of cmd_uart.c port define */

/* cmd_spi.c port define */
typedef SpiPeripheral CmdSpiHandler;

#define CMD_PORT_SPI_MODE_MASTER	SPI_MASTER
#define CMD_PORT_SPI_MODE_SLAVE		SPI_SLAVE

#define CmdPortSpiSendSync(h, stream, length)	SpiSend(h, stream, length)
#define CmdPortSpiSendRecSync(h, s, r, l)		SpiSendReceive(h, s, r, l)
#define CmdPortSpiGetMode(h)					SpiGetMode(h)
#define CmdPortSpiGetSpeed(h)					SpiGetSpeed(h)
#define CmdPortSpiGetWidth(h)					SpiGetWidth(h)
#define CmdPortSpiSetSpeed(h, s)				SpiSetSpeed(h, s)
#define CmdPortSpiSetWidth(h, w)				SpiSetWidth(h, w)
#define CmdPortSpiMisoToEdgeRaise(h)			SpiIOCtl(h, SPI_IO_CTL_MISO_EDGE_RAISE_DETECT)
#define CmdPortSpiMisoStopDetec(h)				SpiMisoExitStopDetec(h)
#define CmdPortSpiMisoToSpi(h)					SpiIOCtl(h, SPI_IO_CTL_MISO_TO_PERIPHERAL)
/* End of cmd_spi.c port define */

#endif
