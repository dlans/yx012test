#include "cmd.h"
#include "system.h"
#include "uart_peripheral.h"
#include "util_cmd.h"
#include "util_str.h"
#include "NuMicro.h"
#include "stdio.h"
#include "string.h"

static void cmdSystemCallback(int argc, char *argv[], int index);
static void parSetProcess(uint8_t *receive, char *param, uint16_t recelen);
static void parGetProcess(uint8_t *receive, uint16_t recelen);

void CmdSystemInit(void)
{
	CmdAppend(&g_cmd_uart_manager, gc_cmd_cmd_system, cmdSystemCallback);
}

static void cmdSystemCallback(int argc, char *argv[], int index)
{
	int16_t offset;
	
	uint16_t recelen = CmdPortGetCommunicateLength();
	uint8_t *receive = CmdPortGetCommunicateCache();
	
	if (argc == 0) {
		CmdPortPutsEndl(gc_cmd_info_ok);
		return;
	}
	else if (argc == 1) {
		if (strcmp(argv[0], gc_cmd_par_name) == 0) {
			CmdPortPutsEndl(SYSTEM_NAME);
		}
		else if (strcmp(argv[0], gc_cmd_par_version) == 0) {
		#ifdef USER_DEBUG_LOG
			CmdPortPrintfEndl("%s @branch @%s with log, build at %s", APP_VERSION, SYSTEM_BRANCH, __DATE__);
		#else
			CmdPortPrintfEndl("%s @branch @%s, build at %s", SYSTEM_NAME, SYSTEM_BRANCH, __DATE__);
		#endif
		}
		else if (strcmp(argv[0], gc_cmd_par_get) == 0) {
			offset = StrGetNextNotSpace((char *)&receive[index]);
			parGetProcess(&receive[index + offset], recelen - index);
		}
		else {
			CmdPortPutsEndl(gc_cmd_info_no_param);
		}
	}
	else if (argc == 2) {
		if (strcmp(argv[0], gc_cmd_par_set) == 0) {
			parSetProcess(&receive[index + offset], argv[1], recelen - index);
		}
	}
}

static void parSetProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	int16_t index, size;
	uint32_t speed;
	
	index = StrGetNextNotSpace((char *)receive);
	if (index == -1) {
		CmdPortPutsEndl(gc_cmd_info_no_param);
		return;
	}
	
	if (strcmp(param, gc_cmd_par_speed) == 0) {
		speed = StrGetDec((char *)&receive[index], 0, 8);
		if ((speed < CMD_PORT_COMMUNICATE_SPEED_MIN) || (speed > CMD_PORT_COMMUNICATE_SPEED_MAX)) {
			CmdPortPutsEndl(gc_cmd_err_range);
		}
		else {
			CmdPortSetCommunicateSpeed(speed);
		}
	}
	else if (strcmp(param, gc_cmd_par_size) == 0) {
		size = StrGetDec((char *)&receive[index], 0, 8);
		SystemSetReceiveSize(size);
	}
}

static void parGetProcess(uint8_t *receive, uint16_t recelen)
{
    /* system -get memory */
	if (strncmp((char *)receive, gc_cmd_par_memory, strlen(gc_cmd_par_memory)) == 0) {
		CmdPortPrintfEndl("Total space: %d Bytes", configTOTAL_HEAP_SIZE);
		CmdPortPrintfEndl("Current free space: %d Bytes", xPortGetFreeHeapSize());
		CmdPortPrintfEndl("Historical minimum: %d Bytes", xPortGetMinimumEverFreeHeapSize());
	}
    /* system -get clock */
	else if (strncmp((char *)receive, gc_cmd_par_clock, strlen(gc_cmd_par_clock)) == 0) {
		CmdPortPutsEndl("Clock as flow:");
		CmdPortPrintfEndl("CPU   Fre = %d Hz", SystemCoreClock);
		CmdPortPrintfEndl("PCLK0 Fre = %d Hz", CLK_GetPCLK0Freq());
		CmdPortPrintfEndl("PCLK1 Fre = %d Hz", CLK_GetPCLK1Freq());
		CmdPortPrintfEndl("PLL   Fre = %d Hz", CLK_GetPLLClockFreq());
	}
	else if (strncmp((char *)receive, gc_cmd_par_size, 4) == 0) {
		CmdPortPrintfEndl("Size: %d Bytes", SystemGetReceiveSize());
	}
	else {
		CmdPortPutsEndl(gc_cmd_info_no_param);
	}
}
