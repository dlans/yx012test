#include "cmd.h"
#include "main.h"
#include "util_str.h"
#include "delay.h"
#include "string.h"

static char * const sc_info1 = "uart state as follow:";
static char * const sc_info2 = "uart receive:";
static char * const sc_warn1 = "receive length equal to cache size, may be some data loss";

struct CmdUartManger {
	uint8_t use_receive : 1;
	uint8_t reserved : 7;
	
    uint16_t cmd_string_length;
    uint16_t send_seq_length;
	uint16_t receive_cache_size;
	uint8_t *receive_cache;
	
	CmdSendStruct *send_seq;
};

static struct CmdUartManger *s_manager = NULL;
static TaskHandle_t s_send_handle = NULL;
static TaskHandle_t s_receive_handle = NULL;
static CmdUartHandler *s_uart_handler[CMD_UART_NUMBER] = {NULL};

static void cmdUartCallback(int argc, char *argv[], int index);
static void cmdUartSendTask(void *parameter);
static void cmdUartReceiveTask(void *parameter);
static void cmdUartShowState(void);

static void cbOnDmaDoneAndTimeout(UartPeripheral *uart);

static void addSendStruct(CmdSendStruct *send_struct);
static void deleteSendStruct(void);
static void clearSendStruct(void);

#ifdef USER_DEBUG_LOG
static void printSendStruct(void);
#endif

static void parRead2Process(void);
static void parSetProcess(uint8_t *receive, char *param, uint16_t recelen);
static void parGetProcess(uint8_t *receive, char *param, uint16_t recelen);
static void parByteProcess(uint8_t *receive, uint16_t recelen);
static void parCharProcess(uint8_t *receive, uint16_t recelen);
static void parUsProcess(uint8_t *receive, uint16_t recelen);
static void parMsProcess(uint8_t *receive, uint16_t recelen);
static void parTxhighProcess(uint8_t *receive, uint16_t recelen);
static void parTxlowhProcess(uint8_t *receive, uint16_t recelen);
static void parRxhighProcess(uint8_t *receive, uint16_t recelen);
static void parRxlowProcess(uint8_t *receive, uint16_t recelen);

void CmdUartInit(CmdUartHandler *handler_array[CMD_UART_NUMBER])
{
	static struct CmdUartManger manager;
	
	uint16_t i;
	
	if (s_manager == NULL) {
		CmdAppend(&g_cmd_uart_manager, gc_cmd_cmd_uart, cmdUartCallback);
		
		manager.use_receive = 0;
		
        manager.cmd_string_length = strlen(gc_cmd_cmd_uart);
        manager.send_seq_length = 0;
		manager.receive_cache_size = 128;
		manager.receive_cache = NULL;
		
		manager.send_seq = NULL;
		
		s_manager = &manager;
	}
	for (i = 0; i < CMD_UART_NUMBER; i++) {
		s_uart_handler[i] = handler_array[i];
	}
	
	/* Only for suppressing warning */
	deleteSendStruct();
}

void CmdUartCreateSendTask(UBaseType_t priority)
{
	xTaskCreate(cmdUartSendTask, "cmdUartSendTask", 256, NULL, priority, &s_send_handle);
    vTaskSuspend(s_send_handle);
}

void CmdUartCreateReceiveTask(UBaseType_t priority)
{
	xTaskCreate(cmdUartReceiveTask, "cmdUartReceiveTask", 256, NULL, priority, &s_receive_handle);
	vTaskSuspend(s_receive_handle);
}

static void cmdUartCallback(int argc, char *argv[], int index)
{
	int i;
	int16_t offset_new, offset_old;
	
	uint8_t resume_task = 1;
	uint16_t recelen = CmdPortGetCommunicateLength();
	uint8_t *receive = CmdPortGetCommunicateCache();
	
	if (argc == 0) {
		cmdUartShowState();
		
		return;
	}
	else if (argc == 1) {
		if ((strcmp(argv[0], gc_cmd_par_set) == 0) || (strcmp(argv[0], gc_cmd_par_set) == 0)) {
			CmdPortPutsEndl(gc_cmd_info_no_param);
			resume_task = 0;
		}
		else if (strcmp(argv[0], gc_cmd_par_read2) == 0) {
			parRead2Process();
			resume_task = 0;
		}
	}
	else if (argc == 2) {
		if (strcmp(argv[0], gc_cmd_par_set) == 0) {
			parSetProcess(&receive[index], argv[1], recelen - index);
			resume_task = 0;
		}
		else if (strcmp(argv[0], gc_cmd_par_get) == 0) {
			parGetProcess(&receive[index], argv[1], recelen - index);
			resume_task = 0;
		}
	}
	
	if (resume_task) {
		offset_old = 0;
		for (i = 0; i < argc; i++) {
			offset_new = StrGetCHIndex((char *)&receive[offset_old], '-');
			offset_new += offset_old;
			
			if (strcmp(argv[i], gc_cmd_par_byte) == 0) {
				parByteProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_char) == 0) {
				parCharProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_us) == 0) {
				parUsProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_ms) == 0) {
				parMsProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_txhigh) == 0) {
				parTxhighProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_txlow) == 0) {
				parTxlowhProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_rxhigh) == 0) {
				parRxhighProcess(&receive[offset_new], recelen - offset_new);
			}
			else if (strcmp(argv[i], gc_cmd_par_rxlow) == 0) {
				parRxlowProcess(&receive[offset_new], recelen - offset_new);
			}
			/* 1 is added to ensure that it's offset to the right of the processed symbol '-' */
			offset_old = offset_new + 1;
		}
		vTaskResume(s_send_handle);
	}
}

static void cmdUartSendTask(void *parameter)
{
	uint32_t remainder, quotient;
	
	CmdSendStruct *send_struct = NULL;
	
    while (1) {
	#ifdef USER_DEBUG_LOG
		printSendStruct();
	#endif
		
		send_struct = s_manager->send_seq;
		if (send_struct == NULL) {
			vTaskSuspend(s_send_handle);
		}
		
		while (send_struct != NULL) {
			if (send_struct->seq_enum == CMD_SEND_SEQ_US) {
				if (send_struct->length > 999) {
					quotient = send_struct->length / 1000;
					remainder = send_struct->length % 1000;
					vTaskDelay(quotient);
					delayUs(remainder);
				}
				else {
					CmdPortUartWaitSendDone(s_uart_handler[0]);
					delayUs(send_struct->length);
				}
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_MS) {
				vTaskDelay(send_struct->length);
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_BYTE) {
				CmdPortUartTXToUart(0);
				CmdPortUartSendSync(s_uart_handler[0], send_struct->stream, send_struct->length);
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_TX_PULLDOWN) {
				CmdPortUartTXToCore(0);
				CmdPortUartTXPullDown(s_uart_handler[0]);
			}
			else if (send_struct->seq_enum == CMD_SEND_SEQ_TX_PULLUP) {
				CmdPortUartTXToCore(0);
				CmdPortUartTXPullUp(s_uart_handler[0]);
			}
			
			send_struct = send_struct->next;
		}
		clearSendStruct();
		CmdPortPutsEndl(gc_cmd_info_done);
		vTaskSuspend(s_send_handle);
	}
}

static void cmdUartReceiveTask(void *parameter)
{
	uint16_t i, recelen;
	
	while (1) {
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		
		recelen = CmdPortUartGetRecLength(s_uart_handler[0]);
		if (recelen >= s_manager->receive_cache_size) {
			CmdPortPutsEndl(sc_warn1);
			recelen = s_manager->receive_cache_size;
		}
		CmdPortPutsEndl(sc_info2);
		for (i = 0; i < recelen; i++) {
			CmdPortPrintf("%02X ", s_manager->receive_cache[i]);
		}
		CmdPortPutsEndl(NULL);
		
		CmdPortUartRecAsyncAbort(s_uart_handler[0]);
		CmdPortUartRecAsync(s_uart_handler[0], s_manager->receive_cache, s_manager->receive_cache_size);
	}
}

static void cmdUartShowState(void)
{
	uint32_t ret;
	
	CmdPortPutsEndl(sc_info1);
	CmdPortPrintfEndl("receive_cache_size = %d", s_manager->receive_cache_size);
	#ifdef USER_DEBUG_LOG
		CmdPortPrintfEndl("receive_cache address: 0x%8X", s_manager->receive_cache);
	#endif
	
	CmdPortPuts("receive function = ");
	if (s_manager->use_receive) {
		CmdPortPutsEndl(gc_cmd_par_enable);
	}
	else {
		CmdPortPutsEndl(gc_cmd_par_disable);
	}
	
	ret = CmdPortUartGetBaudrate(s_uart_handler[0]);
	CmdPortPrintf("baudrate = %d\r\n", ret);
	
	ret = CmdPortUartGetStopbit(s_uart_handler[0]);
	CmdPortPuts("stopbit = ");
	if (ret == CMD_PORT_UART_STOPBIT_1) {
		CmdPortPutsEndl("1");
	}
	else if (ret == CMD_PORT_UART_STOPBIT_2) {
		CmdPortPutsEndl("2");
	}
	else if (ret == CMD_PORT_UART_STOPBIT_1_5) {
		CmdPortPutsEndl("1.5");
	}
	else {
		CmdPortPutsEndl(gc_cmd_err_error);
	}
}

static void cbOnDmaDoneAndTimeout(CmdUartHandler *uart)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	if (uart == s_uart_handler[0]) {
		vTaskNotifyGiveFromISR(s_receive_handle, &xHigherPriorityTaskWoken);
	}
}

static void addSendStruct(CmdSendStruct *send_struct)
{
    CmdSendStruct *send_struct1 = s_manager->send_seq;

    if (s_manager->send_seq == NULL) {
        s_manager->send_seq = send_struct;
        s_manager->send_seq_length++;
    }
    else {
        while (send_struct1->next != NULL) {
            send_struct1 = send_struct1->next;
        }
        send_struct1->next = send_struct;
        s_manager->send_seq_length++;
    }
}

static void deleteSendStruct(void)
{
    CmdSendStruct *send_struct1 = s_manager->send_seq;
    CmdSendStruct *send_struct2 = s_manager->send_seq;

    if (s_manager->send_seq == NULL) {
        return;
    }
	
	if (s_manager->send_seq->next == NULL) {
		if (s_manager->send_seq->stream != NULL) {
			vPortFree(s_manager->send_seq->stream);
		}
		vPortFree(s_manager->send_seq);
		s_manager->send_seq = NULL;
		
		return;
	}

    while (send_struct2->next != NULL) {
        send_struct1 = send_struct2;
        send_struct2 = send_struct1->next;
    }
    if (send_struct2->stream != NULL) {
        vPortFree(send_struct2->stream);
    }
    vPortFree(send_struct2);
    send_struct1->next = NULL;
	s_manager->send_seq_length--;
}

static void clearSendStruct(void)
{
    CmdSendStruct *send_struct1 = s_manager->send_seq;
    CmdSendStruct *send_struct2 = s_manager->send_seq;

    if (s_manager->send_seq == NULL) {
        return;
    }

    while (send_struct2->next != NULL) {
        send_struct1 = send_struct2;
        send_struct2 = send_struct1->next;
        if (send_struct1->stream != NULL) {
            vPortFree(send_struct1->stream);
        }
        vPortFree(send_struct1);
    }
    if (send_struct2->stream != NULL) {
        vPortFree(send_struct2->stream);
    }
    vPortFree(send_struct2);

    s_manager->send_seq = NULL;
	s_manager->send_seq_length = 0;
}

#ifdef USER_DEBUG_LOG
static void printSendStruct(void)
{
	uint8_t i, j;
	
	CmdSendStruct *send_struct1 = s_manager->send_seq;
	
	if (s_manager->send_seq == NULL) {
		CmdPortPuts("s_manager->send_seq == NULL\r\n");
	}
	
	i = 0;
	CmdPortPrintf("There are %d nodes\r\n", s_manager->send_seq_length);
	while (send_struct1 != NULL) {
		CmdPortPrintf("Node[%d]:\r\n", i);
		CmdPortPrintf("next = 0x%X\r\n", (uint32_t)send_struct1->next);
		CmdPortPrintf("seq_enum = %d\r\n", send_struct1->seq_enum);
		CmdPortPrintf("length = %d\r\n", send_struct1->length);
		if (send_struct1->seq_enum == CMD_SEND_SEQ_BYTE) {
			CmdPortPuts("Stream as follow:\r\n");
			for (j = 0; j < send_struct1->length; j++) {
				CmdPortPrintf("%02X ", send_struct1->stream[j]);
			}
			CmdPortPutsEndl(NULL);
		}
		CmdPortPutsEndl(NULL);
		send_struct1 = send_struct1->next;
		i++;
	}
}
#endif

static void parRead2Process(void)
{
	uint16_t i;

	uint16_t recelen = CmdPortUartGetRecAsyncLength(s_uart_handler[0]);

	if (recelen) {
		CmdPortPutsEndl(sc_info2);
		for (i = 0; i < recelen; i++) {
			CmdPortPrintf("%02X ", s_manager->receive_cache[i]);
		}
		CmdPortPutsEndl(NULL);

		CmdPortUartRecAsyncAbort(s_uart_handler[0]);
		CmdPortUartRecAsync(s_uart_handler[0], s_manager->receive_cache, s_manager->receive_cache_size);
	}
	else {
		CmdPortPutsEndl(gc_cmd_warn_nd);
	}
}

static void parSetProcess(uint8_t *receive, char *param, uint16_t recelen)
{
	uint8_t ret;
	uint16_t size, i;
	int16_t index;
	uint32_t baud;
	
	if (strcmp(param, gc_cmd_par_baud) == 0) {
		baud = StrGetDec((char *)receive, 0, 8);
		if ((baud < UART_MIN_BAUDRATE) || (baud > UART_MAX_BAUDRATE)) {
			CmdPortPutsEndl(gc_cmd_err_range);
		}
		else {
			ret = CmdPortUartSetBaudrate(s_uart_handler[0], baud);
			if (ret == UART_RETURN_TIMEOUT) {
				CmdPortPutsEndl(gc_cmd_warn_time);
			}
			else {
				CmdPortPrintf("baudrate has been set to %d\r\n", baud);
			}
		}
	}
	else if (strcmp(param, gc_cmd_par_rxen) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		if (index == -1) {
			CmdPortPutsEndl(gc_cmd_info_no_param);
		}
		else {
			if (strncmp((char *)&receive[index], gc_cmd_par_enable, 6) == 0) {
				if (s_manager->use_receive) {
					CmdPortPutsEndl(gc_cmd_info_done);
				}
				else {
					s_manager->receive_cache = (uint8_t *)pvPortMalloc(s_manager->receive_cache_size);
					if (s_manager->receive_cache == NULL) {
						s_manager->use_receive = 0;
						CmdPortPutsEndl(gc_cmd_err_memory);
					}
					else {
						s_manager->use_receive = 1;
						vTaskResume(s_receive_handle);
						CmdPortUartRegisterCb(s_uart_handler[0], CMD_PORT_UART_REC_ASYNC_FRAME, cbOnDmaDoneAndTimeout);
						CmdPortUartRegisterCb(s_uart_handler[0], CMD_PORT_UART_REC_ASYNC_DONE, cbOnDmaDoneAndTimeout);
						CmdPortUartRecAsync(s_uart_handler[0], s_manager->receive_cache, s_manager->receive_cache_size);
						CmdPortPutsEndl(gc_cmd_info_done);
					}
				}
			}
			else if (strncmp((char *)&receive[index], gc_cmd_par_disable, 7) == 0) {
				s_manager->use_receive = 0;
				CmdPortUartRecAsyncAbort(s_uart_handler[0]);
				vPortFree(s_manager->receive_cache);
				s_manager->receive_cache = NULL;
				vTaskSuspend(s_send_handle);
				CmdPortPutsEndl(gc_cmd_info_done);
			}
			else {
				CmdPortPutsEndl(gc_cmd_warn_nmp);
			}
		}
	}
	else if (strcmp(param, gc_cmd_par_size) == 0) {
		index = StrGetNextNotSpace((char *)receive);
		if (index == -1) {
			CmdPortPutsEndl(gc_cmd_info_no_param);
		}

		if (s_manager->use_receive) {
			taskENTER_CRITICAL();

			CmdPortUartRecAsyncAbort(s_uart_handler[0]);
			vPortFree(s_manager->receive_cache);
			s_manager->receive_cache = NULL;

			size = StrGetDec((char *)receive, 0, 6);
			if (size != 0) {
				s_manager->receive_cache = (uint8_t *)pvPortMalloc(size * sizeof(uint8_t));
				if (s_manager->receive_cache == NULL) {
					CmdPortPutsEndl(gc_cmd_err_memory);
				}
				else {
					for (i = 0; i < size; i++) {
						s_manager->receive_cache[i] = 0;
					}
					
					CmdPortUartRecAsync(s_uart_handler[0], s_manager->receive_cache, size);
					s_manager->receive_cache_size = size;
					CmdPortPrintfEndl("new size: %d", size);
				}
			}
			else {
				CmdPortPutsEndl(gc_cmd_err_range);
			}

			taskEXIT_CRITICAL();
		}
	}
	else {
		CmdPortPutsEndl(gc_cmd_warn_nmp);
	}
}

static void parGetProcess(uint8_t *receive, char *param, uint16_t recelen)
{
#ifdef USER_DEBUG_LOG
	UartRegs regs;
	
	if (strcmp(param, gc_cmd_par_regs) == 0) {
		UartDumpRegs(&g_uart2, &regs);
		CmdPortPrintfEndl("DAT      = 0x%08X", regs.dat);
		CmdPortPrintfEndl("INTEN    = 0x%08X", regs.inten);
		CmdPortPrintfEndl("FIFO     = 0x%08X", regs.fifo);
		CmdPortPrintfEndl("LINE     = 0x%08X", regs.line);
		CmdPortPrintfEndl("MODEM    = 0x%08X", regs.modem);
		CmdPortPrintfEndl("MODEMSTS = 0x%08X", regs.modemsts);
		CmdPortPrintfEndl("FIFOSTS  = 0x%08X", regs.fifosts);
		CmdPortPrintfEndl("INTSTS   = 0x%08X", regs.intsts);
		CmdPortPrintfEndl("TOUT     = 0x%08X", regs.tout);
		CmdPortPrintfEndl("BAUD     = 0x%08X", regs.baud);
		CmdPortPrintfEndl("IRDA     = 0x%08X", regs.irda);
		CmdPortPrintfEndl("ALTCTL   = 0x%08X", regs.altctl);
		CmdPortPrintfEndl("FUNCSEL  = 0x%08X", regs.funcsel);
		CmdPortPrintfEndl("LINCTL   = 0x%08X", regs.linctl);
		CmdPortPrintfEndl("BRCOMP   = 0x%08X", regs.brcomp);
		CmdPortPrintfEndl("WKCTL    = 0x%08X", regs.wkctl);
		CmdPortPrintfEndl("WKSTS    = 0x%08X", regs.wksts);
		CmdPortPrintfEndl("DWKCOMP  = 0x%08X", regs.dwkcomp);
	}
#endif
}

static void parByteProcess(uint8_t *receive, uint16_t recelen)
{
    uint16_t i;

    CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
    send_struct->next = NULL;
    send_struct->seq_enum = CMD_SEND_SEQ_BYTE;
    send_struct->length = receive[0];
    send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length);
    for (i = 0; i < send_struct->length; i++) {
        send_struct->stream[i] = receive[i+1];
    }
    addSendStruct(send_struct);
}

static void parCharProcess(uint8_t *receive, uint16_t recelen)
{
    int16_t index, offset;

    CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));

    send_struct->next = NULL;
    send_struct->seq_enum = CMD_SEND_SEQ_BYTE;
    index = StrGetNextNotSpace((char *)receive);
    offset = index;
    send_struct->length = StrGetHex((char *)&receive[index], 0, 6);
    index = StrGetCHIndex((char *)&receive[offset], ' ');
    offset += index;
    send_struct->stream = (uint8_t *)pvPortMalloc(send_struct->length);
    StrChar2HexByByte((char *)&receive[offset], 0, recelen, send_struct->stream, send_struct->length);
    addSendStruct(send_struct);
}

static void parUsProcess(uint8_t *receive, uint16_t recelen)
{
    int16_t index;

    CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));

    send_struct->next = NULL;
    send_struct->seq_enum = CMD_SEND_SEQ_US;
    send_struct->stream = NULL;
    index = StrGetNextNotSpace((char *)receive);
    send_struct->length = StrGetHex((char *)&receive[index], 0, 6);
    addSendStruct(send_struct);
}

static void parMsProcess(uint8_t *receive, uint16_t recelen)
{
	int16_t index;
	
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_MS;
	send_struct->stream = NULL;
	index = StrGetHex((char *)&receive[index], 0, 6);
	addSendStruct(send_struct);
}

static void parTxhighProcess(uint8_t *receive, uint16_t recelen)
{
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_TX_PULLUP;
	send_struct->length = 0;
	send_struct->stream = NULL;
	addSendStruct(send_struct);
}

static void parTxlowhProcess(uint8_t *receive, uint16_t recelen)
{
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_TX_PULLDOWN;
	send_struct->length = 0;
	send_struct->stream = NULL;
	addSendStruct(send_struct);
}

static void parRxhighProcess(uint8_t *receive, uint16_t recelen)
{
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_RX_PULLUP;
	send_struct->length = 0;
	send_struct->stream = NULL;
	addSendStruct(send_struct);
}

static void parRxlowProcess(uint8_t *receive, uint16_t recelen)
{
	CmdSendStruct *send_struct = (CmdSendStruct *)pvPortMalloc(sizeof(CmdSendStruct));
	
	send_struct->next = NULL;
	send_struct->seq_enum = CMD_SEND_SEQ_RX_PULLDOWN;
	send_struct->length = 0;
	send_struct->stream = NULL;
	addSendStruct(send_struct);
}
