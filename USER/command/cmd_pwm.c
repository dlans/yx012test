/**
 * @file	pwm_cmd.c
 * @version	V0.1
 * @brief	提供 PWM 控制相关指令: pwm0、pwm1
 * @detail	pwm[0][1] -[output][fre][duty0][duty1] [value]
 *			[1.] pwm0 -output [option]	输出开/关，接受参数 en/unen
 *					ex: "pwm0 -output en"		输出打开
 *			[2.] pwm0 -fre [value]		设置输出频率，接受一个十进制数，以 Hz 为单位
 *					ex: "pwm0 -fre 1000000"	输出频率设置为 1MHz
 *			[3.] pwm0 -duty0 [value]	设置通道 0 占空比
 *					ex: "pwm0 -duty0 20"		通道 0 占空比设为 20%
 * @note	当设定频率高于 1MHz 时，建议使用 pwm[0][1] 查看实际输出频率
 */
#include "pwm_cmd.h"
#include "main.h"
#include "bpwm_peripheral.h"
#include "uart_peripheral.h"
#include "util_cmd.h"
#include "util_str.h"
#include "string.h"

static char * const sc_cmd_pwm0 = "pwm0";
static char * const sc_cmd_pwm1 = "pwm1";

static char * const sc_par_output = "output";
static char * const sc_par_fre = "fre";
static char * const sc_par_duty0 = "duty0";
static char * const sc_par_duty1 = "duty1";

static char * const sc_opt_on = "on";
static char * const sc_opt_off = "off";

static char * const sc_warn1 = "pwm: No param\r\n";

static void showStatus0(void)
{
	UartPuts(&g_uart1, "PWM0 Status:\r\n");
	UartPrintf(&g_uart1, "freTarget = %d, ", BPWMGetFre(BPWM_PERIPHERAL_0));
	UartPrintf(&g_uart1, "freReal = %f\r\n", BPWMGetFreReal(BPWM_PERIPHERAL_0));
	UartPrintf(&g_uart1, "CH0 duty = %d%%, ", BPWMGetDuty(BPWM_PERIPHERAL_0, BPWM_CH0_MASK));
	UartPrintf(&g_uart1, "CH1 duty = %d%%, ", BPWMGetDuty(BPWM_PERIPHERAL_0, BPWM_CH1_MASK));
	if (BPWMGetOnOff(BPWM_PERIPHERAL_0)) {
		UartPuts(&g_uart1, "Output: On\r\n");
	}
	else {
		UartPuts(&g_uart1, "Output: Off\r\n");
	}
}

static void showStatus1(void)
{
    UartPuts(&g_uart1, "PWM1 Status:\r\n");
	UartPrintf(&g_uart1, "freTarget = %d, ", BPWMGetFre(BPWM_PERIPHERAL_1));
	UartPrintf(&g_uart1, "freReal = %f\r\n", BPWMGetFreReal(BPWM_PERIPHERAL_1));
	UartPrintf(&g_uart1, "CH0 duty = %d%%, ", BPWMGetDuty(BPWM_PERIPHERAL_1, BPWM_CH0_MASK));
	UartPrintf(&g_uart1, "CH1 duty = %d%%, ", BPWMGetDuty(BPWM_PERIPHERAL_1, BPWM_CH1_MASK));
	if (BPWMGetOnOff(BPWM_PERIPHERAL_1)) {
		UartPuts(&g_uart1, "Output: On\r\n");
	}
	else {
		UartPuts(&g_uart1, "Output: Off\r\n");
	}
}

static void parOutputProcess(uint8_t bpwm, uint8_t *receive, uint16_t recelen)
{
    int16_t offset;

    offset = StrGetNextNotSpace((char *)receive);
    if (offset == -1) {
        UartPutsDMA(&g_uart1, sc_warn1);

        return;
    }

    if (strncmp((char *)&receive[offset], sc_opt_on, 2) == 0) {
        if (bpwm == 0) {
            BPWMSetEnable(BPWM_PERIPHERAL_0, 1);
        }
        else {
            BPWMSetEnable(BPWM_PERIPHERAL_1, 1);
        }
    }
    else if (strncmp((char *)&receive[offset], sc_opt_off, 3) == 0) {
        if (bpwm == 0) {
            BPWMSetEnable(BPWM_PERIPHERAL_0, 0);
        }
        else {
            BPWMSetEnable(BPWM_PERIPHERAL_1, 0);
        }
    }
}

static void parFreProcess(uint8_t bpwm, uint8_t *receive, uint16_t recelen)
{
    uint32_t fre;
    int16_t offset;

    offset = StrGetNextNotSpace((char *)receive);
    if (offset == -1) {
        UartPutsDMA(&g_uart1, sc_warn1);

        return;
    }

    fre = StrGetDec((char *)&receive[offset], 0, 10);
    if (bpwm == 0) {
        BPWMSetFre(BPWM_PERIPHERAL_0, fre);
    }
    else {
        BPWMSetFre(BPWM_PERIPHERAL_1, fre);
    }
}

static void parDuty0Process(uint8_t bpwm, uint8_t *receive, uint16_t recelen)
{
    uint16_t duty;
    int16_t offset;

    offset = StrGetNextNotSpace((char *)receive);
    if (offset == -1) {
        UartPutsDMA(&g_uart1, sc_warn1);

        return;
    }

    duty = StrGetDec((char *)&receive[offset], 0, 8);
    if (bpwm == 0) {
        BPWMSetDuty(BPWM_PERIPHERAL_0, duty, BPWM_CH0_MASK);
    }
    else {
        BPWMSetDuty(BPWM_PERIPHERAL_1, duty, BPWM_CH0_MASK);
    }
}

static void parDuty1Process(uint8_t bpwm, uint8_t *receive, uint16_t recelen)
{
    uint16_t duty;
    int16_t offset;

    offset = StrGetNextNotSpace((char *)receive);
    if (offset == -1) {
        UartPutsDMA(&g_uart1, sc_warn1);

        return;
    }

    duty = StrGetDec((char *)&receive[offset], 0, 8);
    if (bpwm == 0) {
        BPWMSetDuty(BPWM_PERIPHERAL_0, duty, BPWM_CH1_MASK);
    }
    else {
        BPWMSetDuty(BPWM_PERIPHERAL_1, duty, BPWM_CH1_MASK);
    }
}

static void cmdPwm0Callback(int argc, char *argv[], int index)
{
	uint8_t *receive = UartGetRxBuffer(&g_uart1);
    uint16_t receivelen = UartGetReceiveLen(&g_uart1);

    if (argc == 0) {
        taskENTER_CRITICAL();
		showStatus0();
		taskEXIT_CRITICAL();
    }

    if (argc == 1) {
        if (strcmp(sc_par_output, argv[0]) == 0) {
            parOutputProcess(0, &receive[index], receivelen - index);
        }
        else if (strcmp(sc_par_fre, argv[0]) == 0) {
            parFreProcess(0, &receive[index], receivelen - index);
        }
        else if (strcmp(sc_par_duty0, argv[0]) == 0) {
            parDuty0Process(0, &receive[index], receivelen - index);
        }
        else if (strcmp(sc_par_duty1, argv[0]) == 0) {
            parDuty1Process(0, &receive[index], receivelen - index);
        }
    }
}

static void cmdPwm1Callback(int argc, char *argv[], int index)
{
    uint8_t *receive = UartGetRxBuffer(&g_uart1);
    uint16_t receivelen = UartGetReceiveLen(&g_uart1);

    if (argc == 0) {
        taskENTER_CRITICAL();
		showStatus1();
		taskEXIT_CRITICAL();
    }

    if (argc == 1) {
        if (strcmp(sc_par_output, argv[0]) == 0) {
            parOutputProcess(1, &receive[index], receivelen - index);
        }
        else if (strcmp(sc_par_fre, argv[0]) == 0) {
            parFreProcess(1, &receive[index], receivelen - index);
        }
        else if (strcmp(sc_par_duty0, argv[0]) == 0) {
            parDuty0Process(1, &receive[index], receivelen - index);
        }
        else if (strcmp(sc_par_duty1, argv[0]) == 0) {
            parDuty1Process(1, &receive[index], receivelen - index);
        }
    }
}

void PWMCmdInit(void)
{
	CmdAppend(sc_cmd_pwm0, cmdPwm0Callback);
    CmdAppend(sc_cmd_pwm1, cmdPwm1Callback);
}
