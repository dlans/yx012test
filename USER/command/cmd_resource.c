#include "cmd.h"

char * const gc_cmd_cmd_adc = "adc";
char * const gc_cmd_cmd_rz = "rz";
char * const gc_cmd_cmd_spi = "spi";
char * const gc_cmd_cmd_srz = "srz";
char * const gc_cmd_cmd_system = "system";
char * const gc_cmd_cmd_timer = "timer";
char * const gc_cmd_cmd_uart = "uart";
char * const gc_cmd_cmd_urz = "urz";

char * const gc_cmd_par_address = "address";
char * const gc_cmd_par_baud = "baud";
char * const gc_cmd_par_byte = "byte";
char * const gc_cmd_par_char = "char";
char * const gc_cmd_par_clk0 = "clk0";
char * const gc_cmd_par_clock = "clock";
char * const gc_cmd_par_disable = "disable";
char * const gc_cmd_par_enable = "enable";
char * const gc_cmd_par_endian = "endian";
char * const gc_cmd_par_fre = "fre";
char * const gc_cmd_par_fs = "fs";
char * const gc_cmd_par_get = "get";
char * const gc_cmd_par_hex = "hex";
char * const gc_cmd_par_icdiv = "icdiv";
char * const gc_cmd_par_idle = "idle";
char * const gc_cmd_par_lsb = "lsb";
char * const gc_cmd_par_memory = "memory";
char * const gc_cmd_par_ms = "ms";
char * const gc_cmd_par_msb = "msb";
char * const gc_cmd_par_name = "name";
char * const gc_cmd_par_pulen = "pulen";
char * const gc_cmd_par_pulse = "pulse";
char * const gc_cmd_par_pulwidth = "pulwidth";
char * const gc_cmd_par_read = "read";
char * const gc_cmd_par_read2 = "read2";
char * const gc_cmd_par_readf = "readf";
char * const gc_cmd_par_readv = "readv";
char * const gc_cmd_par_receive = "receive";
char * const gc_cmd_par_regs = "regs";
char * const gc_cmd_par_reset = "reset";
char * const gc_cmd_par_rxen = "rxen";
char * const gc_cmd_par_rxhigh = "rxhigh";
char * const gc_cmd_par_rxlow = "rxlow";
char * const gc_cmd_par_rxmode = "rxmode";
char * const gc_cmd_par_set = "set";
char * const gc_cmd_par_size = "size";
char * const gc_cmd_par_speed = "speed";
char * const gc_cmd_par_spis = "spis";
char * const gc_cmd_par_th0 = "th0";
char * const gc_cmd_par_th1 = "th1";
char * const gc_cmd_par_tl0 = "tl0";
char * const gc_cmd_par_tl1 = "tl1";
char * const gc_cmd_par_txhigh = "txhigh";
char * const gc_cmd_par_txlow = "txlow";
char * const gc_cmd_par_us = "us";
char * const gc_cmd_par_version = "version";
char * const gc_cmd_par_width = "width";
char * const gc_cmd_par_write = "write";

char * const gc_cmd_info_done = "done";
char * const gc_cmd_warn_nd = "no data";
char * const gc_cmd_info_no = "no";
char * const gc_cmd_info_no_param = "no param";
char * const gc_cmd_info_ok = "OK";
char * const gc_cmd_info_yes = "yes";

char * const gc_cmd_warn_mp = "missing param";
char * const gc_cmd_warn_nmp = "no match param";
char * const gc_cmd_warn_time = "timeout";

char * const gc_cmd_err_error = "error";
char * const gc_cmd_err_fail = "fail";
char * const gc_cmd_err_memory = "can't malloc memory";
char * const gc_cmd_err_nsp = "not support param";
char * const gc_cmd_err_range = "out of range";
char * const gc_cmd_err_limit = "out of limit";

void CmdAddSendStruct(CmdSendStruct *head, CmdSendStruct *node)
{
	CmdSendStruct *send_struct1 = NULL;
	
	if (head == NULL) {
		return;
	}
	
	if (head->next == NULL) {
		head->next = node;
		return;
	}
	
	send_struct1 = head->next;
	while (send_struct1->next != NULL) {
		send_struct1 = send_struct1->next;
	}
	send_struct1->next = node;
	head->length++;
}

void CmdDeleteSendStruct(CmdSendStruct *head)
{
	CmdSendStruct *send_struct1 = NULL;
	CmdSendStruct *send_struct2 = NULL;
	
	if (head == NULL) {
		return;
	}
	
	send_struct1 = head->next;
	send_struct2 = head->next;
	
	if (send_struct1 == NULL) {
		return;
	}
	
	while (send_struct2->next != NULL) {
		send_struct1 = send_struct2;
        send_struct2 = send_struct1->next;
	}
	vPortFree(send_struct2->stream);
	vPortFree(send_struct2);
	send_struct1->next = NULL;
	head->length--;
}

void CmdCleanSendStruct(CmdSendStruct *head)
{
	CmdSendStruct *send_struct1 = NULL;
	CmdSendStruct *send_struct2 = NULL;
	
	if (head == NULL) {
		return;
	}
	
	send_struct1 = head->next;
    send_struct2 = head->next;
    if (send_struct1 == NULL) {
        return;
    }
	
	while (send_struct2->next != NULL) {
        send_struct1 = send_struct2;
        send_struct2 = send_struct1->next;
        vPortFree(send_struct1->stream);
        vPortFree(send_struct1);
    }
    vPortFree(send_struct2->stream);
    vPortFree(send_struct2);
	
	head->next = NULL;
	head->length = 0;
}

#ifdef USER_DEBUG_LOG

#include "main.h"

void CmdPrintSendStruct(CmdSendStruct *first)
{
	uint8_t i, j;
	
	CmdSendStruct *send_struct1 = first;
	
	if (first == NULL) {
		UartPutsEndl(&g_uart1, "first == NULL");
	}
	
	i = 0;
	while (send_struct1 != NULL) {
		UartPrintfEndl(&g_uart1, "Node[%d]:", i);
		UartPrintfEndl(&g_uart1, "next = 0x%X", (uint32_t)send_struct1->next);
		UartPrintfEndl(&g_uart1, "seq_enum = %d", send_struct1->seq_enum);
		UartPrintfEndl(&g_uart1, "length = %d", send_struct1->length);
		if (send_struct1->seq_enum == CMD_SEND_SEQ_BYTE) {
			UartPutsEndl(&g_uart1, "Stream as follow:");
			for (j = 0; j < send_struct1->length; j++) {
				UartPrintf(&g_uart1, "%02X ", send_struct1->stream[j]);
			}
			UartPuts(&g_uart1, "\r\n");
		}
		UartPuts(&g_uart1, "\r\n");
		send_struct1 = send_struct1->next;
		i++;
	}
}

#endif
